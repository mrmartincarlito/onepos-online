<?php 
  require_once("./api/config.php");
  require_once("./api/auto_back.php");
  session_destroy();
  header('Location: login.php');
?>