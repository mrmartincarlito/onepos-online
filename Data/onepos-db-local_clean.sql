-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 25, 2021 at 10:44 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onepos-db-local`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_levels`
--

CREATE TABLE `access_levels` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `access_accounts` int(11) NOT NULL DEFAULT 0,
  `access_backoffice_access_levels` int(11) NOT NULL DEFAULT 0,
  `access_frontoffice_access_levels` int(11) NOT NULL DEFAULT 0,
  `access_dashboard` int(11) NOT NULL DEFAULT 0,
  `access_categories` int(11) NOT NULL DEFAULT 0,
  `access_denominations` int(11) NOT NULL DEFAULT 0,
  `access_discounts` int(11) NOT NULL DEFAULT 0,
  `access_menu` int(11) NOT NULL DEFAULT 0,
  `access_products_groups` int(11) NOT NULL DEFAULT 0,
  `access_reports` int(11) NOT NULL DEFAULT 0,
  `access_tender_types` int(11) NOT NULL DEFAULT 0,
  `access_backup` int(11) NOT NULL DEFAULT 0,
  `is_frontoffice` varchar(3) NOT NULL DEFAULT 'NO',
  `cashier_cash_fund` int(11) NOT NULL DEFAULT 0,
  `cashier_recall_transaction` int(11) NOT NULL DEFAULT 0,
  `cashier_refund` int(11) NOT NULL DEFAULT 0,
  `cashier_cash_withdrawal` int(11) NOT NULL DEFAULT 0,
  `cashier_suspended_transaction` int(11) NOT NULL DEFAULT 0,
  `cashier_x_reading` int(11) NOT NULL DEFAULT 0,
  `cashier_y_reading` int(11) NOT NULL DEFAULT 0,
  `cashier_void_transaction` int(11) NOT NULL DEFAULT 0,
  `cashier_void_current_transaction` int(11) NOT NULL DEFAULT 0,
  `cashier_item_void` int(11) NOT NULL DEFAULT 0,
  `cashier_electric_journal` int(11) NOT NULL DEFAULT 0,
  `cashier_z_reading` int(11) NOT NULL DEFAULT 0,
  `cashier_price_override` int(11) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_levels`
--

INSERT INTO `access_levels` (`id`, `description`, `access_accounts`, `access_backoffice_access_levels`, `access_frontoffice_access_levels`, `access_dashboard`, `access_categories`, `access_denominations`, `access_discounts`, `access_menu`, `access_products_groups`, `access_reports`, `access_tender_types`, `access_backup`, `is_frontoffice`, `cashier_cash_fund`, `cashier_recall_transaction`, `cashier_refund`, `cashier_cash_withdrawal`, `cashier_suspended_transaction`, `cashier_x_reading`, `cashier_y_reading`, `cashier_void_transaction`, `cashier_void_current_transaction`, `cashier_item_void`, `cashier_electric_journal`, `cashier_z_reading`, `cashier_price_override`, `is_deleted`) VALUES
(1, 'ADMIN', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'NO', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0),
(2, 'CASHIER', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'YES', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `role` int(11) NOT NULL DEFAULT 0,
  `active` int(11) NOT NULL DEFAULT 1,
  `branch_code` varchar(10) NOT NULL DEFAULT '',
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `name`, `username`, `password`, `role`, `active`, `branch_code`, `date_time`) VALUES
(1, 'admin', 'admin', '$2y$10$Wq6SFrb5Q2I5PE63pL5cYuOiWEdCk7e/sZite5kQweNF1Z/nYJFmq', 1, 0, 'B-001', '2021-09-02 05:55:12');

-- --------------------------------------------------------

--
-- Table structure for table `cash_fund`
--

CREATE TABLE `cash_fund` (
  `id` int(11) NOT NULL,
  `cash_fund_value` decimal(10,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_code` varchar(10) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cash_withdrawal`
--

CREATE TABLE `cash_withdrawal` (
  `id` int(11) NOT NULL,
  `cash_amount` decimal(10,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_code` varchar(10) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `active_tag` text NOT NULL,
  `image_file` text DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `bday` date NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `denominations`
--

CREATE TABLE `denominations` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `equivalent_value` float NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` int(11) NOT NULL,
  `description` varchar(12) NOT NULL,
  `equivalent_value` int(11) NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'PERCENTAGE',
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `account_id` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `account_id`, `description`, `ip`, `hostname`, `timestamp`, `is_deleted`) VALUES
(3897, '1', 'Logged in', '::1', 'ip6-localhost', '2021-10-25 08:41:10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `logs_view`
--

CREATE TABLE `logs_view` (
  `id` int(11) DEFAULT NULL,
  `account_id` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` tinyint(1) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_date`
--

CREATE TABLE `log_date` (
  `id` int(11) NOT NULL,
  `open_log` datetime NOT NULL,
  `close_log` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `branch_code` varchar(10) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `log_date`
--

INSERT INTO `log_date` (`id`, `open_log`, `close_log`, `branch_code`, `date_time`) VALUES
(144, '2021-10-25 04:41:22', '0000-00-00 00:00:00', 'B-001', '2021-10-25 08:41:22');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `short_descp` varchar(10) NOT NULL,
  `long_descp` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_group_id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `inventory_cost` decimal(10,2) NOT NULL,
  `cost` double NOT NULL,
  `active_tag` text NOT NULL,
  `print_label_sticker_tag` text NOT NULL,
  `vatable_tag` text NOT NULL,
  `slip_tag` text NOT NULL,
  `image_file` text DEFAULT NULL,
  `added_by` text NOT NULL,
  `branch_code` varchar(10) NOT NULL,
  `quantity` decimal(10,0) DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu_journal`
--

CREATE TABLE `menu_journal` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `beg_stocks` int(11) NOT NULL,
  `receiving_stocks` int(11) NOT NULL,
  `sales_stocks` int(11) NOT NULL,
  `returned_stocks` int(11) NOT NULL,
  `actual_stocks` int(11) NOT NULL,
  `end_stocks` int(11) NOT NULL,
  `log_date_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ordered_items`
--

CREATE TABLE `ordered_items` (
  `id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `total_cost` decimal(10,2) NOT NULL,
  `discount` varchar(50) DEFAULT NULL,
  `discount_amount` decimal(10,2) DEFAULT 0.00,
  `service_type` varchar(10) DEFAULT 'DINE IN',
  `special_request` text DEFAULT NULL,
  `is_voided` tinyint(4) NOT NULL DEFAULT 0,
  `is_refunded` tinyint(4) NOT NULL DEFAULT 0,
  `is_overridden` tinyint(4) DEFAULT 0,
  `override_counter` int(11) DEFAULT NULL,
  `original_price` decimal(10,2) DEFAULT 0.00,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `discount_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ordered_tenders`
--

CREATE TABLE `ordered_tenders` (
  `id` int(11) NOT NULL,
  `trn_number` int(11) NOT NULL,
  `tender_id` int(11) NOT NULL,
  `amount_payed` decimal(10,2) NOT NULL DEFAULT 0.00,
  `amount_change` decimal(10,2) NOT NULL DEFAULT 0.00,
  `account_name` text DEFAULT NULL,
  `account_number` text DEFAULT NULL,
  `approval_number` varchar(50) DEFAULT NULL,
  `bank` text DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_number` int(50) NOT NULL,
  `trn_number` int(50) NOT NULL,
  `guest_no` int(11) NOT NULL,
  `is_suspended` tinyint(4) NOT NULL DEFAULT 0,
  `log_date_id` int(11) NOT NULL,
  `cashier_username` varchar(50) NOT NULL,
  `branch_code` varchar(10) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `is_voided` tinyint(4) NOT NULL DEFAULT 0,
  `amount_payable` decimal(10,2) NOT NULL DEFAULT 0.00,
  `amount_payed` decimal(10,2) NOT NULL DEFAULT 0.00,
  `amount_change` decimal(10,2) NOT NULL DEFAULT 0.00,
  `is_settled` tinyint(4) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `product_groups`
--

CREATE TABLE `product_groups` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `printer_name` text NOT NULL,
  `printer_port` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `special_request`
--

CREATE TABLE `special_request` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `supplier_name` varchar(50) NOT NULL,
  `supplier_address` text NOT NULL,
  `active_tag` text NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tender_types`
--

CREATE TABLE `tender_types` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `allow_change` text NOT NULL,
  `active_tag` text NOT NULL,
  `required_detail_tag` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `trn_number` int(11) NOT NULL,
  `order_id` int(11) NOT NULL DEFAULT 0,
  `or_number` int(11) NOT NULL DEFAULT 0,
  `type` varchar(10) NOT NULL DEFAULT 'ORDER',
  `amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `branch_code` varchar(10) NOT NULL,
  `log_date` int(11) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_levels`
--
ALTER TABLE `access_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `role` (`role`);

--
-- Indexes for table `cash_fund`
--
ALTER TABLE `cash_fund`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `denominations`
--
ALTER TABLE `denominations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_date`
--
ALTER TABLE `log_date`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `product_group_id` (`product_group_id`);

--
-- Indexes for table `menu_journal`
--
ALTER TABLE `menu_journal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordered_items`
--
ALTER TABLE `ordered_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `discount_id` (`discount_id`);

--
-- Indexes for table `ordered_tenders`
--
ALTER TABLE `ordered_tenders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_groups`
--
ALTER TABLE `product_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `special_request`
--
ALTER TABLE `special_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tender_types`
--
ALTER TABLE `tender_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1635142709;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1632969108;

--
-- AUTO_INCREMENT for table `denominations`
--
ALTER TABLE `denominations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3898;

--
-- AUTO_INCREMENT for table `log_date`
--
ALTER TABLE `log_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1632898477;

--
-- AUTO_INCREMENT for table `menu_journal`
--
ALTER TABLE `menu_journal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1492;

--
-- AUTO_INCREMENT for table `ordered_tenders`
--
ALTER TABLE `ordered_tenders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=326;

--
-- AUTO_INCREMENT for table `product_groups`
--
ALTER TABLE `product_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `special_request`
--
ALTER TABLE `special_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tender_types`
--
ALTER TABLE `tender_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1634886243;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
