-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2021 at 04:05 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onepos-db-local`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_levels`
--

CREATE TABLE `access_levels` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `access_accounts` int(11) NOT NULL DEFAULT 0,
  `access_backoffice_access_levels` int(11) NOT NULL DEFAULT 0,
  `access_frontoffice_access_levels` int(11) NOT NULL DEFAULT 0,
  `access_dashboard` int(11) NOT NULL DEFAULT 0,
  `access_categories` int(11) NOT NULL DEFAULT 0,
  `access_denominations` int(11) NOT NULL DEFAULT 0,
  `access_discounts` int(11) NOT NULL DEFAULT 0,
  `access_menu` int(11) NOT NULL DEFAULT 0,
  `access_products_groups` int(11) NOT NULL DEFAULT 0,
  `access_reports` int(11) NOT NULL DEFAULT 0,
  `access_tender_types` int(11) NOT NULL DEFAULT 0,
  `access_backup` int(11) NOT NULL DEFAULT 0,
  `is_frontoffice` varchar(3) NOT NULL DEFAULT 'NO',
  `cashier_cash_fund` int(11) NOT NULL DEFAULT 0,
  `cashier_recall_transaction` int(11) NOT NULL DEFAULT 0,
  `cashier_refund` int(11) NOT NULL DEFAULT 0,
  `cashier_cash_withdrawal` int(11) NOT NULL DEFAULT 0,
  `cashier_suspended_transaction` int(11) NOT NULL DEFAULT 0,
  `cashier_x_reading` int(11) NOT NULL DEFAULT 0,
  `cashier_y_reading` int(11) NOT NULL DEFAULT 0,
  `cashier_void_transaction` int(11) NOT NULL DEFAULT 0,
  `cashier_void_current_transaction` int(11) NOT NULL DEFAULT 0,
  `cashier_item_void` int(11) NOT NULL DEFAULT 0,
  `cashier_electric_journal` int(11) NOT NULL DEFAULT 0,
  `cashier_z_reading` int(11) NOT NULL DEFAULT 0,
  `cashier_price_override` int(11) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_levels`
--

INSERT INTO `access_levels` (`id`, `description`, `access_accounts`, `access_backoffice_access_levels`, `access_frontoffice_access_levels`, `access_dashboard`, `access_categories`, `access_denominations`, `access_discounts`, `access_menu`, `access_products_groups`, `access_reports`, `access_tender_types`, `access_backup`, `is_frontoffice`, `cashier_cash_fund`, `cashier_recall_transaction`, `cashier_refund`, `cashier_cash_withdrawal`, `cashier_suspended_transaction`, `cashier_x_reading`, `cashier_y_reading`, `cashier_void_transaction`, `cashier_void_current_transaction`, `cashier_item_void`, `cashier_electric_journal`, `cashier_z_reading`, `cashier_price_override`, `is_deleted`) VALUES
(1, 'ADMINISTRATOR', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'NO', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0),
(2, 'CASHIER_ADMINISTRATOR', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'YES', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0),
(1629098529, 'CASHIER_ONLY', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'YES', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `role` int(11) NOT NULL DEFAULT 0,
  `active` int(11) NOT NULL DEFAULT 1,
  `branch_code` varchar(10) NOT NULL DEFAULT '',
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `name`, `username`, `password`, `role`, `active`, `branch_code`, `date_time`) VALUES
(1, 'admin', 'admin', '$2y$10$Wq6SFrb5Q2I5PE63pL5cYuOiWEdCk7e/sZite5kQweNF1Z/nYJFmq', 1, 0, 'B-001', '2021-09-02 05:55:12'),
(7, 'Cashier_Admin', 'Uly', '$2y$10$qLf3HjXJBVS7NaEB4hluCOsi9DkH5UL0TPN52NwhNs.G0KBgf1KzO', 2, 1, 'B-001', '2021-09-02 05:54:24'),
(8, 'Cashier_Yuwee', 'cashier', '$2y$10$8aavwHgfUGZRIH60G6OIvO5e7TRxwUMyirD6uqpWF24ARlHSTbmsO', 1629098529, 1, 'B-001', '2021-09-02 05:53:29'),
(9, 'Main Branch', 'own', '$2y$10$oxYz7gGwenbUld.pjMokW.n.FPAgoULhvwZmDlsMnHi/shMgTiNyi', 1, 0, 'B-001', '2021-09-02 05:54:34'),
(10, '1TEQ PROVIDERS INC', '1teq_providers', '$2y$10$M/pzdQGTpCuCRMQe9bODCukIk/vnyk1rQcVWQ/3dnWC2cyTmWrklG', 1, 1, 'B-001', '2021-09-02 05:55:07'),
(11, 'user', 'user', '$2y$10$OGP2HXpBZ/or6SQllpQ6HOtuGoMfTNrKCkY03YZ.59WiCKPwqz6Yq', 2, 1, 'B-002', '2021-09-20 08:53:50'),
(12, 'Branch2', 'Branch2', '$2y$10$pY7DLNzm8gVL23GrOwqLnuUWWrNL9vGDAR9Q7eGxCHaVZh6TY5iD2', 2, 1, 'B-003', '2021-09-20 09:02:24'),
(13, 'Branch2Admin', 'B2Admin', '$2y$10$3Tm9kmWmsvVCd33BdWItX.GyxTMgwebGe8tkNZHQGim2EMRonm2iu', 1, 1, 'B-003', '2021-09-20 09:03:49');

-- --------------------------------------------------------

--
-- Table structure for table `cash_fund`
--

CREATE TABLE `cash_fund` (
  `id` int(11) NOT NULL,
  `cash_fund_value` decimal(10,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_code` varchar(10) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cash_withdrawal`
--

CREATE TABLE `cash_withdrawal` (
  `id` int(11) NOT NULL,
  `cash_amount` decimal(10,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_code` varchar(10) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `active_tag` text NOT NULL,
  `image_file` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `description`, `active_tag`, `image_file`, `date_time`) VALUES
(1632895735, 'Drinks', 'YES', 'uploads/download.jfif', '2021-09-29 06:08:55'),
(1632896645, 'Pasta', 'YES', 'uploads/1504715566-delish-fettuccine-alfredo.jpg', '2021-09-29 06:24:05'),
(1632897006, 'Burger', 'YES', 'uploads/download (2).jfif', '2021-09-29 06:30:06'),
(1632897233, 'Main Dish', 'YES', 'uploads/1387918756116.jpeg', '2021-09-29 06:33:53'),
(1632898011, 'Dessert', 'YES', 'uploads/17244-caramel-topped-ice-cream-dessert-760x580.jpg', '2021-09-29 06:53:47'),
(1633661535, 'test', 'YES', '', '2021-10-08 02:52:14');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `bday` date NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `fname`, `lname`, `address`, `bday`, `date_added`) VALUES
(1632969074, 'user', '1', 'address', '2021-09-08', '2021-09-30 02:31:13'),
(1632969079, 'user', '2', 'address', '2021-09-16', '2021-09-30 02:31:18'),
(1632969084, 'user', '3', 'address', '2021-09-12', '2021-09-30 02:31:24'),
(1632969090, 'user', '4', 'address', '2021-09-09', '2021-09-30 02:31:30'),
(1632969094, 'user', '5', 'address', '2021-09-15', '2021-09-30 02:31:33'),
(1632969098, 'user', '6', 'address', '2021-09-04', '2021-09-30 02:31:37'),
(1632969102, 'user', '7', 'address', '2021-09-07', '2021-09-30 02:31:41'),
(1632969107, 'user', '8', 'address', '2021-09-01', '2021-09-30 02:31:47');

-- --------------------------------------------------------

--
-- Table structure for table `denominations`
--

CREATE TABLE `denominations` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `equivalent_value` float NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `denominations`
--

INSERT INTO `denominations` (`id`, `description`, `equivalent_value`, `date_time`) VALUES
(5, '.25¢', 0.25, '2021-09-02 06:47:02'),
(6, '.50¢', 0.5, '2021-09-02 06:47:06'),
(8, 'P1', 1, '2021-09-02 06:47:16'),
(9, 'P5', 5, '2021-09-02 06:47:23'),
(10, 'P10', 10, '2021-09-02 06:47:28'),
(11, 'P20', 20, '2021-09-02 06:47:33'),
(12, 'P50', 50, '2021-09-02 06:47:37'),
(13, 'P100', 100, '2021-09-02 06:47:44'),
(14, 'P200', 200, '2021-09-02 06:47:51'),
(15, 'P500', 500, '2021-09-02 06:47:56'),
(16, 'P1000', 1000, '2021-09-02 06:48:10'),
(17, 'test', 123, '2021-10-08 02:52:59');

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` int(11) NOT NULL,
  `description` varchar(12) NOT NULL,
  `equivalent_value` int(11) NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'PERCENTAGE',
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `description`, `equivalent_value`, `type`, `date_time`) VALUES
(1, 'Discount', 0, 'ABSOLUTE', '2021-10-06 06:32:14'),
(5, 'SENIOR/PWD', 20, 'PERCENTAGE', '2021-09-02 06:49:17'),
(16, 'test', 12, 'PERCENTAGE', '2021-10-08 02:53:07');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `account_id` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `account_id`, `description`, `ip`, `hostname`, `timestamp`, `is_deleted`) VALUES
(3116, '1', 'Logged in', '::1', 'ROLAND', '2021-09-29 06:04:27', 0),
(3117, '1', 'Add category: drinks', '::1', 'ROLAND', '2021-09-29 06:08:55', 0),
(3118, '1', 'Save new transaction # 1 ', '::1', 'ROLAND', '2021-09-29 06:09:00', 0),
(3119, '1', 'Add menu: coke', '::1', 'ROLAND', '2021-09-29 06:09:39', 0),
(3120, '1', 'Add menu: mtn dew', '::1', 'ROLAND', '2021-09-29 06:10:39', 0),
(3121, '1', 'Add menu: pepsi', '::1', 'ROLAND', '2021-09-29 06:10:57', 0),
(3122, '1', 'Add menu: pepsi', '::1', 'ROLAND', '2021-09-29 06:11:01', 0),
(3123, '1', 'Add menu: pepsi', '::1', 'ROLAND', '2021-09-29 06:11:09', 0),
(3124, '1', 'Add menu: pepsi', '::1', 'ROLAND', '2021-09-29 06:11:24', 0),
(3125, '1', 'Add menu: sprite', '::1', 'ROLAND', '2021-09-29 06:11:51', 0),
(3126, '7', 'Logged in', '192.168.1.2', 'AKAVIRI', '2021-09-29 06:21:59', 0),
(3127, '7', 'Save new transaction # 2 ', '192.168.1.2', 'AKAVIRI', '2021-09-29 06:22:05', 0),
(3128, '1', 'Add category: pasta', '::1', 'ROLAND', '2021-09-29 06:24:05', 0),
(3129, '1', 'Add menu: crbonara', '::1', 'ROLAND', '2021-09-29 06:24:37', 0),
(3130, '1', 'Add menu: spagh', '::1', 'ROLAND', '2021-09-29 06:24:54', 0),
(3131, '1', 'Add menu: spagh', '::1', 'ROLAND', '2021-09-29 06:24:57', 0),
(3132, '1', 'Add category: burger', '::1', 'ROLAND', '2021-09-29 06:30:06', 0),
(3133, '1', 'Add menu: brgr', '::1', 'ROLAND', '2021-09-29 06:30:44', 0),
(3134, '1', 'Add menu: brgr chz', '::1', 'ROLAND', '2021-09-29 06:31:25', 0),
(3135, '1', 'Add menu: brgr egg', '::1', 'ROLAND', '2021-09-29 06:31:57', 0),
(3136, '1', 'Add menu: brgr egg', '::1', 'ROLAND', '2021-09-29 06:31:59', 0),
(3137, '1', 'Add menu: brgr dbl', '::1', 'ROLAND', '2021-09-29 06:32:29', 0),
(3138, '1', 'Add category: main dish', '::1', 'ROLAND', '2021-09-29 06:33:53', 0),
(3139, '1', 'Add menu: kalderet', '::1', 'ROLAND', '2021-09-29 06:34:52', 0),
(3140, '1', 'Add menu: menudo', '::1', 'ROLAND', '2021-09-29 06:36:08', 0),
(3141, '1', 'Add menu: lech kaw', '::1', 'ROLAND', '2021-09-29 06:37:32', 0),
(3142, '1', 'Add menu: p sisig', '::1', 'ROLAND', '2021-09-29 06:37:59', 0),
(3143, '1', 'Add menu: sngg hpn', '::1', 'ROLAND', '2021-09-29 06:40:27', 0),
(3144, '1', 'Add menu: sngg hpn', '::1', 'ROLAND', '2021-09-29 06:40:30', 0),
(3145, '1', 'Add menu: lobster', '::1', 'ROLAND', '2021-09-29 06:41:56', 0),
(3146, '1', 'Add menu: ihw bang', '::1', 'ROLAND', '2021-09-29 06:43:45', 0),
(3147, '1', 'Add menu: bf steak', '::1', 'ROLAND', '2021-09-29 06:44:44', 0),
(3148, '1', 'Add category: dessert', '::1', 'ROLAND', '2021-09-29 06:46:51', 0),
(3149, '1', 'Add menu: halohalo', '::1', 'ROLAND', '2021-09-29 06:47:52', 0),
(3150, '1', 'Add menu: ice crm', '::1', 'ROLAND', '2021-09-29 06:48:23', 0),
(3151, '1', 'Add menu: lch fln', '::1', 'ROLAND', '2021-09-29 06:50:23', 0),
(3152, '1', 'Add menu: bko pndn', '::1', 'ROLAND', '2021-09-29 06:51:22', 0),
(3153, '1', 'Edit menu id 1632897826', '::1', 'ROLAND', '2021-09-29 06:53:17', 0),
(3154, '1', 'Edit category id 1632898011', '::1', 'ROLAND', '2021-09-29 06:53:47', 0),
(3155, '1', 'Edit menu id 1632898282', '::1', 'ROLAND', '2021-09-29 06:54:09', 0),
(3156, '1', 'Edit menu id 1632898282', '::1', 'ROLAND', '2021-09-29 06:54:12', 0),
(3157, '1', 'Add menu: cake', '::1', 'ROLAND', '2021-09-29 06:54:35', 0),
(3158, '7', 'Suspend transaction # 2', '192.168.1.2', 'AKAVIRI', '2021-09-29 07:14:36', 0),
(3159, '7', 'Save new transaction # 3 ', '192.168.1.2', 'AKAVIRI', '2021-09-29 07:15:40', 0),
(3160, '7', 'Save order itemid 1632895839 at qty 1 transaction # 3 ', '192.168.1.2', 'AKAVIRI', '2021-09-29 07:26:56', 0),
(3161, '7', 'Save order itemid 1632897120 at qty 1 transaction # 3 ', '192.168.1.2', 'AKAVIRI', '2021-09-29 07:27:01', 0),
(3162, '1', 'Save order itemid 1632896678 at qty 1 transaction # 1 ', '::1', 'ROLAND', '2021-09-29 07:28:09', 0),
(3163, '7', 'Recall transaction # 2', '192.168.1.2', 'AKAVIRI', '2021-09-29 07:34:45', 0),
(3164, '7', 'Save order itemid 1632895780 at qty 1 transaction # 2 ', '192.168.1.2', 'AKAVIRI', '2021-09-29 07:34:50', 0),
(3165, '7', 'Logged in', '::1', 'ROLAND', '2021-09-30 02:18:53', 0),
(3166, '7', 'Close log date 122 at 2021-09-30 10:19:08', '::1', 'ROLAND', '2021-09-30 02:19:08', 0),
(3167, '7', 'Logged in', '::1', 'ROLAND', '2021-09-30 02:19:15', 0),
(3168, '7', 'Logged in', '::1', 'ROLAND', '2021-09-30 02:22:36', 0),
(3169, '1', 'Logged in', '::1', 'ROLAND', '2021-09-30 02:22:42', 0),
(3170, '1', 'Save new transaction # 4 ', '::1', 'ROLAND', '2021-09-30 02:30:49', 0),
(3171, '1', 'Save order itemid 1632895839 at qty 1 transaction # 4 ', '::1', 'ROLAND', '2021-09-30 02:30:52', 0),
(3172, '1', 'Save new customer with id 1632969073.6813', '::1', 'ROLAND', '2021-09-30 02:31:13', 0),
(3173, '1', 'Save new customer with id 1632969078.8863', '::1', 'ROLAND', '2021-09-30 02:31:18', 0),
(3174, '1', 'Save new customer with id 1632969084.3741', '::1', 'ROLAND', '2021-09-30 02:31:24', 0),
(3175, '1', 'Save new customer with id 1632969090.3434', '::1', 'ROLAND', '2021-09-30 02:31:30', 0),
(3176, '1', 'Save new customer with id 1632969093.6386', '::1', 'ROLAND', '2021-09-30 02:31:33', 0),
(3177, '1', 'Save new customer with id 1632969097.9084', '::1', 'ROLAND', '2021-09-30 02:31:37', 0),
(3178, '1', 'Save new customer with id 1632969101.6143', '::1', 'ROLAND', '2021-09-30 02:31:41', 0),
(3179, '1', 'Save new customer with id 1632969107.4061', '::1', 'ROLAND', '2021-09-30 02:31:47', 0),
(3180, '1', 'Save service type as takeout from transaction # 4 ', '::1', 'ROLAND', '2021-09-30 02:38:47', 0),
(3181, '7', 'Logged in', '192.168.1.2', 'AKAVIRI', '2021-09-30 03:33:02', 0),
(3182, '1', 'Save order itemid 1632896678 at qty 1 transaction # 4 ', '::1', 'ROLAND', '2021-09-30 03:34:05', 0),
(3183, '1', 'Save order itemid 1632898073 at qty 1 transaction # 4 ', '::1', 'ROLAND', '2021-09-30 03:34:11', 0),
(3184, '1', 'Save order itemid 1632897120 at qty 1 transaction # 4 ', '::1', 'ROLAND', '2021-09-30 03:34:16', 0),
(3185, '1', 'Save order itemid 1632897120 at qty 2 transaction # 4 ', '::1', 'ROLAND', '2021-09-30 03:34:18', 0),
(3186, '1', 'Save order itemid 1632897716 at qty 6 transaction # 4 ', '::1', 'ROLAND', '2021-09-30 03:34:22', 0),
(3187, '7', 'Logged in', '::1', 'ROLAND', '2021-09-30 04:38:31', 0),
(3188, '7', 'Save new transaction # 5 ', '::1', 'ROLAND', '2021-09-30 04:38:35', 0),
(3189, '7', 'Save order itemid 1632895780 at qty 1 transaction # 5 ', '::1', 'ROLAND', '2021-09-30 04:38:37', 0),
(3190, '1', 'Save new transaction # 6 ', '::1', 'ROLAND', '2021-09-30 05:07:27', 0),
(3191, '1', 'Save order itemid 1632896678 at qty 1 transaction # 6 ', '::1', 'ROLAND', '2021-09-30 05:07:39', 0),
(3192, '1', 'Save service type as takeout from transaction # 6 ', '::1', 'ROLAND', '2021-09-30 05:12:40', 0),
(3193, '1', 'Save service type as dine in from transaction # 6 ', '::1', 'ROLAND', '2021-09-30 05:12:42', 0),
(3194, '1', 'Save service type as takeout from transaction # 6 ', '::1', 'ROLAND', '2021-09-30 05:12:48', 0),
(3195, '1', 'Suspend transaction # 6', '::1', 'ROLAND', '2021-09-30 05:20:11', 0),
(3196, '1', 'Logged in', '192.168.1.2', 'AKAVIRI', '2021-09-30 05:21:49', 0),
(3197, '1', 'Edit access level id 2', '192.168.1.2', 'AKAVIRI', '2021-09-30 05:22:11', 0),
(3198, '1', 'Save new transaction # 7 ', '::1', 'ROLAND', '2021-09-30 05:49:30', 0),
(3199, '1', 'Save order itemid 1632896678 at qty 1 transaction # 7 ', '::1', 'ROLAND', '2021-09-30 05:57:23', 0),
(3200, '1', 'Save order itemid 1632896698 at qty 1 transaction # 7 ', '::1', 'ROLAND', '2021-09-30 05:57:24', 0),
(3201, '1', 'Save order itemid 1632896698 at qty 1 transaction # 7 ', '::1', 'ROLAND', '2021-09-30 05:57:25', 0),
(3202, '1', 'Save order itemid 1632897630 at qty 1 transaction # 7 ', '::1', 'ROLAND', '2021-09-30 05:57:29', 0),
(3203, '1', 'Save order itemid 1632897479 at qty 1 transaction # 7 ', '::1', 'ROLAND', '2021-09-30 05:57:30', 0),
(3204, '1', 'Save order itemid 1632897368 at qty 1 transaction # 7 ', '::1', 'ROLAND', '2021-09-30 05:57:31', 0),
(3205, '1', 'Save order itemid 1632897716 at qty 1 transaction # 7 ', '::1', 'ROLAND', '2021-09-30 05:57:32', 0),
(3206, '1', 'Save order itemid 1632897453 at qty 1 transaction # 7 ', '::1', 'ROLAND', '2021-09-30 05:57:33', 0),
(3207, '1', 'Save order itemid 1632897292 at qty 1 transaction # 7 ', '::1', 'ROLAND', '2021-09-30 05:57:35', 0),
(3208, '1', 'Save order itemid 1632897826 at qty 1 transaction # 7 ', '::1', 'ROLAND', '2021-09-30 05:57:36', 0),
(3209, '1', 'Save order itemid 1632897885 at qty 1 transaction # 7 ', '::1', 'ROLAND', '2021-09-30 05:57:38', 0),
(3210, '1', 'Save order itemid 1632895780 at qty 1 transaction # 7 ', '::1', 'ROLAND', '2021-09-30 05:57:48', 0),
(3211, '1', 'Save order itemid 1632895839 at qty 1 transaction # 7 ', '::1', 'ROLAND', '2021-09-30 05:57:49', 0),
(3212, '1', 'Save order itemid 1632895884 at qty 1 transaction # 7 ', '::1', 'ROLAND', '2021-09-30 05:57:50', 0),
(3213, '1', 'Save order itemid 1632895912 at qty 1 transaction # 7 ', '::1', 'ROLAND', '2021-09-30 05:57:51', 0),
(3214, '1', 'Logged in', '::1', 'ROLAND', '2021-09-30 05:59:20', 0),
(3215, '7', 'Logged in', '::1', 'ROLAND', '2021-10-04 02:52:42', 0),
(3216, '7', 'Void all transactions at 2021-10-04 10:52:55', '::1', 'ROLAND', '2021-10-04 02:52:55', 0),
(3217, '7', 'Close log date 123 at 2021-10-04 10:53:03', '::1', 'ROLAND', '2021-10-04 02:53:03', 0),
(3218, '7', 'Logged in', '::1', 'ROLAND', '2021-10-04 02:53:11', 0),
(3219, '7', 'Save new transaction # 8 ', '::1', 'ROLAND', '2021-10-04 05:13:39', 0),
(3220, '7', 'Save order itemid 1632898282 at qty 1 transaction # 8 ', '::1', 'ROLAND', '2021-10-04 05:13:41', 0),
(3221, '7', 'Save order itemid 1632898223 at qty 1 transaction # 8 ', '::1', 'ROLAND', '2021-10-04 05:13:42', 0),
(3222, '7', 'Save order itemid 1632898223 at qty 1 transaction # 8 ', '::1', 'ROLAND', '2021-10-04 05:13:45', 0),
(3223, '7', 'Save order itemid 1632898103 at qty 1 transaction # 8 ', '::1', 'ROLAND', '2021-10-04 05:13:46', 0),
(3224, '7', 'Save order itemid 1632898073 at qty 1 transaction # 8 ', '::1', 'ROLAND', '2021-10-04 05:13:48', 0),
(3225, '7', 'Save order itemid 1632898476 at qty 1 transaction # 8 ', '::1', 'ROLAND', '2021-10-04 05:13:49', 0),
(3226, '7', 'Save new transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:30:40', 0),
(3227, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:30:43', 0),
(3228, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:30:44', 0),
(3229, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:30:45', 0),
(3230, '7', 'Save order itemid 1632895839 at qty 3 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:30:46', 0),
(3231, '7', 'Save order itemid 1633325443 at qty 5 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:30:50', 0),
(3232, '7', 'Applied discount id 5 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:30:55', 0),
(3233, '7', 'Save order itemid 1633325443 at qty 6 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:30:58', 0),
(3234, '7', 'Remove discount at transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:31:02', 0),
(3235, '7', 'Applied discount id 5 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:31:05', 0),
(3236, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:34:17', 0),
(3237, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:39:46', 0),
(3238, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:41:16', 0),
(3239, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:41:47', 0),
(3240, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:44:02', 0),
(3241, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:44:39', 0),
(3242, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:45:23', 0),
(3243, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:46:30', 0),
(3244, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:47:21', 0),
(3245, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:47:40', 0),
(3246, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:47:48', 0),
(3247, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:48:19', 0),
(3248, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:48:32', 0),
(3249, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:48:45', 0),
(3250, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:48:56', 0),
(3251, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:49:10', 0),
(3252, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:50:46', 0),
(3253, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:51:04', 0),
(3254, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:51:33', 0),
(3255, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:53:13', 0),
(3256, '7', 'Save order itemid 1632895780 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:53:38', 0),
(3257, '7', 'Applied discount id 5 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:54:08', 0),
(3258, '7', 'Save order itemid 1632895780 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:54:14', 0),
(3259, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:55:34', 0),
(3260, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:58:22', 0),
(3261, '7', 'Save order itemid 1632895780 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:58:36', 0),
(3262, '7', 'Save order itemid 1632895780 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:58:53', 0),
(3263, '7', 'Remove discount at transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:59:38', 0),
(3264, '7', 'Applied discount id 5 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:59:42', 0),
(3265, '7', 'Applied discount id 5 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 05:59:46', 0),
(3266, '7', 'Save order itemid 1632895839 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 06:00:02', 0),
(3267, '7', 'Save order itemid 1632895780 at qty 1 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 06:00:21', 0),
(3268, '7', 'Save order itemid 1633326819 at qty 10 transaction # 9 ', '::1', 'ROLAND', '2021-10-04 06:00:29', 0),
(3269, '7', 'Save new transaction # 10 ', '::1', 'ROLAND', '2021-10-04 06:03:11', 0),
(3270, '7', 'Save order itemid 1632898282 at qty 1 transaction # 10 ', '::1', 'ROLAND', '2021-10-04 06:03:18', 0),
(3271, '7', 'Save order itemid 1632898282 at qty 9 transaction # 10 ', '::1', 'ROLAND', '2021-10-04 06:03:27', 0),
(3272, '7', 'Applied discount id 5 to all transaction # 10', '::1', 'ROLAND', '2021-10-04 06:27:55', 0),
(3273, '7', 'Remove discount at transaction # 10 ', '::1', 'ROLAND', '2021-10-04 06:27:58', 0),
(3274, '7', 'Remove discount at transaction # 10 ', '::1', 'ROLAND', '2021-10-04 06:30:44', 0),
(3275, '7', 'Save order itemid 1632896698 at qty 1 transaction # 10 ', '::1', 'ROLAND', '2021-10-04 06:30:59', 0),
(3276, '7', 'Applied discount id 5 to all transaction # 10', '::1', 'ROLAND', '2021-10-04 06:31:02', 0),
(3277, '7', 'Remove discount at transaction # 10 ', '::1', 'ROLAND', '2021-10-04 06:31:05', 0),
(3278, '7', 'Applied discount id 5 transaction # 10 ', '::1', 'ROLAND', '2021-10-04 06:31:08', 0),
(3279, '7', 'Applied discount id 5 to all transaction # 10', '::1', 'ROLAND', '2021-10-04 06:31:10', 0),
(3280, '7', 'Remove discount at transaction # 10 ', '::1', 'ROLAND', '2021-10-04 06:31:13', 0),
(3281, '1', 'Logged in', '::1', 'ROLAND', '2021-10-04 06:45:00', 0),
(3282, '1', 'Logged in', '192.168.1.26', '192.168.1.26', '2021-10-04 06:59:01', 0),
(3283, '1', 'Logged in', '192.168.1.26', '192.168.1.26', '2021-10-04 06:59:06', 0),
(3284, '1', 'Save new transaction # 11 ', '192.168.1.26', '192.168.1.26', '2021-10-04 06:59:23', 0),
(3285, '1', 'Save order itemid 1632898223 at qty 1 transaction # 11 ', '192.168.1.26', '192.168.1.26', '2021-10-04 06:59:37', 0),
(3286, '7', 'Logged in', '::1', 'ROLAND', '2021-10-04 07:05:57', 0),
(3287, '7', 'Save order itemid 1632895839 at qty 1 transaction # 10 ', '::1', 'ROLAND', '2021-10-04 07:07:07', 0),
(3288, '7', 'Logged in', '::1', 'ROLAND', '2021-10-04 07:26:14', 0),
(3289, '10', 'Logged in', '::1', 'ROLAND', '2021-10-04 07:26:47', 0),
(3290, '11', 'Logged in', '::1', 'ROLAND', '2021-10-04 07:28:02', 0),
(3291, '7', 'Logged in', '::1', 'ROLAND', '2021-10-04 07:30:08', 0),
(3292, '11', 'Logged in', '::1', 'ROLAND', '2021-10-04 07:30:19', 0),
(3293, '11', 'Save new transaction # 1 ', '::1', 'ROLAND', '2021-10-04 07:30:21', 0),
(3294, '7', 'Logged in', '::1', 'ROLAND', '2021-10-04 07:31:39', 0),
(3295, '7', 'Save order itemid 1632896678 at qty 1 transaction # 1 ', '::1', 'ROLAND', '2021-10-04 07:31:43', 0),
(3296, '7', 'Save service type as takeout from transaction # 1 ', '::1', 'ROLAND', '2021-10-04 08:47:38', 0),
(3297, '7', 'Save order itemid 1632896678 at qty 1 transaction # 1 ', '::1', 'ROLAND', '2021-10-04 09:08:36', 0),
(3298, '7', 'Logged in', '::1', 'ROLAND', '2021-10-04 09:26:59', 0),
(3299, '7', 'Logged in', '::1', 'ROLAND', '2021-10-05 01:24:43', 0),
(3300, '7', 'Void transaction # 1 ', '::1', 'ROLAND', '2021-10-05 01:32:43', 0),
(3301, '7', 'Close log date 124 at 2021-10-05 09:32:55', '::1', 'ROLAND', '2021-10-05 01:32:55', 0),
(3302, '7', 'Close log date 126 at 2021-10-05 09:33:12', '::1', 'ROLAND', '2021-10-05 01:33:12', 0),
(3303, '7', 'Close log date 128 at 2021-10-06 09:42:33', '::1', 'ROLAND', '2021-10-06 01:42:33', 0),
(3304, '7', 'Logged in', '::1', 'ROLAND', '2021-10-06 01:42:45', 0),
(3305, '7', 'Logged in', '::1', 'ROLAND', '2021-10-06 01:42:46', 0),
(3306, '7', 'Save new transaction # 12 ', '::1', 'ROLAND', '2021-10-06 01:42:50', 0),
(3307, '7', 'Save order itemid 1632895839 at qty 1 transaction # 12 ', '::1', 'ROLAND', '2021-10-06 01:42:51', 0),
(3308, '7', 'Save new transaction # 13 ', '::1', 'ROLAND', '2021-10-06 02:28:20', 0),
(3309, '7', 'Save order itemid 1632895839 at qty 1 transaction # 13 ', '::1', 'ROLAND', '2021-10-06 02:28:22', 0),
(3310, '7', 'Applied discount id 5 transaction # 13 ', '::1', 'ROLAND', '2021-10-06 02:28:25', 0),
(3311, '7', 'Save order itemid 1633487302 at qty 6 transaction # 13 ', '::1', 'ROLAND', '2021-10-06 02:28:28', 0),
(3312, '7', 'Save service type as takeout from transaction # 13 ', '::1', 'ROLAND', '2021-10-06 02:28:47', 0),
(3313, '7', 'Apply special request aasd at transaction # 13', '::1', 'ROLAND', '2021-10-06 02:28:51', 0),
(3314, '7', 'Save new transaction # 14 ', '::1', 'ROLAND', '2021-10-06 04:42:55', 0),
(3315, '7', 'Save order itemid 1632895839 at qty 10 transaction # 14 ', '::1', 'ROLAND', '2021-10-06 04:43:01', 0),
(3316, '7', 'Save new transaction # 15 ', '::1', 'ROLAND', '2021-10-06 04:53:02', 0),
(3317, '7', 'Save order itemid 1632895839 at qty 1 transaction # 15 ', '::1', 'ROLAND', '2021-10-06 04:53:03', 0),
(3318, '7', 'Save new transaction # 16 ', '::1', 'ROLAND', '2021-10-06 04:54:32', 0),
(3319, '7', 'Save order itemid 1632895839 at qty 1 transaction # 16 ', '::1', 'ROLAND', '2021-10-06 04:54:34', 0),
(3320, '7', 'Save new transaction # 17 ', '::1', 'ROLAND', '2021-10-06 04:55:37', 0),
(3321, '7', 'Save order itemid 1632898282 at qty 1 transaction # 17 ', '::1', 'ROLAND', '2021-10-06 04:55:38', 0),
(3322, '1', 'Logged in', '::1', 'ROLAND', '2021-10-06 05:03:21', 0),
(3323, '7', 'Logged in', '::1', 'ROLAND', '2021-10-06 05:06:31', 0),
(3324, '7', 'Save new transaction # 18 ', '::1', 'ROLAND', '2021-10-06 05:46:22', 0),
(3325, '7', 'Save order itemid 1632895839 at qty 1 transaction # 18 ', '::1', 'ROLAND', '2021-10-06 05:46:23', 0),
(3326, '7', 'Save new transaction # 19 ', '::1', 'ROLAND', '2021-10-06 05:48:52', 0),
(3327, '7', 'Save order itemid 1632895839 at qty 1 transaction # 19 ', '::1', 'ROLAND', '2021-10-06 05:48:59', 0),
(3328, '7', 'Save new transaction # 20 ', '::1', 'ROLAND', '2021-10-06 05:52:16', 0),
(3329, '7', 'Save order itemid 1632895839 at qty 1 transaction # 20 ', '::1', 'ROLAND', '2021-10-06 05:52:18', 0),
(3330, '7', 'Save new transaction # 21 ', '::1', 'ROLAND', '2021-10-06 05:53:01', 0),
(3331, '7', 'Save order itemid 1632895839 at qty 1 transaction # 21 ', '::1', 'ROLAND', '2021-10-06 05:53:03', 0),
(3332, '1', 'Logged in', '::1', 'ROLAND', '2021-10-06 05:55:22', 0),
(3333, '1', 'Add menu: test', '::1', 'ROLAND', '2021-10-06 06:00:02', 0),
(3334, '7', 'Logged in', '::1', 'ROLAND', '2021-10-06 06:03:32', 0),
(3335, '7', 'Save new transaction # 22 ', '::1', 'ROLAND', '2021-10-06 06:03:40', 0),
(3336, '1', 'Logged in', '::1', 'ROLAND', '2021-10-06 06:04:11', 0),
(3337, '1', 'Save order itemid 1632895780 at qty 1 transaction # 22 ', '::1', 'ROLAND', '2021-10-06 06:04:40', 0),
(3338, '1', 'Save order itemid 1632895839 at qty 1 transaction # 22 ', '::1', 'ROLAND', '2021-10-06 06:04:41', 0),
(3339, '1', 'Save order itemid 1632898282 at qty 1 transaction # 22 ', '::1', 'ROLAND', '2021-10-06 06:04:58', 0),
(3340, '1', 'Save new transaction # 23 ', '::1', 'ROLAND', '2021-10-06 06:05:26', 0),
(3341, '1', 'Save order itemid 1632895839 at qty 1 transaction # 23 ', '::1', 'ROLAND', '2021-10-06 06:05:28', 0),
(3342, '1', 'Save new transaction # 24 ', '::1', 'ROLAND', '2021-10-06 06:05:52', 0),
(3343, '1', 'Save order itemid 1632895839 at qty 1 transaction # 24 ', '::1', 'ROLAND', '2021-10-06 06:05:54', 0),
(3344, '1', 'Save new transaction # 25 ', '::1', 'ROLAND', '2021-10-06 06:06:48', 0),
(3345, '1', 'Save order itemid 1632895839 at qty 1 transaction # 25 ', '::1', 'ROLAND', '2021-10-06 06:06:53', 0),
(3346, '1', 'Suspend transaction # 25', '::1', 'ROLAND', '2021-10-06 06:07:01', 0),
(3347, '1', 'Recall transaction # 25', '::1', 'ROLAND', '2021-10-06 06:07:26', 0),
(3348, '1', 'Void transaction # 25 ', '::1', 'ROLAND', '2021-10-06 06:07:33', 0),
(3349, '1', 'Save new transaction # 26 ', '::1', 'ROLAND', '2021-10-06 06:08:01', 0),
(3350, '1', 'Save order itemid 1632898223 at qty 1 transaction # 26 ', '::1', 'ROLAND', '2021-10-06 06:08:04', 0),
(3351, '1', 'Save order itemid 1632898476 at qty 1 transaction # 26 ', '::1', 'ROLAND', '2021-10-06 06:08:05', 0),
(3352, '1', 'Logged in', '::1', 'ROLAND', '2021-10-06 06:08:30', 0),
(3353, '1', 'Edit access level id 2', '::1', 'ROLAND', '2021-10-06 06:08:42', 0),
(3354, '1', 'Price overriden from 20.00 to 10 on order id 1633500482', '::1', 'ROLAND', '2021-10-06 06:09:04', 0),
(3355, '1', 'Void itemid 1633500484 ', '::1', 'ROLAND', '2021-10-06 06:09:26', 0),
(3356, '1', 'Applied discount id 1 order id # 1633500486 ', '::1', 'ROLAND', '2021-10-06 06:09:59', 0),
(3357, '1', 'Applied discount id 5 transaction # 26 ', '::1', 'ROLAND', '2021-10-06 06:10:08', 0),
(3358, '1', 'Void transaction # 26 ', '::1', 'ROLAND', '2021-10-06 06:18:38', 0),
(3359, '1', 'Save new transaction # 27 ', '::1', 'ROLAND', '2021-10-06 06:18:42', 0),
(3360, '1', 'Save order itemid 1632896678 at qty 1 transaction # 27 ', '::1', 'ROLAND', '2021-10-06 06:18:43', 0),
(3361, '1', 'Suspend transaction # 27', '::1', 'ROLAND', '2021-10-06 06:18:55', 0),
(3362, '1', 'Save new transaction # 28 ', '::1', 'ROLAND', '2021-10-06 06:42:02', 0),
(3363, '1', 'Save order itemid 1632898103 at qty 1 transaction # 28 ', '::1', 'ROLAND', '2021-10-06 06:42:04', 0),
(3364, '1', 'Logged in', '::1', 'ROLAND', '2021-10-06 07:09:23', 0),
(3365, '1', 'Delete menu id 1633500002', '::1', 'ROLAND', '2021-10-06 07:22:28', 0),
(3366, '1', 'Add menu: test', '::1', 'ROLAND', '2021-10-06 07:22:59', 0),
(3367, '1', 'Save new transaction # 29 ', '::1', 'ROLAND', '2021-10-06 07:48:56', 0),
(3368, '1', 'Save order itemid 1632896698 at qty 1 transaction # 29 ', '::1', 'ROLAND', '2021-10-06 07:48:59', 0),
(3369, '1', 'Save new transaction # 30 ', '::1', 'ROLAND', '2021-10-06 08:27:50', 0),
(3370, '1', 'Save order itemid 1632895839 at qty 1 transaction # 30 ', '::1', 'ROLAND', '2021-10-06 08:27:51', 0),
(3371, '1', 'Save new transaction # 31 ', '::1', 'ROLAND', '2021-10-06 08:29:46', 0),
(3372, '1', 'Save order itemid 1632897453 at qty 1 transaction # 31 ', '::1', 'ROLAND', '2021-10-06 08:29:48', 0),
(3373, '1', 'Save new transaction # 32 ', '::1', 'ROLAND', '2021-10-06 08:32:23', 0),
(3374, '1', 'Save order itemid 1632895839 at qty 1 transaction # 32 ', '::1', 'ROLAND', '2021-10-06 08:33:08', 0),
(3375, '1', 'Void transaction # 32 ', '::1', 'ROLAND', '2021-10-06 08:33:13', 0),
(3376, '1', 'Logged in', '::1', 'ROLAND', '2021-10-07 01:51:28', 0),
(3377, '1', 'Delete tender type id ', '::1', 'ROLAND', '2021-10-07 02:20:09', 0),
(3378, '1', 'Add supplier name: supplier 1', '::1', 'ROLAND', '2021-10-07 02:38:53', 0),
(3379, '1', 'Add supplier name: supplier 2', '::1', 'ROLAND', '2021-10-07 02:39:50', 0),
(3380, '1', 'Edit supplier id 0', '::1', 'ROLAND', '2021-10-07 02:40:40', 0),
(3381, '1', 'Add supplier name: supplier 1', '::1', 'ROLAND', '2021-10-07 02:41:55', 0),
(3382, '1', 'Logged in', '192.168.1.20', '192.168.1.20', '2021-10-07 05:46:22', 0),
(3383, '1', 'Void all transactions at 2021-10-07 02:12:14', '::1', 'ROLAND', '2021-10-07 06:12:14', 0),
(3384, '1', 'Close log date 129 at 2021-10-07 02:12:23', '::1', 'ROLAND', '2021-10-07 06:12:23', 0),
(3385, '7', 'Logged in', '::1', 'ROLAND', '2021-10-07 06:12:57', 0),
(3386, '7', 'Save new transaction # 33 ', '::1', 'ROLAND', '2021-10-07 06:13:41', 0),
(3387, '7', 'Save order itemid 1632895839 at qty 1 transaction # 33 ', '::1', 'ROLAND', '2021-10-07 06:13:43', 0),
(3388, '7', 'Save order itemid 1632897630 at qty 3 transaction # 33 ', '::1', 'ROLAND', '2021-10-07 06:16:29', 0),
(3389, '1', 'Logged in', '::1', 'ROLAND', '2021-10-07 06:41:39', 0),
(3390, '1', 'Close log date 130 at 2021-10-08 10:11:16', '::1', 'ROLAND', '2021-10-08 02:11:16', 0),
(3391, '7', 'Logged in', '::1', 'ROLAND', '2021-10-08 02:11:33', 0),
(3392, '7', 'Logged in', '::1', 'ROLAND', '2021-10-08 02:11:33', 0),
(3393, '1', 'Logged in', '::1', 'ROLAND', '2021-10-08 02:52:03', 0),
(3394, '1', 'Add category: test', '::1', 'ROLAND', '2021-10-08 02:52:14', 0),
(3395, '1', 'Add product group: test', '::1', 'ROLAND', '2021-10-08 02:52:26', 0),
(3396, '1', 'Add menu: test', '::1', 'ROLAND', '2021-10-08 02:52:45', 0),
(3397, '1', 'Add tender type: test', '::1', 'ROLAND', '2021-10-08 02:52:53', 0),
(3398, '1', 'Add denomination: test', '::1', 'ROLAND', '2021-10-08 02:52:59', 0),
(3399, '1', 'Add discount: test', '::1', 'ROLAND', '2021-10-08 02:53:07', 0),
(3400, '1', 'Add special request: test', '::1', 'ROLAND', '2021-10-08 02:53:13', 0),
(3401, '1', 'Add supplier name: test', '::1', 'ROLAND', '2021-10-08 02:53:17', 0),
(3402, '1', 'Save new transaction # 34 ', '::1', 'ROLAND', '2021-10-08 05:55:41', 0),
(3403, '1', 'Save order itemid 1632898282 at qty 1 transaction # 34 ', '::1', 'ROLAND', '2021-10-08 05:55:44', 0),
(3404, '1', 'Save order itemid 1632898476 at qty 1 transaction # 34 ', '::1', 'ROLAND', '2021-10-08 05:55:49', 0),
(3405, '1', 'Logged in', '192.168.1.20', '192.168.1.20', '2021-10-08 05:58:47', 0),
(3406, '1', 'Save new transaction # 35 ', '192.168.1.20', '192.168.1.20', '2021-10-08 05:59:06', 0),
(3407, '1', 'Save order itemid 1632896678 at qty 1 transaction # 35 ', '192.168.1.20', '192.168.1.20', '2021-10-08 05:59:19', 0),
(3408, '1', 'Save new transaction # 36 ', '192.168.1.20', '192.168.1.20', '2021-10-08 05:59:52', 0),
(3409, '1', 'Save new transaction # 37 ', '::1', 'ROLAND', '2021-10-08 05:59:55', 0),
(3410, '1', 'Save order itemid 1632895839 at qty 1 transaction # 37 ', '::1', 'ROLAND', '2021-10-08 05:59:59', 0),
(3411, '1', 'Save order itemid 1632896678 at qty 1 transaction # 36 ', '192.168.1.20', '192.168.1.20', '2021-10-08 06:00:21', 0),
(3412, '1', 'Save new transaction # 38 ', '192.168.1.20', '192.168.1.20', '2021-10-08 06:01:36', 0),
(3413, '1', 'Save order itemid 1632896678 at qty 1 transaction # 38 ', '192.168.1.20', '192.168.1.20', '2021-10-08 06:01:49', 0),
(3414, '1', 'Save service type as dine in from transaction # 38 ', '192.168.1.20', '192.168.1.20', '2021-10-08 06:02:11', 0),
(3415, '7', 'Logged in', '192.168.1.20', '192.168.1.20', '2021-10-08 06:10:23', 0),
(3416, '7', 'Save new transaction # 39 ', '192.168.1.20', '192.168.1.20', '2021-10-08 06:10:40', 0),
(3417, '7', 'Save order itemid 1632898103 at qty 1 transaction # 39 ', '192.168.1.20', '192.168.1.20', '2021-10-08 06:10:52', 0),
(3418, '7', 'Save order itemid 1632895780 at qty 2 transaction # 39 ', '192.168.1.20', '192.168.1.20', '2021-10-08 06:13:37', 0),
(3419, '7', 'Logged in', '192.168.1.20', '192.168.1.20', '2021-10-08 07:00:24', 0),
(3420, '7', 'Logged in', '192.168.1.42', '192.168.1.42', '2021-10-08 07:55:13', 0),
(3421, '7', 'Save new transaction # 40 ', '192.168.1.42', '192.168.1.42', '2021-10-08 07:55:27', 0),
(3422, '7', 'Save order itemid 1632897085 at qty 1 transaction # 40 ', '192.168.1.42', '192.168.1.42', '2021-10-08 07:55:48', 0),
(3423, '7', 'Save order itemid 1632896698 at qty 1 transaction # 40 ', '192.168.1.42', '192.168.1.42', '2021-10-08 07:56:05', 0),
(3424, '1', 'Logged in', '192.168.1.20', '192.168.1.20', '2021-10-08 08:36:36', 0),
(3425, '7', 'Logged in', '192.168.1.42', '192.168.1.42', '2021-10-08 13:38:13', 0),
(3426, '7', 'Logged in', '192.168.1.42', '192.168.1.42', '2021-10-08 13:38:18', 0),
(3427, '7', 'Save new transaction # 42 ', '192.168.1.42', '192.168.1.42', '2021-10-08 14:25:53', 0),
(3428, '7', 'Save order itemid 1632897085 at qty 1 transaction # 42 ', '192.168.1.42', '192.168.1.42', '2021-10-08 14:26:02', 0),
(3429, '7', 'Save new transaction # 43 ', '192.168.1.42', '192.168.1.42', '2021-10-08 14:28:41', 0),
(3430, '7', 'Save order itemid 1632897085 at qty 1 transaction # 43 ', '192.168.1.42', '192.168.1.42', '2021-10-08 14:29:09', 0),
(3431, '1', 'Close log date 131 at 2021-10-11 09:39:15', '::1', 'ROLAND', '2021-10-11 01:39:15', 0),
(3432, '7', 'Logged in', '::1', 'ROLAND', '2021-10-11 01:39:22', 0),
(3433, '1', 'Logged in', '::1', 'ROLAND', '2021-10-11 02:08:58', 0),
(3434, '1', 'Add account: inihaw na bangus', '::1', 'ROLAND', '2021-10-11 02:09:16', 0),
(3435, '1', 'Edit access level id 1', '::1', 'ROLAND', '2021-10-11 02:11:39', 0),
(3436, '1', 'Edit access level id 1', '::1', 'ROLAND', '2021-10-11 02:11:49', 0),
(3437, '1', 'Logged in', '::1', 'ROLAND', '2021-10-11 02:21:21', 0),
(3438, '1', 'Edit access level id 1', '::1', 'ROLAND', '2021-10-11 02:22:13', 0),
(3439, '1', 'Logged in', '::1', 'ROLAND', '2021-10-11 02:27:44', 0),
(3440, '1', 'Save new transaction # 45 ', '::1', 'ROLAND', '2021-10-11 06:20:41', 0),
(3441, '1', 'Save order itemid 1632898282 at qty 1 transaction # 45 ', '::1', 'ROLAND', '2021-10-11 06:20:43', 0),
(3442, '1', 'Save order itemid 1632898103 at qty 1 transaction # 45 ', '::1', 'ROLAND', '2021-10-11 06:20:45', 0),
(3443, '1', 'Save order itemid 1632897716 at qty 6 transaction # 45 ', '::1', 'ROLAND', '2021-10-11 06:20:48', 0),
(3444, '1', 'Save service type as takeout from transaction # 45 ', '::1', 'ROLAND', '2021-10-11 06:20:53', 0),
(3445, '1', 'Save service type as takeout from transaction # 45 ', '::1', 'ROLAND', '2021-10-11 06:21:11', 0),
(3446, '1', 'Save service type as takeout from transaction # 45 ', '::1', 'ROLAND', '2021-10-11 06:21:36', 0),
(3447, '1', 'Save new transaction # 46 ', '::1', 'ROLAND', '2021-10-11 06:57:57', 0),
(3448, '1', 'Void transaction # 46 ', '::1', 'ROLAND', '2021-10-11 06:58:04', 0),
(3449, '7', 'Logged in', '::1', 'ROLAND', '2021-10-11 07:57:04', 0),
(3450, '7', 'Close log date 132 at 2021-10-12 09:46:58', '::1', 'ROLAND', '2021-10-12 01:46:58', 0),
(3451, '7', 'Logged in', '::1', 'ROLAND', '2021-10-12 01:47:05', 0),
(3452, '7', 'Save new transaction # 47 ', '::1', 'ROLAND', '2021-10-12 01:48:47', 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `logs_view`
-- (See below for the actual view)
--
CREATE TABLE `logs_view` (
`id` int(11)
,`account_id` varchar(255)
,`description` varchar(255)
,`ip` varchar(255)
,`hostname` varchar(255)
,`timestamp` timestamp
,`is_deleted` tinyint(1)
,`name` varchar(100)
);

-- --------------------------------------------------------

--
-- Table structure for table `log_date`
--

CREATE TABLE `log_date` (
  `id` int(11) NOT NULL,
  `open_log` datetime NOT NULL,
  `close_log` datetime NOT NULL,
  `branch_code` varchar(10) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `log_date`
--

INSERT INTO `log_date` (`id`, `open_log`, `close_log`, `branch_code`, `date_time`) VALUES
(122, '2021-09-29 02:08:59', '2021-09-30 10:19:08', 'B-001', '2021-09-30 02:19:08'),
(123, '2021-09-30 10:19:15', '2021-10-04 10:53:03', 'B-001', '2021-10-04 02:53:03'),
(124, '2021-10-04 10:53:12', '2021-10-05 09:32:55', 'B-001', '2021-10-05 01:32:55'),
(125, '2021-10-04 03:28:04', '0000-00-00 00:00:00', 'B-002', '2021-10-04 07:28:04'),
(128, '2021-10-05 09:33:49', '2021-10-06 09:42:33', 'B-001', '2021-10-06 01:42:33'),
(129, '2021-10-06 09:42:48', '2021-10-07 02:12:23', 'B-001', '2021-10-07 06:12:23'),
(130, '2021-10-07 02:12:59', '2021-10-08 10:11:16', 'B-001', '2021-10-08 02:11:16'),
(131, '2021-10-08 10:11:34', '2021-10-11 09:39:15', 'B-001', '2021-10-11 01:39:15'),
(132, '2021-10-11 09:39:23', '2021-10-12 09:46:58', 'B-001', '2021-10-12 01:46:58'),
(133, '2021-10-12 09:47:08', '0000-00-00 00:00:00', 'B-001', '2021-10-12 01:47:08');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `short_descp` varchar(10) NOT NULL,
  `long_descp` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_group_id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `inventory_cost` decimal(10,2) NOT NULL,
  `cost` double NOT NULL,
  `active_tag` text NOT NULL,
  `print_label_sticker_tag` text NOT NULL,
  `vatable_tag` text NOT NULL,
  `slip_tag` text NOT NULL,
  `image_file` text NOT NULL,
  `added_by` text NOT NULL,
  `branch_code` varchar(10) NOT NULL,
  `quantity` decimal(10,0) DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `short_descp`, `long_descp`, `category_id`, `product_group_id`, `code`, `name`, `inventory_cost`, `cost`, `active_tag`, `print_label_sticker_tag`, `vatable_tag`, `slip_tag`, `image_file`, `added_by`, `branch_code`, `quantity`, `date_time`) VALUES
(1632895780, 'Coke', 'Coca Cola', 1632895735, 1, '000001', 'Coca Cola', '5.00', 10, 'YES', 'YES', 'NO', 'YES', 'uploads/CocaCola_900.jpg', 'admin', 'B-001', '298', '2021-10-08 08:40:06'),
(1632895839, 'Mtn Dew', 'Mountain Dew', 1632895735, 1, '000002', 'Mountain Dew', '5.00', 10, 'YES', 'YES', 'YES', 'NO', 'uploads/1200px-Mountain_Dew_logo.svg.png', 'admin', 'B-001', '661', '2021-10-08 08:40:06'),
(1632895884, 'Pepsi', 'Pepsi', 1632895735, 1, '000003', 'Pepsi', '5.00', 10, 'YES', 'YES', 'YES', 'NO', 'uploads/1200px-Pepsi_logo_2014.svg.png', 'admin', 'B-001', '299', '2021-10-08 08:40:06'),
(1632895912, 'Sprite', 'Sprite', 1632895735, 1, '000004', 'Sprite', '5.00', 10, 'YES', 'YES', 'NO', 'YES', 'uploads/sprite_PNG98771.png', 'admin', 'B-001', '299', '2021-10-08 08:40:06'),
(1632896678, 'Crbonara', 'Carbonara', 1632896645, 1, '000005', 'Carbonara', '40.00', 50, 'YES', 'YES', 'NO', 'YES', 'uploads/download (1).jfif', 'admin', 'B-001', '93', '2021-10-08 06:01:40'),
(1632896698, 'Spagh', 'Spaghetti', 1632896645, 1, '000006', 'Spaghetti', '40.00', 50, 'YES', 'YES', 'NO', 'YES', 'uploads/b01d809ae03b3751bc39c90498eb9f33_Pinoy-Spaghetti_Main_708_600.jpg', 'admin', 'B-001', '96', '2021-10-08 07:55:56'),
(1632897044, 'Brgr', 'Hamburger', 1632897006, 1, '000007', 'Hamburger', '20.00', 30, 'YES', 'YES', 'NO', 'YES', 'uploads/download (2).jfif', 'admin', 'B-001', '100', '2021-10-06 07:14:44'),
(1632897085, 'brgr chz', 'Cheese Burger', 1632897006, 1, '000008', 'Cheese Burger', '30.00', 40, 'YES', 'YES', 'NO', 'YES', 'uploads/download (3).jfif', 'admin', 'B-001', '97', '2021-10-08 14:29:05'),
(1632897120, 'brgr egg', 'Egg Burger', 1632897006, 1, '000009', 'Hamburger with egg', '40.00', 50, 'YES', 'YES', 'NO', 'YES', 'uploads/download (4).jfif', 'admin', 'B-001', '97', '2021-10-06 07:14:58'),
(1632897149, 'brgr dbl', 'Double Burger', 1632897006, 1, '000010', 'Double Hamburger', '60.00', 70, 'YES', 'YES', 'NO', 'YES', 'uploads/Double-Cheeseburger-square-FS-42.jpg', 'admin', 'B-001', '100', '2021-10-06 07:15:03'),
(1632897292, 'Kalderet', 'Kaldereta', 1632897233, 1, '000011', 'Kaldereta', '80.00', 100, 'YES', 'YES', 'NO', 'YES', 'uploads/__opt__aboutcom__coeus__resources__content_migration__serious_eats__seriouseats.com__recipes__20110114-goatstewcebu-primary_2-e88cf43a217b441581d6dfa2d345ca3f.jpg', 'admin', 'B-001', '99', '2021-10-06 07:15:05'),
(1632897368, 'Menudo', 'Menudo', 1632897233, 1, '000012', 'Menudo', '80.00', 100, 'YES', 'YES', 'NO', 'YES', 'uploads/filipino-pork-menudo-3-500x375.jpg', 'admin', 'B-001', '99', '2021-10-06 07:15:07'),
(1632897453, 'lech kaw', 'Lechon Kawali', 1632897233, 1, '000013', 'Lechon Kawali', '80.00', 100, 'YES', 'YES', 'YES', 'YES', 'uploads/download (5).jfif', 'admin', 'B-001', '98', '2021-10-06 08:29:48'),
(1632897479, 'P Sisig', 'Pork Sisig', 1632897233, 1, '000014', 'Pork Sisig', '80.00', 100, 'YES', 'YES', 'NO', 'YES', 'uploads/scmp_16apr15_fe_foodshoot7_jonw6710.jpg', 'admin', 'B-001', '99', '2021-10-06 07:15:12'),
(1632897630, 'Sngg Hpn', 'Sinigang Hipon', 1632897233, 1, '000015', 'Sinigang na Hipon', '80.00', 100, 'YES', 'YES', 'NO', 'YES', 'uploads/lutong-bahay-sinigang-na-hipon-573x381.jpg', 'admin', 'B-001', '96', '2021-10-07 06:16:29'),
(1632897716, 'lobster', 'Lobster', 1632897233, 1, '000016', 'Lobster', '150.00', 200, 'YES', 'YES', 'NO', 'YES', 'uploads/0001547_whole-frozen-lobster.jpeg', 'admin', 'B-001', '87', '2021-10-11 06:20:48'),
(1632897826, 'Ihw bang', 'ihaw na Bangus', 1632897233, 1, '000017', 'Inihaw na Bangus', '80.00', 100, 'YES', 'YES', 'NO', 'YES', 'uploads/lutong-bahay-inihaw-na-bangus-1200x900.jpg', 'admin', 'B-001', '99', '2021-10-06 07:15:19'),
(1632897885, 'bf steak', 'Beef Steak', 1632897233, 1, '000018', 'Beef Steak', '80.00', 100, 'YES', 'YES', 'NO', 'YES', 'uploads/b57ee35f-bce2-4229-8bf5-19b97876a4cb.jpg', 'admin', 'B-001', '99', '2021-10-06 07:15:23'),
(1632898073, 'halohalo', 'Halo-Halo', 1632898011, 1, '000019', 'Halo-Halo', '10.00', 20, 'YES', 'YES', 'NO', 'YES', 'uploads/Halo-Halo-Recipe-2021.jpg', 'admin', 'B-001', '98', '2021-10-06 07:15:25'),
(1632898103, 'Ice crm', 'Ice Cream', 1632898011, 1, '000020', 'Ice Cream', '10.00', 20, 'YES', 'YES', 'NO', 'YES', 'uploads/1200px-Ice_cream_with_whipped_cream,_chocolate_syrup,_and_a_wafer_(cropped).jpg', 'admin', 'B-001', '96', '2021-10-11 06:20:45'),
(1632898223, 'lch fln', 'Leche Flan', 1632898011, 1, '000021', 'Leche Flan', '10.00', 20, 'YES', 'YES', 'NO', 'YES', 'uploads/best-leche-flan-recipe.jpg', 'admin', 'B-001', '98', '2021-10-06 07:15:32'),
(1632898282, 'bko pndn', 'Buko Pandan', 1632898011, 1, '000022', 'Buko Pandan', '10.00', 20, 'YES', 'YES', 'NO', 'YES', 'uploads/buko-pandan-salad-recipe-735x490.jpg', 'admin', 'B-001', '86', '2021-10-11 06:20:43'),
(1632898476, 'cake', 'Cake', 1632898011, 1, '000023', 'Cake', '20.00', 30, 'YES', 'YES', 'NO', 'YES', 'uploads/7c1096c7-bfd0-4806-a794-1d3001fe0063.jpg', 'admin', 'B-001', '97', '2021-10-08 05:55:49'),
(1633504979, 'test', 'test', 1632895735, 1, '000024', 'test', '5.00', 10, 'YES', 'NO', 'NO', 'NO', '', 'admin', 'B-001', '70', '2021-10-07 03:19:14'),
(1633661566, 'test', 'test', 1633661535, 2, '000025', 'test', '123.00', 123, 'YES', 'NO', 'NO', 'NO', '', 'admin', 'B-001', '123', '2021-10-08 02:52:45');

-- --------------------------------------------------------

--
-- Table structure for table `menu_journal`
--

CREATE TABLE `menu_journal` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `beg_stocks` int(11) NOT NULL,
  `receiving_stocks` int(11) NOT NULL,
  `sales_stocks` int(11) NOT NULL,
  `returned_stocks` int(11) NOT NULL,
  `actual_stocks` int(11) NOT NULL,
  `end_stocks` int(11) NOT NULL,
  `log_date_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu_journal`
--

INSERT INTO `menu_journal` (`id`, `menu_id`, `order_id`, `beg_stocks`, `receiving_stocks`, `sales_stocks`, `returned_stocks`, `actual_stocks`, `end_stocks`, `log_date_id`, `username`, `date_time`) VALUES
(1260, 1632895839, 1632900417, 100, 0, 1, 0, 0, 99, 122, 'Uly', '2021-09-29 07:26:56'),
(1261, 1632897120, 1632900422, 100, 0, 1, 0, 0, 99, 122, 'Uly', '2021-09-29 07:27:01'),
(1262, 1632896678, 1632900489, 100, 0, 1, 0, 0, 99, 122, 'admin', '2021-09-29 07:28:09'),
(1263, 1632895780, 1632900891, 100, 0, 1, 0, 0, 99, 122, 'Uly', '2021-09-29 07:34:50'),
(1264, 1632895839, 1632969052, 99, 0, 1, 0, 0, 98, 123, 'admin', '2021-09-30 02:30:52'),
(1265, 1632896678, 1632972845, 99, 0, 1, 0, 0, 98, 123, 'admin', '2021-09-30 03:34:05'),
(1266, 1632898073, 1632972851, 100, 0, 1, 0, 0, 99, 123, 'admin', '2021-09-30 03:34:11'),
(1267, 1632897120, 1632972856, 99, 0, 1, 0, 0, 98, 123, 'admin', '2021-09-30 03:34:16'),
(1268, 1632897120, 1632972856, 98, 0, 1, 0, 0, 97, 123, 'admin', '2021-09-30 03:34:18'),
(1269, 1632897716, 1632972862, 100, 0, 6, 0, 0, 94, 123, 'admin', '2021-09-30 03:34:22'),
(1270, 1632895780, 1632976717, 99, 0, 1, 0, 0, 98, 123, 'Uly', '2021-09-30 04:38:37'),
(1271, 1632896678, 1632978459, 98, 0, 1, 0, 0, 97, 123, 'admin', '2021-09-30 05:07:39'),
(1272, 1632896678, 1632981444, 97, 0, 1, 0, 0, 96, 123, 'admin', '2021-09-30 05:57:23'),
(1273, 1632896698, 1632981446, 100, 0, 1, 0, 0, 99, 123, 'admin', '2021-09-30 05:57:25'),
(1274, 1632897630, 1632981449, 100, 0, 1, 0, 0, 99, 123, 'admin', '2021-09-30 05:57:29'),
(1275, 1632897479, 1632981450, 100, 0, 1, 0, 0, 99, 123, 'admin', '2021-09-30 05:57:30'),
(1276, 1632897368, 1632981451, 100, 0, 1, 0, 0, 99, 123, 'admin', '2021-09-30 05:57:31'),
(1277, 1632897716, 1632981453, 94, 0, 1, 0, 0, 93, 123, 'admin', '2021-09-30 05:57:32'),
(1278, 1632897453, 1632981454, 100, 0, 1, 0, 0, 99, 123, 'admin', '2021-09-30 05:57:33'),
(1279, 1632897292, 1632981455, 100, 0, 1, 0, 0, 99, 123, 'admin', '2021-09-30 05:57:34'),
(1280, 1632897826, 1632981456, 100, 0, 1, 0, 0, 99, 123, 'admin', '2021-09-30 05:57:36'),
(1281, 1632897885, 1632981458, 100, 0, 1, 0, 0, 99, 123, 'admin', '2021-09-30 05:57:38'),
(1282, 1632895780, 1632981468, 98, 0, 1, 0, 0, 97, 123, 'admin', '2021-09-30 05:57:48'),
(1283, 1632895839, 1632981469, 98, 0, 1, 0, 0, 97, 123, 'admin', '2021-09-30 05:57:49'),
(1284, 1632895884, 1632981471, 100, 0, 1, 0, 0, 99, 123, 'admin', '2021-09-30 05:57:50'),
(1285, 1632895912, 1632981472, 100, 0, 1, 0, 0, 99, 123, 'admin', '2021-09-30 05:57:51'),
(1286, 1632898282, 1633324422, 100, 0, 1, 0, 0, 99, 124, 'Uly', '2021-10-04 05:13:41'),
(1287, 1632898223, 1633324425, 100, 0, 1, 0, 0, 99, 124, 'Uly', '2021-10-04 05:13:45'),
(1288, 1632898103, 1633324427, 100, 0, 1, 0, 0, 99, 124, 'Uly', '2021-10-04 05:13:46'),
(1289, 1632898073, 1633324428, 99, 0, 1, 0, 0, 98, 124, 'Uly', '2021-10-04 05:13:48'),
(1290, 1632898476, 1633324429, 100, 0, 1, 0, 0, 99, 124, 'Uly', '2021-10-04 05:13:49'),
(1291, 1632895839, 1633325443, 97, 0, 1, 0, 0, 96, 124, 'Uly', '2021-10-04 05:30:43'),
(1292, 1632895839, 1633325443, 96, 0, 0, 0, 0, 96, 124, 'Uly', '2021-10-04 05:30:44'),
(1293, 1632895839, 1633325443, 96, 0, -1, 0, 0, 97, 124, 'Uly', '2021-10-04 05:30:45'),
(1294, 1632895839, 1633325443, 97, 0, 0, 0, 0, 97, 124, 'Uly', '2021-10-04 05:30:46'),
(1295, 1632895839, 1633325443, 97, 0, 1, 0, 0, 98, 124, 'Uly', '2021-10-04 05:30:50'),
(1296, 1632895839, 1633325443, 98, 0, 1, 0, 0, 97, 124, 'Uly', '2021-10-04 05:30:58'),
(1297, 1632895839, 1633325443, 97, 0, -5, 0, 0, 102, 124, 'Uly', '2021-10-04 05:34:17'),
(1298, 1632895839, 1633325443, 102, 0, -6, 0, 0, 108, 124, 'Uly', '2021-10-04 05:39:46'),
(1299, 1632895839, 1633325443, 108, 0, -7, 0, 0, 115, 124, 'Uly', '2021-10-04 05:41:16'),
(1300, 1632895839, 1633325443, 115, 0, -8, 0, 0, 123, 124, 'Uly', '2021-10-04 05:41:47'),
(1301, 1632895839, 1633325443, 123, 0, -9, 0, 0, 132, 124, 'Uly', '2021-10-04 05:44:01'),
(1302, 1632895839, 1633325443, 132, 0, -10, 0, 0, 142, 124, 'Uly', '2021-10-04 05:44:39'),
(1303, 1632895839, 1633325443, 142, 0, -11, 0, 0, 153, 124, 'Uly', '2021-10-04 05:45:23'),
(1304, 1632895839, 1633325443, 153, 0, -12, 0, 0, 165, 124, 'Uly', '2021-10-04 05:46:30'),
(1305, 1632895839, 1633325443, 165, 0, -13, 0, 0, 178, 124, 'Uly', '2021-10-04 05:47:21'),
(1306, 1632895839, 1633325443, 178, 0, -14, 0, 0, 192, 124, 'Uly', '2021-10-04 05:47:40'),
(1307, 1632895839, 1633325443, 192, 0, -15, 0, 0, 207, 124, 'Uly', '2021-10-04 05:47:48'),
(1308, 1632895839, 1633325443, 207, 0, -16, 0, 0, 223, 124, 'Uly', '2021-10-04 05:48:19'),
(1309, 1632895839, 1633325443, 223, 0, -17, 0, 0, 240, 124, 'Uly', '2021-10-04 05:48:32'),
(1310, 1632895839, 1633325443, 240, 0, -18, 0, 0, 258, 124, 'Uly', '2021-10-04 05:48:45'),
(1311, 1632895839, 1633325443, 258, 0, -19, 0, 0, 277, 124, 'Uly', '2021-10-04 05:48:56'),
(1312, 1632895839, 1633325443, 277, 0, -20, 0, 0, 297, 124, 'Uly', '2021-10-04 05:49:10'),
(1313, 1632895839, 1633325443, 297, 0, -21, 0, 0, 318, 124, 'Uly', '2021-10-04 05:50:46'),
(1314, 1632895839, 1633325443, 318, 0, -22, 0, 0, 340, 124, 'Uly', '2021-10-04 05:51:04'),
(1315, 1632895839, 1633325443, 340, 0, -23, 0, 0, 363, 124, 'Uly', '2021-10-04 05:51:33'),
(1316, 1632895839, 1633325443, 363, 0, -24, 0, 0, 387, 124, 'Uly', '2021-10-04 05:52:51'),
(1317, 1632895839, 1633325443, 387, 0, -25, 0, 0, 412, 124, 'Uly', '2021-10-04 05:53:13'),
(1318, 1632895780, 1633326819, 97, 0, 1, 0, 0, 96, 124, 'Uly', '2021-10-04 05:53:38'),
(1319, 1632895780, 1633326819, 96, 0, 0, 0, 0, 96, 124, 'Uly', '2021-10-04 05:54:14'),
(1320, 1632895839, 1633325443, 412, 0, -26, 0, 0, 438, 124, 'Uly', '2021-10-04 05:55:34'),
(1321, 1632895839, 1633325443, 438, 0, -27, 0, 0, 465, 124, 'Uly', '2021-10-04 05:58:22'),
(1322, 1632895780, 1633326819, 96, 0, -1, 0, 0, 97, 124, 'Uly', '2021-10-04 05:58:36'),
(1323, 1632895780, 1633326819, 97, 0, -2, 0, 0, 99, 124, 'Uly', '2021-10-04 05:58:53'),
(1324, 1632895839, 1633325443, 465, 0, -28, 0, 0, 493, 124, 'Uly', '2021-10-04 06:00:02'),
(1325, 1632895780, 1633326819, 99, 0, -3, 0, 0, 102, 124, 'Uly', '2021-10-04 06:00:21'),
(1326, 1632895780, 1633326819, 102, 0, 5, 0, 0, 97, 124, 'Uly', '2021-10-04 06:00:29'),
(1327, 1632898282, 1633327398, 99, 0, 1, 0, 0, 98, 124, 'Uly', '2021-10-04 06:03:18'),
(1328, 1632898282, 1633327398, 98, 0, 8, 0, 0, 90, 124, 'Uly', '2021-10-04 06:03:27'),
(1329, 1632896698, 1633329059, 99, 0, 1, 0, 0, 98, 124, 'Uly', '2021-10-04 06:30:59'),
(1330, 1632898223, 1633330773, 99, 0, 1, 0, 0, 98, 124, 'admin', '2021-10-04 06:59:33'),
(1331, 1632895839, 1633331228, 493, 0, 1, 0, 0, 492, 124, 'Uly', '2021-10-04 07:07:07'),
(1332, 1632896678, 1632900489, 96, 0, 0, 0, 0, 96, 124, 'Uly', '2021-10-04 07:31:43'),
(1333, 1632896678, 1632900489, 96, 0, -1, 0, 0, 97, 124, 'Uly', '2021-10-04 09:08:36'),
(1334, 1632895839, 1633484572, 492, 0, 1, 0, 0, 491, 129, 'Uly', '2021-10-06 01:42:51'),
(1335, 1632895839, 1633487302, 491, 0, 1, 0, 0, 490, 129, 'Uly', '2021-10-06 02:28:22'),
(1336, 1632895839, 1633487302, 490, 0, 5, 0, 0, 485, 129, 'Uly', '2021-10-06 02:28:28'),
(1337, 1632895839, 1633495381, 485, 0, 10, 0, 0, 475, 129, 'Uly', '2021-10-06 04:43:01'),
(1338, 1632895839, 1633495983, 475, 0, 1, 0, 0, 474, 129, 'Uly', '2021-10-06 04:53:03'),
(1339, 1632895839, 1633496075, 474, 0, 1, 0, 0, 473, 129, 'Uly', '2021-10-06 04:54:34'),
(1340, 1632898282, 1633496139, 90, 0, 1, 0, 0, 89, 129, 'Uly', '2021-10-06 04:55:38'),
(1341, 1632895839, 1633499184, 473, 0, 1, 0, 0, 472, 129, 'Uly', '2021-10-06 05:46:23'),
(1342, 1632895839, 1633499339, 472, 0, 1, 0, 0, 471, 129, 'Uly', '2021-10-06 05:48:59'),
(1343, 1632895839, 1633499539, 471, 0, 1, 0, 0, 470, 129, 'Uly', '2021-10-06 05:52:18'),
(1344, 1632895839, 1633499584, 470, 0, 1, 0, 0, 469, 129, 'Uly', '2021-10-06 05:53:03'),
(1345, 1632895780, 2, 97, 100, 0, 0, 0, 197, 129, 'admin', '2021-10-06 05:56:47'),
(1346, 1632895839, 2, 469, 100, 0, 0, 0, 569, 129, 'admin', '2021-10-06 05:56:47'),
(1347, 1632895884, 2, 99, 100, 0, 0, 0, 199, 129, 'admin', '2021-10-06 05:56:47'),
(1348, 1632895912, 2, 99, 100, 0, 0, 0, 199, 129, 'admin', '2021-10-06 05:56:47'),
(1349, 1632895780, 1633500280, 197, 0, 1, 0, 0, 196, 129, 'admin', '2021-10-06 06:04:40'),
(1350, 1632895839, 1633500282, 569, 0, 1, 0, 0, 568, 129, 'admin', '2021-10-06 06:04:41'),
(1351, 1632898282, 1633500298, 89, 0, 1, 0, 0, 88, 129, 'admin', '2021-10-06 06:04:58'),
(1352, 1632895839, 1633500329, 568, 0, 1, 0, 0, 567, 129, 'admin', '2021-10-06 06:05:28'),
(1353, 1632895839, 1633500354, 567, 0, 1, 0, 0, 566, 129, 'admin', '2021-10-06 06:05:54'),
(1354, 1632895839, 1633500414, 566, 0, 1, 0, 0, 565, 129, 'admin', '2021-10-06 06:06:53'),
(1355, 1632898223, 1633500484, 98, 0, 1, 0, 0, 97, 129, 'admin', '2021-10-06 06:08:04'),
(1356, 1632898476, 1633500486, 99, 0, 1, 0, 0, 98, 129, 'admin', '2021-10-06 06:08:05'),
(1357, 1632898223, 1633500482, 97, 0, 0, 1, 0, 98, 129, 'admin', '2021-10-06 06:09:26'),
(1358, 1632896678, 1633501124, 97, 0, 1, 0, 0, 96, 129, 'admin', '2021-10-06 06:18:43'),
(1359, 1632898103, 1633502524, 99, 0, 1, 0, 0, 98, 129, 'admin', '2021-10-06 06:42:04'),
(1360, 1632895780, 2, 196, 10, 0, 0, 0, 206, 129, 'admin', '2021-10-06 07:27:22'),
(1361, 1632896698, 1633506539, 98, 0, 1, 0, 0, 97, 129, 'admin', '2021-10-06 07:48:59'),
(1362, 1632895839, 1633508872, 565, 0, 1, 0, 0, 564, 129, 'admin', '2021-10-06 08:27:51'),
(1363, 1632897453, 1633508988, 99, 0, 1, 0, 0, 98, 129, 'admin', '2021-10-06 08:29:48'),
(1364, 1632895839, 1633509188, 564, 0, 1, 0, 0, 563, 129, 'admin', '2021-10-06 08:33:08'),
(1365, 1633504979, 2, 0, 70, 0, 0, 0, 70, 129, 'admin', '2021-10-07 03:19:14'),
(1366, 1632895780, 2, 186, 10, 0, 0, 0, 196, 129, 'admin', '2021-10-07 05:35:27'),
(1367, 1632895780, 2, 196, 1, 0, 0, 0, 197, 129, 'admin', '2021-10-07 05:35:50'),
(1368, 1632895839, 1633587223, 563, 0, 1, 0, 0, 562, 130, 'Uly', '2021-10-07 06:13:43'),
(1369, 1632897630, 1633587390, 99, 0, 3, 0, 0, 96, 130, 'Uly', '2021-10-07 06:16:29'),
(1370, 1632895780, 2, 197, 3, 0, 0, 0, 200, 130, 'admin', '2021-10-08 02:11:02'),
(1371, 1632898282, 1633672545, 88, 0, 1, 0, 0, 87, 131, 'admin', '2021-10-08 05:55:44'),
(1372, 1632898476, 1633672550, 98, 0, 1, 0, 0, 97, 131, 'admin', '2021-10-08 05:55:49'),
(1373, 1632896678, 1633672750, 96, 0, 1, 0, 0, 95, 131, 'admin', '2021-10-08 05:59:09'),
(1374, 1632895839, 1633672799, 562, 0, 1, 0, 0, 561, 131, 'admin', '2021-10-08 05:59:59'),
(1375, 1632896678, 1633672812, 95, 0, 1, 0, 0, 94, 131, 'admin', '2021-10-08 06:00:12'),
(1376, 1632896678, 1633672901, 94, 0, 1, 0, 0, 93, 131, 'admin', '2021-10-08 06:01:40'),
(1377, 1632898103, 1633673444, 98, 0, 1, 0, 0, 97, 131, 'Uly', '2021-10-08 06:10:43'),
(1378, 1632895780, 1633673608, 200, 0, 2, 0, 0, 198, 131, 'Uly', '2021-10-08 06:13:28'),
(1379, 1632897085, 1633679740, 100, 0, 1, 0, 0, 99, 131, 'Uly', '2021-10-08 07:55:39'),
(1380, 1632896698, 1633679756, 97, 0, 1, 0, 0, 96, 131, 'Uly', '2021-10-08 07:55:56'),
(1381, 1632895780, 2, 198, 100, 0, 0, 0, 298, 131, 'admin', '2021-10-08 08:40:06'),
(1382, 1632895839, 2, 561, 100, 0, 0, 0, 661, 131, 'admin', '2021-10-08 08:40:06'),
(1383, 1632895884, 2, 199, 100, 0, 0, 0, 299, 131, 'admin', '2021-10-08 08:40:06'),
(1384, 1632895912, 2, 199, 100, 0, 0, 0, 299, 131, 'admin', '2021-10-08 08:40:06'),
(1385, 1632897085, 1633703158, 99, 0, 1, 0, 0, 98, 131, 'Uly', '2021-10-08 14:25:58'),
(1386, 1632897085, 1633703345, 98, 0, 1, 0, 0, 97, 131, 'Uly', '2021-10-08 14:29:05'),
(1387, 1632898282, 1633933243, 87, 0, 1, 0, 0, 86, 132, 'admin', '2021-10-11 06:20:43'),
(1388, 1632898103, 1633933245, 97, 0, 1, 0, 0, 96, 132, 'admin', '2021-10-11 06:20:45'),
(1389, 1632897716, 1633933249, 93, 0, 6, 0, 0, 87, 132, 'admin', '2021-10-11 06:20:48');

-- --------------------------------------------------------

--
-- Table structure for table `ordered_items`
--

CREATE TABLE `ordered_items` (
  `id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `total_cost` decimal(10,2) NOT NULL,
  `discount` varchar(50) NOT NULL,
  `discount_amount` decimal(10,2) NOT NULL,
  `service_type` varchar(10) NOT NULL DEFAULT 'DINE IN',
  `special_request` text NOT NULL,
  `is_voided` tinyint(4) NOT NULL DEFAULT 0,
  `is_refunded` tinyint(4) NOT NULL DEFAULT 0,
  `is_overridden` tinyint(4) DEFAULT 0,
  `override_counter` int(11) NOT NULL,
  `original_price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `discount_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ordered_items`
--

INSERT INTO `ordered_items` (`id`, `order_id`, `item_id`, `cost`, `quantity`, `total_cost`, `discount`, `discount_amount`, `service_type`, `special_request`, `is_voided`, `is_refunded`, `is_overridden`, `override_counter`, `original_price`, `date_time`, `discount_id`) VALUES
(1632900417, 1632899740, 1632895839, '10.00', 1, '10.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-29 07:26:56', NULL),
(1632900422, 1632899740, 1632897120, '50.00', 1, '50.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-29 07:27:01', NULL),
(1632900489, 1632895741, 1632896678, '50.00', 3, '150.00', '.00', '0.00', 'TAKEOUT', '', 0, 0, 0, 0, '0.00', '2021-10-04 09:08:36', NULL),
(1632900891, 1632896525, 1632895780, '10.00', 1, '10.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-29 07:34:50', NULL),
(1632969052, 1632969050, 1632895839, '10.00', 1, '10.00', '', '0.00', 'TAKEOUT', '', 0, 0, 0, 0, '0.00', '2021-09-30 02:38:47', NULL),
(1632972845, 1632969050, 1632896678, '50.00', 1, '50.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 03:34:05', NULL),
(1632972851, 1632969050, 1632898073, '20.00', 1, '20.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 03:34:11', NULL),
(1632972856, 1632969050, 1632897120, '50.00', 3, '150.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 03:34:18', NULL),
(1632972862, 1632969050, 1632897716, '200.00', 6, '1200.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 03:34:22', NULL),
(1632976717, 1632976715, 1632895780, '10.00', 1, '10.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 04:38:37', NULL),
(1632978459, 1632978448, 1632896678, '50.00', 1, '50.00', '', '0.00', 'TAKEOUT', '', 0, 0, 0, 0, '0.00', '2021-09-30 05:12:48', NULL),
(1632981444, 1632980971, 1632896678, '50.00', 1, '50.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 05:57:23', NULL),
(1632981446, 1632980971, 1632896698, '50.00', 1, '50.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 05:57:25', NULL),
(1632981449, 1632980971, 1632897630, '100.00', 1, '100.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 05:57:29', NULL),
(1632981450, 1632980971, 1632897479, '100.00', 1, '100.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 05:57:30', NULL),
(1632981451, 1632980971, 1632897368, '100.00', 1, '100.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 05:57:31', NULL),
(1632981453, 1632980971, 1632897716, '200.00', 1, '200.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 05:57:32', NULL),
(1632981454, 1632980971, 1632897453, '100.00', 1, '100.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 05:57:33', NULL),
(1632981455, 1632980971, 1632897292, '100.00', 1, '100.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 05:57:34', NULL),
(1632981456, 1632980971, 1632897826, '100.00', 1, '100.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 05:57:36', NULL),
(1632981458, 1632980971, 1632897885, '100.00', 1, '100.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 05:57:38', NULL),
(1632981468, 1632980971, 1632895780, '10.00', 1, '10.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 05:57:48', NULL),
(1632981469, 1632980971, 1632895839, '10.00', 1, '10.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 05:57:49', NULL),
(1632981471, 1632980971, 1632895884, '10.00', 1, '10.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 05:57:50', NULL),
(1632981472, 1632980971, 1632895912, '10.00', 1, '10.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-09-30 05:57:51', NULL),
(1633324422, 1633324420, 1632898282, '20.00', 1, '20.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-04 05:13:41', NULL),
(1633324425, 1633324420, 1632898223, '20.00', 1, '20.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-04 05:13:45', NULL),
(1633324427, 1633324420, 1632898103, '20.00', 1, '20.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-04 05:13:46', NULL),
(1633324428, 1633324420, 1632898073, '20.00', 1, '20.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-04 05:13:48', NULL),
(1633324429, 1633324420, 1632898476, '30.00', 1, '30.00', '', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-04 05:13:49', NULL),
(1633325443, 1633325440, 1632895839, '10.00', 30, '240.00', '20%', '60.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-04 06:00:02', 5),
(1633326819, 1633325440, 1632895780, '10.00', 10, '80.00', '20%', '20.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-04 06:00:29', 5),
(1633327398, 1633327391, 1632898282, '20.00', 10, '200.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-04 06:31:13', NULL),
(1633329059, 1633327391, 1632896698, '50.00', 1, '40.00', '20%', '10.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-04 06:31:10', 5),
(1633330773, 1633330759, 1632898223, '20.00', 1, '20.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-04 06:59:33', NULL),
(1633331228, 1633327391, 1632895839, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-04 07:07:07', NULL),
(1633484572, 1633484570, 1632895839, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 01:42:51', NULL),
(1633487302, 1633487300, 1632895839, '10.00', 6, '48.00', '20%', '12.00', 'TAKEOUT', 'aasd', 0, 0, 0, 0, '0.00', '2021-10-06 02:28:51', 5),
(1633495381, 1633495376, 1632895839, '10.00', 10, '100.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 04:43:01', NULL),
(1633495983, 1633495982, 1632895839, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 04:53:03', NULL),
(1633496075, 1633496073, 1632895839, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 04:54:34', NULL),
(1633496139, 1633496137, 1632898282, '20.00', 1, '20.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 04:55:38', NULL),
(1633499184, 1633499182, 1632895839, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 05:46:23', NULL),
(1633499339, 1633499332, 1632895839, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 05:48:59', NULL),
(1633499539, 1633499537, 1632895839, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 05:52:18', NULL),
(1633499584, 1633499582, 1632895839, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 05:53:03', NULL),
(1633500280, 1633500220, 1632895780, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 06:04:40', NULL),
(1633500282, 1633500220, 1632895839, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 06:04:41', NULL),
(1633500298, 1633500220, 1632898282, '20.00', 1, '20.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 06:04:58', NULL),
(1633500329, 1633500326, 1632895839, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 06:05:28', NULL),
(1633500354, 1633500353, 1632895839, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 06:05:54', NULL),
(1633500414, 1633500408, 1632895839, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 06:06:53', NULL),
(1633500484, 1633500482, 1632898223, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 1, 0, 1, 1, '20.00', '2021-10-06 06:09:26', NULL),
(1633500486, 1633500482, 1632898476, '30.00', 1, '24.00', '20%', '6.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 06:10:08', 5),
(1633501124, 1633501122, 1632896678, '50.00', 1, '50.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 06:18:43', NULL),
(1633502524, 1633502523, 1632898103, '20.00', 1, '20.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 06:42:04', NULL),
(1633506539, 1633506536, 1632896698, '50.00', 1, '50.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 07:48:59', NULL),
(1633508872, 1633508870, 1632895839, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 08:27:51', NULL),
(1633508988, 1633508986, 1632897453, '100.00', 1, '100.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 08:29:48', NULL),
(1633509188, 1633509143, 1632895839, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-06 08:33:08', NULL),
(1633587223, 1633587222, 1632895839, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-07 06:13:43', NULL),
(1633587390, 1633587222, 1632897630, '100.00', 3, '300.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-07 06:16:29', NULL),
(1633672545, 1633672542, 1632898282, '20.00', 1, '20.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-08 05:55:44', NULL),
(1633672550, 1633672542, 1632898476, '30.00', 1, '30.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-08 05:55:49', NULL),
(1633672750, 1633672738, 1632896678, '50.00', 1, '50.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-08 05:59:10', NULL),
(1633672799, 1633672796, 1632895839, '10.00', 1, '10.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-08 05:59:59', NULL),
(1633672812, 1633672783, 1632896678, '50.00', 1, '50.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-08 06:00:12', NULL),
(1633672901, 1633672887, 1632896678, '50.00', 1, '50.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-08 06:01:40', NULL),
(1633673444, 1633673431, 1632898103, '20.00', 1, '20.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-08 06:10:43', NULL),
(1633673608, 1633673431, 1632895780, '10.00', 2, '20.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-08 06:13:28', NULL),
(1633679740, 1633679718, 1632897085, '40.00', 1, '40.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-08 07:55:39', NULL),
(1633679756, 1633679718, 1632896698, '50.00', 1, '50.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-08 07:55:56', NULL),
(1633703158, 1633703149, 1632897085, '40.00', 1, '40.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-08 14:25:58', NULL),
(1633703345, 1633703317, 1632897085, '40.00', 1, '40.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-08 14:29:05', NULL),
(1633933243, 1633933242, 1632898282, '20.00', 1, '20.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-11 06:20:43', NULL),
(1633933245, 1633933242, 1632898103, '20.00', 1, '20.00', '.00', '0.00', 'DINE IN', '', 0, 0, 0, 0, '0.00', '2021-10-11 06:20:45', NULL),
(1633933249, 1633933242, 1632897716, '200.00', 6, '1200.00', '.00', '0.00', 'TAKEOUT', '', 0, 0, 0, 0, '0.00', '2021-10-11 06:20:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ordered_tenders`
--

CREATE TABLE `ordered_tenders` (
  `id` int(11) NOT NULL,
  `trn_number` int(11) NOT NULL,
  `tender_id` int(11) NOT NULL,
  `amount_payed` decimal(10,2) NOT NULL DEFAULT 0.00,
  `amount_change` decimal(10,2) NOT NULL,
  `account_name` text NOT NULL,
  `account_number` text NOT NULL,
  `approval_number` varchar(50) NOT NULL,
  `bank` text NOT NULL,
  `remarks` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ordered_tenders`
--

INSERT INTO `ordered_tenders` (`id`, `trn_number`, `tender_id`, `amount_payed`, `amount_change`, `account_name`, `account_number`, `approval_number`, `bank`, `remarks`, `date_time`) VALUES
(177, 3, 1, '60.00', '0.00', '', '', '', '', '', '2021-09-29 07:27:10'),
(178, 1, 1, '50.00', '0.00', '', '', '', '', '', '2021-09-29 07:28:12'),
(179, 2, 1, '10.00', '0.00', '', '', '', '', '', '2021-09-29 07:34:53'),
(180, 4, 1, '1430.00', '0.00', '', '', '', '', '', '2021-09-30 03:34:27'),
(181, 5, 1, '10.00', '0.00', '', '', '', '', '', '2021-09-30 04:38:40'),
(182, 8, 1, '110.00', '0.00', '', '', '', '', '', '2021-10-04 05:13:54'),
(183, 9, 1, '260.00', '0.00', '', '', '', '', '', '2021-10-04 06:00:37'),
(184, 12, 1, '10.00', '0.00', '', '', '', '', '', '2021-10-06 01:42:56'),
(185, 13, 1, '48.00', '0.00', '', '', '', '', '', '2021-10-06 02:28:57'),
(186, 14, 3, '50.00', '0.00', '', '', '', '', '', '2021-10-06 04:43:09'),
(187, 14, 1, '100.00', '50.00', '', '', '', '', '', '2021-10-06 04:43:33'),
(188, 15, 1, '10.00', '0.00', '', '', '', '', '', '2021-10-06 04:53:07'),
(189, 16, 3, '5.00', '0.00', '', '', '', '', '', '2021-10-06 04:54:40'),
(190, 16, 1, '10.00', '5.00', '', '', '', '', '', '2021-10-06 04:54:43'),
(191, 17, 3, '10.00', '0.00', '', '', '', '', '', '2021-10-06 04:55:43'),
(192, 17, 1, '20.00', '10.00', '', '', '', '', '', '2021-10-06 04:55:46'),
(193, 18, 1, '10.00', '0.00', '', '', '', '', '', '2021-10-06 05:46:26'),
(194, 19, 1, '10.00', '0.00', '', '', '', '', '', '2021-10-06 05:49:03'),
(195, 20, 1, '10.00', '0.00', '', '', '', '', '', '2021-10-06 05:52:23'),
(196, 21, 1, '10.00', '0.00', '', '', '', '', '', '2021-10-06 05:53:07'),
(197, 22, 3, '40.00', '0.00', '', '', '', '', '', '2021-10-06 06:05:18'),
(198, 23, 3, '10.00', '0.00', '', '', '', '', '', '2021-10-06 06:05:32'),
(199, 24, 3, '5.00', '0.00', '', '', '', '', '', '2021-10-06 06:05:57'),
(200, 24, 1, '10.00', '5.00', '', '', '', '', '', '2021-10-06 06:06:25'),
(201, 28, 1, '20.00', '0.00', '', '', '', '', '', '2021-10-06 06:42:07'),
(202, 29, 3, '20.00', '0.00', '', '', '', '', '', '2021-10-06 08:26:00'),
(203, 29, 3, '10.00', '0.00', '', '', '', '', '', '2021-10-06 08:26:12'),
(204, 29, 1, '20.00', '0.00', '', '', '', '', '', '2021-10-06 08:27:22'),
(205, 30, 3, '10.00', '0.00', '', '', '', '', '', '2021-10-06 08:28:30'),
(206, 31, 3, '20.00', '0.00', '', '', '', '', '', '2021-10-06 08:29:55'),
(207, 31, 4, '20.00', '0.00', '', '', '', '', '', '2021-10-06 08:30:30'),
(208, 31, 6, '10.00', '0.00', '', '', '', '', '', '2021-10-06 08:31:16'),
(209, 31, 7, '50.00', '0.00', '', '', '', '', '', '2021-10-06 08:31:44'),
(210, 33, 1, '310.00', '0.00', '', '', '', '', '', '2021-10-07 06:41:33'),
(211, 34, 1, '50.00', '0.00', '', '', '', '', '', '2021-10-08 05:57:36'),
(212, 35, 3, '30.00', '0.00', '', '', '', '', '', '2021-10-08 05:59:27'),
(213, 35, 1, '20.00', '0.00', '', '', '', '', '', '2021-10-08 05:59:32'),
(214, 37, 3, '3.00', '0.00', '', '', '', '', '', '2021-10-08 06:00:04'),
(215, 37, 1, '7.00', '0.00', '', '', '', '', '', '2021-10-08 06:00:06'),
(216, 36, 3, '30.00', '0.00', '', '', '', '', '', '2021-10-08 06:00:37'),
(217, 36, 1, '20.00', '0.00', '', '', '', '', '', '2021-10-08 06:00:49'),
(218, 39, 1, '1000.00', '960.00', '', '', '', '', '', '2021-10-08 06:13:55'),
(219, 42, 1, '40.00', '0.00', '', '', '', '', '', '2021-10-08 14:26:14'),
(220, 43, 1, '40.00', '0.00', '', '', '', '', '', '2021-10-08 14:29:18'),
(221, 45, 4, '1240.00', '0.00', '', '', '', '', '', '2021-10-11 06:24:22');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_number` int(50) NOT NULL,
  `trn_number` int(50) NOT NULL,
  `guest_no` int(11) NOT NULL,
  `is_suspended` tinyint(4) NOT NULL DEFAULT 0,
  `log_date_id` int(11) NOT NULL,
  `cashier_username` varchar(50) NOT NULL,
  `branch_code` varchar(10) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `is_voided` tinyint(4) NOT NULL DEFAULT 0,
  `amount_payable` decimal(10,2) NOT NULL DEFAULT 0.00,
  `amount_payed` decimal(10,2) NOT NULL DEFAULT 0.00,
  `amount_change` decimal(10,2) NOT NULL DEFAULT 0.00,
  `is_settled` tinyint(4) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_number`, `trn_number`, `guest_no`, `is_suspended`, `log_date_id`, `cashier_username`, `branch_code`, `customer_id`, `is_voided`, `amount_payable`, `amount_payed`, `amount_change`, `is_settled`, `date_time`) VALUES
(1632895741, 1, 1, 1, 0, 122, 'admin', 'B-001', 0, 1, '150.00', '50.00', '0.00', 1, '2021-10-05 01:32:43'),
(1632896525, 2, 2, 1, 0, 122, 'Uly', 'B-001', 0, 0, '10.00', '10.00', '0.00', 1, '2021-09-29 07:34:53'),
(1632899740, 3, 3, 1, 0, 122, 'Uly', 'B-001', 0, 0, '60.00', '60.00', '0.00', 1, '2021-09-29 07:27:10'),
(1632969050, 1, 4, 1, 0, 123, 'admin', 'B-001', 0, 0, '1430.00', '1430.00', '0.00', 1, '2021-09-30 03:34:27'),
(1632976715, 2, 5, 1, 0, 123, 'Uly', 'B-001', 0, 0, '10.00', '10.00', '0.00', 1, '2021-09-30 04:38:40'),
(1632978448, 3, 6, 1, 0, 123, 'admin', 'B-001', 0, 1, '50.00', '0.00', '0.00', 0, '2021-10-04 02:52:55'),
(1632980971, 4, 7, 1, 0, 123, 'admin', 'B-001', 0, 0, '1040.00', '0.00', '0.00', 0, '2021-09-30 05:57:51'),
(1633324420, 1, 8, 1, 0, 124, 'Uly', 'B-001', 0, 0, '110.00', '110.00', '0.00', 1, '2021-10-04 05:13:54'),
(1633325440, 2, 9, 1, 0, 124, 'Uly', 'B-001', 0, 0, '260.00', '260.00', '0.00', 1, '2021-10-04 06:00:37'),
(1633327391, 3, 10, 1, 0, 124, 'Uly', 'B-001', 0, 0, '240.00', '0.00', '0.00', 0, '2021-10-04 07:07:07'),
(1633330759, 4, 11, 1, 0, 124, 'admin', 'B-001', 0, 0, '20.00', '0.00', '0.00', 0, '2021-10-04 06:59:33'),
(1633332622, 1, 1, 1, 0, 125, 'user', 'B-002', 0, 1, '0.00', '0.00', '0.00', 0, '2021-10-05 01:32:43'),
(1633484570, 1, 12, 1, 0, 129, 'Uly', 'B-001', 0, 0, '10.00', '10.00', '0.00', 1, '2021-10-06 01:42:56'),
(1633487300, 2, 13, 1, 0, 129, 'Uly', 'B-001', 0, 0, '48.00', '48.00', '0.00', 1, '2021-10-06 02:28:57'),
(1633495376, 3, 14, 1, 0, 129, 'Uly', 'B-001', 0, 0, '100.00', '150.00', '50.00', 1, '2021-10-06 04:43:33'),
(1633495982, 4, 15, 1, 0, 129, 'Uly', 'B-001', 0, 0, '10.00', '10.00', '0.00', 1, '2021-10-06 04:53:07'),
(1633496073, 5, 16, 1, 0, 129, 'Uly', 'B-001', 0, 0, '10.00', '15.00', '5.00', 1, '2021-10-06 04:54:43'),
(1633496137, 6, 17, 1, 0, 129, 'Uly', 'B-001', 0, 0, '20.00', '30.00', '10.00', 1, '2021-10-06 04:55:46'),
(1633499182, 7, 18, 1, 0, 129, 'Uly', 'B-001', 0, 0, '10.00', '10.00', '0.00', 1, '2021-10-06 05:46:26'),
(1633499332, 8, 19, 1, 0, 129, 'Uly', 'B-001', 0, 0, '10.00', '10.00', '0.00', 1, '2021-10-06 05:49:03'),
(1633499537, 9, 20, 1, 0, 129, 'Uly', 'B-001', 0, 0, '10.00', '10.00', '0.00', 1, '2021-10-06 05:52:23'),
(1633499582, 10, 21, 1, 0, 129, 'Uly', 'B-001', 0, 0, '10.00', '10.00', '0.00', 1, '2021-10-06 05:53:07'),
(1633500220, 11, 22, 1, 0, 129, 'Uly', 'B-001', 0, 0, '40.00', '40.00', '0.00', 1, '2021-10-06 06:05:18'),
(1633500326, 12, 23, 1, 0, 129, 'admin', 'B-001', 0, 0, '10.00', '10.00', '0.00', 1, '2021-10-06 06:05:32'),
(1633500353, 13, 24, 1, 0, 129, 'admin', 'B-001', 0, 0, '10.00', '15.00', '5.00', 1, '2021-10-06 06:06:25'),
(1633500408, 14, 25, 1, 0, 129, 'admin', 'B-001', 0, 1, '10.00', '0.00', '0.00', 0, '2021-10-06 06:07:33'),
(1633500482, 15, 26, 1, 0, 129, 'admin', 'B-001', 0, 1, '10.00', '0.00', '0.00', 0, '2021-10-06 06:18:38'),
(1633501122, 16, 27, 1, 0, 129, 'admin', 'B-001', 0, 1, '50.00', '0.00', '0.00', 0, '2021-10-07 06:12:14'),
(1633502523, 17, 28, 1, 0, 129, 'admin', 'B-001', 0, 0, '20.00', '20.00', '0.00', 1, '2021-10-06 06:42:07'),
(1633506536, 18, 29, 1, 0, 129, 'admin', 'B-001', 0, 0, '50.00', '50.00', '0.00', 1, '2021-10-06 08:27:22'),
(1633508870, 19, 30, 1, 0, 129, 'admin', 'B-001', 0, 0, '10.00', '10.00', '0.00', 1, '2021-10-06 08:28:51'),
(1633508986, 20, 31, 1, 0, 129, 'admin', 'B-001', 0, 0, '100.00', '100.00', '0.00', 1, '2021-10-06 08:31:44'),
(1633509143, 21, 32, 1, 0, 129, 'admin', 'B-001', 0, 1, '10.00', '0.00', '0.00', 0, '2021-10-06 08:33:13'),
(1633587222, 1, 33, 1, 0, 130, 'Uly', 'B-001', 0, 0, '310.00', '310.00', '0.00', 1, '2021-10-07 06:41:33'),
(1633672542, 1, 34, 1, 0, 131, 'admin', 'B-001', 0, 0, '50.00', '50.00', '0.00', 1, '2021-10-08 05:57:36'),
(1633672738, 2, 35, 1, 0, 131, 'admin', 'B-001', 0, 0, '50.00', '50.00', '0.00', 1, '2021-10-08 05:59:32'),
(1633672783, 3, 36, 1, 0, 131, 'admin', 'B-001', 0, 0, '50.00', '50.00', '0.00', 1, '2021-10-08 06:00:49'),
(1633672796, 4, 37, 1, 0, 131, 'admin', 'B-001', 0, 0, '10.00', '10.00', '0.00', 1, '2021-10-08 06:00:06'),
(1633672887, 5, 38, 1, 0, 131, 'admin', 'B-001', 0, 0, '50.00', '0.00', '0.00', 0, '2021-10-08 06:01:40'),
(1633673431, 6, 39, 1, 0, 131, 'Uly', 'B-001', 0, 0, '40.00', '1000.00', '960.00', 1, '2021-10-08 06:13:55'),
(1633679718, 7, 40, 1, 0, 131, 'Uly', 'B-001', 0, 0, '90.00', '0.00', '0.00', 0, '2021-10-08 07:55:56'),
(1633703149, 8, 42, 1, 0, 131, 'Uly', 'B-001', 0, 0, '40.00', '40.00', '0.00', 1, '2021-10-08 14:26:14'),
(1633703317, 9, 43, 1, 0, 131, 'Uly', 'B-001', 0, 0, '40.00', '40.00', '0.00', 1, '2021-10-08 14:29:18'),
(1633933242, 1, 45, 1, 0, 132, 'admin', 'B-001', 0, 0, '1240.00', '1240.00', '0.00', 1, '2021-10-11 06:24:22'),
(1633935478, 2, 46, 1, 0, 132, 'admin', 'B-001', 0, 1, '0.00', '0.00', '0.00', 0, '2021-10-11 06:58:04'),
(1634003328, 1, 47, 1, 0, 133, 'Uly', 'B-001', 0, 0, '0.00', '0.00', '0.00', 0, '2021-10-12 01:48:47');

-- --------------------------------------------------------

--
-- Table structure for table `product_groups`
--

CREATE TABLE `product_groups` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `printer_name` text NOT NULL,
  `printer_port` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_groups`
--

INSERT INTO `product_groups` (`id`, `description`, `printer_name`, `printer_port`, `date_time`) VALUES
(1, 'Bar Printer', 'Bar ', '3030', '2021-09-02 06:01:02'),
(2, 'test', 'test', 'test', '2021-10-08 02:52:26');

-- --------------------------------------------------------

--
-- Table structure for table `special_request`
--

CREATE TABLE `special_request` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `special_request`
--

INSERT INTO `special_request` (`id`, `description`, `date_time`) VALUES
(1, '20%', '2021-09-27 05:14:27'),
(2, 'test', '2021-10-08 02:53:13');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `supplier_name` varchar(50) NOT NULL,
  `supplier_address` text NOT NULL,
  `active_tag` text NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `supplier_name`, `supplier_address`, `active_tag`, `datetime`) VALUES
(1, 'Supplier 1', 'Supplier, Address', 'YES', '2021-10-07 02:41:55'),
(2, 'test', 'test', 'YES', '2021-10-08 02:53:17');

-- --------------------------------------------------------

--
-- Table structure for table `tender_types`
--

CREATE TABLE `tender_types` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `allow_change` text NOT NULL,
  `active_tag` text NOT NULL,
  `required_detail_tag` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tender_types`
--

INSERT INTO `tender_types` (`id`, `description`, `allow_change`, `active_tag`, `required_detail_tag`, `date_time`) VALUES
(1, 'CASH', 'YES', 'YES', 'YES', '2021-09-09 03:33:42'),
(3, 'GCASH', 'NO', 'YES', 'YES', '2021-09-20 09:24:17'),
(4, 'FOODPANDA', 'NO', 'YES', 'YES', '2021-09-20 09:24:28'),
(5, 'GRABFOOD', 'NO', 'YES', 'YES', '2021-09-20 09:24:38'),
(6, 'MASTER CARD', 'NO', 'YES', 'YES', '2021-09-20 09:24:47'),
(7, 'DEBIT', 'NO', 'YES', 'YES', '2021-09-20 09:29:20'),
(8, 'test', 'NO', 'YES', 'YES', '2021-10-08 02:52:53');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `trn_number` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `or_number` int(11) NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'ORDER',
  `amount` decimal(10,2) NOT NULL,
  `branch_code` varchar(10) NOT NULL,
  `log_date` int(11) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `trn_number`, `order_id`, `or_number`, `type`, `amount`, `branch_code`, `log_date`, `date_time`) VALUES
(1632895742, 1, 1632895741, 11, 'ORDER', '0.00', 'B-001', 122, '2021-10-04 08:47:47'),
(1632896526, 2, 1632896525, 3, 'ORDER', '0.00', 'B-001', 122, '2021-09-29 07:34:53'),
(1632899741, 3, 1632899740, 1, 'ORDER', '0.00', 'B-001', 122, '2021-09-29 07:27:10'),
(1632901116, 1, 0, 0, 'XREADING', '0.00', 'B-001', 122, '2021-09-29 07:38:35'),
(1632968350, 1, 0, 0, 'ZREADING', '0.00', 'B-001', 122, '2021-09-30 02:19:09'),
(1632969051, 4, 1632969050, 5, 'ORDER', '0.00', 'B-001', 123, '2021-09-30 03:34:27'),
(1632976716, 5, 1632976715, 6, 'ORDER', '0.00', 'B-001', 123, '2021-09-30 04:38:40'),
(1632978449, 6, 1632978448, 7, 'ORDER', '0.00', 'B-001', 123, '2021-09-30 05:12:53'),
(1632980972, 7, 1632980971, 0, 'ORDER', '0.00', 'B-001', 123, '2021-09-30 05:49:30'),
(1633315984, 2, 0, 0, 'ZREADING', '0.00', 'B-001', 123, '2021-10-04 02:53:03'),
(1633324421, 8, 1633324420, 8, 'ORDER', '0.00', 'B-001', 124, '2021-10-04 05:13:54'),
(1633324459, 2, 0, 0, 'XREADING', '0.00', 'B-001', 124, '2021-10-04 05:14:17'),
(1633325441, 9, 1633325440, 9, 'ORDER', '0.00', 'B-001', 124, '2021-10-04 06:00:37'),
(1633327392, 10, 1633327391, 0, 'ORDER', '0.00', 'B-001', 124, '2021-10-04 06:03:11'),
(1633330760, 11, 1633330759, 0, 'ORDER', '0.00', 'B-001', 124, '2021-10-04 06:59:18'),
(1633332623, 1, 1633332622, 0, 'ORDER', '0.00', 'B-002', 125, '2021-10-04 07:30:21'),
(1633397577, 3, 0, 0, 'ZREADING', '0.00', 'B-001', 124, '2021-10-05 01:32:55'),
(1633397594, 4, 0, 0, 'ZREADING', '0.00', 'B-001', 126, '2021-10-05 01:33:12'),
(1633484555, 5, 0, 0, 'ZREADING', '0.00', 'B-001', 128, '2021-10-06 01:42:33'),
(1633484571, 12, 1633484570, 12, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 01:42:56'),
(1633486851, 3, 0, 0, 'XREADING', '0.00', 'B-001', 129, '2021-10-06 02:20:50'),
(1633487301, 13, 1633487300, 14, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 02:28:57'),
(1633495377, 14, 1633495376, 15, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 04:43:33'),
(1633495983, 15, 1633495982, 16, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 04:53:07'),
(1633496074, 16, 1633496073, 17, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 04:54:43'),
(1633496138, 17, 1633496137, 18, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 04:55:46'),
(1633499183, 18, 1633499182, 19, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 05:46:26'),
(1633499333, 19, 1633499332, 20, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 05:49:03'),
(1633499538, 20, 1633499537, 21, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 05:52:23'),
(1633499583, 21, 1633499582, 22, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 05:53:07'),
(1633500221, 22, 1633500220, 23, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 06:05:19'),
(1633500327, 23, 1633500326, 24, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 06:05:32'),
(1633500354, 24, 1633500353, 25, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 06:06:25'),
(1633500409, 25, 1633500408, 0, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 06:06:48'),
(1633500483, 26, 1633500482, 0, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 06:08:01'),
(1633501123, 27, 1633501122, 0, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 06:18:42'),
(1633501375, 4, 0, 0, 'XREADING', '0.00', 'B-001', 129, '2021-10-06 06:22:54'),
(1633502194, 5, 0, 0, 'XREADING', '0.00', 'B-001', 129, '2021-10-06 06:36:32'),
(1633502280, 6, 0, 0, 'XREADING', '0.00', 'B-001', 129, '2021-10-06 06:37:58'),
(1633502299, 7, 0, 0, 'XREADING', '0.00', 'B-001', 129, '2021-10-06 06:38:18'),
(1633502319, 8, 0, 0, 'XREADING', '0.00', 'B-001', 129, '2021-10-06 06:38:38'),
(1633502524, 28, 1633502523, 26, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 06:42:07'),
(1633506537, 29, 1633506536, 27, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 08:27:22'),
(1633508871, 30, 1633508870, 28, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 08:27:56'),
(1633508987, 31, 1633508986, 29, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 08:31:44'),
(1633509144, 32, 1633509143, 0, 'ORDER', '0.00', 'B-001', 129, '2021-10-06 08:32:23'),
(1633509207, 9, 0, 0, 'XREADING', '0.00', 'B-001', 129, '2021-10-06 08:33:25'),
(1633587145, 6, 0, 0, 'ZREADING', '0.00', 'B-001', 129, '2021-10-07 06:12:24'),
(1633587223, 33, 1633587222, 30, 'ORDER', '0.00', 'B-001', 130, '2021-10-07 06:41:33'),
(1633659078, 7, 0, 0, 'ZREADING', '0.00', 'B-001', 130, '2021-10-08 02:11:17'),
(1633672543, 34, 1633672542, 31, 'ORDER', '0.00', 'B-001', 131, '2021-10-08 05:57:36'),
(1633672739, 35, 1633672738, 32, 'ORDER', '0.00', 'B-001', 131, '2021-10-08 05:59:33'),
(1633672784, 36, 1633672783, 34, 'ORDER', '0.00', 'B-001', 131, '2021-10-08 06:00:49'),
(1633672797, 37, 1633672796, 33, 'ORDER', '0.00', 'B-001', 131, '2021-10-08 06:00:06'),
(1633672888, 38, 1633672887, 35, 'ORDER', '0.00', 'B-001', 131, '2021-10-08 06:02:13'),
(1633673432, 39, 1633673431, 36, 'ORDER', '0.00', 'B-001', 131, '2021-10-08 06:13:55'),
(1633679719, 40, 1633679718, 37, 'ORDER', '0.00', 'B-001', 131, '2021-10-08 07:56:07'),
(1633700312, 41, 1632895741, 0, 'REPRINT', '0.00', 'B-001', 131, '2021-10-08 13:38:31'),
(1633703150, 42, 1633703149, 38, 'ORDER', '0.00', 'B-001', 131, '2021-10-08 14:26:14'),
(1633703318, 43, 1633703317, 39, 'ORDER', '0.00', 'B-001', 131, '2021-10-08 14:29:18'),
(1633703404, 44, 1632896525, 0, 'REPRINT', '0.00', 'B-001', 131, '2021-10-08 14:30:02'),
(1633916357, 8, 0, 0, 'ZREADING', '0.00', 'B-001', 131, '2021-10-11 01:39:15'),
(1633933243, 45, 1633933242, 40, 'ORDER', '0.00', 'B-001', 132, '2021-10-11 06:24:22'),
(1633933594, 10, 0, 0, 'XREADING', '0.00', 'B-001', 132, '2021-10-11 06:26:32'),
(1633935479, 46, 1633935478, 0, 'ORDER', '0.00', 'B-001', 132, '2021-10-11 06:57:57'),
(1634003220, 9, 0, 0, 'ZREADING', '0.00', 'B-001', 132, '2021-10-12 01:46:58'),
(1634003329, 47, 1634003328, 0, 'ORDER', '0.00', 'B-001', 133, '2021-10-12 01:48:47');

-- --------------------------------------------------------

--
-- Structure for view `logs_view`
--
DROP TABLE IF EXISTS `logs_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`oneteq_onepos`@`localhost` SQL SECURITY DEFINER VIEW `logs_view`  AS SELECT `l`.`id` AS `id`, `l`.`account_id` AS `account_id`, `l`.`description` AS `description`, `l`.`ip` AS `ip`, `l`.`hostname` AS `hostname`, `l`.`timestamp` AS `timestamp`, `l`.`is_deleted` AS `is_deleted`, `a`.`name` AS `name` FROM (`logs` `l` join `accounts` `a` on(`l`.`account_id` = `a`.`id`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_levels`
--
ALTER TABLE `access_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `role` (`role`);

--
-- Indexes for table `cash_fund`
--
ALTER TABLE `cash_fund`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cash_withdrawal`
--
ALTER TABLE `cash_withdrawal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `denominations`
--
ALTER TABLE `denominations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_date`
--
ALTER TABLE `log_date`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `product_group_id` (`product_group_id`);

--
-- Indexes for table `menu_journal`
--
ALTER TABLE `menu_journal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordered_items`
--
ALTER TABLE `ordered_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `discount_id` (`discount_id`);

--
-- Indexes for table `ordered_tenders`
--
ALTER TABLE `ordered_tenders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_groups`
--
ALTER TABLE `product_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `special_request`
--
ALTER TABLE `special_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tender_types`
--
ALTER TABLE `tender_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `cash_withdrawal`
--
ALTER TABLE `cash_withdrawal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1629269304;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1633661536;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1632969108;

--
-- AUTO_INCREMENT for table `denominations`
--
ALTER TABLE `denominations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3453;

--
-- AUTO_INCREMENT for table `log_date`
--
ALTER TABLE `log_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1633661567;

--
-- AUTO_INCREMENT for table `menu_journal`
--
ALTER TABLE `menu_journal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1390;

--
-- AUTO_INCREMENT for table `ordered_tenders`
--
ALTER TABLE `ordered_tenders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;

--
-- AUTO_INCREMENT for table `product_groups`
--
ALTER TABLE `product_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `special_request`
--
ALTER TABLE `special_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tender_types`
--
ALTER TABLE `tender_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1634003330;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`role`) REFERENCES `access_levels` (`id`);

--
-- Constraints for table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `menu_ibfk_2` FOREIGN KEY (`product_group_id`) REFERENCES `product_groups` (`id`);

--
-- Constraints for table `ordered_items`
--
ALTER TABLE `ordered_items`
  ADD CONSTRAINT `ordered_items_ibfk_1` FOREIGN KEY (`discount_id`) REFERENCES `discounts` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
