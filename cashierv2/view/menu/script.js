function closeManager() {
    $("#manager").slideUp()
    $("#product").slideDown()

    $('#order_content tr').each(function() {
        $("#" + this.id).attr("class", "bg-green")
    });
}

function showSuspend() {
    $(".suspend_transaction").slideDown()
    $.when($(".disabled_background").show(DEFAULT_TIME_VALUE))
    .done(() => {
        $("#suspend_transaction_number").focus()
    })
}

function showPriceOverride() {
    let flag = false
    $('#order_content tr').each(function() {
        if($("#" + this.id).hasClass("bg-yellow")) {
            flag = true
        }
    });

    if(flag == false) {
        showNotification("No selected Item!", "error")
        return
    }

    $(".price_override").slideDown()
    $(".disabled_background").show(DEFAULT_TIME_VALUE)
    $("#price_override_price").focus()
}

function showXRead() {
    $(".x_read").slideDown()
    $(".disabled_background").show(DEFAULT_TIME_VALUE)
}

function showZRead() {
    $(".z_read").slideDown()
    $(".disabled_background").show(DEFAULT_TIME_VALUE)
}