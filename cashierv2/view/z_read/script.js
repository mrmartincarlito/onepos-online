$("#z_read_form").on('submit', function(e) {
    e.preventDefault()
    
    $(".z_read").slideUp()
    $(".disabled_background").hide(DEFAULT_TIME_VALUE)
})

function hideZRead() {
    $(".z_read").slideUp()
    $(".disabled_background").hide(DEFAULT_TIME_VALUE)
}