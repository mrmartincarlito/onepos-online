$(() => {
    $("#manager_login_form").on('submit', function(e) {
        e.preventDefault()
        
        $(".manager_login").slideUp()
        $(".disabled_background").hide(DEFAULT_TIME_VALUE)

        $(".background").hide(DEFAULT_TIME_VALUE)
        $("#product").slideUp()
        $("#manager").slideDown()
    })
})

function cancelLogin() {
    $(".manager_login").slideUp()
    $(".disabled_background").hide(DEFAULT_TIME_VALUE)
}