function selectOrder(app) {
    const id = app.id

    if($("#" + id).attr("class") == "bg-yellow") {
        clearSelection()
        $("#" + id).attr("class", "bg-green")
    }
    else {
        clearSelection()
        $("#" + id).attr("class", "bg-yellow")
    }
}

function clearSelection() {
    $('#order_content tr').each(function() {
        $("#" + this.id).attr("class", "bg-green")
    });
}

function selectAll() {
    $('#order_content tr').each(function() {
        $("#" + this.id).attr("class", "bg-yellow")
    });
}