$(() => {
    $("#reprint_form").on('submit', function(e) {
        e.preventDefault()
        
        $(".reprint").slideUp()
        $(".disabled_background").hide(DEFAULT_TIME_VALUE)
    })
})

function hideReprint() {
    $(".reprint").slideUp()
    $(".disabled_background").hide(DEFAULT_TIME_VALUE)
}