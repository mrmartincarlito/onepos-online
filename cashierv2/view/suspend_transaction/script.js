$(() => {
    $("#suspend_transaction_form").on('submit', function(e) {
        e.preventDefault()
        
        $(".suspend_transaction").slideUp()
        $(".disabled_background").hide(DEFAULT_TIME_VALUE)
    })
})

function hideSuspendTransaction() {
    $(".suspend_transaction").slideUp()
    $(".disabled_background").hide(DEFAULT_TIME_VALUE)
}