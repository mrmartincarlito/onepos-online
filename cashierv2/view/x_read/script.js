$("#x_read_form").on('submit', function(e) {
    e.preventDefault()
    
    $(".x_read").slideUp()
    $(".disabled_background").hide(DEFAULT_TIME_VALUE)
})

function hideXRead() {
    $(".x_read").slideUp()
    $(".disabled_background").hide(DEFAULT_TIME_VALUE)
}