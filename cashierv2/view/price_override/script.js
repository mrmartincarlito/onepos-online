$("#price_override_form").on('submit', function(e) {
    e.preventDefault()
    
    $(".price_override").slideUp()
    $(".disabled_background").hide(DEFAULT_TIME_VALUE)
})

function hidePriceOverride() {
    $(".price_override").slideUp()
    $(".disabled_background").hide(DEFAULT_TIME_VALUE)
}