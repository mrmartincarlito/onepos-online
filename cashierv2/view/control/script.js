function openManager() {
    $(".manager_login").slideDown()
    $.when($(".disabled_background").show(DEFAULT_TIME_VALUE))
    .done(() => {
        $("#username").focus()
    })

    checkSelected()
}

function checkSelected() {
    $('#order_content tr').each(function() {
        if($("#" + this.id).hasClass("bg-yellow")) {
            console.log(this.id)
        }
    });
}

function showReprint() {
    $(".reprint").slideDown()
    $(".disabled_background").show(DEFAULT_TIME_VALUE)
}