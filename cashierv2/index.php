<?php
    $contents = json_decode(file_get_contents("content.json"), true);
?>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@500&family=Zen+Antique+Soft&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="design/color.css" />
	<link rel="stylesheet" type="text/css" href="design/index.css" />
	<link rel="stylesheet" type="text/css" href="design/mobile.css" />
	<link rel="icon" type="image/png" sizes="16x16" href="design/icon.png">
	<script src="https://kit.fontawesome.com/8b6b1fa9e8.js" crossorigin="anonymous"></script>
    <script src = "scripts/jquery-3.5.1.min.js" ></script>
    <script src = "scripts/jquery-ui.min.js" ></script>
    <script src = "scripts/notification/notification-too.js" ></script>
    <script src = "scripts/script.js" ></script>
    <script src = "scripts/function.js" ></script>
    <script src = "scripts/ajax.js" ></script>

    <?php
      foreach($contents as $content){
        echo "<script src='{$content['script']}'></script>";
      }
    ?>


</head>
<body>
    <div class="parent" id = "parent">
        <div class="div1 bg-blue border-bottom">
            <?php include $contents["navbar"]["path"] ?>
        </div>
        <div class="div2 bg-red">
            <?php include $contents["transaction_bar"]["path"] ?>
        </div>
        <div class="div3 bg-inherit">
            <?php include $contents["footer"]["path"] ?>
        </div>
        <div class="div4 bg-gray">
            <?php include $contents["menu"]["path"] ?>
        </div>
        <div class="div5 bg-black">
            <?php include $contents["total"]["path"] ?>
        </div>
        <div class="div6 bg-black">
            <?php include $contents["order"]["path"] ?>
        </div>
        <div class="div7">
            <?php include $contents["control"]["path"] ?>
        </div>
        <div class="suspend_transaction bg-white">
            <?php include $contents["suspend_transaction"]["path"] ?>
        </div>
        <div class="price_override bg-white">
            <?php include $contents["price_override"]["path"] ?>
        </div>
        <div class = "manager_login bg-white">
            <?php include $contents["manager_login"]["path"] ?>
        </div>
        <div class = "reprint bg-white">
            <?php include $contents["reprint"]["path"] ?>
        </div>
        <div class = "x_read bg-white">
            <?php include $contents["x_read"]["path"] ?>
        </div>
        <div class = "z_read bg-white">
            <?php include $contents["z_read"]["path"] ?>
        </div>
        <div class = "log_date_error bg-white">
            <?php include $contents["log_date_error"]["path"] ?>
        </div>
        <div class = "disabled_background">
        </div>
        <div class = "background">
        </div>
        <div class = "loading">
            <h1>Loading...</h1>
            <div id="preloader">
                <div id="loader"></div>
            </div>
        </div>
    </div>
</body>
</html>