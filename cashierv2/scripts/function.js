function checkCurrentTransaction(){
    $(".loading").show(DEFAULT_TIME_VALUE)
    let current_transaction = sessionStorage.getItem("current-transaction")
    console.log(current_transaction)

    if( current_transaction != "NONE" && current_transaction != null ){
       getCurrentTransaction(current_transaction)
    }
    else {
        $(".loading").hide(DEFAULT_TIME_VALUE)
    }
}

function autoComputeTotalAmount(){
  let totalAmount = 0
  $('#order_content tr').each(function() {
      let amount = this.cells[3].innerHTML;
      totalAmount = totalAmount + parseFloat(amount)
  });

  $("#total_amount").html(totalAmount.toFixed(2))
}

function addZeroes(data) {
    const character_length = 6
    const converted_data = String(data)
    const splitted_data = converted_data.split("")
    const remaining_char = character_length - splitted_data.length
    let newData = ""

    if(remaining_char >= 0) {
        for(let i = 1; i<= remaining_char; i++) {
            newData += "0"
        }

        newData += data
    }

    return newData
}

function determineLogDateError(current_date, log_date) {
    const splitted_current_date = current_date.split("-")
    const splitted_log_date = log_date.split("-")
    let message

    if(parseFloat(splitted_current_date[0]) < parseFloat(splitted_log_date[0])) {
        message = "Your log date is ahead. Please check if your system time is correct!"
    }
    else if(parseFloat(splitted_current_date[0]) == parseFloat(splitted_log_date[0])) {
        if(parseFloat(splitted_current_date[1]) < parseFloat(splitted_log_date[1])) {
            message = "Your log date is ahead. Please check if your system time is correct!"
        }
        else if(parseFloat(splitted_current_date[1]) == parseFloat(splitted_log_date[1])) {
            if(parseFloat(splitted_current_date[2]) < parseFloat(splitted_log_date[2])) {
                message = "Your log date is ahead. Please check if your system time is correct!"
            }
            else {
                message = "Your log date is behind!"
                $("#log_date_error").html(message + " Do you want to Zread?")
                $(".log_date_error").slideDown()
                $(".disabled_background").show()
            }
        }
        else {
            message = "Your log date is behind!"
            $("#log_date_error").html(message + " Do you want to Zread?")
            $(".log_date_error").slideDown()
            $(".disabled_background").show()
        }
    }
    else {
        message = "Your log date is behind!"
        $("#log_date_error").html(message + " Do you want to Zread?")
        $(".log_date_error").slideDown()
        $(".disabled_background").show()
    }
    
    showNotification(message, "error", 10000)
}

function timer() {
    const today = new Date()
    let seconds = today.getSeconds()
    let minutes = today.getMinutes()
    let hours = today.getHours()
    let daylight = "AM"
    if(hours > 12) {
        hours = hours - 12
        daylight = "PM"
    }
    if(minutes < 10) {
        minutes = "0" + minutes
    }
    if(seconds < 10) {
        seconds = "0" + seconds
    }
    const time = hours + ":" + minutes + ":" + seconds + " " + daylight

    $("#log_time").html(time)

    setTimeout(timer, 1000)
}

function logout() {
    window.location.href = "../logout.php"
}

function renderCategory(array, length, type) {
    let render = ""

    if(type == "no-image") {
        render = `<button class = "bg-white" onClick = "getProducts(` + array.id + `);">` + array.description + `</button>`
        return render
    }

    render = `<button style = "background: url('../api/` + array.image_file + `') rgba(250, 250, 250, 0.5); background-size: 100% 100%; background-repeat: no-repeat;" class = "bg-white" onClick = "getProducts(` + array.id + `);"><p>` + array.description + `</p></button>`
    return render
}