function getCurrentTransaction(current_transaction_ref_no, type = "old") {
    $.ajax({
        type: "GET",
        url: CASHIER_API + 'order.php',
        data: jQuery.param({ getCurrentTransaction: current_transaction_ref_no }),
        processData: false
    })
     .done(response => {
         responseJSON = JSON.parse(response)
 
        if(responseJSON.order == null) {
            $(".loading").hide(DEFAULT_TIME_VALUE)
            return
        }

         setTransactionBar(addZeroes(responseJSON.order.trn_number), addZeroes(responseJSON.order.order_number))
         //$("#guest-number-count").html("GUEST NO: " + responseJSON.order.guest_no)
 
         let render
 
         if(responseJSON.item.items.length > 0) {
            for(let i in responseJSON.item.items) {
                render += `<tr class = "bg-green" id = "` + responseJSON.item.items[i].id + `" onclick = "selectOrder(this);"><td class = "right-text" style = "cursor: pointer;">` + responseJSON.item.items[i].quantity + `</td><td style = "cursor: pointer;">` + responseJSON.item.items[i].item.name + `</td><td class = "right-text" style = "cursor: pointer;">` + responseJSON.item.items[i].cost + `</td><td class = "right-text" style = "cursor: pointer;">` + responseJSON.item.items[i].total_cost + `</td><td class = "right-text font-075" style = "cursor: pointer;">` + responseJSON.item.items[i].service_type + `</td></tr>`
            }
        }
        else {
            render = ""
        }
        $("#order_content").html(render)
        $("#new_transaction").attr("disabled", true)
        $("#settle").attr("disabled", false)
        $("#print_bill").attr("disabled", false)
        autoComputeTotalAmount()
        $(".loading").hide(DEFAULT_TIME_VALUE)
 
        if(type == "old") {
            $(".products-menu").hide(DEFAULT_TIME_VALUE)
            getCategory()
        }
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}
 
function getUser() { //ajax for getting user info
	return $.ajax({
	    type: "GET",
	    url: '../api/auth.php',
	    data: jQuery.param({ getLoggedUserDetails: "yes" }),
		processData: false
	})
}

function getLogDate() { //ajax for getting logdate
	return $.ajax({
	    type: "GET",
	    url: CASHIER_API + 'generic.php',
	    data: jQuery.param({ getLogDate: "yes" }),
		processData: false
	})
}

function getCategory() { //ajax for getting categories
    $(".loading").show(DEFAULT_TIME_VALUE)
    $(".background").hide(DEFAULT_TIME_VALUE)
    $("#categories").show(DEFAULT_TIME_VALUE)
	$.ajax({
	    type: "GET",
	    url: '../api/categories.php',
	    data: jQuery.param({ getCategories: "yes" }),
		processData: false
	})
	.done(response => {
        $.when(getGridSize())
        .done(response_grid => {
            const grid_size = JSON.parse(response_grid)
            responseJSON = JSON.parse(response)
            let render = ""
            let counter = 1
            const image = "with-image"
            const noimage = "no-image"
    
            for(let data in responseJSON) {
                if(counter < grid_size) {
                    if(responseJSON[data].image_file == "") {
                        render += renderCategory(responseJSON[data], grid_size, noimage)
                    }
                    else {
                        render += renderCategory(responseJSON[data], grid_size, image)
                    }
                }
                else {
                    counter = 0

                    if(responseJSON[data].image_file == "") {
                        render += renderCategory(responseJSON[data], grid_size, noimage) + `</tr>`
                    }
                    else {
                        render += renderCategory(responseJSON[data], grid_size, image) + `</tr>`
                    }
                }
                counter++
            }
            $("#categories").html(render)
            $(".loading").hide(DEFAULT_TIME_VALUE)
        })
	})
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function getGridSize() {
    return $.ajax({
        type: "GET",
        url: CASHIER_API + 'generic.php',
        data: jQuery.param({ getGridSize: "yes" }),
        processData: false
    })
}