const REPORTS_API = './api/report.'

$(() => {
	
	let url = window.location.search.substring(1)
	let urlData = url.split("&")
	let keys = []
	let values = []

	for(let data in urlData) {
		if(data > 0) {
			let returned_date = convertToArray(urlData[data])

			keys.push(returned_date.key)
			values.push(returned_date.value)
		}
	}

	let apiValue = ".php"
	let goToFunction

	for(let index in keys) {
		if(index > 0) {
			apiValue += "&" + keys[index] + "=" + values[index]
		}
		if(index == 0) {
			if(values[index] == "sales_by_menu"){
				apiValue += "?getAllReportByMenu=yes"

				goToFunction = "sales_by_menu"
			}

			if(values[index] == "sales_by_cashier_name") {
				apiValue += "?getAllReportByCashier=yes"

				goToFunction = "sales_by_cashier_name"
			}

			if(values[index] == "discount") {
				apiValue += "?getAllReportByDiscount=yes"

				goToFunction = "discount"
			}

			if(values[index] == "item_void") {
				apiValue += "?getAllReportByMenuVoid=yes"

				goToFunction = "item_void"
			}

			if(values[index] == "transaction_void") {

				apiValue += "?getAllReportByTransactionVoid=yes"

				goToFunction = "transaction_void"
			}

			if(values[index] == "refunded") {
				apiValue += "?getAllReportByMenuRefunded=yes"

				goToFunction = "refunded"
			}
		}
	}

	if(goToFunction == "sales_by_menu") {
		renderTable(REPORTS_API + values[0] + apiValue, "transactions")
	}

	if(goToFunction == "sales_by_cashier_name") {
		renderTable(REPORTS_API + values[0] + apiValue, "cashier")
	}

	if(goToFunction == "discount") {
		renderTable(REPORTS_API + values[0] + apiValue, "discount")
	}

	if(goToFunction == "item_void") {
		renderTable(REPORTS_API + values[0] + apiValue, "item_void")
	}

	if(goToFunction == "transaction_void") {
		renderTable(REPORTS_API + values[0] + apiValue, "transaction_void")
	}

	if(goToFunction == "refunded") {
		renderTable(REPORTS_API + values[0] + apiValue, "refunded")
	}
})

function convertToArray(data) {
	let splitted_data = data.split("=")

	return {
		key: splitted_data[0],
		value: splitted_data[1]
	}
}

function renderTable(dataSource, form) {
    $.ajax({
        url: dataSource,
        processData: false
    })
    .done(response => {
		let transaction = JSON.parse(response)

		for(transactions in transaction) {
			console.log(transaction[transactions])
			for(item in transaction[transactions].transaction.items) {
				let renderTable = `<tr><td style = "border: 1px solid #ddd !important;padding: 8px !important;">` + transaction[transactions].transaction.order.trn_number + `</td><td style = "border: 1px solid #ddd !important;padding: 8px !important;">` + transaction[transactions].transaction.items[item].name + `</td><td style = "border: 1px solid #ddd !important;padding: 8px !important;text-align: right!important;">` + transaction[transactions].transaction.items[item].cost + `</td><td style = "border: 1px solid #ddd !important;padding: 8px !important;text-align: right!important;">` + transaction[transactions].transaction.items[item].quantity + `</td><td style = "border: 1px solid #ddd !important;padding: 8px !important;">` + transaction[transactions].transaction.order.cashier_username + `</td><td style = "border: 1px solid #ddd !important;padding: 8px !important;">` + transaction[transactions].transaction.order.date_time + `</td><td style = "border: 1px solid #ddd !important;padding: 8px !important;text-align: right!important;">` + transaction[transactions].transaction.items[item].total_cost + `</td></tr>`

				$("#" + form).append(renderTable)
			}
		}

		$("#" + form + "-div").fadeIn()
		computeTotal(form)
    })
}

function printThis(form) {
	let data = $("#" + form + "-print").html()
	let report_name

	if(form == "transactions") {
		report_name = "Menu Sales Report"
	}

	if(form == "cashier") {
		report_name = "Cashier Sales Report"
	}

	if(form == "discount") {
		report_name = "Discount Sales Report"
	}

	if(form == "item_void") {
		report_name = "Item void Sales Report"
	}

	if(form == "transaction_void") {
		report_name = "Transactions void Sales Report"
	}

	if(form == "refunded") {
		report_name = "Refunded Sales Report"
	}

	let win = window.open();
	self.focus();
	win.document.open();
	win.document.write('<'+'html'+'><'+'head'+'><'+'style'+`>
	.transactions-table {
	  font-family: Arial, Helvetica, sans-serif;
	  border-collapse: collapse;
	  width: 100%;
	}

	h1 {
		text-align: center;
	}
	
	.transactions-table td, .transactions-table th {
	  border: 1px solid #ddd;
	  padding: 8px;
	}
	
	.transactions-table tr:nth-child(even){background-color: #f2f2f2;}
	
	.transactions-table tr:hover {background-color: #ddd;}
	
	.transactions-table th {
	  padding-top: 12px;
	  padding-bottom: 12px;
	  text-align: left;
	  background-color: #04AA6D;
	  color: white;
	}<`+'/style'+'><'+'/head'+'><'+'body'+'>');
	win.document.write("<h1>" + report_name + "</h1>");
	win.document.write(data);
	win.document.write('<'+'/body'+'><'+'/html'+'>');
	win.print();
	win.close();
}

function computeTotal(data) {
	let totalAmount = 0
	let totalQuantity = 0

	$('#' + data + ' tr').each(function() {
		let amount = this.cells[6].innerHTML
		totalAmount = totalAmount + parseFloat(amount)
		let quantity = this.cells[3].innerHTML
		totalQuantity = totalQuantity + parseFloat(quantity)
	});

	$("#" + data).append(`<tr><td style = "border: 1px solid #ddd !important;padding: 8px !important;"></td><td style = "border: 1px solid #ddd !important;padding: 8px !important;"></td><td style = "border: 1px solid #ddd !important;padding: 8px !important;">Total Quantity: </td><td style = "border: 1px solid #ddd !important;padding: 8px !important;text-align: right !important;">` + totalQuantity + `</td><td style = "border: 1px solid #ddd !important;padding: 8px !important;"></td><td style = "border: 1px solid #ddd !important;padding: 8px !important;">Total Amount: </td><td style = "border: 1px solid #ddd !important;padding: 8px !important;text-align: right!important;">` + totalAmount.toFixed(2) + `</td></tr>`)
}