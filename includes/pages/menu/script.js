
const API = './api/menu.php'
const MENU_JOURNAL = './api/cashier/menu_journal.php'
const PRODUCT_GROUPS = './api/product_groups.php'
const CATEGORIES = './api/categories.php'
const IMAGE = './api/upload_image.php'
const INVENTORY = './api/cashier/inventory.php'
const SUPPLIER = './api/supplier.php'

let selectedItems = []

	let myTable = $('#datatable').DataTable({
		processing: true,
		ajax: {
			url: API + '?get',
			dataSrc: function(json) {
				let return_data = new Array()
				for (let i = 0; i < json.length; i++) {
					return_data.push({
						// check: `<input class="btn-check" id="btn-check-2" checked autocomplete="off" name="inventory_items[]" type = "checkbox" value = "` + json[i].id + `"/>`,
						id: json[i].id,
						image : '<img src="api/'+json[i].image_file+'" alt="Menu Image" class="img-thumbnail">',
						short_descp: json[i].short_descp,
						long_descp: json[i].long_descp,
						category_id: json[i].category_id,
						product_group_id: json[i].product_group_id,
						code: json[i].code,
						name: json[i].name,
						cost: json[i].inventory_cost,
						srp: json[i].cost.toFixed(2),
						qty: json[i].quantity,
						//   active_tag: json[i].active_tag,
						//   print_label_sticker_tag: json[i].print_label_sticker_tag,
						//   vatable_tag: json[i].vatable_tag,
						//   slip_tag: json[i].slip_tag,
						action : `<button class="btn-warning" onclick="modify(`+json[i].id+`)">MODIFY</button>`,
						journal : `<button class="btn-danger" onclick="journal(`+json[i].id+`)">VIEW</button>`
						})
				}
				return return_data
			},

			complete: function() {
				$('#datatable tbody').on('dblclick', 'tr', function() {
					let data = $('#datatable')
						.DataTable()
						.row(this)
						.data()

					data["value"] = "<input type='number' item_id='"+data['id']+"'   id='item_"+data['id']+"' onkeyup='compute(this)'>"
					data["qty_total"] = "<input type='text' readonly id='qty_total_"+data['id']+"' value='"+data['qty']+"'>"
					data["variance"] = "<input type='number' readonly id='variance_"+data['id']+"'>"

					if(!selectedItems.includes(data)){
						selectedItems.push(data)

						new PNotify({
							"type" : "success",
							"title" : "Success",
							"text" : "Item Selected"
						})

						$(this).addClass('selected');

					}else{
						const index = selectedItems.indexOf(data)
						if(index > -1){
							selectedItems.splice(index , 1)
						}

						$(this).removeClass('selected');
						new PNotify({
							"type" : "error",
							"title" : "Error",
							"text" : "Removed selected item"
						})
					}

					
				})

				$('#addModal').on('click', function() {
						$('form').trigger('reset')
						getMenuCode()
						$(`input[name*="modifyId"]`).val('')
						$(`input[name*="action"]`).val('add')
						$('#formModal').modal('show')
				})

					$("#receiveModal").on('click', () => {
						viewReceivingTable()
						$('#receiving').modal('show')
					})
			}
		},
		columns: [
			// { data: 'check' },
			{ data: 'id' },
			{ data: 'image' },
			{ data: 'short_descp' },
			{ data: 'long_descp' },
			{ data: 'category_id' },
			{ data: 'product_group_id' },
			{ data: 'code' },
			{ data: 'name' },
			{ data: 'cost' },
			{ data: 'srp' },
			{ data: 'qty' },
			//   { data: 'active_tag' },
			//   { data: 'print_label_sticker_tag' },
			//   { data: 'vatable_tag' },
			//   { data: 'slip_tag' },
			{ data: 'action' },
			{ data: 'journal' }
		],
		rowReorder: {
			selector: 'td:nth-child(3)'
		},
		createdRow: function(row, data, dataIndex) {
			if(data["qty"] < 5) {
				//$(row).attr("style", "background: rgba(250, 100, 100, 1);")
				$(row).css("color", "red");
			}
		},
		responsive: true
	})

	$('form').on('submit', function(e) {
		e.preventDefault()

		$.blockUI({
			baseZ: 2000
		})

		let data = $('form').serializeArray()
		let params = postParams('', data)

		$.ajax({
			url: API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				responseJSON = JSON.parse(data)

				new PNotify(responseJSON)

				if (responseJSON.type == 'success') {
					saveImage()
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	})

  const data = getBranches()
  window.onload = getSupplier()

  //delete button
  $('#deleteButton').on('click', function() {
		if (!confirm('Are you sure you want to remove this user?')) {
			return false
		}

		$.blockUI({
			baseZ: 2000
		})

		$(`input[name*="action"]`).val('delete')

		let data = $('form').serializeArray()
		let params = postParams('', data)

		$.ajax({
				url: API,
				type: 'post',
				data: 'data=' + params,
				processData: false
		})
			.done(data => {
					$.unblockUI()
					console.log('Delete Response: ', data)

					responseJSON = JSON.parse(data)
					new PNotify(responseJSON)
					if (responseJSON.type == 'success') {
						myTable.ajax.reload(null, false)
						$('#formModal').modal('hide')
					}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Delete Post Error: ', errorThrown)
				return false
			})

		return false
  })

  //select product groups
	$.ajax({
		url: PRODUCT_GROUPS + '?get',
		processData: false
	})
		.done(data => {
			let json = JSON.parse(data)
			let newVal = '<option selected disabled>Select Product Groups</option>'

			newVal += json.map(value => {
				return `<option value="${value.id}">${value.description}</option>`
			})

			$(`#product_group_id`).html(newVal)
		})
		.fail(errorThrown => {
			console.log('Roles Get Error: ', errorThrown)
			return false
		})

	 //select categories
	 $.ajax({
		url: CATEGORIES + '?get',
		processData: false
	})
		.done(data => {
			let json = JSON.parse(data)
			let newVal = '<option selected disabled>Select Categories</option>'

			newVal += json.map(value => {
				return `<option value="${value.id}">${value.description}</option>`
			})

			$(`#category_id`).html(newVal)
		})
		.fail(errorThrown => {
			console.log('Roles Get Error: ', errorThrown)
			return false
		})

		$('#menu_code_check').change(function() {
			if(this.checked) {
				$("#code").attr("readonly", false)
			}
			else {
				$("#code").attr("readonly", true)
				getMenuCode()
			}
		});
	
function saveImage() {
	let formData = new FormData();
	formData.append('image_file', $('#image_file')[0].files[0]);
	
	$.ajax({
		url: IMAGE,
		type: 'post',
		data: formData,
		cache: false,
		contentType: false,
		processData: false
	})
	.done(data => {
		response = JSON.parse(data)
	})
	.fail(errorThrown => {
		console.log('Save Changes Post Error: ', errorThrown)
		return false
	})

}

function modify(data){
	$(`input[name*="modifyId"]`).val(data)
	$(`input[name*="action"]`).val('edit')

	$.ajax({
		url: API + '?getDetails=' + data,
		processData: false
	})
		.done(data => {
			let json = JSON.parse(data)

			$.unblockUI()
			populateForm($('form'), json)
			$('#formModal').modal('show')
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Account Details Get Error', errorThrown)
			return false
		})
}

function getMenuCode() {
	$.ajax({
		url: API + '?getLast',
		type: 'GET',
		processData: false
	})
		.done(data => {
			let json = JSON.parse(data)

			$("#code").val(json.menu_code)
		})
		.fail(errorThrown => {
			console.log('Roles Get Error: ', errorThrown)
			return false
		})
}

function getBranches() {
	$.ajax({
			url: API + '?getBranches',
			processData: false
	})
	.done(response => {
		let json = JSON.parse(response)
		let newVal = ""

		if(json.is_tech == "0") {
			newVal = `<option value="${json.branch_code}">${json.branch_name}</option>`
		}
		else {
			newVal = '<option selected disabled>Select Branch</option>'
			newVal += json.map(value => {
				return `<option value="${value.branch_code}">${value.branch_name}</option>`
			})
		}

		$(`#branch`).html(newVal)
	})
}

function getSupplier() {
	$.ajax({
			url: SUPPLIER + '?get',
			processData: false
	})
	.done(response => {
			let json = JSON.parse(response)
			let newVal = '<option selected disabled>SELECT SUPPLIER</option>'

			newVal += json.map(value => {
				return `<option value="${value.id}">${value.supplier_name}</option>`
			})

			$(`#supplier`).html(newVal)
	})
}

function journal(itemId){
	$('#inventoryModal').modal('show')

	$('#inventoryTable').DataTable().clear()
	$('#inventoryTable').DataTable().destroy()

	$('#inventoryTable').DataTable({
		processing: true,
		ajax: {
			url: MENU_JOURNAL + '?getMenuJournalByMenuId='+ itemId,
			dataSrc: function(json) {
				let return_data = new Array()
				for (let i = 0; i < json.length; i++) {
					return_data.push({
						menu_id: json[i].menu_id,
						order_id: json[i].order_id,
						beg_stocks: json[i].beg_stocks,
						receiving_stocks: json[i].receiving_stocks,
						sales_stocks: json[i].sales_stocks,
						refund: json[i].returned_stocks,
						actual_stocks: json[i].actual_stocks,
						end_stocks: json[i].end_stocks,
						username: json[i].username
					  })
				}
				return return_data
			},
  
			complete: function() {
				
			}
		},
		columns: [
			{ data: 'menu_id' },
			{ data: 'order_id' },
			{ data: 'beg_stocks' },
			{ data: 'receiving_stocks' },
			{ data: 'sales_stocks' },
			{ data: 'refund' },
			{ data: 'actual_stocks' },
			{ data: 'end_stocks' },
			{ data: 'username' }
		],
		rowReorder: {
			  selector: 'td:nth-child(2)'
		  },
		  responsive: true
	})
}

function inventory(id, qty){
	$('#inputStocksModal').modal('show')
	$('form#stocksForm').trigger('reset')
	$("#itemID").html(id)
	$("#current_qty").val(qty)
}

$( "#stocks" ).keyup(function() {
	let currentQty = $("#current_qty").val()
	let stocks = $("#stocks").val()

	$("#variance").val(currentQty - stocks)
});


function submitStocks(){
	let data = {
		"menu_id" : $("#itemID").html(),
		"type" : $("#type").val(),
		"qty" : $("#stocks").val()
	} 

	$.ajax({
		url: INVENTORY,
		type: 'post',
		data: 'saveInventory=' + JSON.stringify(data),
		processData: false
	})
		.done(data => {
			
			console.log('Save Changes Response: ', data)

			responseJSON = JSON.parse(data)

			new PNotify(responseJSON)

			if (responseJSON.type == 'success') {
				$('#inputStocksModal').modal('hide')
				myTable.ajax.reload(null, false)	
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})

}

let items_table = null
function viewReceivingTable(){
	$('#itemSelectedTable').DataTable().clear()
	$('#itemSelectedTable').DataTable().destroy()

	items_table = $('#itemSelectedTable').DataTable({
		"data" : selectedItems,
        "columns" : [
            { "data" : "id" },
            { "data": "long_descp" },
			{ "data": "qty_total" },
			{ "data": "value" },
			// { "data": "variance" },
        ]
	})
}

function receiveItems(){

	if(!confirm("Are you sure you want to save?")){
		return
	}

	let process = $("#processName").val()
	
	let values = []

	for(let i=0; i<selectedItems.length; i++){

		let value = {}
		value.item_id = selectedItems[i]['id']
		value.input_qty = $("#item_"+selectedItems[i]['id']).val()

		values.push(value)
	}

	$.ajax({
		url: INVENTORY,
		type: 'post',
		data: 'updateItemsByBatch=' + JSON.stringify(values) + '&process='+process,
		processData: false
	})
		.done(data => {
			
			console.log('Save Changes Response: ', data)

			responseJSON = JSON.parse(data)

			new PNotify(responseJSON)
			myTable.ajax.reload(null, false)
			$('#receiving').modal('hide')
			window.location.reload()
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
}

function compute(element){
	
	let current = $("#qty_total_"+ element.item_id).val()
	let value = element.value

	let variance = parseFloat(current) - parseFloat(value)
	
	$("#variance_"+ element.item_id).val(variance)
}

function goToSupplier() {
	window.location.href = "./?supplier"
}