const MENU_API = './api/menu.php'
const ACCOUNTS_API = './api/accounts.php'
const DISCOUNTS_API = './api/discounts.php'

window.onload = init;

function init() {
	//close all
	$("#sales_by_menu").hide()
	$("#sales_by_cashier_name").hide()
	$("#discount_report").hide()
	$("#item_void_report").hide()
	$("#transaction_void_report").hide()
	$("#refunded_report").hide()

	getBranches()
}

function choiceReportBy(value) {

	init()

	$("#" + value).show()

	if (value == "sales_by_menu") {
		getMenu()
	}

	if (value == "sales_by_cashier_name") {
		getCashierNameByBranch("")
	}

	if(value == "discount"){
		getDiscount()
		$("#discount_report").show()
	}
}

function getDiscount(){
	$.ajax({
		url: DISCOUNTS_API + '?get',
		processData: false
	})
		.done(response => {
			let json = JSON.parse(response)
			let newVal = '<option selected value="ALLDISCOUNT">All Discounts</option>'

			newVal += json.map(value => {
				return `<option value="${value.id}">${value.description}</option>`
			})

			$(`#discount_list`).html(newVal)
		})
}

function getBranches() {
	$.ajax({
		url: MENU_API + '?getBranches',
		processData: false
	})
		.done(response => {
			let json = JSON.parse(response)
			let newVal = '<option selected value="ALLBRANCH">All Branch</option>'

			newVal += json.map(value => {
				return `<option value="${value.branch_code}">${value.branch_name}</option>`
			})

			$(`#branch`).html(newVal)
		})
}

function selectBranch(value) {
	getCashierNameByBranch(value)
}


function getMenu() {
	$.ajax({
		url: MENU_API + '?get',
		processData: false
	})
		.done(response => {
			let json = JSON.parse(response)
			let newVal = '<option selected value="ALLMENU">All Menu</option>'

			newVal += json.map(value => {
				return `<option value="${value.id}">(${value.short_descp}) ${value.long_descp}</option>`
			})

			$(`#menu`).html(newVal)
		})
}

function getCashierNameByBranch(branch) {
	$.ajax({
		url: ACCOUNTS_API + '?getAccountsByBranch=' + branch,
		processData: false
	})
		.done(response => {
			let json = JSON.parse(response)
			let newVal = '<option selected disabled>Select cashier</option>'

			newVal += json.map(value => {
				return `<option value="${value.username}">${value.name}</option>`
			})

			$(`#cashier`).html(newVal)
		})
}

function proceedToReports() {
	const report_type = $("#selectedreports").val()
	const branch = $("#branch").val()
	const start_date = $("#start").val()
	let end_date = $("#end").val()

	if(report_type == null) {
		const error_message = {
			"type" : "error",
			"title" : "Error!",
			"text" : "Choose a report!"
		}

		new PNotify(error_message)
		return
	}

	makeUrlString(report_type, branch, start_date, end_date)
}

function makeUrlString(report_type, branch, start_date, end_date) {
	let url = "./?generate_reports&report_type=" + report_type

	if(branch != "ALLBRANCH") {
		url += "&branch_code=" + branch
	}

	if(start_date != "") {
		url += "&start=" + start_date
	}

	if(end_date != "") {
		url += "&end=" + end_date
	}

	if(report_type == "sales_by_menu") {
		const menu = $("#menu").val()

		url += menu == "ALLMENU" ? "" : "&item_id=" + menu
	}

	if(report_type == "sales_by_cashier_name") {
		const cashier = $("#cashier").val()

		url += cashier == null ? "" : "&username=" + cashier
	}

	if(report_type == "discount") {
		const discount = $("#discount_list").val()

		url += discount == "ALLDISCOUNT" ? "" : "&discount_id=" + discount
	}

	window.location = url
}