const API = './api/starter_page.php'

function showCriticalItems() {
	$.ajax({
		url: API + "?getCriticalItems",
		processData: false
	})
	.done(response => {
		let data = $.parseJSON(response)
		let renderTable = ""
		
		for(let values in data) {
			renderTable += `<tr><td style = "border: 1px solid #ddd !important;padding: 8px !important;">` + data[values].name + `</td><td style = "border: 1px solid #ddd !important;padding: 8px !important;">` + data[values].quantity + `</td></tr>`
		}

		$("#critical_items").html(renderTable)
		$("#responsive-modal-critical").show()
	})
}

function moveToMenu() {
	window.location = "?menu"
}

function hideCriticalItems() {
	$("#responsive-modal-critical").hide()
}

function loadLineChart() {
	//reset line chart
	$("#line-chart").empty();

	let year = $("#year").val()
	let month = $("#month").val()
	let branch = $("#branch").val()

	if(branch == null) {
		branch = ""
	}

	$.ajax({
		type: "GET",
		url: './api/dashboard.chart.php?year=' + year + '&month=' + month + '&branch=' + branch,
		data: jQuery.param({ _getLineChartForTransactions: "yes" }),
		processData: false
	})
		.done(response => {
			let json = $.parseJSON(response)

			let config = {
				data: json,
				xkey: 'm',
				ykeys: ['a', 'b'],
				labels: ['Total Income', 'Total Transactions'],
				xLabels: "month",
				parseTime: false,
				fillOpacity: 0.6,
				hideHover: 'auto',
				behaveLikeLine: true,
				resize: true,
				pointFillColors: ['#ffffff'],
				pointStrokeColors: ['black', 'blue'],
				lineColors: ['gray', 'red'],
				element: 'line-chart'
			};

			Morris.Line(config);
		})
}


$(() => {
	loadLineChart()
	getBranches()
	$.ajax({
		url: './api/auth.php?getLoggedUserDetails',
		processData: false
	})
		.done(data => {
			let json = $.parseJSON(data)

			checkRole(json.role)

			$.when(getBranchName(json.branch_code))
				.done(branch => {
					let jsonBranch = $.parseJSON(branch)

					if (jsonBranch.type == "success") {
						$("#branch_name").html(jsonBranch.text)
					}

					displayData()
				})
		})
		.fail(errorThrown => {
			console.log('Roles Get Error: ', errorThrown)
			return false
		})
})

function checkRole(role) {
	$.ajax({
		type: "GET",
		url: './api/access_levels.php',
		data: jQuery.param({ checkAccess: "yes", role: role }),
		processData: false
	})
		.done(response => {
			let json = $.parseJSON(response)

			if (json.access == "YES") {
				window.location.href = "./?access_cashier"
			}
		})
}

function getBranchName(branch_code) {
	return $.ajax({
		type: "GET",
		url: API + "?getBranch",
		data: jQuery.param({ getBranchName: "yes", branch_code: branch_code }),
		processData: false
	})
}

function displayData() {
	$.ajax({
		type: "GET",
		url: API,
		data: jQuery.param({ displayData: "yes" }),
		processData: false
	})
	.done(response => {
		let json = JSON.parse(response)

		isChanged(json)

		setTimeout(function () {
			displayData()
		}, 5000)
	})
}

function isChanged(data_array) {

	for (const key in data_array) {
		const existing_data = $("#counter_" + key).html()
		let peso = "₱" + numberWithCommas(data_array[key].toFixed(2))
		let counter = numberWithCommas(data_array[key])
	
		if(peso.localeCompare(existing_data) === 0 || counter.localeCompare(existing_data) === 0) {
			//console.log("Nothing changed...")
		}
		else {
			if(key == "transactions_today" || key == "void" || key == "critical_items" || key == "transactions") {
				$("#counter_" + key).html(numberWithCommas(data_array[key]))
			}
			else {
				if(key == "total_sales") {
					loadLineChart()
				}
				$("#counter_" + key).html("₱" + numberWithCommas(data_array[key].toFixed(2)))
			}
		}
	}
}

function getBranches() {
  $.ajax({
	  url: API + '?getBranches',
	  processData: false
  })
  .done(response => {
	  let json = JSON.parse(response)
	  let newVal = ""

	  if(json.is_tech == "0") {
		  newVal = `<option value="${json.branch_code}">${json.branch_name}</option>`
	  }
	  else {
		  newVal = '<option selected disabled>Select Branch</option>'
		  newVal += json.map(value => {
			  return `<option value="${value.branch_code}">${value.branch_name}</option>`
		  })
	  }

	  $(`#branch`).html(newVal)
  })
}


function numberWithCommas(x) {
    let parts = x.toString().split(".")
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    return parts.join(".")
}