$(document).ready(() => {
	$('form').submit(function(e) {
		let data = $(this).serializeArray()
		let params = postParams('login', data)

		$.ajax({
			url: './api/auth.php',
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				console.log('Login Response: ', data)
				responseJSON = JSON.parse(data)
				new PNotify(responseJSON)
				if (responseJSON.type == 'success') {
					window.location.href = './?'
				}
			})
			.fail(errorThrown => {
				console.log('Login POST Response: ', errorThrown.statusText)
				alert(errorThrown.statusText)
				return false
			})

		e.preventDefault()
	})
})
