const API = './api/categories.php'
const IMAGE_CATEGORY = './api/upload_image_category.php'

	let myTable = $('#datatable').DataTable({
		processing: true,
		ajax: {
			url: API + '?get',
			dataSrc: function(json) {
				let return_data = new Array()
				for (let i = 0; i < json.length; i++) {
					return_data.push({
						id: json[i].id,
						description: json[i].description,
						active_tag: json[i].active_tag,
						action : `<button class="btn-warning" onclick="modify(`+json[i].id+`)">MODIFY</button>`
					})
				}
				return return_data
			},

			complete: function() {
				$('#datatable tbody').on('dblclick', 'tr', function() {
					$('form').trigger('reset')
					$('input[type=checkbox]').prop('checked', false)

					let data = $('#datatable')
						.DataTable()
						.row(this)
						.data().id
					$(`input[name*="modifyId"]`).val(data)
					$(`input[name*="action"]`).val('edit')

					modify(data)
				})

				$('#addModal').on('click', function() {
					$('form').trigger('reset')
					$(`input[name*="modifyId"]`).val('')
					$(`input[name*="action"]`).val('add')
					$('#formModal').modal('show')
				})
			}
		},
		columns: [
			{ data: 'id' },
			{ data: 'description' },
			{ data: 'active_tag' },
			{ data: 'action' }
		],
    rowReorder: {
      selector: 'td:nth-child(2)'
    },
    responsive: true
	})

	$('form').on('submit', function(e) {
		e.preventDefault()

		$.blockUI({
			baseZ: 2000
		})

		let data = $('form').serializeArray()
		let params = postParams('', data)
		
		/*let test_data = {
		    "description": $("#description").val(),
		    "actve_tag": $("#active_tag").val(),
		    "test": $("#test").val()
		}*/

		$.ajax({
			url: API,
			type: 'post',
            //data: jQuery.param({ test_data: test_data }),
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				responseJSON = JSON.parse(data)

				new PNotify(responseJSON)

				if (responseJSON.type == 'success') {
					saveImage()
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	})

	//delete button
	$('#deleteButton').on('click', function() {
		if (!confirm('Are you sure you want to remove this user?')) {
			return false
		}

		$.blockUI({
			baseZ: 2000
		})

		$(`input[name*="action"]`).val('delete')

		let data = $('form').serializeArray()
		let params = postParams('', data)

		$.ajax({
			url: API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Delete Response: ', data)

				responseJSON = JSON.parse(data)
				new PNotify(responseJSON)
				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Delete Post Error: ', errorThrown)
				return false
			})

		return false
	})

	function modify(data){
  	$(`input[name*="modifyId"]`).val(data)
  	$(`input[name*="action"]`).val('edit')

  	$.ajax({
  		url: API + '?getDetails=' + data,
  		processData: false
  	})
  		.done(data => {
  			let json = JSON.parse(data)

  			$.unblockUI()
  			populateForm($('form'), json)
  			$('#formModal').modal('show')
  		})
  		.fail(errorThrown => {
  			$.unblockUI()
  			console.log('Account Details Get Error', errorThrown)
  			return false
  		})
  }

	
function saveImage() {
	let formData = new FormData();
	formData.append('image_file', $('#image_file')[0].files[0]);
	
	$.ajax({
		url: IMAGE_CATEGORY,
		type: 'post',
		data: formData,
		cache: false,
		contentType: false,
		processData: false
	})
	.done(data => {
		response = JSON.parse(data)
	})
	.fail(errorThrown => {
		console.log('Save Changes Post Error: ', errorThrown)
		return false
	})

}
