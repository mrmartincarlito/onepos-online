const API = './api/accounts.php'

$(document).ready(function() {
	let myTable = $('#datatable').DataTable({
		processing: true,
		ajax: {
			url: API + '?get',
			dataSrc: function(json) {
				let return_data = new Array()
				for (let i = 0; i < json.length; i++) {
					return_data.push({
						id: json[i].id,
						complete_name: json[i].name,
						username: json[i].username,
						role: json[i].description,
						date: moment(json[i].date).format('LLL'),
						action : `<button class="btn-warning" onclick="modify(`+json[i].id+`)">MODIFY</button>`
					})
				}
				return return_data
			},

			complete: function() {
				$('#datatable tbody').on('dblclick', 'tr', function() {
					$('form').trigger('reset')
					$('input[type=checkbox]').prop('checked', false)

					let data = $('#datatable')
						.DataTable()
						.row(this)
						.data().id
					$(`input[name*="modifyId"]`).val(data)
					$(`input[name*="action"]`).val('edit')

					modify(data)
				})

				$('#addModal').on('click', function() {
					$('form').trigger('reset')
					$(`input[name*="modifyId"]`).val('')
					$(`input[name*="action"]`).val('add')
					$(`input[name*="password"]`).val()
					$(`input[name*="confirm_password"]`).val()
					$(`input[name*="password"]`).attr("required", "true")
					$(`input[name*="confirm_password"]`).attr("required", "true")
					$("#password").show()
					$("#confirm").show()
					$('#formModal').modal('show')
				})
			}
		},
		columns: [
			{ data: 'id' },
			{ data: 'complete_name' },
			{ data: 'username' },
			{ data: 'role' },
			{ data: 'date' },
			{ data: 'action' }
		],
		order: [[0, 'desc'], [1, 'asc'], [3, 'asc']],
    rowReorder: {
      selector: 'td:nth-child(2)'
    },
    responsive: true
	})

	$('form').on('submit', function(e) {
		e.preventDefault()

		$.blockUI({
			baseZ: 2000
		})

		let data = $('form').serializeArray()
		let params = postParams('', data)

		$.ajax({
			url: API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				responseJSON = JSON.parse(data)

				new PNotify(responseJSON)

				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
					$("#formModal").modal('reset')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	})

	//delete button
	$('#deleteButton').on('click', function() {
		if (!confirm('Are you sure you want to remove this user?')) {
			return false
		}

		$.blockUI({
			baseZ: 2000
		})

		$(`input[name*="action"]`).val('delete')

		let data = $('form').serializeArray()
		let params = postParams('', data)

		$.ajax({
			url: API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Delete Response: ', data)

				responseJSON = JSON.parse(data)
				new PNotify(responseJSON)
				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Delete Post Error: ', errorThrown)
				return false
			})

		return false
	})

	//select role combobox
	$.ajax({
		url: './api/access_levels.php?get',
		processData: false
	})
		.done(data => {
			let json = JSON.parse(data)
			let newVal = '<option selected disabled>Select role</option>'

			newVal += json.map(value => {
				return `<option value="${value.id}">${value.description}</option>`
			})

			$(`#role`).html(newVal)
		})
		.fail(errorThrown => {
			console.log('Roles Get Error: ', errorThrown)
			return false
		})

		getBranches()
})

function modify(data){
  	$(`input[name*="modifyId"]`).val(data)
  	$(`input[name*="action"]`).val('edit')

  	$.ajax({
  		url: API + '?getDetails=' + data,
  		processData: false
  	})
  		.done(data => {
  			let json = JSON.parse(data)

  			$.unblockUI()
        populateForm($('form'), json)
        $(`input[name*="action"]`).val('edit')
        $('#formModal').modal('show')
        /*if(json[0] == "hide") {
          $("#password").hide()
          $("#confirm").hide()
          $(`input[name*="password"]`).removeAttr("required")
          $(`input[name*="confirm_password"]`).removeAttr("required")
          $(`input[name*="isAccount"]`).val('hide')
        }
        else {
          $("#password").show()
          $("#confirm").show()
          $(`input[name*="password"]`).attr("required", "true")
          $(`input[name*="confirm_password"]`).attr("required", "true")
          $(`input[name*="isAccount"]`).val('show')
        }*/
  		})
  		.fail(errorThrown => {
  			$.unblockUI()
  			console.log('Account Details Get Error', errorThrown)
  			return false
  		})
  }

  function getBranches() {
	$.ajax({
		url: API + '?getBranches',
		processData: false
	})
	.done(response => {
		let json = JSON.parse(response)
		let newVal = ""

		if(json.is_tech == "0") {
			newVal = `<option value="${json.branch_code}">${json.branch_name}</option>`
		}
		else {
			newVal = '<option selected disabled>Select Branch</option>'
			newVal += json.map(value => {
				return `<option value="${value.branch_code}">${value.branch_name}</option>`
			})
		}

		$(`#branch`).html(newVal)
	})
  }