
  const API = './api/product_groups.php'
  let myTable = $('#datatable').DataTable({
	  processing: true,
	  ajax: {
		  url: API + '?get',
		  dataSrc: function(json) {
			  let return_data = new Array()
			  for (let i = 0; i < json.length; i++) {
				  return_data.push({
					  id: json[i].id,
					  description: json[i].description,
					  printer_name: json[i].printer_name,
					  printer_port: json[i].printer_port,
					   action : `<button class="btn-warning" onclick="modify(`+json[i].id+`)">MODIFY</button>`
				  })
			  }
			  return return_data
		  },

		  complete: function() {
			  $('#datatable tbody').on('dblclick', 'tr', function() {
				  $('form').trigger('reset')
				  $('input[type=checkbox]').prop('checked', false)

				  let data = $('#datatable')
					  .DataTable()
					  .row(this)
					  .data().id
				  $(`input[name*="modifyId"]`).val(data)
				  $(`input[name*="action"]`).val('edit')

				  modify(data)
			  })

			  $('#addModal').on('click', function() {
				  $('form').trigger('reset')
				  $(`input[name*="modifyId"]`).val('')
				  $(`input[name*="action"]`).val('add')
				  $('#formModal').modal('show')
			  })
		  }
	  },
	  columns: [
		  { data: 'id' },
		  { data: 'description' },
		  { data: 'printer_name' },
		  { data: 'printer_port' },
		  { data: 'action' }
	  ],
    rowReorder: {
      selector: 'td:nth-child(2)'
    },
    responsive: true
  })

  $('form').on('submit', function(e) {
	  e.preventDefault()

	  $.blockUI({
		  baseZ: 2000
	  })

	  let data = $('form').serializeArray()
	  let params = postParams('', data)

	  $.ajax({
		  url: API,
		  type: 'post',
		  data: 'data=' + params,
		  processData: false
	  })
		  .done(data => {
			  $.unblockUI()
			  console.log('Save Changes Response: ', data)

			  responseJSON = JSON.parse(data)

			  new PNotify(responseJSON)

			  if (responseJSON.type == 'success') {
				  myTable.ajax.reload(null, false)
				  $('#formModal').modal('hide')
			  }
		  })
		  .fail(errorThrown => {
			  $.unblockUI()
			  console.log('Save Changes Post Error: ', errorThrown)
			  return false
		  })
  })

  //delete button
  $('#deleteButton').on('click', function() {
	  if (!confirm('Are you sure you want to remove this user?')) {
		  return false
	  }

	  $.blockUI({
		  baseZ: 2000
	  })

	  $(`input[name*="action"]`).val('delete')

	  let data = $('form').serializeArray()
	  let params = postParams('', data)

	  $.ajax({
		  url: API,
		  type: 'post',
		  data: 'data=' + params,
		  processData: false
	  })
		  .done(data => {
			  $.unblockUI()
			  console.log('Delete Response: ', data)

			  responseJSON = JSON.parse(data)
			  new PNotify(responseJSON)
			  if (responseJSON.type == 'success') {
				  myTable.ajax.reload(null, false)
				  $('#formModal').modal('hide')
			  }
		  })
		  .fail(errorThrown => {
			  $.unblockUI()
			  console.log('Delete Post Error: ', errorThrown)
			  return false
		  })

	  return false
  })

  function modify(data){
    $(`input[name*="modifyId"]`).val(data)
    $(`input[name*="action"]`).val('edit')

    $.ajax({
      url: API + '?getDetails=' + data,
      processData: false
    })
      .done(data => {
        let json = JSON.parse(data)

        $.unblockUI()
        populateForm($('form'), json)
        $('#formModal').modal('show')
      })
      .fail(errorThrown => {
        $.unblockUI()
        console.log('Account Details Get Error', errorThrown)
        return false
      })
  }

