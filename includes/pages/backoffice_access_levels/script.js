const API = './api/access_levels.php'

$(document).ready(function() {
	let myTable = $('#datatable').DataTable({
		processing: true,
		ajax: {
			url: API + '?get=NO',
			dataSrc: function(json) {
				$('#accesslevel_checkboxes').html(
					Object.keys(json[0]).map(value => {
						if (value.includes('access') || value.includes('cashier')) {
							let access_text = value
								.substring(value.indexOf('_') + 1)
								.replace('_', ' ')
								.toUpperCase()

							return `
								<div class="checkbox checkbox-success checkbox-circle">
									<input id="${value}" type="checkbox" value="1" name="${value}">
									<label for="${value}"> ${access_text} </label>
								</div>
							`
						}
					})
				)

				let return_data = new Array()
				for (let i = 0; i < json.length; i++) {
					return_data.push({
						id: json[i].id,
						description: json[i].description,
						is_frontoffice: json[i].is_frontoffice,
						action : `<button class="btn-warning" onclick="modify(`+json[i].id+`)">MODIFY</button>`
					})
				}
				return return_data
			},

			complete: function() {
				$('#datatable tbody').on('dblclick', 'tr', function() {
					$('form').trigger('reset')

					let data = $('#datatable')
						.DataTable()
						.row(this)
						.data().id
					$(`input[name*="modifyId"]`).val(data)
					$(`input[name*="action"]`).val('edit')

					modify(data)
				})

				$('#addModal').on('click', function() {
					$('form').trigger('reset')
					$('input[type=checkbox]').prop('checked', false)
					$(`input[name*="modifyId"]`).val('')
					$(`input[name*="action"]`).val('add')
					$('#formModal').modal('show')
				})
			}
		},
		columns: [{ data: 'id' }, { data: 'description' }, { data: 'is_frontoffice' }, { data: 'action'}],
		order: [[0, 'desc']],
   rowReorder: {
        selector: 'td:nth-child(2)'
      },
      responsive: true
	})

	$('form').on('submit', function(e) {
		e.preventDefault()

		$.blockUI({
			baseZ: 2000
		})

		let data = $(this).serializeArray()
		let params = postParams('', data)

		$.ajax({
			url: API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				responseJSON = JSON.parse(data)

				new PNotify(responseJSON)

				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	})

	//delete button
	$('#deleteButton').on('click', function() {
		if (!confirm('Are you sure you want to remove this user?')) {
			return false
		}

		$.blockUI({
			baseZ: 2000
		})

		$(`input[name*="action"]`).val('delete')

		let data = $('form').serializeArray()
		let params = postParams('', data)

		$.ajax({
			url: API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Delete Response: ', data)

				responseJSON = JSON.parse(data)
				new PNotify(responseJSON)
				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Delete Post Error: ', errorThrown)
				return false
			})

		return false
	})
})

function modify(data){
  	$(`input[name*="modifyId"]`).val(data)
  	$(`input[name*="action"]`).val('edit')

  	$.ajax({
  		url: API + '?getDetails=' + data,
  		processData: false
  	})
  		.done(data => {
  			let json = JSON.parse(data)

  			$.unblockUI()
  			populateForm($('form'), json)
  			$('#formModal').modal('show')
  		})
  		.fail(errorThrown => {
  			$.unblockUI()
  			console.log('Account Details Get Error', errorThrown)
  			return false
  		})
  }
