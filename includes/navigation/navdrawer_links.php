<?php
    $links = array( 
        array( 
            "name" => "Dashboard", 
            "data-icon" => "ti-dashboard", 
            "link" => "./?starter_page", 
            "child_items" => array(
            ) 
        ),
        array( 
            "name" => "Access Cashier", 
            "data-icon" => "ti-tablet", 
            "link" => "./?access_cashier", 
            "child_items" => array(
            ) 
        ),
        array( 
            "name" => "Cashier Settings", 
            "data-icon" => "ti-harddrives", 
            "link" => "javascript:void(0)", 
            "child_items" => array(
                array( 
                    "name" => "Categories", 
                    "data-icon" => "ti-clipboard", 
                    "link" => "./?categories", 
                ),
                array( 
                    "name" => "Product Groups", 
                    "data-icon" => "ti-clipboard", 
                    "link" => "./?product_groups", 
                ),
                array( 
                    "name" => "Menu", 
                    "data-icon" => "ti-book", 
                    "link" => "./?menu", 
                ), 
                array( 
                    "name" => "Tender Types", 
                    "data-icon" => "ti-wallet", 
                    "link" => "./?tender_types", 
                ), 
                array( 
                    "name" => "Denominations", 
                    "data-icon" => "ti-credit-card", 
                    "link" => "./?denominations", 
                ), 
                array( 
                    "name" => "Discounts", 
                    "data-icon" => "ti-shortcode", 
                    "link" => "./?discounts", 
                ), 
                array( 
                    "name" => "Special Request", 
                    "data-icon" => "ti-pin-alt", 
                    "link" => "./?special_request", 
                ), 
                array( 
                    "name" => "Supplier", 
                    "data-icon" => "ti-truck", 
                    "link" => "./?supplier", 
                ), 
            ) 
        ),
        array( 
            "name" => "User Maintenance", 
            "data-icon" => "ti-harddrives", 
            "link" => "javascript:void(0)", 
            "child_items" => array(
                array( 
                    "name" => "Accounts", 
                    "data-icon" => "ti-user", 
                    "link" => "./?accounts", 
                ),
                array( 
                    "name" => "Backoffice Access Levels", 
                    "data-icon" => "ti-direction-alt", 
                    "link" => "./?backoffice_access_levels", 
                ),
                array( 
                    "name" => "Frontoffice Access Levels", 
                    "data-icon" => "ti-direction-alt", 
                    "link" => "./?frontoffice_access_levels", 
                ),
            ) 
        ),
        array( 
            "name" => "Reports", 
            "data-icon" => "ti-bar-chart", 
            "link" => "./?reports",
            "child_items" => array(
            )
        ),
        array( 
            "name" => "Audit Logs", 
            "data-icon" => "ti-save", 
            "link" => "./?logs",
            "child_items" => array(
            )
        )
    ); 

    //do not meddle with the code below

    $links_HTML = "";
    foreach ($links as $link) {
        $isactive = '';
        if($page_name == strtolower($link['name'])){
            $isactive = 'active';
        }

        $child_links = '';
        $arrow_extra = '';
        if(count($link['child_items']) > 0){
            $child_links = '<ul class="nav nav-second-level">';
            $arrow_extra = '<span class="fa arrow"></span>';
            foreach ($link['child_items'] as $child_item) {
                $child_links .= '
                <li>
                    <a href="'.$child_item['link'].'">
                        <i class="'.$child_item['data-icon'].'"></i> <span class="hide-menu">'.$child_item['name'].'</span>
                    </a>
                </li>
                ';
            }
            $child_links .= '</ul>';
        }
            $links_HTML .= '
            <li>
                <a href="'.$link['link'].'" class="waves-effect '.$isactive.'">
                    <i class="'.$link['data-icon'].'"></i>
                    <span class="hide-menu">'.$link['name'].'</span>
                    '.$arrow_extra.'
                </a> 
                    '.$child_links.'
            </li>
        ';
        
    }
?>
