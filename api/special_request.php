<?php
require_once "config.php";
require_once "logs.php";

define("TABLE_NAME", "special_request");

if (isset($_POST["data"])) {
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if ($data->action == "add") {
        $insertData = array(
            "description" => $data->description,
        );

        $id = $database->insert(TABLE_NAME, $insertData);
        if ($id) {
            echo json_encode(array(
                "type" => "success",
                "title" => "Successful!",
                "text" => "Special Request added successfully!",
            ));
        } else {
            echo json_encode(array(
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError(),
            ));
        }
    }

    if ($data->action == "edit") {

        $updateData = array(
            "description" => $data->description
        );

        $database->where('id', $data->modifyId);
        $id = $database->update(TABLE_NAME, $updateData);
        if ($id) {
            echo json_encode(array(
                "type" => "success",
                "title" => "Successful!",
                "text" => "Special Request details modified successfully!",
            ));
        } else {
            echo json_encode(array(
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError(),
            ));
        }
    }

    if ($data->action == "delete") {

        $database->where('id', $data->modifyId);
        $id = $database->delete(TABLE_NAME);
        if ($id) {
            echo json_encode(array(
                "type" => "success",
                "title" => "Successful!",
                "text" => "Special deleted succesfully!",
            ));
        } else {
            echo json_encode(array(
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError(),
            ));
        }
    }

    if ($data->action == "add") {
        saveLog($database, "{$data->action} Special Request: {$data->description}");
    } else {
        saveLog($database, "{$data->action} Special Request ID {$data->modifyId}");
    }

}

//GET METHODS
if (isset($_GET["get"])) {
    $special_request = $database->get(TABLE_NAME);
    echo json_encode($special_request);
}

if (isset($_GET["getDetails"])) {
    $id = $_GET["getDetails"];

    $database->where("id", $id);
    $special_request = $database->getOne(TABLE_NAME);
    echo json_encode($special_request);
}
