<?php
require_once("config.php");

define("TABLE_NAME", "categories");

$category_id = $_SESSION['image-category-id'];

if(isset($_FILES["image_file"]["name"])) {
    $path = "uploads/".$_FILES['image_file']['name'];
    
    if(move_uploaded_file($_FILES['image_file']['tmp_name'], $path)){
    
        $database->where("id", $category_id);
        $isUpdated = $database->update(TABLE_NAME, $updateArray = Array(
            "image_file" => $path
        ));
    
        if($isUpdated){
            $response = json_encode(Array (
                "type" => "success",
                "title" => "Success",
                "text" => "Successfully inserted image"
            ));
        }else{
            $response = json_encode(Array (
                "type" => "error",
                "title" => "Error",
                "text" => "Error while inserting ".$database->getLastError()
            ));
        }
    
    }else{
        $response = json_encode(Array (
            "type" => "error",
            "title" => "Error",
            "text" => "Uploading error!"
        ));
    }
    
}
else {
    $response = json_encode(Array (
        "type" => "error",
        "title" => "Error",
        "text" => "No picture!"
    ));
}
echo $response;
