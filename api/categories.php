<?php
require_once("config.php");
require_once("logs.php");

define("TABLE_NAME", "categories");

if(isset($_POST["data"])){

    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->action == "add"){
        $id = microtime(true);
        $insertData = Array (
            "id" => $id,
            "description" => $data->description,
            "active_tag" => $data->active_tag
        );  

        $id = $database->insert (TABLE_NAME, $insertData);
        if($id){
            $_SESSION['image-category-id'] = $id;
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Category added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->action == "edit"){

        $updateData = Array (
           "description" => $data->description,
            "active_tag" => $data->active_tag
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (TABLE_NAME, $updateData);
        if($id){
            $_SESSION['image-category-id'] = $data->modifyId;
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Category details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->action == "delete"){

        $database->where ('id', $data->modifyId);
        $id = $database->delete (TABLE_NAME);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Category deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->action == "add"){
        saveLog($database,"{$data->action} CATEGORY: {$data->description}");
    }else{
        saveLog($database,"{$data->action} CATEGORY ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $categories = $database->get(TABLE_NAME);
    echo json_encode($categories);
}

if(isset($_GET["getCategories"])){
    $database->orderby("description", "asc");
    $database->where ('active_tag', "YES");
    $categories = $database->get(TABLE_NAME);
    echo json_encode($categories);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $categories = $database->getOne(TABLE_NAME);
    echo json_encode($categories);
}