<?php
require_once("config.php");
require_once("logs.php");

define("TABLE_NAME", "supplier");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->action == "add"){
        $insertData = Array (
            "supplier_name" => $data->supplier_name,
            "supplier_address" => $data->supplier_address,
            "active_tag" => $data->active_tag
        );  

        $id = $database->insert (TABLE_NAME, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Supplier added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->action == "edit"){

        $updateData = Array (
            "supplier_name" => $data->supplier_name,
            "supplier_address" => $data->supplier_address,
            "active_tag" => $data->active_tag
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (TABLE_NAME, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Supplier details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->action == "delete"){

        $database->where ('id', $data->modifyId);
        $id = $database->delete (TABLE_NAME);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Supplier deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->action == "add"){
        saveLog($database,"{$data->action} SUPPLIER NAME: {$data->supplier_name}");
    }else{
        saveLog($database,"{$data->action} SUPPLIER ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $supplier = $database->get(TABLE_NAME);
    echo json_encode($supplier);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $supplier = $database->getOne(TABLE_NAME);
    echo json_encode($supplier);
}