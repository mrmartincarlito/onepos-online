<?php
require_once("config.php");
require_once("logs.php");

$branches = json_decode(file_get_contents("branches.json"), true);

if(isset($_GET["getBranchName"])) {
    $branch_code = $_GET["branch_code"];

    foreach($branches as $branch) {
        if($branch["branch_code"] == $branch_code) {
            $data = $branch["branch_name"];
            break;
        }
    }
    
    if(isset($data)) {
        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => $data
        ));
    }
    else {
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }
}

if(isset($_GET["displayData"])) {
    $log_date = getCurrentLogDate($database);
    $branch_code = getBranchCode($database);

    $transactions_today = getTransactionToday($database, $branch_code, $log_date["id"]);
    $total_sales_today = computeTotalSalesToday($database, $branch_code, $log_date["id"]);
    $transactions = getTransaction($database, $branch_code);
    $critical_items = getCriticalItems($database, $branch_code);
    $void = getVoidTransactions($database, $log_date["id"], $branch_code);
    $total_sales = computeTotalSales($database, $branch_code);
    $inventory_cost = getInventoryCost($database, $branch_code)["total_cost"];
    $total_accumulated_sales = computeAccumulatedSales($database, $branch_code);

    echo json_encode(Array (
        "transactions_today" => $transactions_today,
        "total_sales_today" => $total_sales_today,
        "transactions" => $transactions,
        "critical_items" => $critical_items,
        "void" => $void,
        "total_sales" => $total_sales,
        "inventory_cost" => $inventory_cost,
        "total_accumulated_sales" => $total_accumulated_sales
    ));
}

if(isset($_GET["getCriticalItems"])) {
    $branch_code = getBranchCode($database);
    
    $database->where("branch_code", $branch_code);
    $database->where("quantity", 5, "<=");
    $menu = $database->get("menu");

    echo json_encode($menu);
}

if(isset($_GET["getBranches"])){
    $branch_code = getBranchCode($database);
    $ctr = 0;

    if($branch_code != "1teq") {
        while($ctr < count($branches) - 1) {
            if($branches[$ctr]["branch_code"] == $branch_code) {
                $branch_details = Array(
                    "branch_name" => $branches[$ctr]["branch_name"],
                    "branch_code" => $branch_code,
                    "is_tech" => 0
                );
            }
            $ctr++;
        }
    }
    else {
        $branch_details = $branches;
    }
    echo json_encode($branch_details);
}

function getTransaction($database, $branch_code) {
    $orders = $database->rawQuery("select * from `orders` where `branch_code` = '$branch_code' and `is_settled` = '1' and YEAR(date_time) = YEAR(NOW()) and MONTH(date_time) = MONTH(NOW())");

    return count($orders);
}

function getTransactionToday($database, $branch_code, $log_date) {
    $orders = $database->rawQuery("select * from `orders` where `branch_code` = '$branch_code' and  `is_settled` = '1' and `log_date_id` = '$log_date'");

    return count($orders);
}

function getCriticalItems($database, $branch_code) {
    $database->where("branch_code", $branch_code);
    $database->where("quantity", 5, "<=");
    $menu = $database->get("menu");

    return count($menu);
}

function getVoidTransactions($database, $log_date, $branch_code) {
    $void = $database->rawQuery("select * from `orders` where `branch_code` = '$branch_code' 
    and `is_voided` = '1' and `log_date_id` = '$log_date'");

    return count($void);
}

function getCurrentLogDate($database) {
    $branch_code = getBranchCode($database);

    $database->where("branch_code", $branch_code);
    $database->orderby ("id");
    $log_date = $database->getOne("log_date");

    return $log_date;
} 

function computeTotalSales($database, $branch_code) {
    $total = $database->rawQuery("Select sum(amount_payable) as total_amount from `orders` where 
    `is_settled` = '1' 
    and `branch_code` = '$branch_code' 
    and YEAR(date_time) = YEAR(NOW())
    and MONTH(date_time) = MONTH(NOW())");
    
    return floatval($total[0]["total_amount"]);
}

function computeTotalSalesToday($database, $branch_code, $log_date) {
    $total = $database->rawQuery("Select sum(amount_payable) as total_amount from `orders` where `is_settled` = '1' and `branch_code` = '$branch_code' and `log_date_id` = '$log_date'");
    
    return floatval($total[0]["total_amount"]);
}

function getInventoryCost($database, $branch_code) {
    $database->where("branch_code", $branch_code);
    $menu = $database->get("menu");
    $total = 0;
    $total_price = 0;
    $counter = 0;

    foreach($menu as $items) {
        $total += $items["quantity"] * $items["inventory_cost"];
        $total_price += $items["quantity"] * $items["cost"];
    }

    return array(
        "total_cost" => $total,
        "total_price" => $total_price
    );
}

function computeAccumulatedSales($database, $branch_code) {
    $total = $database->rawQuery("Select sum(amount_payable) as total_amount from `orders` where `is_settled` = '1' and `branch_code` = '$branch_code'");
    
    return floatval($total[0]["total_amount"]);
}
?>