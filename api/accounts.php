<?php
require_once("config.php");
require_once("logs.php");

$branches = json_decode(file_get_contents("branches.json"), true);

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->action == "add"){
        if($data->password != $data->confirm_password){
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => "Passwords must match!"
            ));
            exit;
        }

        $insertData = Array (
            "name" => $data->name,
            "username" => $data->username,
            "password" => password_hash($data->password,PASSWORD_DEFAULT),
            "role" => $data->role,
            "active" => 1,
            "branch_code" => $data->branch
        );

        $id = $database->insert ('accounts', $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Account added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->action == "edit"){
        //if($data->isAccount == "show") {
            if($data->password != $data->confirm_password){
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => "Passwords must match!"
                ));
                exit;
            }
    
            $updateData = Array (
                "name" => $data->name,
                "username" => $data->username,
                "password" => password_hash($data->password,PASSWORD_DEFAULT),
                "role" => $data->role,
                "active" => 1,
                "branch_code" => $data->branch
            );
    
            $database->where ('id', $data->modifyId);
            $id = $database->update ('accounts', $updateData);
            if($id){
                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "text" => "Account details modified successfully!"
                ));
            }else{
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }
        //}
        /*else {
    
            $updateData = Array (
                "name" => $data->name,
                "username" => $data->username,
                "role" => $data->role,
                "active" => 1
            );
    
            $database->where ('id', $data->modifyId);
            $id = $database->update ('accounts', $updateData);
            if($id){
                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "text" => "Account details modified successfully!"
                ));
            }else{
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }
        }*/
    }

    if($data->action == "delete"){
        $updateData = Array (
            "active" => 0
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update ('accounts', $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Account deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->action == "add"){
        saveLog($database,"{$data->action} ACCOUNT: {$data->name}");
    }else{
        saveLog($database,"{$data->action} ACCOUNT ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $branch_code = getBranchCode($database);
    $database->where ('s.active', 1);
    if($branch_code != "1teq") {
        $database->where ('s.branch_code', $branch_code);
    }
    $database->join ("access_levels a", "s.role=a.id", "LEFT");
    $userDB = $database->get("accounts s", null, "s.*, a.description");
    echo json_encode($userDB);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];
    //$username = @$_SESSION["username"];

    $cols = Array ("id", "name", "username", "role");
    $database->where ("id", $id);
    $userDB = $database->getOne("accounts", $cols);

    /*if($username == $userDB["username"]) {
        array_push($userDB, "show");
    }
    else {
        array_push($userDB, "hide");
    }*/
    echo json_encode($userDB);
}

if(isset($_GET["getBranches"])){
    $branch_code = getBranchCode($database);
    $ctr = 0;

    if($branch_code != "1teq") {
        while($ctr < count($branches) - 1) {
            if($branches[$ctr]["branch_code"] == $branch_code) {
                $branch_details = Array(
                    "branch_name" => $branches[$ctr]["branch_name"],
                    "branch_code" => $branch_code,
                    "is_tech" => 0
                );
            }
            $ctr++;
        }
    }
    else {
        $branch_details = $branches;
    }
    echo json_encode($branch_details);
}

if(isset($_GET['getAccountsByBranch'])){
    $branch_code = $_GET['getAccountsByBranch'];

    if($branch_code != "ALLBRANCH"){
        $database->where("branch_code", $branch_code);
    }

    echo json_encode($database->get("accounts"));
}
?>