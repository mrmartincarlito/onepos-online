<?php
require_once("config.php");
require_once("logs.php");

define("TABLE_NAME", "discounts");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);
    $flag = false;

    if(checkValue($data->equivalent_value)) {
        $flag = true;
    }

    if($flag) {
        if($data->action == "add"){
            $insertData = Array (
                "description" => $data->description,
                "equivalent_value" => $data->equivalent_value,
                "type" => $data->discount_type
            );  
    
            $id = $database->insert (TABLE_NAME, $insertData);
            if($id){
                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "text" => "Discount added successfully!"
                ));
            }else{
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }
        }
    
        if($data->action == "edit"){
    
            $updateData = Array (
               "description" => $data->description,
                "equivalent_value" => $data->equivalent_value,
                "type" => $data->discount_type
            );
    
            $database->where ('id', $data->modifyId);
            $id = $database->update (TABLE_NAME, $updateData);
            if($id){
                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "text" => "Discount details modified successfully!"
                ));
            }else{
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }
        }
    
        if($data->action == "delete"){
    
            $database->where ('id', $data->modifyId);
            $id = $database->delete (TABLE_NAME);
            if($id){
                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "text" => "Discount deleted succesfully!"
                ));
            }else{
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }
        }
    
        if($data->action == "add"){
            saveLog($database,"{$data->action} DISCOUNT: {$data->description}");
        }else{
            saveLog($database,"{$data->action} DISCOUNT ID {$data->modifyId}");
        }
    }
    else {
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "Value should not be negative!"
        ));
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $discounts = $database->get(TABLE_NAME);
    echo json_encode($discounts);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $discounts = $database->getOne(TABLE_NAME);
    echo json_encode($discounts);
}

function checkValue($value) {
    if($value < 0) {
        return false;
    }
    else {
        return true;
    }
}