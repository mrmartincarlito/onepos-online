<?php
require_once("config.php");
require_once("logs.php");

define("TABLE_NAME", "product_groups");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->action == "add"){
        $insertData = Array (
            "description" => $data->description,
            "printer_name" => $data->printer_name,
            "printer_port" => $data->printer_port
        );  

        $id = $database->insert (TABLE_NAME, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Product Group added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->action == "edit"){

        $updateData = Array (
           "description" => $data->description,
           "printer_name" => $data->printer_name,
           "printer_port" => $data->printer_port
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (TABLE_NAME, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Product Group details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->action == "delete"){

        $database->where ('id', $data->modifyId);
        $id = $database->delete (TABLE_NAME);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Product Group deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->action == "add"){
        saveLog($database,"{$data->action} PRODUCT GROUP: {$data->description}");
    }else{
        saveLog($database,"{$data->action} PRODUCT GROUP ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $categories = $database->get(TABLE_NAME);
    echo json_encode($categories);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $categories = $database->getOne(TABLE_NAME);
    echo json_encode($categories);
}