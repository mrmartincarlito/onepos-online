<?php
require_once("config.php");
require_once("logs.php");

$branches = json_decode(file_get_contents("branches.json"), true);

define("TABLE_NAME", "menu");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);
    $flag = false;

    if(checkValue($data->cost)) {
        $flag = true;
    }

    if($flag) {
        if($data->action == "add"){
            $id = microtime(true);
            $insertData = Array (
                "id" => $id,
                "short_descp" => $data->short_descp,
                "long_descp" => $data->long_descp,
                "category_id" => $data->category_id,
                "product_group_id" => $data->product_group_id,
                "code" => $data->code,
                "name" => $data->name,
                "inventory_cost" => $data->inventory_cost,
                "cost" => $data->cost,
                "active_tag" => $data->active_tag,
                "print_label_sticker_tag" => isset($data->print_label_sticker_tag) ? $data->print_label_sticker_tag : "",
                "vatable_tag" => isset($data->vatable_tag) ? $data->vatable_tag : "",
                "slip_tag" => isset($data->slip_tag) ? $data->slip_tag : "",
                "branch_code" => $data->branch,
                "added_by" => $_SESSION["username"],
                "quantity" => isset($data->quantity) ? $data->quantity : 0
            );  
    
            $id = $database->insert (TABLE_NAME, $insertData);
            if($id){
                $_SESSION['image-menu-id'] = $id;
                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "text" => "Menu added successfully!"
                ));
            }else{
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }
        }
    
        if($data->action == "edit"){
    
            $updateData = Array (
                "short_descp" => $data->short_descp,
                "long_descp" => $data->long_descp,
                "category_id" => $data->category_id,
                "product_group_id" => $data->product_group_id,
                "code" => $data->code,
                "name" => $data->name,
                "inventory_cost" => $data->inventory_cost,
                "cost" => $data->cost,
                "active_tag" => $data->active_tag,
                "print_label_sticker_tag" => isset($data->print_label_sticker_tag) ? $data->print_label_sticker_tag : "",
                "vatable_tag" => isset($data->vatable_tag) ? $data->vatable_tag : "",
                "slip_tag" => isset($data->slip_tag) ? $data->slip_tag : "",
                "branch_code" => $data->branch,
                "added_by" => $_SESSION["username"]
            );
    
            $database->where ('id', $data->modifyId);
            $id = $database->update (TABLE_NAME, $updateData);
            if($id){
                $_SESSION['image-menu-id'] = $data->modifyId;
                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "text" => "Menu details modified successfully!"
                ));
            }else{
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }
        }
    
        if($data->action == "delete"){

            $updateData = Array (
                "active_tag" => "NO",
            );
    
            $database->where ('id', $data->modifyId);
            $id = $database->update (TABLE_NAME, $updateData);
            if($id){
                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "text" => "Menu deleted succesfully!"
                ));
            }else{
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }
        }
    
        if($data->action == "add"){
            saveLog($database,"{$data->action} MENU: {$data->short_descp}");
        }else{
            saveLog($database,"{$data->action} MENU ID {$data->modifyId}");
        }
    }
    else {
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "Cost should not be negative!"
        ));
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $branch_code = getBranchCode($database);
    if($branch_code != "1teq") {
        $database->where ('branch_code', $branch_code);
    }
    $database->where ("active_tag", "YES");
    $menu = $database->get(TABLE_NAME);
    echo json_encode($menu);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $menu = $database->getOne(TABLE_NAME);
    echo json_encode($menu);
}

if(isset($_GET["getLast"])){
    $database->orderby("id", "desc");
    $menu = $database->getOne(TABLE_NAME);
    $response['menu_code'] = isset($menu["code"]) ? sprintf("%06d", $menu["code"] + 1): "000001";
    
    echo json_encode($response);
}

if(isset($_GET["getProducts"])){
    $category = $_GET["category"];
    
    $database->where("username", $_SESSION["username"]);
    $accounts = $database->getOne("accounts");

    $database->orderby("name", "asc");
    $database->where ("branch_code", $accounts["branch_code"]);
    $database->where ("category_id", $category);
    $database->where ("active_tag", "YES");
    $menu = $database->get(TABLE_NAME);

    echo json_encode($menu);
}

if(isset($_GET["getBranches"])){
    $branch_code = getBranchCode($database);
    $ctr = 0;

    if($branch_code != "1teq") {
        while($ctr < count($branches) - 1) {
            if($branches[$ctr]["branch_code"] == $branch_code) {
                $branch_details = Array(
                    "branch_name" => $branches[$ctr]["branch_name"],
                    "branch_code" => $branch_code,
                    "is_tech" => 0
                );
            }
            $ctr++;
        }
    }
    else {
        $branch_details = $branches;
    }
    echo json_encode($branch_details);
}

function checkValue($value) {
    if($value < 0) {
        return false;
    }
    else {
        return true;
    }
}