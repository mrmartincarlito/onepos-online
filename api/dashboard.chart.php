<?php
require_once("config.php");
require_once("logs.php");

define("ORDERS", "orders");
define("ORDERED_ITEMS", "ordered_items");
define("MENU", "menu");

if(isset($_GET['_getLineChartForTransactions'])){

    $months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');

    $groupByDefault = "MONTH(date_time)";

    $year = "YEAR(NOW())";
    if(!empty($_GET['year'])){
        $year = $_GET['year'];
    }

    $response = array();
    $days = 0;
   
    $whereMonthClause = "";

    if(!empty($_GET['month'])){
        $groupByDefault = "DAY(date_time)";
        $whereMonthClause = " and MONTH(date_time) = ".$_GET['month'];
        $days = cal_days_in_month(CAL_GREGORIAN, $_GET['month'], $_GET['year']);

        for($i=1; $i<=$days; $i++){
            $values["m"] = "$i";
            $values["a"] = "0";
            $values["b"] = "0";

            array_push($response, $values);
        }
    }

    $whereBranchClause = "";
    if(!empty($_GET['branch'])){
        $string = ' branch_code = "'.$_GET['branch'].'"';
        $whereBranchClause = empty($whereMonthClause) ? " and $string" : " and $string";
    }

    for($i=1; $i<=12; $i++){
        if($days == 0){
            $values["m"] = $months[$i - 1] ;
            $values["a"] = "0";
            $values["b"] = "0";

            array_push($response, $values);
        }
    }

    $data = $database->rawQuery("
        select  count(id) as b, 
        sum(amount_payable) as a, 
        $groupByDefault as m 
        from orders 
        where is_settled = 1 and 
        YEAR(date_time) = $year
        $whereMonthClause 
        $whereBranchClause 
        group by $groupByDefault
    ");
   
    foreach($data as $x){
        $response[$x["m"] - 1]["a"] = "{$x["a"]}";
        $response[$x["m"] - 1]["b"] = "{$x["b"]}";

    }

    echo json_encode($response);
}