<?php
require_once("../config.php");
require_once("../logs.php");

$trn_number = $_GET['trn_number'];

$database->where("trn_number", $trn_number);
$order = $database->getOne("orders");

if(empty($order)){
    echo json_encode(array(
        "error" => "true",
        "message" => "Cannot find transaction number"
    ));
    $printer -> close();
    return;
}

$header = file_get_contents('receiptheader','r');

//ordered items iteration
$database->where("order_id", $order['id']);
$items = $database->get("ordered_items");

$total_qty = 0;

$itemsTable = "";

foreach($items as $item){
    $qty = $item['quantity'];
    $descp = getItem($database, $item['item_id'])['long_descp'];
    $cost = number_format((float)$item['cost'], 2, '.', '');
    $total_cost = number_format((float)$item['total_cost'], 2, '.', '');
   
    $itemsTable .= "$qty    $descp                      <br/>";
    $itemsTable .= "        @ $cost                  $total_cost<br/>";

    $total_qty += $item['quantity'];
}

$receipt = "
<center>
<pre>
$header

----------------------------------------

<center>P R I N T B I L L</center>

----------------------------------------
QTY    DESCRIPTION                AMOUNT
----------------------------------------
$itemsTable

----------------------------------------
<center>SUBTOTAL: $total_qty ITEM(s) </center>
</pre>

</center>

";

echo $receipt;

function getItem($database, $item_id){
    $database->where("id", $item_id);
    return $database->getOne("menu");
}
