<?php
    require_once("../config.php");
    require_once("../logs.php");
    require_once("functions.php");

    $log_date = getCurrentLogDate($database);
    $branch_code = getBranchCode($database);
    $number = 1;
        
    if(isset($_POST["type"])) {
        if($_POST["type"] == "z") {

            $database->where("branch_code", $branch_code);
            $database->where("type", "ZREADING");
            $database->orderby("id");
            $transaction = $database->getOne("transactions");
            $transaction_number = isset($transaction["trn_number"]) ? $transaction["trn_number"] + 1 : 1;

            $transaction_id = idGenerator() + 1;
                    
            $insert_data = Array (
                "id" => $transaction_id,
                "trn_number" => $transaction_number,
                "type" => "ZREADING",
                "branch_code" => $branch_code,
                "log_date" => $log_date["id"]
            );  
            
            if(!$database->insert ("transactions", $insert_data)) {
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
                return;
            }

            $number = $transaction_number;
        }
        if($_POST["type"] == "x") {

            $database->where("branch_code", $branch_code);
            $database->where("type", "XREADING");
            $database->orderby("id");
            $transaction = $database->getOne("transactions");
            $transaction_number = isset($transaction["trn_number"]) ? $transaction["trn_number"] + 1 : 1;

            $transaction_id = idGenerator() + 1;
                    
            $insert_data = Array (
                "id" => $transaction_id,
                "trn_number" => $transaction_number,
                "type" => "XREADING",
                "branch_code" => $branch_code,
                "log_date" => $log_date["id"]
            );  
            
            if(!$database->insert ("transactions", $insert_data)) {
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
                return;
            }

            $number = $transaction_number;
        }
    }

    $response["cashier_report"] = array (
        "administrator" => $_SESSION["manager"],
        "cashier" => $_SESSION["username"],
        "logdate" => date("Y-m-d", strtotime($log_date["open_log"])),
        "datetime" => date("Y-m-d h:i:s a"),
        "terminal" => $terminal_code,
        "branch_code" => getBranchCode($database),
        "transaction_number" => $number
    );

    $discount_amount = getDiscountAmount($database, $branch_code, $log_date)["discount"];
    $sub_total = salesSummary($database, $branch_code, $log_date)["total_amount"];

    $response["sales_summary"] = array (
        "gross_sales" => computeGrossSales($sub_total, $discount_amount),
        "discount" => $discount_amount,
        "sub_total" => $sub_total
    );

    $response["transaction_details"] = array (
        "beginning_or_number" => getTransactionDetails($database, $branch_code, $log_date)["beginning"],
        "ending_or_number" => getTransactionDetails($database, $branch_code, $log_date)["ending"],
        "total" => getTransactionDetails($database, $branch_code, $log_date)["count"],
        "void_transactions" => getVoidNumber($database, $branch_code, $log_date)["void_transactions"],
        "void_items" => getVoidNumber($database, $branch_code, $log_date)["void_items"],
        "refunded" => getRefundAmount($database, $branch_code, $log_date)["refunded"],
        "void_transaction_total_cost" => getVoidNumber($database, $branch_code, $log_date)["void_transaction_total_cost"],
        "void_item_total_cost" => getVoidNumber($database, $branch_code, $log_date)["void_item_total_cost"],
        "refund_item_total_cost" => getRefundAmount($database, $branch_code, $log_date)["refund_item_total_cost"]
    );

    $response["tender_summary"] = array (
        "tender" => getAllTender($database, $branch_code, $log_date),
        "refund" => getAllRefunds($database, $branch_code, $log_date)
    );

    $response["sales_by_cashier"] = array (
        "cashier" => getAllCashier($database, $branch_code, $log_date)
    );

    $response["sales_by_category"] = array (
        "category" => getSalesByCategory($database, $branch_code, $log_date)
    );

    $response["sales_by_menu"] = array (
        "menu" => getSalesByMenu($database, $branch_code, $log_date)
    );

    $response["refund_amount"] = getRefundAmount($database, $branch_code, $log_date)["refund_item_total_cost"];

    echo json_encode($response);
    //print_r($response);

    function salesSummary($database, $branch_code, $log_date) {
        $total_amount = 0;
        $order_list = $database->rawQuery("Select * from `orders` where `is_settled` = '1' and `is_suspended` = '0' and `is_voided` = '0' and `branch_code` = '$branch_code' and `log_date_id` = '".$log_date["id"]."' ");

        foreach($order_list as $order) {
            $order_list = $database->rawQuery("Select sum(total_cost) as total_amount from `ordered_items` where `order_id` = '".$order['id']."' and `is_voided` = '0'");

            $total_amount += $order_list[0]["total_amount"];
        }

        return array(
            "total_amount" => $total_amount,
        );
    }

    function getDiscountAmount($database, $branch_code, $log_date) {
        $order_list = $database->rawQuery("Select * from `orders` where `is_settled` = '1' and `is_suspended` = '0' and `is_voided` = '0' and `branch_code` = '$branch_code' and `log_date_id` = '".$log_date["id"]."' ");

        $discounts = $database->get("discounts");

        foreach($discounts as $discount) {
            $discount_list[$discount["id"]] = array(
                "description" => $discount["description"],
                "value" => $discount["equivalent_value"],
                "total" => 0,
                "discount_count" => 0
            );
        }

        foreach($order_list as $order){
            $database->where("order_id", $order['id']);
            $items = $database->get("ordered_items");
        
            foreach($items as $item){
                if($item["discount_id"] != null) {
                    $discount_list[$item["discount_id"]]["total"] += $item["discount_amount"];
                    $discount_list[$item["discount_id"]]["discount_count"] += 1;
                }
            }
        }

        return array(
            "discount" => $discount_list
        );
    }

    function computeGrossSales($total, $discount) {
        $total_discount = 0;
        foreach($discount as $discounts) {
            $total_discount += $discounts["total"];
        }

        return $total_discount + $total;
    }

    function getTransactionDetails($database, $branch_code, $log_date) {
        $order_list = $database->rawQuery("Select * from `orders` where `is_settled` = '1' and `is_suspended` = '0' and `is_voided` = '0' and `branch_code` = '$branch_code' and `log_date_id` = '".$log_date["id"]."' order by `order_number` = 'asc'");

        $or_number = $database->rawQuery("Select * from transactions where `type` != 'XREADING' and `type` != 'ZREADING' and `log_date` = '".$log_date["id"]."' and `or_number` != '0' order by `trn_number` asc ");


        if(count($order_list) > 0) {
            return array (
                "beginning" => $or_number[0]["or_number"],
                "ending" => $or_number[count($or_number) - 1]["or_number"],
                "count" => count($order_list)
            );
        }
        return array (
            "beginning" => 000000,
            "ending" => 000000,
            "count" => 0
        );
    }

    function getVoidNumber($database, $branch_code, $log_date) {
        $void_transactions = 0;
        $void_items = 0;
        $refunded = 0;
        $void_total_cost = 0;
        $void_item_cost = 0;
        $refund_item_cost = 0;
        $database->where("log_date_id", $log_date["id"]);
        $database->where("branch_code", $branch_code);
        $void = $database->get("orders");

        foreach($void as $order) {
            $transac = true;
            $item = false;
            $void_transactions += $order["is_voided"];

            if($order["is_voided"] == 1) {
                $void_list_total = $database->rawQuery("Select sum(total_cost) as void_total_cost from `ordered_items` where `order_id` = '".$order["id"]."' ");
            }
            else {
                $transac = false;
                $item = true;
                $void_item_total = $database->rawQuery("Select sum(total_cost) as void_total_cost from `ordered_items` where `order_id` = '".$order["id"]."' and `is_voided` = '1' ");
                $refund_item_total = $database->rawQuery("Select sum(total_cost) as refund_item_total from `ordered_items` where `order_id` = '".$order["id"]."' and `is_refunded` = '1' ");
            }

            $void_list = $database->rawQuery("Select sum(is_voided) as void_items, sum(is_refunded) as refunded, sum(total_cost) as total_cost from `ordered_items` where `order_id` = '".$order["id"]."' ");

            $void_items += $void_list[0]["void_items"];
            $refunded += $void_list[0]["refunded"];

            $void_total_cost += $transac ? $void_list_total[0]["void_total_cost"] : 0;
            $void_item_cost += $item ? $void_item_total[0]["void_total_cost"] : 0;
            $refund_item_cost += $item ? $refund_item_total[0]["refund_item_total"] : 0;
        } 

        return array(
            "void_transactions" => $void_transactions,
            "void_items" => $void_items,
            "void_transaction_total_cost" => $void_total_cost,
            "void_item_total_cost" => $void_item_cost,
        );
    }

    function getRefundAmount($database, $branch_code, $log_date) {
        $database->where("log_date", $log_date["id"]);
        $database->where("branch_code", $branch_code);
        $database->where("type", "REFUND");
        $refund = $database->get("transactions");
        $refund_item_cost = 0;

        foreach($refund as $refunds) {
            $refund_item_cost += $refunds["amount"];
        }

        return array(
            "refunded" => count($refund),
            "refund_item_total_cost" => $refund_item_cost,
        );
    }

    function getAllTender($database, $branch_code, $log_date) {
        $count = 0;
        $tenders = $database->get("tender_types");
        $tender_array = array();
            
        $database->where("branch_code", $branch_code);
        $database->where("is_settled", 1);
        $database->where("log_date_id", $log_date["id"]);
        $orders = $database->get("orders");

        foreach($tenders as $tender) {
            $total_amount = 0;
            $total_counter = 0;

            foreach($orders as $order) {
                $database->where("trn_number", $order["trn_number"]);
                $tender_list = $database->get("ordered_tenders");
                
                foreach($tender_list as $tender_order) {
                    if($tender_order["tender_id"] == $tender["id"]) {
                        $total_amount += $tender_order["amount_payed"] - $tender_order["amount_change"];
                        $total_counter++;
                    }
                }

            }

            $tender_array[$count] = array(
                "tender" => $tender["description"],
                "total_amount" => $total_amount,
                "total_counter" => $total_counter
            );

            $count++;
        }

        return $tender_array;
    }

    function getAllRefunds($database, $branch_code, $log_date) {

    }

    function getAllCashier($database, $branch_code, $log_date) {
        $count = 0;
        $cashier_array = array();

        $database->orderby("cashier_username");
        $database->where("log_date_id", $log_date["id"]);
        $database->where("branch_code", $branch_code);
        $database->where("is_settled", 1);
        $orders = $database->get("orders");

        $database->where("branch_code", $branch_code);
        $accounts = $database->get("accounts");

        foreach($accounts as $user) {
            $total_sale = 0;
            $username = $user["username"];

            foreach($orders as $order) {
                if($username == $order["cashier_username"]) {
                    $database->where("is_refunded", 0);
                    $database->where("is_voided", 0);
                    $database->where("order_id", $order["id"]);
                    $ordered_items = $database->get("ordered_items");
                    foreach($ordered_items as $ordered_item) {
                        $total_sale += $ordered_item["total_cost"];
                    }
                }
            }

            $cashier_array[$count] = array(
                "username" => $username,
                "total_sale" => $total_sale
            );

            $count++;
        }

        return $cashier_array;
    }

    function getSalesByCategory($database, $branch_code, $log_date) {
        $count = 0;
        $database->where("active_tag", "YES");
        $categories = $database->get("categories");
        $category_array = array();

        foreach($categories as $category) {
            $total = 0;
            $total_cost = 0;
            $database->where("category_id", $category["id"]);
            $database->where("branch_code", $branch_code);
            $database->where("active_tag", "YES");
            $menu = $database->get("menu");
            
            $database->where("branch_code", $branch_code);
            $database->where("is_suspended", 0);
            $database->where("is_voided", 0);
            $database->where("log_date_id", $log_date["id"]);
            $orders = $database->get("orders");

            foreach($menu as $item) {
                foreach($orders as $order) {

                    $result = $database->rawQuery("Select sum(quantity) as quantity, sum(total_cost) as total_cost from `ordered_items` where `item_id` = '".$item["id"]."' and `order_id` = '".$order["id"]."' and `is_voided` = '0' and `is_refunded` = '0' ");
    
                    $total += $result[0]["quantity"];
                    $total_cost += $result[0]["total_cost"];
                }
            }

            $category_array[$count] = array(
                "category_name" => $category["description"],
                "total_sale" => $total,
                "total_cost" => $total_cost
            );

            $count++;
        }

        return $category_array;
    }

    function getSalesByMenu($database, $branch_code, $log_date) {
        $count = 0;
        $database->where("active_tag", "YES");
        $categories = $database->get("categories");
        $category_array = array();

        foreach($categories as $category) {
            $total = 0;
            $total_cost = 0;
            $database->where("category_id", $category["id"]);
            $database->where("branch_code", $branch_code);
            $database->where("active_tag", "YES");
            $menu = $database->get("menu");
            
            $database->where("branch_code", $branch_code);
            $database->where("is_suspended", 0);
            $database->where("is_voided", 0);
            $database->where("log_date_id", $log_date["id"]);
            $orders = $database->get("orders");
            $menu_array = array();

            foreach($menu as $item) {
                $total_by_menu = 0;
                $total_cost_by_menu = 0;
                foreach($orders as $order) {

                    $result = $database->rawQuery("Select sum(quantity) as quantity, sum(total_cost) as total_cost from `ordered_items` where `item_id` = '".$item["id"]."' and `order_id` = '".$order["id"]."' and `is_voided` = '0' and `is_refunded` = '0' ");
    
                    $total += $result[0]["quantity"];
                    $total_cost += $result[0]["total_cost"];
    
                    $total_by_menu += $result[0]["quantity"];
                    $total_cost_by_menu += $result[0]["total_cost"];
                }
                
                $menu_array[$item["long_descp"]] = array(
                    "total_sale" => $total_by_menu,
                    "total_cost" => $total_cost_by_menu
                );
            }

            $category_array[$count] = array(
                "category_name" => $category["description"],
                "menu" => $menu_array,
                "total_sale" => $total,
                "total_cost" => $total_cost

            );

            $count++;
        }

        return $category_array;
    }
?>