<?php
    require_once("../config.php");
    require_once("../logs.php");
    require_once("functions.php");
    require_once("inventory.php");
    require_once("tenders.php");

    //new transaction
    if(isset($_GET["newTransaction"])) {
        $id = idGenerator();
        $branch_code = getBranchCode($database);

        $transaction = $database->rawQuery("Select * from transactions where `branch_code` = '$branch_code' and `type` != 'XREADING' and `type` != 'ZREADING' order by `trn_number` desc ");
    
        $transaction_number = empty($transaction) ? 1: $transaction[0]["trn_number"] + 1;

        $database->where("branch_code", $branch_code);
        $database->orderby("id");
        $order = $database->getOne("orders");
        $guest_no = 1;
        $log_date = getCurrentLogDate($database);
        $splitted_date = explode(" ", $log_date["open_log"]);
        $date = getSpecificLogDate($database, $order["log_date_id"]);

        if($date != $splitted_date[0]) {
            $order_number = getNewOrderNumber($database, "reset");
        }
        else {
            $order_number = getNewOrderNumber($database, "increment");
        }
        
        $insert_data = Array (
            "id" => $id,
            "order_number" => $order_number,
            "trn_number" => $transaction_number,
            "guest_no" => $guest_no,
            "log_date_id" => $log_date["id"],
            "cashier_username" => $_SESSION["username"],
            "branch_code" => $branch_code
        );  

        $return_data = Array (
            "trn_number" => $transaction_number,
            "order_number" => $order_number
        );

        if(!$database->insert ("orders", $insert_data)) {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
            return;
        }
        
        $transaction_id = idGenerator() + 1;
        
        $insert_data = Array (
            "id" => $transaction_id,
            "order_id" => $id,
            "trn_number" => $transaction_number,
            "branch_code" => $branch_code,
            "log_date" => $log_date["id"]
        );  

        if(!$database->insert ("transactions", $insert_data)) {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
            return;
        }

        saveLog($database, "Save New Transaction # $transaction_number ");

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "data" => $return_data
        ));
    }

    //update guest count
    if(isset($_GET["saveGuestcount"])) {
        $count = $_GET["guestCount"];
        $transaction_number = $_GET["trnNumber"];

        $update_data = Array (
            "guest_no" => $count
        );

        $database->where ('trn_number', $transaction_number);
        $flag = $database->update ("orders", $update_data);

        if($flag){

            saveLog($database, "Save Guess No $count at Transaction # $transaction_number ");

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "success"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    //update guest count
    if(isset($_GET["saveServiceType"])) {
        $transaction_number = $_GET["trnNumber"];
        $product = $_GET["product"];
        $type = $_GET["type"];
    
        $update_data = Array (
            "service_type" => $type
        );

        $database->where ('id', $product);
        $flag = $database->update ("ordered_items", $update_data);
    
        if($flag) {

            saveLog($database, "Save Service Type as $type from Transaction # $transaction_number ");

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Service type saved!"
            ));
        }
        else {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if(isset($_GET['getCurrentTransaction'])) {
      $transaction_number = $_GET['getCurrentTransaction'];
      $branch_code = getBranchCode($database);
    
      $database->where("trn_number", $transaction_number);
      $database->where("branch_code", $branch_code);
      $order = $database->getOne("orders");
      
      $response_items = getOrderList($database, $order["id"]);
    
      $response["order"] = $order;
      $response["item"] = $response_items;
    
      echo json_encode($response);
    }

    //add product to order
    if(isset($_GET["newOrder"])) {
        $data = $_GET["data"];
        $product_id = $data["product"];
        $quantity = $data["quantity"];
        $transaction_number = $data["trnNumber"];
    
        $id = idGenerator();
        $order_id = getOneTransaction($database, $transaction_number)["id"];

        $database->where("id", $product_id);
        $order_by_product_id = $database->getOne("ordered_items");

        $database->where("order_id", $order_id);
        $database->where("item_id", $product_id);
        $order_by_item_id = $database->getOne("ordered_items");
        
        if(isset($order_by_product_id)) { //checks if product is already added to that order then update new quantity
            $existing_id = $product_id;
            $item_id = $order_by_product_id["item_id"];
            $cost =  $order_by_product_id["cost"];
            $old_quantity = $order_by_product_id["quantity"];
 
            $inventory = checkInventoryIfEnough($database, $item_id, $quantity);
    
            if(isset($inventory["message"])){
            
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $inventory['message']
                ));
    
                return;
            }
    
            $total_cost = computeTotalCost($cost, $quantity);
    
            $update_data = Array (
                "quantity" => $quantity,
                "total_cost" => $total_cost,
                "is_voided" => 0
            );
    
            $database->where ('item_id', $item_id);
            $database->where("order_id", $order_id);
            $flag = $database->update ("ordered_items", $update_data);

            if($quantity > $old_quantity) {
                $stockQuantityUpdate = $quantity - $old_quantity;
                $func = "less";
            }
            else if($quantity == $old_quantity) {
                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "menu" => "Successful!"
                ));
                return;
            }
            else {
                $stockQuantityUpdate = $old_quantity - $quantity;
                $func = "more";
            }

            if($flag) {
                updateItemStocks($database, $item_id, $stockQuantityUpdate, $existing_id, $func);
                if(updateTotalAmount($database, $order_id) == false) {
                    echo json_encode(Array (
                        "type" => "error",
                        "title" => "Error!",
                        "text" => $database->getLastError()
                    ));
                    return;
                }

                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "menu" => "Successful!"
                ));
            }
            else {
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }

        }
        
        else if(isset($order_by_item_id)) { //checks if product is already added to that order then update new quantity
            $order_item_id = $order_by_item_id["id"];
            $item_id = $product_id;
            $cost = $order_by_item_id["cost"];
            $old_quantity = $order_by_item_id["quantity"];
 
            $inventory = checkInventoryIfEnough($database, $item_id, $quantity);

    
            if(isset($inventory["message"])){
    
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $inventory['message']
                ));
    
                return;
            }
    
            $total_cost = computeTotalCost($cost, $quantity + $old_quantity);
    
            $update_data = Array (
                "quantity" => $quantity + $old_quantity,
                "total_cost" => $total_cost,
                "is_voided" => 0
            );
    
            $database->where ('id', $order_item_id);
            $flag = $database->update ("ordered_items", $update_data);

            $stockQuantityUpdate = $quantity - $old_quantity;
            $func = "less";

            if($flag) {
                updateItemStocks($database, $item_id, $stockQuantityUpdate, $order_item_id, $func);
                if(updateTotalAmount($database, $order_id) == false) {
                    echo json_encode(Array (
                        "type" => "error",
                        "title" => "Error!",
                        "text" => $database->getLastError()
                    ));
                    return;
                }

                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "menu" => "Successful!"
                ));
            }
            else {
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }

        }
        else { //adding new product to an order
            $inventory = checkInventoryIfEnough($database, $product_id, $quantity);
    
            if($inventory["error"]){
    
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $inventory['message']
                ));
    
                return;
            }

            $menu = getMenu($database, $product_id);
            $item_id = $menu["id"];
            $cost = $menu["cost"];
            $product = $menu["name"];
            $total_cost = computeTotalCost($cost, $quantity);
        
            $insert_data = Array (
                "id" => $id,
                "order_id" => $order_id,
                "item_id" => $item_id,
                "cost" => $cost,
                "quantity" => $quantity,
                "total_cost" => $total_cost
            );
        
            $flag = $database->insert ("ordered_items", $insert_data);
            if($flag) {
                updateItemStocks($database, $product_id, $quantity, $id);
                if(updateTotalAmount($database, $order_id) == false) {
                    echo json_encode(Array (
                        "type" => "error",
                        "title" => "Error!",
                        "text" => $database->getLastError()
                    ));
                    return;
                }

                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "menu" => "Successful!"
                ));
            }
            else {
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }
        }

        saveLog($database, "Save Order ItemID $product_id at Qty $quantity Transaction # $transaction_number ");
    }

    if(isset($_GET['applyDiscount'])){
        $data = $_GET['data'];
        $allFlag = true;

        $order = getOneTransaction($database, $data["transaction"]);

        if($data["product"] == "all") {
            $database->where("order_id", $order["id"]);
            $ordered_items = $database->get("ordered_items");

            foreach($ordered_items as $ordered_item) {
                $discount = computeDiscount($database, $ordered_item["id"], $data["discount"], false, "");

                $update_data = Array (
                    "total_cost" => $discount["new_total_cost"],
                    "discount" => $discount["discount_value"],
                    "discount_amount" => $discount["discount_amount"],
                    "discount_id" => $data["discount"]
                );
            
                $database->where ('order_id', $order["id"]);
                $database->where ('item_id', $ordered_item["item_id"]);
                $flag = $database->update ("ordered_items", $update_data);
            
                if(!$flag) {
                    $allFlag = false;
                    break;
                }
            }
            
            if($allFlag) {
                if(updateTotalAmount($database, $order["id"]) == false) {
                    echo json_encode(Array (
                        "type" => "error",
                        "title" => "Error!",
                        "text" => $database->getLastError()
                    ));
                    return;
                }

                saveLog($database, "Applied Discount ID {$data["discount"]} to All Transaction # {$data["transaction"]}");

                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "text" => "Discount applied!"
                ));
            }else{
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }

        }
        else {
            $discount = computeDiscount($database, $data["product"], $data["discount"], true, "");

            if(isset($discount["is_existing"])) {
                echo json_encode(Array (
                    "type" => "warning",
                    "title" => "Error!",
                    "text" => "A discount is already applied!"
                ));
            }
            else {
                $update_data = Array (
                    "total_cost" => $discount["new_total_cost"],
                    "discount" => $discount["discount_value"],
                    "discount_amount" => $discount["discount_amount"],
                    "discount_id" => $data["discount"]
                );
            
                $database->where ('id', $data["product"]);
                $flag = $database->update ("ordered_items", $update_data);
            
                if($flag) {
                    if(updateTotalAmount($database, $order["id"]) == false) {
                        echo json_encode(Array (
                            "type" => "error",
                            "title" => "Error!",
                            "text" => $database->getLastError()
                        ));
                        return;
                    }

                    saveLog($database, "Applied Discount ID {$data["discount"]} Transaction # {$data["transaction"]} ");

                    echo json_encode(Array (
                        "type" => "success",
                        "title" => "Successful!",
                        "text" => "Discount applied!"
                    ));
                }else{
                    echo json_encode(Array (
                        "type" => "error",
                        "title" => "Error!",
                        "text" => $database->getLastError()
                    ));
                }
            }
        }
    }

    if(isset($_GET['removeDiscount'])){
        $data = $_GET['data'];
        $all_flag = true;
        $check_if_nothing_changed = false;
        $reset_data = 0.00;
        $transaction_number = intval($data["transaction"]);
    
        $order = $database->rawQuery("Select * from `orders` where `trn_number` = '$transaction_number'");
        $order = $order[0];
    
        if($data["product"] == "all") {
            $database->where("order_id", $order["id"]);
            $ordered_items = $database->get("ordered_items");
    
            foreach($ordered_items as $ordered_item) {
                $total_cost = computeTotalCost($ordered_item["cost"], $ordered_item["quantity"]);
    
                if($ordered_item["discount"] == 0) {
                    $check_if_nothing_changed = true;
                }
    
                $update_data = Array (
                    "total_cost" => $total_cost,
                    "discount" => $reset_data,
                    "discount_amount" => $reset_data,
                    "discount_id" => null
                );
            
                $database->where ('order_id', $order["id"]);
                $database->where ('item_id', $ordered_item["item_id"]);
                $flag = $database->update ("ordered_items", $update_data);
            
                if(!$flag) {
                    $all_flag = false;
                    break;
                }
            }
            
            if($check_if_nothing_changed) {
                echo json_encode(Array (
                    "type" => "warning",
                    "title" => "Successful!",
                    "text" => "Nothing is removed!"
                ));
            }
            else {
                if($all_flag) {
                    if(updateTotalAmount($database, $order["id"]) == false) {
                        echo json_encode(Array (
                            "type" => "error",
                            "title" => "Error!",
                            "text" => $database->getLastError()
                        ));
                        return;
                    }
                    
                    echo json_encode(Array (
                        "type" => "success",
                        "title" => "Successful!",
                        "text" => "Discount removed!"
                    ));
                }else{
                    echo json_encode(Array (
                        "type" => "error",
                        "title" => "Error!",
                        "text" => $database->getLastError()
                    ));
                }
            }
    
        }
        else {
            $database->where ('id', $data["product"]);
            $ordered_items = $database->getOne("ordered_items");
    
            $total_cost = computeTotalCost($ordered_items["cost"], $ordered_items["quantity"]);
            
            if($ordered_items["discount"] > 0) {
                $update_data = Array (
                    "total_cost" => $total_cost,
                    "discount" => $reset_data,
                    "discount_amount" => $reset_data,
                    "discount_id" => null
                );
                
                $database->where ('id', $data["product"]);
                $flag = $database->update ("ordered_items", $update_data);
                
                if($flag) {
                    if(updateTotalAmount($database, $order["id"]) == false) {
                        echo json_encode(Array (
                            "type" => "error",
                            "title" => "Error!",
                            "text" => $database->getLastError()
                        ));
                        return;
                    }
                    
                    echo json_encode(Array (
                        "type" => "success",
                        "title" => "Successful!",
                        "menu" => "Discount applied!"
                    ));
                }
                else {
                    echo json_encode(Array (
                        "type" => "error",
                        "title" => "Error!",
                        "text" => $database->getLastError()
                    ));
                }
            }
            else {
                echo json_encode(Array (
                    "type" => "warning",
                    "title" => "Error!",
                    "text" => "No discount applied"
                ));
            }
        }

        saveLog($database, "Remove Discount at Transaction # {$data["transaction"]} ");
    }

    if(isset($_POST['applyCustomer'])) {
        $data = $_POST['data'];
        
        $orders = getOneTransaction($database, $data["transaction"]);
    
        if($orders["customer_id"] > 0 ) {
            echo json_encode(Array (
                "type" => "warning",
                "title" => "Error!",
                "text" => "A customer is already added!"
            ));
        }
        else {
            $update_data = Array (
                "customer_id" => $data["customer"]
            );
        
            $database->where ('trn_number', $data["transaction"]);
            $flag = $database->update ("orders", $update_data);
        
            if($flag) {

                saveLog($database, "Apply CustomerID {$data["customer"]} at Transaction # {$data["transaction"]}");

                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "text" => "Customer applied!"
                ));
            }else{
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }
        }
    }

    if(isset($_POST['applyRequest'])) {
        $data = $_POST['data'];
        
        $orders = getOneTransaction($database, $data["transaction"]);

        $ordered_items = getOneOrderedItem($database, $orders["id"], $data["product"]);
    
        if($ordered_items["special_request"] == "") {
            $updateData = Array (
                "special_request" => $data["request"]
            );
    
            $database->where ('id', $data["product"]);
            $flag = $database->update ("ordered_items", $updateData);
        
            if($flag) {

                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "menu" => "Special request submitted!"
                ));
            }
            else {
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }
        }
        else {
            $existing_request = $ordered_items["special_request"];
    
            $update_data = Array (
                "special_request" => $existing_request." ".$data["request"]
            );
    
            $database->where("order_id", $orders["id"]);
            $database->where ('item_id', $data["product"]);
            $flag = $database->update ("ordered_items", $update_data);
        
            if($flag) {
                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "menu" => "Special request submitted!"
                ));
            }
            else {
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }
        }

        saveLog($database, "Apply Special Request {$data["request"]} at Transaction # {$data["transaction"]}");
    }

    if(isset($_POST['savePayment'])) {
        $settled = 1;
        $data = $_POST["savePayment"];
    
        $database->where ('trn_number', $data["transaction_number"]);
        $order = $database->getOne ("orders");

        if($data["amount_payed"] < $data["amount_total"]) {
            $settled = 0;
        }

        $update_data = Array (
            "amount_payed" => $order["amount_payed"] + $data["amount_payed"],
            "amount_change" => $data["amount_change"],
            "is_settled" => $settled
        );

        $transaction_number = intval($data["transaction_number"]);
    
        $database->where ('trn_number', $data["transaction_number"]);
        $flag = $database->update ("orders", $update_data);

        if($flag) {
            
            if($data["tender"] == 1) {
                saveTenders($data["transaction_number"], $data["tender"], $data["amount_payed"], $data["amount_change"]);
            }
            else {
                saveTenders($data["transaction_number"], $data["tender"], $data["amount_payed"], $data["amount_change"], $data["details"]["account_name"], $data["details"]["card_number"], $data["details"]["approval_number"], $data["details"]["bank_name"]);
            }

            if($settled == 0) {
                echo json_encode(Array (
                    "type" => "partial",
                    "title" => "Successful!",
                    "text" => "Amount Settled!"
                ));
            }
            else {
                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "text" => "Amount Settled!"
                ));
            }
        }
        else {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if(isset($_GET['manualDiscount'])) {
        $data = $_GET["data"];
        
        $database->where("id", $data["order_id"]);
        $ordered_items = $database->getOne("ordered_items");

        $discount = computeDiscount($database, $data["order_id"], 1, true, $data["discount"]);

        if(isset($discount["is_existing"])) {
            echo json_encode(Array (
                "type" => "warning",
                "title" => "Error!",
                "text" => "A discount is already applied!"
            ));
        }
        else {
            $update_data = Array (
                "total_cost" => $discount["new_total_cost"],
                "discount" => $discount["discount_value"],
                "discount_amount" => $discount["discount_amount"],
                "discount_id" => $discount["discount"]
            );
        
            $database->where ('id', $data["order_id"]);
            $flag = $database->update ("ordered_items", $update_data);
        
            if($flag) {
                if(updateTotalAmount($database, $ordered_items["order_id"]) == false) {
                    echo json_encode(Array (
                        "type" => "error",
                        "title" => "Error!",
                        "text" => $database->getLastError()
                    ));
                    return;
                }

                saveLog($database, "Applied Discount ID 1 ORDER ID # {$data["order_id"]} ");

                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "text" => "Discount applied!"
                ));
            }else{
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }
        }
    }
?>