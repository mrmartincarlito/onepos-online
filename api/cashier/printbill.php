<?php
require_once("../config.php");
require_once("../logs.php");
require_once("dao/Item.php");

require_once ('printer_connector/autoload.php');

use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

@$connector = new WindowsPrintConnector($PRINTER_PATH);
@$printer = new Printer($connector);


try{

    $trn_number = $_GET['trn_number'];

    $database->where("trn_number", $trn_number);
    $order = $database->getOne("orders");
  
    if(empty($order)){
        echo json_encode(array(
            "error" => "true",
            "message" => "Cannot find transaction number"
        ));
        $printer -> close();
        return;
    }
    
    $printer -> setJustification(Printer::JUSTIFY_CENTER);
    $printer -> text("1TEQ PROVIDERS INCORPORATED\n");
    $printer -> text("Owned & Operated by\n");
    $printer -> text("Unit D MCY Bldg.137 Roosevelt Ave\n");
    $printer -> text("Brgy Paraiso Quezon City Manila\n");
    $printer -> text("----------------------------------------\n");
    $printer -> text("P R I N T  B I L L\n");
  
    
    $printer -> setEmphasis(true);
    $printer -> setJustification(Printer::JUSTIFY_CENTER);
    $printer -> text("----------------------------------------\n");
    $printer -> text(" QTY  DESCRIPTION                 AMOUNT\n");
    $printer -> text("----------------------------------------\n");
    $printer -> setEmphasis(false);

    //ordered items iteration
    $database->where("order_id", $order['id']);
    $items = $database->get("ordered_items");

    $total_qty = 0;
    $total_amount = 0.00;

    foreach($items as $item){
        $printer -> text(new Item($item['quantity'], getItem($database, $item['item_id'])['long_descp']." @ ".$item['cost'] , $item['total_cost']));
       
        $total_qty += $item['quantity'];
        $total_amount += $item['total_cost'];
    }

    $printer -> feed();
    $printer -> text("---------------------------------------\n");
    $printer -> text("SUBTOTAL: $total_qty ITEM(s) \n");
    $printer -> text("TOTAL:                          ".sprintf("%6.2f",$total_amount)."\n");
    $printer -> text("---------------------------------------\n");
    $printer -> text("END OF BILLING\n");
    $printer -> text("THANK YOU FOR DINING!\n");
    $printer -> text("========================================\n");


   
    echo json_encode(array(
        "error" => "false",
        "message" => "Printing receipt"
    ));

    saveLog($database, "Print Bill Transaction # $trn_number");

}catch (Exception $e) { 
    echo json_encode(array(
        "error" => "true",
        "message" => "Couldn't print to this printer: " . $e -> getMessage() . "\n"
    ));
}
$printer -> cut();
$printer -> close();

function getItem($database, $item_id){
    $database->where("id", $item_id);
    return $database->getOne("menu");
}