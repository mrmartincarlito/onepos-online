<?php
require_once("../config.php");
require_once("../logs.php");
require_once("functions.php");

define("ORDERED_ITEMS", "ordered_items");
define("MENU", "menu");

$trn_number = $_GET['trn_number'];
$branch_code = getBranchCode($database);
$flag = 0;
$trn_number = intval($trn_number);

$order = $database->rawQuery("Select * from `orders` where `trn_number` = '$trn_number' and `branch_code` = '$branch_code' ");
  
if(empty($order)){
    echo json_encode(array(
        "error" => "true",
        "message" => "Cannot find transaction number"
    ));
    return;
}
//ordered items iteration

$database->where("order_id", $order[0]['id']);
$database->join(MENU." m", "m.id=o.item_id", "LEFT");
$items = $database->get(ORDERED_ITEMS." o", null, "o.*, m.long_descp");

$total_qty = 0;
$total_cost_bill = 0;
$discount_list = array();

$discounts = $database->get("discounts");

foreach($discounts as $discount) {
    $discount_list[$discount["id"]] = array(
        "description" => $discount["description"],
        "value" => $discount["equivalent_value"],
        "total" => 0,
        "type" => $discount["type"]
    );
}

foreach($items as $item){
    $qty = $item['quantity'];
    $descp = getItem($database, $item['item_id'])['long_descp'];
    $cost = number_format((float)$item['cost'], 2, '.', '');
    $total_cost = number_format((float)$item['total_cost'], 2, '.', '');
    $total_cost_bill += $total_cost;

    if($item["discount_id"] != null) {
        $discount_list[$item["discount_id"]]["total"] += $item["discount_amount"];
    }

    $total_qty += $item['quantity'];
}

$response["items"] = $items;
$response["order"] = $order[0];
$response["logdate"] = getCurrentLogDate($database, $order[0]['log_date_id'])["open_log"];
$response["total_quantity"] = $total_qty;
$response["total_cost_bill"] = $total_cost_bill;
$response["discount"] = $discount_list;
$response["tenders"] = getOrderTenders($database, $trn_number);
        
if(isset($_GET["reprint"])) {
    $log_date = getCurrentLogDate($database);
    $transactions = $database->rawQuery("Select * from transactions where `type` != 'XREADING' and `type` != 'ZREADING' order by `trn_number` desc ");

    $transaction_id = idGenerator() + 1;

    $trn_number = $transactions[0]["trn_number"] + 1;
            
    $insert_data = Array (
        "id" => $transaction_id,
        "order_id" => $order[0]['id'],
        "trn_number" => $trn_number,
        "type" => "REPRINT",
        "branch_code" => $branch_code,
        "log_date" => $log_date["id"]
    );  
    
    if(!$database->insert ("transactions", $insert_data)) {
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
        return;
    }

    $or_number = "";
}
else {
    $database->where("branch_code", $branch_code);
    $database->where("type", "ORDER");
    $database->orderby("or_number", "desc");
    $transactions = $database->get("transactions");

    $or_number = isset($transactions) ? $transactions[0]["or_number"] + 1 : 1;

    $update_data = array (
        "or_number" => $or_number
    );
        
    $database->where("order_id", $order[0]['id']);
    if(!$database->update ("transactions", $update_data)) {
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
        return;
    }
}

$response["trn_number"] = $trn_number;
$response["or_number"] = $or_number;

echo json_encode($response);

function getItem($database, $item_id){
    $database->where("id", $item_id);
    return $database->getOne("menu");
}

function getOrderTenders($database, $transaction_number) {
    $tenders = $database->rawQuery("Select * from `ordered_tenders` where `trn_number` = '$transaction_number'");
    $response = array();
    $count = 0;

    foreach($tenders as $tender) {
        $database->where("id", $tender["tender_id"]);
        $tender_list = $database->getOne("tender_types");
        $response[$count] = array (
            "tender_list" => $tender_list["description"],
            "tender" => $tender
        );
        $count++;
    }

    return $response;
}