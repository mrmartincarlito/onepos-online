<?php
class Item{
    private $name;
    private $qty;
    private $total;
    private $printer_type;

    public function __construct($qty = '',$name = '', $total= '', $printer_type = 'THERMAL')
    {
        $this -> name = $name;
        $this -> qty = $qty;
        $this -> total = $total;
        $this -> printer_type = $printer_type;
    }
    public function __toString()
    {
        $text = '';
        if($this->printer_type == 'DOTMATRIX'){
            $text = sprintf('%3.3s %-20.20s %8.2f'.PHP_EOL, $this -> qty, $this -> name, $this -> total);
        }else if($this->printer_type == 'THERMAL'){
            $text = sprintf('%3.2s   %-26.15s %5.2f'.PHP_EOL, $this -> qty, $this -> name, $this -> total);
        }
        return $text;
    }
}
