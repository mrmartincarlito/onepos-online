<?php
    $branches = json_decode(file_get_contents("../branches.json"), true);
    //get current log date
    function getCurrentLogDate($database) {
        $branch_code = getBranchCode($database);

        $database->where("branch_code", $branch_code);
        $database->orderby ("id");
        return $database->getOne("log_date");
    } 

    function getSpecificLogDate($database, $id) {
        $branch_code = getBranchCode($database);

        $database->where("branch_code", $branch_code);
        $database->where("id", $id);
        $log_date = $database->getOne("log_date");
        $splitted_log_date = explode(" ", $log_date["open_log"]);
        
        return $splitted_log_date[0];
    }

    function getCustomers($database) {
        return $database->get("customer");
    }
    
    function getDiscounts($database, $flag = true) {
        if(!$flag) {
            return $database->rawQuery("Select * from `discounts` where `description` != 'Manual'");
        }
        return $database->get("discounts");
    }

    function getTenderTypes($database) {
        return $database->get("tender_types");
    }

    function getOneTransaction($database, $transaction_number) {
        $branch_code = getBranchCode($database);
        $transaction_number = intval($transaction_number);

        $order = $database->rawQuery("Select * from `orders` where `trn_number` = '$transaction_number' and `branch_code` = '$branch_code' ");

        return empty($order) ? $order = [] : $order[0];
    }

    function getOneOrderedItem($database, $order_id, $product_id) {
        $database->where("order_id", $order_id);
        $database->where("item_id", $product_id);
        return $database->getOne("ordered_items");
    }

    function getOneOrderItem($database, $ordered_item_id) {
        $database->where("id", $ordered_item_id);
        return $database->getOne("ordered_items");
    }

    function getUserId($database, $username) {
        $database->where("username", $username);
        $account = $database->getOne("accounts");
        return $account["id"];
    }

    function getSalesAmount($database) {
        $log_date = getCurrentLogDate($database);
        $database->where("log_date_id", $log_date["id"]);
        return $database->getOne("sales_amount");
    }

    //get menu item
    function getMenu($database, $product_id) {
        $database->where("id", $product_id);
        return $database->getOne("menu");
    }

    function getDiscount($database, $id) {
        $database->where("id", $id);
        return $database->getOne("discounts");
    }

    function idGenerator() {
        return microtime(true);
    }

    //compute total cost
    function computeTotalCost($cost, $quantity) {
        return $cost * $quantity;
    }

    function getNewOrderNumber($database, $type) {
        $branch_code = getBranchCode($database);

        $database->where("branch_code", $branch_code);
        $database->orderby("id");
        $order = $database->getOne("orders");
    
        if($type == "reset") {
            return 1;
        }
        else {
            return $order["order_number"] + 1;
        }
    }

    function getOrderList($database, $order_id) {
        $database->where("order_id", $order_id);
        $database->where("is_voided", 0);
        $database->where("is_refunded", 0);
        $ordered_items = $database->get("ordered_items");
      
        $response_items = array();
      
        foreach($ordered_items as $ordered_item){
          $database->where("id", $ordered_item["item_id"]);
          $item = $database->getOne("menu");
      
          $ordered_item_in_cart = array(
              "id" => $ordered_item['id'],
              "order_id" => $ordered_item['order_id'],
              "item" => $item,
              "cost" => $ordered_item['cost'],
              "quantity" => $ordered_item['quantity'],
              "total_cost" => $ordered_item['total_cost'],
              "service_type" => $ordered_item['service_type'],
              "special_request" => $ordered_item['special_request']
          );
      
          array_push($response_items, $ordered_item_in_cart);
        }
        $response["items"] = $response_items;
    
        return $response;
    }

    function getOneSettledTransaction($database, $transaction_number) {
        $flag = 0;
        $transaction_number = intval($transaction_number);
        
        $branch_code = getBranchCode($database);
        
        return $database->rawQuery("Select * from `orders` where `trn_number` = '$transaction_number' and `is_voided` = '$flag' and `is_suspended` = '$flag' and `branch_code` = '$branch_code' ");
        //return $database->getOne("orders");
    }

    //save log date
    function saveLogDate($database, $data) {
        $branch_code = getBranchCode($database);
        $date = date("Y-m-d");
        $date_time = date("Y-m-d h:i:s");
        
        //if saving current date as log date
        if($data == "current") {
            $insert_date = Array (
                "open_log" => $date_time,
                "branch_code" => $branch_code
            );
        }
        else {
            $log_date = getCurrentLogDate($database);
            $last_log_date = date("Y-m-d", strtotime($log_date["open_log"]));
            $new_time = date("h:i:s");

            //checks if the last log date is equal today or tomorrow then increment it by one
            if($last_log_date >= $date) {
                $date = date("Y-m-d", strtotime("+1 day", strtotime($log_date["open_log"])));
            }
            //if last log date is behind by any number of day it will be set to today
            $insert_date = Array (
                "open_log" => $date . " " . $new_time,
                "branch_code" => $branch_code
            );
        }
        
        return $database->insert ("log_date", $insert_date);
    }

    function computeDiscount($database, $product_id, $discount_id, $status, $manual) {
        $discount = getDiscount($database, $discount_id);

        $item_order = getOneOrderItem($database, $product_id);

        if(!empty($manual)) {
            if($item_order["discount"] > 0) {
                return Array(
                    "is_existing" => true
                );
            }
            else {

                $discount_amount = $manual;
                $new_total_cost = $item_order["total_cost"] - $manual;
    
                if($new_total_cost < 0) {
                    $new_total_cost = 0;
                }
            
                return Array(
                    "new_total_cost" => $new_total_cost,
                    "discount_value" => $manual.".00",
                    "discount_amount" => $discount_amount,
                    "discount" => $discount["id"]
                );
            }
        }
        
        if($status) {
            if($item_order["discount"] > 0) {
                return Array(
                    "is_existing" => true
                );
            }
            else {
                if($discount["type"] == "PERCENTAGE") {
                    $discount_multiplier = $discount["equivalent_value"] / 100;
                    $discount_amount = $item_order["total_cost"] * $discount_multiplier;
                    $new_total_cost = $item_order["total_cost"] - $discount_amount;
                
                    return Array(
                        "new_total_cost" => $new_total_cost,
                        "discount_value" => $discount["equivalent_value"]."%",
                        "discount_amount" => $discount_amount,
                        "discount" => $discount["id"]
                    );
                }
                else {
                    $discount_amount = $discount["equivalent_value"];
                    $new_total_cost = $item_order["total_cost"] - $discount["equivalent_value"];

                    if($new_total_cost < 0) {
                        $new_total_cost = 0;
                    }
                
                    return Array(
                        "new_total_cost" => $new_total_cost,
                        "discount_value" => $discount["equivalent_value"].".00",
                        "discount_amount" => $discount_amount,
                        "discount" => $discount["id"]
                    );
                }
            }
        }
        else {
            if($discount["type"] == "PERCENTAGE") {
                $discount_multiplier = $discount["equivalent_value"] / 100;
                if($item_order["discount"] > 0) {
                    $total_cost = $item_order["cost"] * $item_order["quantity"];
                    $discount_amount = $total_cost * $discount_multiplier;
                    $new_total_cost = $total_cost - $discount_amount;
                }
                else {
                    $discount_amount = $item_order["total_cost"] * $discount_multiplier;
                    $new_total_cost = $item_order["total_cost"] - $discount_amount;
                }
                
                return Array(
                    "new_total_cost" => $new_total_cost,
                    "discount_value" => $discount["equivalent_value"]."%",
                    "discount_amount" => $discount_amount,
                    "discount" => $discount["id"]
                );
            }
            else {
                $discount_amount = $discount["equivalent_value"];
                $new_total_cost = $item_order["total_cost"] - $discount["equivalent_value"];

                if($new_total_cost < 0) {
                    $new_total_cost = 0;
                }
                
                return Array(
                    "new_total_cost" => $new_total_cost,
                    "discount_value" => $discount["equivalent_value"].".00",
                    "discount_amount" => $discount_amount,
                    "discount" => $discount["id"]
                );
            }
        }
    }

    function getPendingTransactions($database, $log_date_id) {
        $is_suspended = 1;

        $database->where("log_date_id", $log_date_id);
        $database->where("is_suspended", $is_suspended);
        return $database->get("orders");
    }

    function updateTotalAmount($database, $order_id) {
        $amount_payable = 0;
        
        $database->where("order_id", $order_id);
        $database->where("is_voided", 0);
        $ordered_items = $database->get("ordered_items");
        $discount_amount = 0;

        foreach($ordered_items as $items) {
            //$discount = computeDiscount($database, $items["id"], $items["discount_id"], false, "");

            $update_data = Array (
                "total_cost" => $items["total_cost"],
                "discount" => $items["discount"],
                "discount_amount" => $items["discount_amount"],
                "discount_id" => $items["discount_id"]
            );

            $amount_payable += $items["total_cost"];
        
            $database->where ('id', $items["id"]);
            $flag = $database->update ("ordered_items", $update_data);

            //$discount_amount += $discount["discount_amount"];
        }
        //echo $discount_amount;
        //echo $amount_payable;

        //$amount_payable -= $discount_amount;

        $newAmount = array(
            "amount_payable" => $amount_payable
        );
        
        $database->where("id", $order_id);
        return $database->update ("orders", $newAmount);
    }
?>