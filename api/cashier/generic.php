<?php
    require_once("../config.php");
    require_once("../logs.php");
    require_once("functions.php");

    //get logdate
    if(isset($_GET["getLogDate"])) {
        $log_date = getCurrentLogDate($database);
        $branch_code = getBranchCode($database);
        //checks if there's a log date record
        if(isset($log_date["open_log"])) {
            //checks if current logdate is closed
            if(strtotime($log_date['close_log']) > 0) {
                //if closed gets next day as log date
                if(saveLogDate($database, "next")) {
                    $log_date = getCurrentLogDate($database);
                    $result = Array (
                        "id" => $log_date["id"],
                        "date" => date("Y-m-d", strtotime($log_date["open_log"])),
                        "branch" => $branch_code
                    );
                }
            }
            //if not close then return current date
            else {
                $result = Array (
                    "id" => $log_date["id"],
                    "date" => date("Y-m-d", strtotime($log_date["open_log"])),
                    "branch" => $branch_code
                );
            }
        }
        else {
            //if no record creates another log date
            if(saveLogDate($database, "current")) {
                $log_date = getCurrentLogDate($database);
                $result = Array (
                    "id" => $log_date["id"],
                    "date" => date("Y-m-d", strtotime($log_date["open_log"])),
                    "branch" => $branch_code
                );
            }
        }
        //returns date
        echo json_encode($result);
    }

    //close log date
    if(isset($_GET["closeLogDate"])) {
        //gets current logdate
        $log_date = getCurrentLogDate($database);
        $new_time = date("Y-m-d h:i:s");
        
        $update_data = Array (
            "close_log" => $new_time
        );

        $database->where ('id', $log_date["id"]);
        $flag = $database->update ("log_date", $update_data);

        if($flag) {

            saveLog($database, "Close Log Date ".$log_date['id']." at $new_time");

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "log date ended!"
            ));
        }
        else {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if(isset($_POST["saveCustomer"])) {
        $data = $_POST["data"];
        $id = idGenerator();
        
        $insert_data = Array (
            "id" => $id,
            "fname" => $data["fname"],
            "lname" => $data["lname"],
            "address" => $data["address"],
            "bday" => $data["bday"],
        ); 
    
        $flag = $database->insert ("customer", $insert_data);
        if($flag) {

            saveLog($database, "Save new customer with ID $id");

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Customer saved Succesfully!"
            ));
        }
        else {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if(isset($_GET["getAllCustomer"])) {
        $customer = getCustomers($database);
    
        echo json_encode($customer);
    }

    if(isset($_GET["getAllDiscounts"])) {
        $discounts = getDiscounts($database, false);
    
        echo json_encode($discounts);
    }
    
    if(isset($_GET["getTenderTypes"])) {
        $tender_types = getTenderTypes($database);
    
        echo json_encode($tender_types);
    }
    
    if(isset($_GET["getGridSize"])) {
        $grid = $grid_size;
    
        echo json_encode($grid);
    }
    
    if(isset($_GET["getPendingTransactions"])) {
        $log_date_id = $_GET["log_date_id"];
        $pending_transactions = getPendingTransactions($database, $log_date_id);
    
        echo json_encode($pending_transactions);
    }
    
    if(isset($_GET["voidAll"])) {
        $log_date_id = $_GET["date"];
        
        $update_data = Array (
            "is_suspended" => 0,
            "is_voided" => 1
        );

        $database->where ('is_suspended', 1);
        $database->where ('log_date_id', $log_date_id);
        $flag = $database->update ("orders", $update_data);

        if($flag) {

            saveLog($database, "Void All Transactions at ".date("Y-m-d h:i:s"));

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Transactions voided successfully!"
            ));
        }
        else {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if(isset($_GET["getEnvironment"])) {
        echo json_encode($is_online);
    }

    if(isset($_GET["getTotalAmount"])) {
        $transaction_number = $_GET["transaction_number"];
        
        $database->where("trn_number", $transaction_number);
        $order = $database->getOne("orders");
        
        $total = $order["amount_payable"] - $order["amount_payed"];

        echo json_encode(array("total" => $total));
    }
?>