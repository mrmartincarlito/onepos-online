<?php
require_once("../config.php");
require_once("../logs.php");
require_once("dao/Item.php");
require_once("functions.php");

require_once ('printer_connector/autoload.php');

use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

@$connector = new WindowsPrintConnector($PRINTER_PATH);
@$printer = new Printer($connector);


try{

    $trn_number = $_GET['trn_number'];
    $product_id = $_GET['product_id'];
    $branch_code = getBranchCode($database);

    $order = $database->rawQuery("Select * from `orders` where CAST(`trn_number` AS INT) = CAST('$trn_number' as INT) and `branch_code` = '$branch_code' ");
  
    if(empty($order)){
        echo json_encode(array(
            "error" => "true",
            "message" => "Cannot find transaction number"
        ));
        $printer -> close();
        return;
    }


    $printer -> setJustification(Printer::JUSTIFY_CENTER);
    $printer -> text(file_get_contents('receiptheader','r'));
    $printer -> feed();

    
    $printer -> setJustification(Printer::JUSTIFY_CENTER);
    $printer -> text("----------------------------------------\n");
    $printer -> text("R  E  F  U  N  D \n");
    
    
    $printer -> setEmphasis(true);
    $printer -> setJustification(Printer::JUSTIFY_CENTER);
    $printer -> text("----------------------------------------\n");
    $printer -> text("  QTY   DESCRIPTION               AMOUNT\n");
    $printer -> text("----------------------------------------\n");
    $printer -> setEmphasis(false);

    //ordered items iteration
    $database->where("item_id", $product_id);
    $database->where("order_id", $order[0]['id']);
    $items = $database->get("ordered_items");

    $total_qty = 0;

    foreach($items as $item){
        $qty = $item['quantity'];
        $descp = getItem($database, $item['item_id'])['long_descp'];
        $cost = number_format((float)$item['cost'], 2, '.', '');
        $total_cost = number_format((float)$item['total_cost'], 2, '.', '');
        $printer -> text ("$qty   $descp @ $cost        $total_cost\n");

        $total_qty += $item['quantity'];
    }

    $printer -> feed();
    $printer -> text("---------------------------------------\n");
    $printer -> text("SUBTOTAL: $total_qty ITEM(s) \n");
    $printer -> text("TOTAL:                          ".sprintf("%6.2f",$order[0]['amount_payable'])."\n");
    $printer -> text("---------------------------------------\n");
    $printer -> text("REFUND VALUE:                   ".sprintf("%6.2f",$order[0]['amount_payable'])."\n");
    
    $printer -> setJustification(Printer::JUSTIFY_CENTER);
    $printer -> text("---------------------------------------\n");
    $printer -> text("CASHIER: ".$order[0]['cashier_username']."\n");
    $printer -> text("TRANS#: ".$order[0]['trn_number']."  ORDER#: ".$order[0]['order_number']."\n");
    $printer -> text("LOGDATE#: ".getCurrentLogDate($database, $order[0]['log_date_id'])["open_log"]."\n");
    $printer -> text("OR: ".$order[0]['order_number']."\n");
    $printer -> text("DATETIME: ".$order[0]['date_time']."\n");
    $printer -> text("GUEST COUNT: ".$order[0]['guest_no']);

    $printer -> feed();
    $printer -> text(file_get_contents('receiptfooter','r'));

    echo json_encode(array(
        "error" => "false",
        "message" => "Printing receipt"
    ));

    saveLog($database, "Settle Transaction # $trn_number");

}catch (Exception $e) { 
    echo json_encode(array(
        "error" => "true",
        "message" => "Couldn't print to this printer: " . $e -> getMessage() . "\n"
    ));
}
$printer -> feed();
$printer -> feed();
$printer -> cut();
$printer -> close();

function getItem($database, $item_id){
    $database->where("id", $item_id);
    return $database->getOne("menu");
}