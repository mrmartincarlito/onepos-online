<?php
require_once "../config.php";
require_once "../logs.php";
require_once "functions.php";

if (isset($_POST['saveInventory'])) {
    $data = json_decode($_POST['saveInventory']);

    if (!isset($data->qty) || $data->qty == "") {
        echo json_encode(array(
            "type" => "error",
            "title" => "Error!",
            "text" => "Invalid Stocks!",
        ));
        return;
    }

    if ($data->type == "ACTUAL COUNT") {
       
        updateStockByActualQty($database, $data->menu_id, $data->qty);

        echo json_encode(array(
            "type" => "success",
            "title" => "Successful",
            "text" => "Updated Sales Stocks!",
        ));
        return;
    }

    if($data->type == "RECEIVING"){
        updateStockByReceiving($database, $data->menu_id, $data->qty);

        echo json_encode(array(
            "type" => "success",
            "title" => "Successful",
            "text" => "Updated Sales Stocks!",
        ));
        return;
    }

}

if(isset($_POST['updateItemsByBatch'])){
    $process = $_POST['process'];
    $values = json_decode($_POST['updateItemsByBatch']);

    if($process == "RECEIVING"){
        foreach($values as $value){
            updateStockByReceiving($database, $value->item_id, $value->input_qty);
        }
    }

    if($process == "SPOILAGE"){
        foreach($values as $value){
            updateStockBySpoilage($database, $value->item_id, $value->input_qty);
        }
    }

    echo json_encode(array(
        "type" => "success",
        "title" => "Successful",
        "text" => "You just updated stocks Stocks!",
    ));
}

function updateStockByReceiving($database, $productId, $qty){

    insertMenuJournalForReceiving($database, $productId, $qty);

    updateQuantityOnMenu($database, $qty, $productId, "+");
}

function updateStockBySpoilage($database, $productId, $qty){

    insertMenuJournalForReceiving($database, $productId, $qty);

    updateQuantityOnMenu($database, $qty, $productId, "-");
}

function updateStockByActualQty($database, $productId, $qty)
{
    insertMenuJournalForActualCount($database, $productId, $qty);

    updateQuantityOnMenu($database, $qty, $productId, "+", true);
}

function checkInventoryIfEnough($database, $productId, $quantity)
{
    $item = getMenu($database, $productId);

    if ($item['quantity'] == 0 ) {

        return array(
            "error" => true,
            "message" => "Out of Stock",
        );
    }

    if ($item['quantity'] < $quantity) {
        return array(
            "error" => true,
            "message" => "Quantity is greater than the input. Current stock(s) " . $item['quantity'],
        );
    }

    return array(
        "error" => false
    );
}

function updateQuantityOnMenu($database, $quantity, $productId, $process = "-", $isActual = false)
{

    if($isActual){
        $database->rawQuery("update menu set quantity = $quantity where id = $productId");
    }else{
        $database->rawQuery("update menu set quantity = quantity $process $quantity where id = $productId");
    }
}

function updateItemStocks($database, $productId, $quantity, $orderId, $func = "less")
{

    insertMenuJournalForSales($database, $productId, $quantity, $orderId, $func);

    if($func == "more") {
        updateQuantityOnMenu($database, $quantity, $productId, "+");
    }
    else {
        updateQuantityOnMenu($database, $quantity, $productId, "-");
    }
}


function voidItemInventory($database, $product)
{

    $orderedItem = getOneOrderItem($database, $product['product_id']);

    insertMenuJournalForVoid($database, $orderedItem);

    updateQuantityOnMenu($database, $orderedItem['quantity'], $orderedItem['item_id'], "+");
}

//------MENU JOURNAL 

function insertMenuJournalForSales($database, $productId, $quantity, $orderId, $func = "less")
{

    $item = getMenu($database, $productId);

    $logDate = getCurrentLogDate($database);

    if($func == "more") {
        $database->insert("menu_journal", array(
            "menu_id" => $productId,
            "order_id" => $orderId,
            "beg_stocks" => $item['quantity'],
            "sales_stocks" => $quantity,
            "end_stocks" => $item['quantity'] + $quantity,
            "log_date_id" => $logDate['id'],
            "username" => $_SESSION['username']
        ));
    }
    else {
        $database->insert("menu_journal", array(
            "menu_id" => $productId,
            "order_id" => $orderId,
            "beg_stocks" => $item['quantity'],
            "sales_stocks" => $quantity,
            "end_stocks" => $item['quantity'] - $quantity,
            "log_date_id" => $logDate['id'],
            "username" => $_SESSION['username']
        ));
    }
}

function insertMenuJournalForVoid($database, $orderedItem)
{

    $item = getMenu($database, $orderedItem['item_id']);

    $logDate = getCurrentLogDate($database);

    $database->insert("menu_journal", array(
        "menu_id" => $orderedItem['item_id'],
        "order_id" => $orderedItem['order_id'],
        "beg_stocks" => $item['quantity'],
        "returned_stocks" => $orderedItem['quantity'],
        "end_stocks" => $item['quantity'] + $orderedItem['quantity'],
        "log_date_id" => $logDate['id'],
        "username" => $_SESSION['username']
    ));

}

function insertMenuJournalForActualCount($database, $productId, $qty)
{

    $item = getMenu($database, $productId);

    $logDate = getCurrentLogDate($database);

    $database->insert("menu_journal", array(
        "menu_id" => $productId,
        "order_id" => 1,
        "beg_stocks" => $item['quantity'],
        "actual_stocks" => $qty,
        "end_stocks" => $qty,
        "log_date_id" => $logDate['id'],
        "username" => $_SESSION['username']
    ));

}

function insertMenuJournalForReceiving($database, $productId, $qty){
    $item = getMenu($database, $productId);

    $logDate = getCurrentLogDate($database);

    $database->insert("menu_journal", array(
        "menu_id" => $productId,
        "order_id" => 2,
        "beg_stocks" => $item['quantity'],
        "receiving_stocks" => $qty,
        "end_stocks" => $item['quantity'] + $qty,
        "log_date_id" => $logDate['id'],
        "username" => $_SESSION['username']
    ));
}
