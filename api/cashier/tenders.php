<?php

require_once "../config.php";
require_once "../logs.php";
require_once "functions.php";

define("TABLE", "ordered_tenders");

function saveTenders($trn_number = "", $tender_id = "", $tender_amount = 0,$tender_change = 0, $account_name = "", $account_number = "", $approval_number = "", $bank = "", $remarks = ""){
    
    global $database;

    $insert = array(
        "trn_number" => $trn_number,
        "tender_id" => $tender_id,
        "amount_payed" => $tender_amount,
        "amount_change" => $tender_change,
        "account_name" => $account_name,
        "account_number" => $account_number,
        "approval_number" => $approval_number,
        "bank" => $bank,
        "remarks" => $remarks
    );

    $database->insert(TABLE, $insert);
}
