<?php

require_once("../config.php");
require_once("../logs.php");
require_once("functions.php");

if(isset($_GET['getMenuJournalByMenuId'])){
    $itemId = $_GET['getMenuJournalByMenuId'];

    $database->orderBy("date_time","DESC");
    $database->where("menu_id", $itemId);
    $journal = $database->get("menu_journal");

    echo json_encode($journal);
}