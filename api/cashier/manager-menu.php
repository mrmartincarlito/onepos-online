<?php
    require_once("../config.php");
    require_once("../logs.php");
    require_once("functions.php");
    require_once("inventory.php");

    if(isset($_GET['suspendTransaction'])) {
        $current_transaction = $_GET["current_transaction"];

        $update_data = Array (
            "is_suspended" => 1,
        );

        $database->where ('trn_number', $current_transaction);
        $flag = $database->update ("orders", $update_data);

        if($flag) {

            saveLog($database, "Suspend Transaction # $current_transaction");

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Transaction succesfully suspended!"
            ));
        }
        else {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if(isset($_GET["getSuspendedTransactions"])) {
        $suspended_value = 1;

        $branch_code = getBranchCode($database);

        $database->where ('branch_code', $branch_code);
        $database->where ('is_suspended', $suspended_value);
        $transactions = $database->get("orders");
    
        echo json_encode($transactions);
    }

    if(isset($_POST['recallTransaction'])) {
        $transaction_number = $_POST["transaction_number"];

        $order = getOneTransaction($database, $transaction_number);
        $branch_code = getBranchCode($database);

    
        if(!empty($order)) {
            if($order["is_suspended"] == 0) {
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => "Invalid Transaction Number!"
                ));

                return;
            }
    
            $update_data = Array (
                "is_suspended" => 0,
            );
        
            $database->where ('branch_code', $branch_code);
            $database->where ('trn_number', $transaction_number);
            $flag = $database->update ("orders", $update_data);
        
            if($flag) {

                saveLog($database, "Recall Transaction # $transaction_number");

                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "text" => "Transaction succesfully recalled!"
                ));
            }
            else {
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }
        }
        else {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => "Invalid Transaction Number!"
            ));
        }
    }

    if(isset($_POST['itemVoid'])) {
        $data = $_POST["data"];

        $update_data = Array (
            "is_voided" => 1,
        );

        $database->where ('id', $data["product_id"]);
        $flag = $database->update ("ordered_items", $update_data);

        if($flag) {

            $database->where ('id', $data["product_id"]);
            $ordered_item = $database->getOne ("ordered_items");
    
            if(!updateTotalAmount($database, $ordered_item["order_id"])) {
    
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
                return;
            }

            saveLog($database, "Void ItemID {$data["product_id"]} ");

            voidItemInventory($database, $data);

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Item succesfully voided!"
            ));
        }
        else {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if(isset($_POST['voidTransaction'])) {
        $transaction_number = $_POST["transaction_number"];
    
        $update_data = Array (
            "is_voided" => 1,
        );
    
        $database->where ('trn_number', $transaction_number);
        $flag = $database->update ("orders", $update_data);
    
        if($flag) {

            saveLog($database, "Void Transaction # $transaction_number ");

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Transaction succesfully voided!"
            ));
        }
        else {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if(isset($_POST["cashDeposit"])) {
        $cash_fund = $_POST["cash_fund"];
        $id = idGenerator();
        $date = date("Y-m-d h:i:s");
        $user_id = getUserId($database, $_SESSION["username"]);
        $sales_amount = getSalesAmount($database);
        $branch_code = getBranchCode($database);
        
        if($cash_fund < 0) {
            echo json_encode(Array (
                "type" => "warning",
                "title" => "Error!",
                "text" => "Cannot deposit negative value"
            ));
        }
        else {
            $insert_data = Array (
                "id" => $id,
                "cash_fund_value" => $cash_fund,
                "user_id" => $user_id,
                "branch_code" => $branch_code,
                "date_added" => $date
            ); 
        
            $flag = $database->insert ("cash_fund", $insert_data);
        
            if($flag) {
                $new_sales_amount = $sales_amount["sales_amount"] + $cash_fund;
            
                $updateData = Array (
                    "sales_amount" => $new_sales_amount
                );
            
                $database->where ('id', $sales_amount["id"]);
                $bool = $database->update ("sales_amount", $updateData);
        
                if($bool) {
                    echo json_encode(Array (
                        "type" => "success",
                        "title" => "Successful!",
                        "text" => "Successfully deposited ₱".$cash_fund
                    ));
                }
                else {
                    echo json_encode(Array (
                        "type" => "error",
                        "title" => "Error!",
                        "text" => $database->getLastError()
                    ));
                }
            }
            else {
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }
        }
    }

    if(isset($_POST["cashWithdraw"])) {
        $cash_amount = $_POST["cash_amount"];
        $id = idGenerator();
        $date = date("Y-m-d h:i:s");
        $user_id = getUserId($database, $_SESSION["username"]);
        $sales_amount = getSalesAmount($database);
        $branch_code = getBranchCode($database);
        
        if($cash_amount < 0) {
            echo json_encode(Array (
                "type" => "warning",
                "title" => "Error!",
                "text" => "Cannot withdraw negative value"
            ));
        }
        else {
            if($sales_amount["sales_amount"] < $cash_amount) {
                echo json_encode(Array (
                    "type" => "warning",
                    "title" => "Error!",
                    "text" => "Cannot withdraw more than the amount in cashier"
                ));
            }
            else {
                $insert_data = Array (
                    "id" => $id,
                    "cash_amount" => $cash_amount,
                    "user_id" => $user,
                    "branch_code" => $branch_code,
                    "date_added" => $date
                ); 
            
                $flag = $database->insert ("cash_withdrawal", $insert_data);
            
                if($flag) {
                    $new_sales_amount = $sales_amount["sales_amount"] - $cash_amount;
                
                    $updateData = Array (
                        "sales_amount" => $new_sales_amount
                    );
                
                    $database->where ('id', $sales_amount["id"]);
                    $bool = $database->update ("sales_amount", $updateData);
            
                    if($bool) {
                        echo json_encode(Array (
                            "type" => "success",
                            "title" => "Successful!",
                            "text" => "Successfully withdrawn ₱".$cash_amount
                        ));
                    }
                    else {
                        echo json_encode(Array (
                            "type" => "error",
                            "title" => "Error!",
                            "text" => $database->getLastError()
                        ));
                    }
                }
                else {
                    echo json_encode(Array (
                        "type" => "error",
                        "title" => "Error!",
                        "text" => $database->getLastError()
                    ));
                }
            }
        }
    }

    if(isset($_GET["searchTransaction"])) {
        $transaction_number = $_GET["transaction_refund"];

        $order = getOneSettledTransaction($database, $transaction_number);
    
        if(empty($order)) {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => "Transaction not found!"
            ));
        }
        else {
            $order_list = getOrderList($database, $order[0]["id"]);
        
            if(count($order_list["items"]) > 0) {
                $order_list["order"] = $order[0];
                echo json_encode($order_list);
            }
            else {
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => "Transaction not found!"
                ));
            }
        }
    }

    if(isset($_POST["refund"])) {
        $data = $_POST["data"];

        $order = getOneSettledTransaction($database, $data["transaction_number"]);
    
        $update_data = Array (
            "is_refunded" => 1
        );
    
        $database->where("order_id", $order[0]["id"]);
        $database->where("item_id", $data["product_id"]);
        $database->where("is_refunded", 0);
        $database->where("is_voided", 0);
        $order_list = $database->getOne("ordered_items");
    
        if(isset($order_list)) {
            $database->where("order_id", $order[0]["id"]);
            $database->where("item_id", $data["product_id"]);
            $flag = $database->update ("ordered_items", $update_data);
    
            if($flag) {

                saveLog($database, "Refunded Order # {$order[0]["id"]} and Item ID {$data["product_id"]} ");

                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "text" => "Refund Successful!"
                ));
            }
            else {
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
            }
        }
        else {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => "Item does not exist or may already be refunded!"
            ));
        }
    }

    if(isset($_POST["priceOverride"])) {
        $data = $_POST["data"];
    
        $database->where("id", $data["order_item_id"]);
        $order_list = $database->getOne("ordered_items");

        $total_cost = computeTotalCost($data["price"], $order_list["quantity"]);
    
        $update_data = Array (
            "cost" => $data["price"],
            "total_cost" => $total_cost,
            "is_overridden" => 1,
            "override_counter" => isset($order_list["override_counter"]) ? $order_list["override_counter"] + 1 : 1,
            "original_price" => $order_list["original_price"] == 0.00 ? $order_list["cost"] : $order_list["original_price"]
        );
    
        $database->where("id", $data["order_item_id"]);
        $flag = $database->update("ordered_items", $update_data);
    
        if($flag) {

            if(!updateTotalAmount($database, $order_list["order_id"])) {
    
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => $database->getLastError()
                ));
                return;
            }

            saveLog($database, "Price Overriden from {$order_list['total_cost']} to $total_cost on Order ID {$order_list['order_id']}");

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Price Override Successful!"
            ));
        }
        else {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

?>