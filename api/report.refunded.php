<?php
require_once "config.php";
require_once "logs.php";

define("ORDERS", "orders");
define("ORDERED_ITEMS", "ordered_items");
define("MENU", "menu");

/**
 * GET Method
 * ?getAllReportByMenuRefunded
 * &branch_code = {value}
 * &start = {dd/mm/yyyy}
 * &end = {dd/mm/yyyy}
 */

if (isset($_GET['getAllReportByMenuRefunded'])) {

    $SETTLED = $ISVOIDED = $ISREFUNDED = 1;

    //when start and end date isset
    if(isset($_GET['start']) && isset($_GET['end'])){
        $end_date = explode("-", $_GET['end']);
        $end_date[2] = $end_date[2] + 1;
        $end_date = implode("-", $end_date);
        $database->where("date_time", array($_GET['start'], $end_date), "BETWEEN");
    }

    $database->where("is_settled", $SETTLED);
    $orders = $database->get(ORDERS);

    $response = array();

    foreach ($orders as $order) {

        //when branch_code isset
        if (isset($_GET['branch_code'])) {
            $branchCode = $_GET['branch_code'];
            $itemsInBranch = getItemsByBranchCode($database, $branchCode);
            $database->where("item_id", $itemsInBranch, "IN");
        }

        $database->where("is_refunded", $ISREFUNDED);
        $database->where("order_id", $order['id']);
        $database->join(MENU." m", "m.id=o.item_id", "LEFT");
        $items = $database->get(ORDERED_ITEMS." o", null, "o.*, m.name");

        if (!empty($items)) {
            $orderedList['transaction']['order'] = $order;
            $orderedList['transaction']['items'] = $items;

            array_push($response, $orderedList);
        }

    }

    saveLog($database,"View Item Refunded Report");

    echo json_encode($response);
}
