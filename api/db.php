<?php

//Database Configuration Properties
$SQL_SERVERADDRESS = "localhost";
$SQL_USERNAME = "root";
$SQL_PASSWORD = "";
$SQL_DATABASE = "onepos-db-local";

if(@(new mysqli($SQL_SERVERADDRESS, $SQL_USERNAME, $SQL_PASSWORD, $SQL_DATABASE))->connect_errno){
    echo "Cannot connect to database, please contact server administrator.";
    exit;
}

$database = new MysqliDb (
    Array (
    'host' => $SQL_SERVERADDRESS,
    'username' => $SQL_USERNAME, 
    'password' => $SQL_PASSWORD,
    'db'=> $SQL_DATABASE,
    'port' => 3306,
    'prefix' => '',
    'charset' => 'utf8'
    )
);

// SQL server connection information (for datatable)
// if needed to connect to external MS SQL Database
$sql_details = array(
    'user' => $SQL_USERNAME,
    'pass' => $SQL_PASSWORD,
    'db'   => $SQL_DATABASE,
    'host' => $SQL_SERVERADDRESS
);