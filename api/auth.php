<?php
require_once("config.php");
require_once("logs.php");

function getLoggedUserDetails($database){
    $username = @$_SESSION["username"];

    $cols = Array ("s.id", "s.name", "s.username", "s.role", "s.branch_code", "a.*", "s.id as aID");
    $database->where ("username", $username);
    $database->join ("access_levels a", "s.role=a.ID", "LEFT");
    $userDB = $database->getOne("accounts s", $cols);
    return json_encode($userDB);
}

function setLoggedUser($username){
    $_SESSION["username"] = $username;
}

if(isset($_GET["getLoggedUserDetails"])){
    echo getLoggedUserDetails($database);
}

if(isset($_POST["data"]) && preg_match('/auth.php/', $actual_link)) {
    $postData = json_decode($_POST["data"]);
    $action = $postData->action;
    $data = json_decode($postData->data);

    if($action == "login"){
        $database->where ("username", $data->username);
        $userDB = $database->getOne ("accounts");
        if($userDB['username'] == $data->username && password_verify($data->password,$userDB['password'])){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Logged in succesfully!"
            ));
            setLoggedUser($userDB['username']);
            saveLog($database,"Logged in");
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => "Wrong username or password!"
            ));
        }
    }
}

if(isset($_POST['cashier_auth'])){
    $credentials = json_decode($_POST['cashier_auth']);

    $columns = Array("s.username", "s.password", "a.cashier_cash_fund", "a.cashier_recall_transaction", "a.cashier_refund",
                    "a.cashier_cash_withdrawal", "a.cashier_suspended_transaction", "a.cashier_x_reading",
                    "a.cashier_y_reading", "a.cashier_z_reading", "a.cashier_void_current_transaction", "cashier_item_void",
                    "a.cashier_electric_journal", "a.cashier_price_override");

    $database->where ("username", $credentials->username);
    $database->join ("access_levels a", "s.role=a.ID", "LEFT");
    $userDB = $database->getOne("accounts s", $columns);

    if($userDB['username'] == $credentials->username && password_verify($credentials->password,$userDB['password'])){
        $_SESSION["manager"] = $credentials->username;
        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Logged in Manager succesfully!",
            "access" => $userDB
        ));

        $_SESSION["cashier-username"] = $userDB['username'];
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "Wrong cashier manager credentials"
        ));
    }


}



?>