<?php
require_once("config.php");
require_once("logs.php");

define("TABLE_NAME", "tender_types");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->action == "add"){
        $insertData = Array (
            "description" => $data->description,
            "allow_change" => $data->allow_change,
            "active_tag" => $data->active_tag,
            "required_detail_tag" => $data->required_detail_tag
        );  

        $id = $database->insert (TABLE_NAME, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Tender Type added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->action == "edit"){

        $updateData = Array (
            "description" => $data->description,
            "allow_change" => $data->allow_change,
            "active_tag" => $data->active_tag,
            "required_detail_tag" => $data->required_detail_tag
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (TABLE_NAME, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Tender Type details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->action == "delete"){

        $database->where ('id', $data->modifyId);
        $id = $database->delete (TABLE_NAME);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Tender Type deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->action == "add"){
        saveLog($database,"{$data->action} TENDER TYPE: {$data->description}");
    }else{
        saveLog($database,"{$data->action} TENDER TYPE ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $tender_types = $database->get(TABLE_NAME);
    echo json_encode($tender_types);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $tender_types = $database->getOne(TABLE_NAME);
    echo json_encode($tender_types);
}