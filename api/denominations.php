<?php
require_once("config.php");
require_once("logs.php");

define("TABLE_NAME", "denominations");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->action == "add"){
        $insertData = Array (
            "description" => $data->description,
            "equivalent_value" => $data->equivalent_value
        );  

        $id = $database->insert (TABLE_NAME, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Denomination added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->action == "edit"){

        $updateData = Array (
           "description" => $data->description,
            "equivalent_value" => $data->equivalent_value
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (TABLE_NAME, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Denomination details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->action == "delete"){

        $database->where ('id', $data->modifyId);
        $id = $database->delete (TABLE_NAME);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Denomintaion deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->action == "add"){
        saveLog($database,"{$data->action} DENOMINATION: {$data->description}");
    }else{
        saveLog($database,"{$data->action} DENOMINATION ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $denominations = $database->get(TABLE_NAME);
    echo json_encode($denominations);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $denominations = $database->getOne(TABLE_NAME);
    echo json_encode($denominations);
}