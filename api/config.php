<?php
$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

require_once ('dbhelper/MysqliDb.php');
require_once("db.php");
require_once("ssp.class.php");
require_once("auth.php");

session_start();
date_default_timezone_set('Asia/Manila');

//
$terminal_code = "001";

//category and products layout
$grid_size = 3;

//Environment
$is_online = 1;

//Reprint configuration
$reprint_enabled = 0;

//PrinterProperties
$PRINTER_PATH = "GTPrinter";
$PRINTER_PORT = "";

$os_details = php_uname();


if(!isset($_SESSION['username']) && !preg_match('/login.php/', $actual_link)&& !preg_match('/auth.php/', $actual_link)){
    header('Location: ./login.php');
}

if(isset($_SESSION['username']) && preg_match('/login.php/', $actual_link)){
    header('Location: index.php');
}

if(isset($_SESSION['username']) && (!preg_match('/login.php/', $actual_link) || !preg_match('/auth.php/', $actual_link))){
    $userDetailsJSON = json_decode(getLoggedUserDetails($database));

    foreach ($userDetailsJSON as $key => $val) {
        if(preg_match('/access/', $key)){
            $url = "access_".substr($actual_link,strpos($actual_link,"?")+1);

            if($val != 1 && preg_match("/{$key}/", $url)){
                header('Location: ?not_allowed');
            }
        }
    }
} 

//get log date
function getLogDate($database) {
    $database->orderby ("id");
    $log_date = $database->getOne("log_date");

    $result = Array (
        "date" => $log_date["open_log"],
        "id" => $log_date["id"]
    );

    return $result;
}

function getItemsByBranchCode($database, $branchCode)
{
    $database->where("branch_code", $branchCode);
    $items = $database->get(MENU);

    $itemsInBranch = array();
    foreach ($items as $item) {
        array_push($itemsInBranch, $item['id']);
    }

    return $itemsInBranch;
}

function getBranchCode($database) {
    $database->where("username", $_SESSION["username"]);
    $accounts = $database->getOne("accounts");

    return $accounts["branch_code"];
}

?>