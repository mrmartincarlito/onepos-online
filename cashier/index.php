<?php
	require_once("../api/config.php");

    $contents = json_decode(file_get_contents("content.json"), true);
?>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="design/index.css" />
	<link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/icon.png">
	<script src="https://kit.fontawesome.com/8b6b1fa9e8.js" crossorigin="anonymous"></script>
    <script src = "scripts/jquery-3.5.1.min.js" ></script>
    <script src = "scripts/jquery-ui.min.js" ></script>
    <script src = "scripts/notification/notification-too.js" ></script>
    <script src = "scripts/script.js" ></script>
    <script src = "scripts/function.js" ></script>
    <script src = "scripts/pagination.js" ></script>
    <script src = "scripts/ajax.js" ></script>
    <script src = "scripts/receipts/receipts.js" ></script>
    <script src = "scripts/receipts/readings.js" ></script>

    <?php
      foreach($contents as $content){
        echo "<script src='{$content['script']}'></script>";
      }
    ?>


</head>
<body>
    <div class="parent" id = "parent">
        <div class="div1 dark-washed-green" label="header-logo">
            <img src = "design/icon.png" class = "img-icon" style = "cursor: pointer;" onclick = "goAdmin();">
            <button id = "POSName" class = "label-3em dark-washed-green" style = "border: 2px solid rgba(35, 132, 197, 1); cursor: pointer;" onclick = "resetPage();">ONEPOS - ONLINE</button>
            <button id = "greetings" class = "label-3em label-right" disabled></button>
        </div>
        <div class="div2 gray" label="navigation-label-header">
            <button id = "transaction-number" class = "button black-font" disabled>TRN: NA</button>
            <button id = "order-number" class = "button black-font" disabled>ORDR #: NA</button>
            <button id = "guest-number-count" style = "display: none;" class = "button black-font" disabled>GUEST NO: 0</button>
            <button id = "terminal-number" class = "button black-font" disabled>TERMINAL NO: 1</button>
        </div>
        <div class="div3" label="manager-menu" style="overflow-y: scroll;">
            <div style = "display: none;" id = "div3">
            </div>
            <?php include $contents["manager-menu"]["path"] ?>
        </div>
        <div class="background" label="manager-menu" style="overflow-y: scroll;">
            <div style = "display: none;" id = "div3">
            </div>
            <?php include $contents["manager-menu"]["path"] ?>
        </div>
        <div class="div4 white right-text" label="log-date-counter">
            <label class="label" style = "float: left !important;" id = "branch_code"></label>
            <label class="label" id = "logdate"></label>
            <label class="label" id = "logtime" style = "padding-right: 10px;"></label>
        </div>
        <div class="div5 dark-washed-green" label="total-amount">
            <button class="label-5em w100 h100 green-font right-text" style = "cursor: text;" id="totalAmount" disabled>0.00</button>
        </div>
        <div class="div6 black" label="order-items">
            <?php include $contents["order-items"]["path"] ?>
        </div>
        <div class="div7 gray" label="menu-controls">
            <?php include $contents["menu-controls"]["path"] ?>
        </div>
        <div class ="div8 white border-2px center-text" style = "display: none;" label="guess-controls">
            <?php include $contents["guess-controls"]["path"] ?>
        </div>
        <div class = "div11 transparent-50 border-2px" style = "display: none;" label="gray-out-background">

        </div>
        <div class = "div9 white border-2px" id = "logout-prompt" style = "display: none;" label="logout-div">
          <button class = "button-disabled w100 h70 white">Are you sure to logout?</button>
          <button class = "button w50 h30 green border-right-2px border-top-2px" id = "logout-yes" onclick = "logout();">Yes</button>
          <button class = "button w50 h30 red white-font border-right-2px border-top-2px" id = "logout-no" onclick = "hideForm('div9');">No</button>
        </div>
        <div class = "div10 white border-2px center-text" style = "display: none;" label="log-date-opening-div">
            <button class = "w100 h20 dark-washed-green white-font center-text font" disabled>ENTER MANAGER CREDENTIALS</button>
            <input type = "text" class = "input-text w80 h13 font" style = "margin-bottom: 4%; margin-left: 8%; margin-top: 2%;" placeholder = "Username" id="username" tabindex="1">
            <input type = "password" class = "input-text w80 h13 font" style = "margin-bottom: 4%; margin-left: 8%;"  placeholder = "Password" id="password" tabindex="2">
            <button class = "button w37 h20 green border-2px" style = "margin-left: 8%;" id = "login-manager-menu" onclick = "loginManagerCredentials();" tabindex="3">Continue</button>
            <button class = "button w37 h20 red white-font border-2px" style = "margin-left: 8%;" id = "login-cancel" onclick = "hideForm('div10');" tabindex="4">Cancel</button>
        </div>
        <div class ="div12 white border-2px center-text" style = "display: none;" label="quantity-controls">
            <?php include $contents["quantity-controls"]["path"] ?>
        </div>
        <div class = "div13 white border-2px" id = "dinein-prompt" style = "display: none;" label = "dinein-takeout-div">
          <button class = "button-disabled w100 h70 white">Dine in or takeout?</button>
          <button class = "button w50 h30 green border-right-2px border-top-2px" id = "dinein_button" onclick = "service(this);">DINE IN</button>
          <button class = "button w50 h30 red white-font border-right-2px border-top-2px" id = "takeout_button" onclick = "service(this);">TAKEOUT</button>
        </div>
        <div class = "div15 white border-2px" style = "display: none;" label="customer">
            <?php include $contents["customer"]["path"] ?>
        </div>
        <div class = "div16 white border-2px center-text" style = "display: none;" label="special-request">
            <?php include $contents["special-request"]["path"] ?>
        </div>
        <div class = "div17 white border-2px center-text" style = "display: none;" label="discount">
            <?php include $contents["discount-menu"]["path"] ?>
        </div>
        <div class = "div18 white border-2px center-text" style = "display: none;" label="cash fund" >
            <button class = " label-5em w100 h20 center-text font3" style="margin-top: 2%;" disabled> CASH FUND </button>
            <input type = "number" class = "input-text w90 h20 right-text font2" style = "margin-top: 8%; margin-bottom: 4%; margin-left: 3.5%;" placeholder = "Enter Amount of Money " id="cashfund" oninput="checkValue(this, 'cash_fund');"> <br>
            <button class = "button center-text w40 h20 red white-font border-2px" style="margin-left: 5%; margin-right: 5%; margin-top: 5%;" onclick="hideForm('div18');"> CANCEL </button>
            <button class = "button center-text w40 h20 green border-2px" style="margin-left: 5%; margin-right: 5%; margin-top: 5%;" disabled id = "cash_fund_done" onclick = "cashDeposit();"> DONE </button>
        </div>
        <div class = "div19 white border-2px center-text" style="display:none;" label="Cash Withdrawal" >
            <button class = " label-5em w100 h20 center-text font3" style="margin-top: 2%;" disabled> Cash Withdrawal </button>
            <input type = "number" class = "input-text w90 h20 right-text font2" style = "margin-top: 8%; margin-bottom: 4%; margin-left: 3.5%;" placeholder = "Enter Amount of Money " id="cash_withdraw" oninput = "checkValue(this, 'cash_deposit');" > <br>
            <button class = "button center-text w40 h20 red white-font border-2px" style="margin-left: 5%; margin-right: 5%; margin-top: 5%;" onclick="hideForm('div19');"> CANCEL </button>
            <button class = "button center-text w40 h20 green border-2px" style="margin-left: 5%; margin-right: 5%; margin-top: 5%;" disabled id = "cash_deposit_done" onclick = "cashWithdraw();"> DONE </button>
        </div>
        <div class = "div20 white border-2px center-text" style="display:none;" label="Refund Transaction" >
        <!-- can be moved to another folder -->
            <div class = "innerdiv w100 h100">
                <div class = "innerdiv w40 h100 transparent border-right-2px">
                    <button class = " label-5em dark-washed-green white-font w100 center-text font2" style="height: auto; margin-top: 2%;" disabled> REFUND </button>
                    <input type = "number" id ="transaction_refund" class = "input-text" style = "width: 82%; margin-left: 6.66%; margin-top: 20px;" placeholder="Transaction Number" oninput = "checkValue(this, 'transaction_refund');">
                    <button type = "button" class = "button w40 red white-font border-2px" style = "margin-left: 6.66%; margin-top: 10px;" onClick = "hideForm('div20');">Close</button>
                    <button type = "button" class = "button w40 green border-2px" style = "margin-left: 6.66%; margin-top: 10px;" onClick = "searchTransaction('1');" id = "transaction_refund_done" disabled>Search</button>
                    <input type = "hidden" id = "refund_id"/>
                </div>
                <div class = "innerdiv w59 h100 transparent">
                    <div class = "innerdiv w100 h80">
                        <table class = "w90" style = "border-collapse: collapse; margin-top: 20px; margin-left: 5px;" id = "refund-order-table">
                            <thead class = "dark-washed-green white-font" style = "padding-top: 12px; padding-bottom: 12px;">
                                <tr>
                                    <th class = "border-1px" style = "padding: 8px;">Transaction</th>
                                    <th class = "border-1px" style = "padding: 8px;">Product</th>
                                    <th class = "border-1px" style = "padding: 8px;">Quantity</th>
                                    <th class = "border-1px" style = "padding: 8px;">Total Cost</th>
                                    <th class = "border-1px" style = "padding: 8px;">Date</th>
                                </tr>
                            </thead>
                            <tbody id = "refund-order-table-content">
                
                            </tbody>
                        </table>
                        <div id = "refund-order-container" class = "h20" style = "margin-top: 10px;"></div>
                    </div>
                    <div class = "innerdiv w100 h20">
                        <button type = "button" class = "button w40 h50 green border-2px" style = "margin-left: 8%; margin-top: 10px;" onClick = "refund();" id = "refund-apply" disabled>Refund</button>
                    </div>
                </div>
            </div>
        <!-- ========================================================================================== -->
        </div>
        <div class = "div24 white border-2px center-text" style="display:none;" label="Tender Form">
            <div class = "innerdiv w100 h70">
                <input type = "hidden" id = "selected_tender_type" >
                <input type = "hidden" id = "selected_tender_required_detail_tag" >
                <input type = "hidden" id = "selected_tender_allow_change" >
                <input type = "hidden" id = "selected_tender_id">
                <button class = " label-5em dark-washed-green white-font w100 center-text font2" style="height: auto; margin-top: 2%; margin-bottom: 10px;" disabled>Tender Type</button>
                <table class = "w80 h80" style = "border-collapse: collapse; margin-top: 20px; margin-left: 10%;" id = "tender-type-table">
                    <thead class = "dark-washed-green white-font" style = "padding-top: 12px; padding-bottom: 12px;">
                        <tr>
                            <th class = "border-1px" style = "padding: 8px;">Description</th>
                        </tr>
                    </thead>
                    <tbody id = "tender-type-table-content">
                
                    </tbody>
                </table>
            </div>
            <div class = "innerdiv w100 h10" id = "tender-type-container"></div>
            <div class = "innerdiv w100 h20">
                <button type = "button" class = "button w40 h60 red white-font border-2px" style = "margin-left: 8%; margin-top: 20px;" onClick = "hideForm('div24');">Close</button>
                <button type = "button" class = "button w40 h60 green border-2px" style = "margin-left: 8%; margin-top: 20px;" onClick = "showSettle('div21');" id = "tender-apply" disabled>Proceed</button>
            </div>
        </div>
        <div class = "div21 white border-2px center-text" style="display:none;" label="Settle Form" >
        <!-- can be moved to another folder -->
            <div class = "innerdiv w100 h100">
                <div class = "innerdiv w40 h100 transparent border-right-2px" id = "settle_left">
                    <button class = " label-5em dark-washed-green white-font w100 center-text font2" style="height: auto; margin-top: 2%;" disabled> SETTLE </button>
                    <button class = " label-5em dark-washed-green white-font w100 center-text font2" style="height: auto; margin-top: 2%;" disabled id = "settle_total_amount"></button>
                    <input type = "number" id ="settle_amount" class = "input-text w80 right-text font2" style = "margin-left: 8%; margin-top: 20px;" placeholder="Enter amount">
                    <button type = "button" class = "button w40 red white-font border-2px" style = "margin-left: 8%; margin-top: 10px;" onClick = "hideForm('div21');">Cancel</button>
                    <button type = "button" class = "button w40 green border-2px" style = "margin-left: 8%; margin-top: 10px;" onClick = "settle('cash');" id = "transaction-recall">Settle</button>
                </div>
                <div class = "innerdiv w59 h100 transparent" id = "settle_right">
                    <div class = "innerdiv w100 h100">
                        <h1>Denominations</h1>
                        <table class = "w100 h70" style = "border-collapse: collapse; margin-top: 20px; margin-left: 5px;" id = "settle-order-table">
                            <tbody id = "denomination-table-content">
                                <!--<tr>
                                    <td class = "white-hover border-2px font2" style = "padding: 4px; cursor: pointer; background: url(design/tender/1.jpg);  background-size: 60% 100%; color: rgba(50, 50, 50, 0); background-repeat: no-repeat;" onclick = "appendSettleValue('1');">1.00</td>
                                    <td class = "white-hover border-2px font2" style = "padding: 4px; cursor: pointer; background: url(design/tender/5.jpg); background-size: 60% 100%; color: rgba(50, 50, 50, 0); background-repeat: no-repeat;" onclick = "appendSettleValue('5');">5.00</td>
                                    <td class = "white-hover border-2px font2" style = "padding: 4px; cursor: pointer; background: url(design/tender/10.jpg); background-size: 60% 100%; color: rgba(50, 50, 50, 0); background-repeat: no-repeat;" onclick = "appendSettleValue('10');">10.00</td>
                                </tr>
                                <tr>
                                    <td class = "white-hover border-2px font2" style = "padding: 4px;cursor: pointer; background: url(design/tender/20.jpg); background-size: 100% 100%; color: rgba(50, 50, 50, 0);" onclick = "appendSettleValue('20');">20.00</td>
                                    <td class = "white-hover border-2px font2" style = "padding: 4px; cursor: pointer; background: url(design/tender/50.jpg); background-size: 100% 100%; color: rgba(50, 50, 50, 0);" onclick = "appendSettleValue('50');">50.00</td>
                                    <td class = "white-hover border-2px font2" style = "padding: 4px; cursor: pointer; background: url(design/tender/100.jpg); background-size: 100% 100%; color: rgba(50, 50, 50, 0);" onclick = "appendSettleValue('100');">100.00</td>
                                </tr>
                                <tr>
                                    <td class = "green white-hover border-2px font2" style = "padding: 4px; cursor: pointer; background: url(design/tender/200.jpg); background-size: 100% 100%; color: rgba(50, 50, 50, 0);" onclick = "appendSettleValue('200');">200.00</td>
                                    <td class = "warning white-hover border-2px font2" style = "padding: 4px; cursor: pointer; background: url(design/tender/500.jpg); background-size: 100% 100%; color: rgba(50, 50, 50, 0);" onclick = "appendSettleValue('500');">500.00</td>
                                    <td class = "white-hover border-2px font2" style = "padding: 4px; cursor: pointer; background: url(design/tender/1000.jpg); background-size: 100% 100%; color: rgba(50, 50, 50, 0);" onclick = "appendSettleValue('1000');">1000.00</td>
                                </tr>-->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <!-- ========================================================================================== -->
        </div>
        <div class = "div22 white border-2px center-text" style="display:none;" label="RecallTransaction" >
        <!-- can be moved to another folder -->
            <div class = "innerdiv w100 h100">
                <div class = "innerdiv w40 h100 transparent border-right-2px">
                    <button class = " label-5em dark-washed-green white-font w100 center-text font2" style="height: auto; margin-top: 2%;" disabled> RECALL TRANSACTION </button>
                    <input type = "number" id ="recall_transaction_number" class = "input-text w80" style = "margin-left: 8%; margin-top: 20px;" placeholder="Transaction Number">
                    <button type = "button" class = "button w40 red white-font border-2px" style = "margin-left: 8%; margin-top: 10px;" onClick = "hideForm('div22');">Close</button>
                    <button type = "button" class = "button w40 green border-2px" style = "margin-left: 8%; margin-top: 10px;" onClick = "recallTransaction();" id = "transaction-recall">Recall</button>
                </div>
                <div class = "innerdiv w59 h100 transparent">
                    <div class = "innerdiv w100 h90">
                        <table class = "w90" style = "border-collapse: collapse; margin-top: 20px; margin-left: 5px;" id = "recall-order-table">
                            <thead class = "dark-washed-green white-font" style = "padding-top: 12px; padding-bottom: 12px;">
                                <tr>
                                    <th class = "border-1px" style = "padding: 8px;">Order Id</th>
                                    <th class = "border-1px" style = "padding: 8px;">Transaction Number</th>
                                    <th class = "border-1px" style = "padding: 8px;">Date</th>
                                </tr>
                            </thead>
                            <tbody id = "recall-order-table-content">
                
                            </tbody>
                        </table>
                    </div>
                    <div class = "innerdiv w100 h10" id = "recall-order-container"></div>
                </div>
            </div>
        <!-- ========================================================================================== -->
        </div>
        <div class = "div23 white border-2px center-text" style="display:none;" label="Void Current Transaction" >
            <button class="label-5em dark-washed-green white-font center-text w100 h40 font3" style="margin-top: 5%;" disabled>Are You Sure You Want to Void Current Transaction</button>
            <button class = "button center-text w40 h20 green border-2px" style="margin-left: 5%; margin-right: 5%; margin-top: 5%;" > YES </button>
            <button class = "button center-text w40 h20 red white-font border-2px" style="margin-left: 5%; margin-right: 5%; margin-top: 5%;" onclick="hideForm('div23');"> NO </button>
        </div>
        <div class = "div25 white border-2px center-text" style="display:none;" label="Electronic Journal" >
            <button class="label-5em dark-washed-green white-font center-text w100 h20 font3" style="margin-top: 1%;" disabled>Electronic Journal</button>
            <button class = "button center-text w40 h20 green border-2px" style="margin-left: 5%; margin-right: 5%; margin-top: 5%;" > PRINT </button>
            <button class = "button center-text w40 h20 red white-font border-2px" style="margin-left: 5%; margin-right: 5%; margin-top: 5%;" onclick="hideForm('div25');"> CANCEL </button>
        </div>
        <div class = "div26 white border-2px center-text" style="display:none;" label="X-Reading" >
            <button class="label-5em dark-washed-green white-font center-text w100 font3" style="height: auto; margin-top: 1%;" disabled>X-Reading (Terminal Reading)</button>
            <button class = "button center-text w40 h20 green border-2px" style="margin-left: 5%; margin-right: 5%; margin-top: 5%;" onclick = "getXreadDetails();" > PRINT </button>
            <button class = "button center-text w40 h20 red white-font border-2px" style="margin-left: 5%; margin-right: 5%; margin-top: 5%;" onclick="hideForm('div26');"> CANCEL </button>
        </div>
        <div class = "div27 white border-2px center-text" style="display:none;" label="Y-Reading" >
            <button class="label-5em dark-washed-green white-font center-text w100 font3" style="height: auto; margin-top: 1%;" disabled>Y-Reading (Cashier Reading)</button>
            <button class = "button center-text w40 h20 green border-2px" style="margin-left: 5%; margin-right: 5%; margin-top: 5%;" > PRINT </button>
            <button class = "button center-text w40 h20 red white-font border-2px" style="margin-left: 5%; margin-right: 5%; margin-top: 5%;" onclick="hideForm('div27');"> CANCEL </button>
        </div>
        <div class = "div28 white border-2px center-text" style="display:none;" label="Z-Reading" >
            <button class="label-5em dark-washed-green white-font center-text w100 font3" style="height: auto; margin-top: 1%;" disabled>Z-READING (End of The Day Reading)</button>
            <button class="label-5em dark-washed-green white-font center-text w100 h20 font3" style="font-family:Verdana, Geneva, Tahoma, sans-serif; margin-top: 1%;" id = "zread_code" disabled></button>
            <input type = "text" id ="zread_code_input" class = "input-text w80" style = "margin-left: 8%; margin-top: 20px;" placeholder="Enter Code">
            <button class = "button center-text w40 h20 green border-2px" style="margin-left: 5%; margin-right: 5%; margin-top: 5%;" onclick = "zRead();"> PRINT </button>
            <button class = "button center-text w40 h20 red white-font border-2px" style="margin-left: 5%; margin-right: 5%; margin-top: 5%;" onclick="hideForm('div28');"> CANCEL </button>
        </div>
        <div class = "div29 white border-2px center-text" style="display:none;" label="cash fund" >
            <?php include $contents["reprint"]["path"] ?>
        </div>
        <div class = "div30 white border-2px center-text" style = "display: none;" label="log-date-opening-div">
          <button class = "button-disabled w100 h70 white" id = "log_date_error"></button>
          <button class = "button w50 h30 green border-right-2px border-top-2px" id = "log-date-yes" onclick = "openLoginManager();">Yes</button>
          <button class = "button w50 h30 red white-font border-right-2px border-top-2px" id = "log-date-no" onclick = "hideForm('div30');">No</button>
        </div>
        <div class = "div31 white border-2px center-text" style = "display: none;"  label="log-date-opening-div">
          <button class = "button-disabled w100 h70 white" id = "zread_error_message"></button>
          <button class = "button w30 h30 green border-right-2px border-top-2px" id = "logout-yes" onclick = "voidAll();">Void All</button>
          <button class = "button w40 h30 green border-right-2px border-top-2px" id = "logout-no" onclick = "goToRecall();">Recall Transactions</button>
          <button class = "button w30 h30 red white-font border-right-2px border-top-2px" id = "logout-no" onclick = "hideForm('div31');">Cancel</button>
        </div>
        <div class = "div32 white border-2px center-text" style = "display: none;" label="Cash Withdrawal" >
            <button class = " label-5em dark-washed-green white-font w100 h20 center-text font3" style="margin-top: 2%;" disabled> Price Override </button>
            <input type = "number" class = "input-text w90 h20 right-text font2" style = "margin-top: 8%; margin-bottom: 4%; margin-left: 3.5%;" placeholder = "Enter Price" id="price_override" oninput = "checkValue(this, 'price_override');" > <br>
            <button class = "button center-text w40 h20 red white-font border-2px" style="margin-left: 5%; margin-right: 5%; margin-top: 5%;" onclick="hideForm('div32');"> CANCEL </button>
            <button class = "button center-text w40 h20 green border-2px" style="margin-left: 5%; margin-right: 5%; margin-top: 5%;" disabled id = "price_override_done" onclick = "priceOverride();"> DONE </button>
        </div>
        <div class = "div33 white border-2px center-text" style = "display: none;" label="Transaction-Settled-div">
          <button class = "button-disabled dark-washed-green white-font w100 h20 white" id = "settle_info" disabled>Transaction Settled</button>
          <button class = "button-disabled w60 h20 white left-text label" id = "" disabled>Total: </button>
          <button class = "button-disabled w40 h20 white right-text label" id = "settle_total" disabled>101,000.00</button>
          <button class = "button-disabled w60 h20 white left-text label" id = "" disabled>Amount Payed: </button>
          <button class = "button-disabled w40 h20 white right-text label" id = "settle_amount_payed" disabled>101,000.00</button>
          <button class = "button-disabled w60 h20 white left-text label" id = "" disabled>Change: </button>
          <button class = "button-disabled w40 h20 white right-text label" id = "settle_change" disabled>101,000.00</button>
          <button class = "button w100 h20 green border-right-2px border-top-2px" id = "log-date-yes" onclick = "hideForm('div33');">Done</button>
        </div>
        <div class = "div34 white border-2px center-text" style = "display: none;" label="log-date-opening-div">
            <button class = " label-5em dark-washed-green white-font w100 center-text font2" style="height: auto; margin-top: 2%;" disabled id = "tender_info">gcash</button>
            <button class = " label-5em dark-washed-green white-font w100 center-text font2" style="height: auto; margin-top: 2%;" disabled id = "settle_total_amount_by_tender">100.00</button>
            <input type = "number" id ="card_amount" class = "input-text w80 h5" style = "margin-left: 8%; margin-top: 20px;" placeholder="Amount">
            <input type = "number" id ="card_number" class = "input-text w80 h5" style = "margin-left: 8%; margin-top: 20px;" placeholder="Card Number">
            <input type = "number" id ="approval_number" class = "input-text w80 h5" style = "margin-left: 8%; margin-top: 20px;" placeholder="Approval Number">
            <input type = "text" id ="account_name" class = "input-text w80 h5" style = "margin-left: 8%; margin-top: 20px;" placeholder="Account Name">
            <input type = "text" id ="bank_name" class = "input-text w80 h5" style = "margin-left: 8%; margin-top: 20px;" placeholder="Bank Name">
          <button class = "button w37 green border-2px" style = "margin-left: 8%; margin-top: 20px;" id = "login-manager-menu" onclick = "saveTenderDetails();" tabindex="3">Continue</button>
          <button class = "button w37 red white-font border-2px" style = "margin-left: 8%; margin-top: 20px;" id = "login-cancel" onclick = "hideForm('div34');" tabindex="4">Cancel</button>
        </div>
        <div class = "loading">
            <h1>Loading...</h1>
            <div id="preloader">
                <div id="loader"></div>
            </div>
        </div>
    </div>
</body>
</html>