function getAllDiscounts(currentPage) {
    $.ajax({
        type: "GET",
        url: CASHIER_API + 'generic.php',
        data: jQuery.param({ getAllDiscounts: "yes" }),
        processData: false
    })
    .done(response => {
        responseJSON = JSON.parse(response)

        $.when(renderTablePage(responseJSON, currentPage, "discount-container", "discount", "getAllDiscounts"))
        .done(response => {
            $.when($("#discount-table-content").html(response))
            .done(() => {
                $(".loading").hide(DEFAULT_TIME_VALUE)
            })
        })
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function closeDiscountForm() {
    clearSelectionDiscount()
    disableButtons()
    hideForm("div17")
}

function selectDiscount(app) {
    clearSelectionDiscount()
    const discount_id = app.id
    $("#discount-apply").attr("disabled", false)
    $("#discount-apply-all").attr("disabled", false)

    $("#" + discount_id).attr("class", "gray")

    $("#selected_discount").val(discount_id)
}

function applyDiscount(value) {
    $(".loading").show(DEFAULT_TIME_VALUE)
    if(value == "one") {
        const data = {
            discount: $("#selected_discount").val(),
            product: $("#product-value").val(),
            transaction: getTransactionNumber()
        }

        applyDiscountProcess(data)
    }
    else {
        const data = {
            discount: $("#selected_discount").val(),
            product: "all",
            transaction: getTransactionNumber()
        }

        applyDiscountProcess(data)
    }
}

function applyDiscountProcess(data) {
    $.ajax({
        type: "GET",
        url: CASHIER_API + 'order.php',
        data: jQuery.param({ applyDiscount: "yes", data: data }),
        processData: false
    })
    .done(response => {
        responseJSON = JSON.parse(response)

        showNotification(responseJSON.text, responseJSON.type)
        getCurrentTransaction(data.transaction)
        $(".loading").hide(DEFAULT_TIME_VALUE)
        closeDiscountForm()
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function removeDiscount(value) {
    $(".loading").show(DEFAULT_TIME_VALUE)
    if(value == "one") {
        const data = {
            product: $("#product-value").val(),
            transaction: getTransactionNumber()
        }

        removeDiscountProcess(data)
    }
    else {
        const data = {
            product: "all",
            transaction: getTransactionNumber()
        }

        removeDiscountProcess(data)
    }
}

function removeDiscountProcess(data) {
    $("#discount-remove").attr("disabled", true)
    $.ajax({
        type: "GET",
        url: CASHIER_API + 'order.php',
        data: jQuery.param({ removeDiscount: "yes", data: data }),
        processData: false
    })
    .done(response => {
        responseJSON = JSON.parse(response)

        showNotification(responseJSON.text, responseJSON.type)
        checkCurrentTransaction()
        closeDiscountForm()
        $(".loading").hide(DEFAULT_TIME_VALUE)
        $("#discount-remove").attr("disabled", false)
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function clearSelectionDiscount() {
    $.when($("#discount-table-content tr").attr("class", "white white-hover"))
    .done(() => {
        $("#selected_discount").val("")
        $("#discount-apply").attr("disabled", true)
        $("#discount-apply-all").attr("disabled", true)
    })
}

function checkManualValue(app) {
    if(app.value > 0 && app.value != "" && app.value != null) {
        $("#manual_input_button").attr("class", "button w20 h60 dark-washed-green white-font border-2px")
        $("#manual_input_button").attr("disabled", false)
        return;
    }
    $("#manual_input_button").attr("class", "button w20 h60 gray white-font border-2px")
    $("#manual_input_button").attr("disabled", true)
}

function manualDiscount() {
    $(".loading").show(DEFAULT_TIME_VALUE)
    const value = $("#manual_input").val()
    const order_id = $("#product-value").val()
    const transaction = getTransactionNumber();

    if(value < 0) {
        return
    }

    const data = {
        discount: value,
        order_id : order_id
    }

    $.ajax({
        type: "GET",
        url: CASHIER_API + 'order.php',
        data: jQuery.param({ manualDiscount: "yes", data: data }),
        processData: false
    })
    .done(response => {
        $(".loading").hide(DEFAULT_TIME_VALUE)
        responseJSON = JSON.parse(response)

        showNotification(responseJSON.text, responseJSON.type)
        getCurrentTransaction(transaction)
        closeDiscountForm()
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}