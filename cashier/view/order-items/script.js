function checkCurrentTransaction(){
    $(".loading").show(DEFAULT_TIME_VALUE)
    let current_transaction = sessionStorage.getItem("current-transaction")

    if( current_transaction != "NONE" && current_transaction != null ){
       getCurrentTransaction(current_transaction)
    }
    else {
        $(".loading").hide(DEFAULT_TIME_VALUE)
    }
}

function saveNewTransaction() { //ajax for saving new transaction
    $.ajax({
        type: "GET",
        url: CASHIER_API + 'order.php',
        data: jQuery.param({ newTransaction: "yes" }),
        processData: false
    })
    .done(response => {
        responseJSON = JSON.parse(response)

        sessionStorage.setItem("current-transaction", responseJSON.data.trn_number)

        $("#transaction-number").html("TRN: " + addZeroes(responseJSON.data.trn_number))
        $("#order-number").html("ORDR #: " + addZeroes(responseJSON.data.order_number))
        $("#guest-number-count").html("GUEST NO: 1")
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function getCurrentTransaction(current_transaction_ref_no, type = "old"){
   $.ajax({
        type: "GET",
        url: CASHIER_API + 'order.php',
        data: jQuery.param({ getCurrentTransaction: current_transaction_ref_no }),
        processData: false
    })
    .done(response => {
        responseJSON = JSON.parse(response)

        if(responseJSON.order == null) {
            $(".loading").hide(DEFAULT_TIME_VALUE)
            return
        }

        $("#transaction-number").html("TRN: " + addZeroes(responseJSON.order.trn_number))
        $("#order-number").html("ORDR #: " + addZeroes(responseJSON.order.order_number))
        //$("#guest-number-count").html("GUEST NO: " + responseJSON.order.guest_no)

        let render

        if(responseJSON.item.items.length > 0) {
            for(let i in responseJSON.item.items) {
                render += `<tr class = "black white-font font border-white" id = "` + responseJSON.item.items[i].id + `" onClick = "enableButtons(this);"><td class = "right-text" style = "cursor: pointer;">` + responseJSON.item.items[i].quantity + `</td><td style = "cursor: pointer;">` + responseJSON.item.items[i].item.name + `</td><td class = "right-text" style = "cursor: pointer;">` + responseJSON.item.items[i].cost + `</td><td class = "right-text" style = "cursor: pointer;">` + responseJSON.item.items[i].total_cost + `</td><td class = "right-text font-075" style = "cursor: pointer;">` + responseJSON.item.items[i].service_type + `</td></tr>`
            }
       }
       else {
           render = ""
       }
       $("#order-table").html(render)
       $("#transaction").attr("disabled", true)
       $("#transaction").attr("class", "button w100 h100 gray font")
       $("#settle").attr("disabled", false)
       $("#settle").attr("class", "button w100 h100 black white-font font")
       $("#print").attr("disabled", false)
       $("#print").attr("class", "button w100 h100 black white-font font")
       autoComputeTotalAmount()
       $(".loading").hide(DEFAULT_TIME_VALUE)

       if(type == "old") {
           $(".products-menu").hide(DEFAULT_TIME_VALUE)
           getCategory()
       }
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function settle(form){
    let amount = 0
    let total = 0
    $.when(getTotalAmount())
    .done(response => {
        responseJSON = JSON.parse(response)

        total = responseJSON.total

        if(form == "cash") {
            amount = $("#settle_amount").val()
        }
        else {
            amount = $("#settle_total_amount_by_tender").html()
            tender = $("#tender_info").html()
        }
        let transaction_number = getTransactionNumber()
    
        if(total == "") {
            showNotification("Select a product first or void this transaction!", "error")
        }
        else {
            if(amount == "") {
                savePayment(total.toFixed(2), transaction_number, total.toFixed(2), 0.00, 1)
            }
            else {
                total = parseFloat(total)
                amount = parseFloat(amount)
    
                if(amount > total) {
                    const change = amount - total
                    savePayment(total.toFixed(2), transaction_number, amount.toFixed(2), change.toFixed(2), 1)
                }
                else if(amount == total) {
                    savePayment(total.toFixed(2), transaction_number, amount, 0.00, 1)
                }
                else {
                    savePayment(total.toFixed(2), transaction_number, amount, 0.00, 1)
                }
            }
        }
    })
}

function succesfulSettlement() {
    $("#settle_amount").val("")
    hideAll("all")
    disableButtons("settle")
    $("#transaction").attr("disabled", false)
    $("#transaction").attr("class", "button w100 h100 black white-font font")
    $("#order-table").html("")
    sessionStorage.setItem("current-transaction", "NONE") //reset current transaction
    $("#totalAmount").html("0.00")
    hideForm("div21")
    $("#transaction-number").html("TRN: NA")
    $("#order-number").html("ORDR #: NA")
    $("#guest-number-count").html("GUEST NO: 0")
    $(".background").show(DEFAULT_TIME_VALUE)
}

function doSettlePrint(type){

    $.ajax({
        type: "GET",
        url: SETTLE_API,
        data: jQuery.param({ trn_number: sessionStorage.getItem("current-transaction") }),
        processData: false
    })
    .done(response => {
        responseJSON = JSON.parse(response)


        if(ENVIRONMENT == 1) {
            setBody("SETTLE", responseJSON, type)
            showSettleDone(responseJSON.order["amount_payable"], responseJSON.order["amount_payed"], responseJSON.order["amount_change"])

            showNotification("Printing...")
        }
        else {
            if(responseJSON.error == "false") {
                showNotification(responseJSON.message)
                return
            }
            showNotification(responseJSON.message, "error")
        }
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function showSettleDone(total, amount, change) {
    showForm("div33")

    $("#settle_total").html(total)
    $("#settle_amount_payed").html(amount)
    $("#settle_change").html(change)
}

function savePayment(total, transaction_number, amount, change, tender, array_details, type = "CASH") {
    const data = {
        amount_total: total,
        amount_payed: amount,
        amount_change: change,
        transaction_number: transaction_number,
        tender: tender,
        details: array_details
    }

    $.ajax({
         type: "POST",
         url: CASHIER_API + 'order.php',
         data: jQuery.param({ savePayment: data }),
         processData: false
     })
     .done(response => {
        responseJSON = JSON.parse(response)

        if(responseJSON.type == "success") {
            $.when(doSettlePrint(type))
            .done(() => {
                succesfulSettlement()
            })  
        }
        else if(responseJSON.type == "partial") {
            showTender()
        }
        else {
            showNotification(responseJSON.text, responseJSON.type)
        }
     })
     .fail(() => {
         showNotification("Error 408: Connection Timeout", "error")
         $(".loading").hide(DEFAULT_TIME_VALUE)
     })
}
