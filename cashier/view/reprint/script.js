function rePrint(){
    let trn_number = $("#trn_number_reprint").val()

    $.ajax({
        type: "GET",
        url: SETTLE_API,
        data: jQuery.param({ trn_number: trn_number, reprint: "yes" }),
        processData: false
    })
    .done(response => {
        responseJSON = JSON.parse(response)

        if(ENVIRONMENT == 1) {
            if(responseJSON.error) {
                showNotification(responseJSON.message, "error")
            }
            else{
                let receipt = setBody("REPRINT", responseJSON)

                if(receipt.type == true) {
                    hideForm("div29")
                }
            }

        }
        else {
            //
        }
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function getEnvironment() {
    return $.ajax({
        url: '../api/cashier/generic.php?getEnvironment',
        processData: false
    })
}