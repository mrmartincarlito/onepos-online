function closeManagerMenu() {
    let current_transaction = sessionStorage.getItem("current-transaction")
  
    if( current_transaction == "NONE" || current_transaction == null ){
        $(".background").show(DEFAULT_TIME_VALUE)
    }


    $("#manager-menu").hide(DEFAULT_TIME_VALUE)
    $(".div3").attr("style", "overflow-y: scroll;")
}

function openLoginManager() {
    hideAll("manager")
    $.when(showForm("div10"))
    .done(() => {
        $("#username").focus()
    })
}

function openManagerMenu(){
  $(".background").hide(DEFAULT_TIME_VALUE)  
  $("#manager-menu").show(DEFAULT_TIME_VALUE)
  $.when($(".div3").attr("style", "overflow-y: scroll;")) //ensures that the div is not scrollable
  .done(() => {
      $('.div3').animate({ //scrolls at the top of manager menu
          scrollTop: $("#div3").offset().top
      },'fast')
  })
}

function loginManagerCredentials(){
    $(".loading").show(DEFAULT_TIME_VALUE)
    const ACTIVE = 1
    const INACTIVE = 0

    let credentials = {
        "username" : $("#username").val(),
        "password" : $("#password").val()
    }

    $.ajax({
        type: "POST",
        url: "../api/auth.php",
        data: "cashier_auth=" + JSON.stringify(credentials),
        processData: false
    })
    .done(response => {
        $(".loading").hide(DEFAULT_TIME_VALUE)

        responseJSON = JSON.parse(response)

        if(responseJSON.type == "success") {
            openManagerMenu()
            $.when($(".div11").hide(DEFAULT_TIME_VALUE))
            .done(() => {
                $(".div10").hide(DEFAULT_TIME_VALUE)
            })

            $.each(responseJSON.access, function( index, value ) {
                if(index != "username" && index != "password"){
                    if(value == INACTIVE){
                        $("#"+ index).prop("onclick", null).off("click")
                        $("#"+ index).attr("class", "grid-4 white center-text border-2px")
                    }
                }
            });
        }
        showNotification(responseJSON.text, responseJSON.type)
        $("#username").val("")
        $("#password").val("")
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function itemVoid() {
    $(".loading").show(DEFAULT_TIME_VALUE)
    const product = $("#product-value").val()
    const current_transaction = sessionStorage.getItem("current-transaction")

    if(product != "" && product != null) {

        const data = {
            product_id: product,
            transaction_number: current_transaction
        }

        $.ajax({
            type: "POST",
            url: CASHIER_API + 'manager-menu.php',
            data: jQuery.param({ itemVoid: "yes", data: data }),
            processData: false
        })
        .done(response => {
            responseJSON = JSON.parse(response)

            if(responseJSON.type == "success") {
                $("#product-value").val("")
                getCurrentTransaction(data.transaction_number)
            }

            showNotification(responseJSON.text, responseJSON.type)
        })
        .fail(() => {
            showNotification("Error 408: Connection Timeout", "error")
            $(".loading").hide(DEFAULT_TIME_VALUE)
        })
    }
    else {
        $(".loading").hide(DEFAULT_TIME_VALUE)
        showNotification("No Item Selected!", "warning")
    }
}

function voidTransaction() {
    $(".loading").show(DEFAULT_TIME_VALUE)
    const current_transaction = sessionStorage.getItem("current-transaction")

    if(current_transaction != "" && current_transaction != null && current_transaction != "NONE") {
        $.ajax({
            type: "POST",
            url: CASHIER_API + 'manager-menu.php',
            data: jQuery.param({ voidTransaction: "yes", transaction_number: current_transaction }),
            processData: false
        })
        .done(response => {
            $(".loading").hide(DEFAULT_TIME_VALUE)
            responseJSON = JSON.parse(response)

            if(responseJSON.type == "success") {
                succesfulSettlement()
                closeManagerMenu()
            }

            showNotification(responseJSON.text, responseJSON.type)
        })
        .fail(() => {
            showNotification("Error 408: Connection Timeout", "error")
            $(".loading").hide(DEFAULT_TIME_VALUE)
        })
    }
    else {
        $(".loading").hide(DEFAULT_TIME_VALUE)
        showNotification("No current transaction!", "warning")
    }
}

function showSuspendedTransaction() {
    $.when(getSuspendedTransactions("1"))
    .done(() => {
        showForm("div22")
    })
}

function suspendTransaction() {
    $(".loading").show(DEFAULT_TIME_VALUE)
    let flag = false
    const current_transaction = sessionStorage.getItem("current-transaction")

    if( current_transaction != "NONE" && current_transaction != null ){
        flag = true
    }

    if(flag == true) {
        $.ajax({
            type: "GET",
            url: CASHIER_API + 'manager-menu.php',
            data: jQuery.param({ suspendTransaction: "yes", current_transaction: current_transaction }),
            processData: false
        })
        .done(response => {
            $(".loading").hide(DEFAULT_TIME_VALUE)
            responseJSON = JSON.parse(response)

            if(responseJSON.type == "success") {
                succesfulSettlement()
                closeManagerMenu()
            }
            showNotification(responseJSON.text, responseJSON.type)
        })
        .fail(() => {
            showNotification("Error 408: Connection Timeout", "error")
            $(".loading").hide(DEFAULT_TIME_VALUE)
        })
    }
    else {
        showNotification("No pending Transaction!", "warning")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    }
}

function getSuspendedTransactions(currentPage) {
    $(".loading").show(DEFAULT_TIME_VALUE)
    $.ajax({
        type: "GET",
        url: CASHIER_API + 'manager-menu.php',
        data: jQuery.param({ getSuspendedTransactions: "yes" }),
        processData: false
    })
    .done(response => {
        responseJSON = JSON.parse(response)

        $.when(renderTablePage(responseJSON, currentPage, "recall-order-container", "recall-order", "getSuspendedTransactions"))
        .done(response => {
            $.when($("#recall-order-table-content").html(response))
            .done(() => {
                $(".loading").hide(DEFAULT_TIME_VALUE)
            })
        })
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function selectSuspendedOrder(app) {
    clearSelection()
    const selected_item = app.id
    let transaction_number

    $("#" + selected_item).attr("class", "gray")
    
    $("#" + selected_item).children('td').each ( (counter, td) => {
        if(counter == 1) {
            transaction_number = $(td).text()
        }
    }); 

    $("#recall_transaction_number").val(transaction_number)
}

function clearSelection() {
    $.when($("#recall-order-table-content tr").attr("class", "white white-hover"))
    .done(() => {
        $("#recall_transaction_number").val("")
    })
}

function recallTransaction() {
    $(".loading").show(DEFAULT_TIME_VALUE)
    current_transaction = sessionStorage.getItem("current-transaction")
    if(current_transaction != "" && current_transaction != null && current_transaction != "NONE") {
        showNotification("There's an active transaction!", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
        return
    }
    const transaction_number = $("#recall_transaction_number").val()

    if(checkIfEmpty(transaction_number).status == true) {
        $.ajax({
            type: "POST",
            url: CASHIER_API + 'manager-menu.php',
            data: jQuery.param({ recallTransaction: "yes", transaction_number: transaction_number }),
            processData: false
        })
        .done(response => {
            responseJSON = JSON.parse(response)
            $(".loading").hide(DEFAULT_TIME_VALUE)

            if(responseJSON.type == "success") {
                $.when(sessionStorage.setItem("current-transaction", transaction_number))
                .done(() => {
                    getSuspendedTransactions("1")
                    getCurrentTransaction(transaction_number)
                    hideForm('div22')
                })
            }
            showNotification(responseJSON.text, responseJSON.type)
        })
        .fail(() => {
            showNotification("Error 408: Connection Timeout", "error")
            $(".loading").hide(DEFAULT_TIME_VALUE)
        })
    }
    else {
        showNotification("Transaction number cannot be empty", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    }
}

function getPendingTransactions(log_date_id) {
    return $.ajax({
        type: "GET",
        url: CASHIER_API + 'generic.php',
        data: jQuery.param({ getPendingTransactions: "yes", log_date_id: log_date_id }),
        processData: false
    })
}

function zRead() { //ajax for filler zread
    current_transaction = sessionStorage.getItem("current-transaction")
    if(current_transaction != "" && current_transaction != null && current_transaction != "NONE") {
        $(".loading").hide(DEFAULT_TIME_VALUE)
        showNotification("There's an active transaction!", "error")
        return
    }
    const input = $("#zread_code_input").val()
    const code = $("#zread_code").html()
    $.when(getLogDate())
    .done(date => {
        dateJSON = JSON.parse(date)

        if(input == code) {
            $.when(getPendingTransactions(dateJSON.id))
            .done(transactions => {
                transactionsJSON = JSON.parse(transactions)

                if(transactionsJSON.length < 1) {
                    $.ajax({
                        type: "GET",
                        url: CASHIER_API + 'generic.php',
                        data: jQuery.param({ closeLogDate: "yes" }),
                        processData: false
                    })
                    .done(response => {
                        responseJSON = $.parseJSON(response)
                        getZreadDetails()
                
                        if(responseJSON.type == "success") {
                            $("#zread_code_input").val("")
                        }
                    })
                    .fail(() => {
                        showNotification("Error 408: Connection Timeout", "error")
                        $(".loading").hide(DEFAULT_TIME_VALUE)
                    })
                }
                else {
                    $(".div28").hide(DEFAULT_TIME_VALUE)
                    $("#zread_error_message").html("You have " + transactionsJSON.length + " pending transaction(s).")
                    $("#zread_code_input").val("")
                    showForm("div31")
                }
            })
        }
        else {
            showNotification("Wrong code", "error")
        }
    })
}

function getZreadDetails() {
    $(".loading").show(DEFAULT_TIME_VALUE)
    $.ajax({
        type: "POST",
        url: READINGS_API,
        data: jQuery.param({ type: "z" }),
        processData: false
    })
    .done(response => {
        $(".loading").hide(DEFAULT_TIME_VALUE)
        responseJSON = JSON.parse(response)
        if(ENVIRONMENT == 1) {
            setReading(responseJSON, "z-read")
            showNotification("Printing...")
        }
        else {
            showNotification(responseJSON.text, responseJSON.type)
        }
        clearTimeout()
        logout()
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function getXreadDetails() {
    $(".loading").show(DEFAULT_TIME_VALUE)
    current_transaction = sessionStorage.getItem("current-transaction")
    if(current_transaction != "" && current_transaction != null && current_transaction != "NONE") {
        $(".loading").hide(DEFAULT_TIME_VALUE)
        showNotification("There's an active transaction!", "error")
        return
    }
    $.ajax({
        type: "POST",
        url: READINGS_API,
        data: jQuery.param({ type: "x" }),
        processData: false
    })
    .done(response => {
        $(".loading").hide(DEFAULT_TIME_VALUE)
        responseJSON = JSON.parse(response)
        if(ENVIRONMENT == 1) {
            setReading(responseJSON, "x-read")
            showNotification("Printing...")
        }
        else {
            showNotification(responseJSON.text, responseJSON.type)
        }
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function cashDeposit() {
    const cash_fund = $("#cashfund").val()

    if(cash_fund > 0) {
        $.ajax({
            type: "POST",
            url: CASHIER_API + 'manager-menu.php',
            data: jQuery.param({ cashDeposit: "yes", cash_fund: cash_fund }),
            processData: false
        })
        .done(response => {
            responseJSON = JSON.parse(response)
    
            if(responseJSON.type == "success") {
                $("#cashfund").val("")
                hideForm("div18")
            }
            
            showNotification(responseJSON.text, responseJSON.type)
        })
        .fail(() => {
            showNotification("Error 408: Connection Timeout", "error")
            $(".loading").hide(DEFAULT_TIME_VALUE)
        })
    }
    else {
        showNotification("Cash fund value cannot be zero or less than zero", "warning")
    }
}

function cashWithdraw() {
    const cash_amount = $("#cash_withdraw").val()

    if(cash_amount > 0) {
        $.ajax({
            type: "POST",
            url: CASHIER_API + 'manager-menu.php',
            data: jQuery.param({ cashWithdraw: "yes", cash_amount: cash_amount }),
            processData: false
        })
        .done(response => {
            responseJSON = JSON.parse(response)

            if(responseJSON.type == "success") {
                $("#cash_withdraw").val("")
                hideForm("div19")
            }
            
            showNotification(responseJSON.text, responseJSON.type)
        })
        .fail(() => {
            showNotification("Error 408: Connection Timeout", "error")
            $(".loading").hide(DEFAULT_TIME_VALUE)
        })
    }
    else {
        showNotification("Cash withdrawal value cannot be zero or less than zero", "warning")
    }
}

function searchTransaction(currentPage) {
    $(".loading").show(DEFAULT_TIME_VALUE)
    const value = $("#transaction_refund").val()

    if(value == "") {
        $(".loading").hide(DEFAULT_TIME_VALUE)
        showNotification("Transaction number cannot be empty!", "warning")
    }
    else {
        $.ajax({
            type: "GET",
            url: CASHIER_API + 'manager-menu.php',
            data: jQuery.param({ searchTransaction: "yes", transaction_refund: value }),
            processData: false
        })
        .done(response => {
            responseJSON = JSON.parse(response)

            if(responseJSON.type == "error") {
                showNotification(responseJSON.text, responseJSON.type)
                $(".loading").hide(DEFAULT_TIME_VALUE)
            }
            else {
                $.when(renderTablePage(responseJSON, currentPage, "refund-order-container", "refund", "searchTransaction"))
                .done(response => {
                    $.when($("#refund-order-table-content").html(response))
                    .done(() => {
                        $(".loading").hide(DEFAULT_TIME_VALUE)
                    })
                })
            }
        })
        .fail(() => {
            showNotification("Error 408: Connection Timeout", "error")
            $(".loading").hide(DEFAULT_TIME_VALUE)
        })
    }
}

function refund() {
    $(".loading").show(DEFAULT_TIME_VALUE)
    const transaction_number = $("#transaction_refund").val()
    const product_id = $("#refund_id").val()

    if(transaction_number != "" && product_id != "") {
        const data = {
            transaction_number: transaction_number,
            product_id: product_id
        }

        $.ajax({
            type: "POST",
            url: CASHIER_API + 'manager-menu.php',
            data: jQuery.param({ refund: "yes", data: data }),
            processData: false
        })
        .done(response => {
            $(".loading").hide(DEFAULT_TIME_VALUE)
            responseJSON = JSON.parse(response)

            if(responseJSON.type == "success") {
                hideForm('div20')
                let refund_api = ""
                if(ENVIRONMENT == 1) {
                    refund_api = "../api/cashier/test-refund.php"
                }
                else {
                    refund_api = CASHIER_API + 'refund.php'
                }
                    
                printRefund(transaction_number, product_id, refund_api)
            }

            showNotification(responseJSON.text, responseJSON.type)
        })
        .fail(() => {
            showNotification("Error 408: Connection Timeout", "error")
            $(".loading").hide(DEFAULT_TIME_VALUE)
        })
    }
    else {
        showNotification("There's an error retrieving data!", "error")
    }
}

function printRefund(trn_number, product_id, refund_api){
    $.ajax({
        type: "GET",
        url: refund_api,
        data: jQuery.param({ trn_number: trn_number, product_id: product_id }),
        processData: false
    })
    .done(response => {
        responseJSON = JSON.parse(response)

        if(ENVIRONMENT == 1) {
            setBody("REFUND", responseJSON)
            showNotification("Printing...")
        }
        else {
            if(responseJSON.error == "false") {
                showNotification(responseJSON.message)
                return
            }
            showNotification(responseJSON.message, "error")
        }
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function priceOverride() {
    $(".loading").show(DEFAULT_TIME_VALUE)
    const product = $("#product-value").val()
    const price = $("#price_override").val()
    const transaction_number = getTransactionNumber()

    if(product != "" && product != null) {

        const data = {
            order_item_id: product,
            price: price,
            transaction_number: transaction_number
        }

        $.ajax({
            type: "POST",
            url: CASHIER_API + 'manager-menu.php',
            data: jQuery.param({ priceOverride: "yes", data: data }),
            processData: false
        })
        .done(response => {
            responseJSON = JSON.parse(response)

            if(responseJSON.type == "success") {
                $("#product-value").val("")
                getCurrentTransaction(data.transaction_number)
            }

            showNotification(responseJSON.text, responseJSON.type)
            hideForm("div32")
        })
        .fail(() => {
            showNotification("Error 408: Connection Timeout", "error")
            $(".loading").hide(DEFAULT_TIME_VALUE)
        })
    }
    else {
        $(".loading").hide(DEFAULT_TIME_VALUE)
        showNotification("No Item Selected!", "warning")
    }
}

function selectOrder(app) {
    clearSelectionRefund()
    const order = app.id
    $("#refund-apply").attr("disabled", false)

    $("#" + order).attr("class", "gray")
    const value = order.split("-")
    $("#refund_id").val(value[1])
}

function checkValue(app, form) {
    const value = app.value
    
    if(value != "") {
        $("#" + form + "_done").attr("disabled", false)
    }
    else {
        $("#" + form + "_done").attr("disabled", true)
    }
}

function clearSelectionRefund() {
    $.when($("#refund-order-table-content tr").attr("class", "white white-hover"))
    .done(() => {
        $("#refund_id").val("")
        $("#refund-apply").attr("disabled", true)
    })
}