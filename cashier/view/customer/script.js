function getAllCustomer(currentPage) {
    $.ajax({
        type: "GET",
        url: CASHIER_API + 'generic.php',
        data: jQuery.param({ getAllCustomer: "yes" }),
        processData: false
    })
    .done(response => {
        responseJSON = JSON.parse(response)

        $.when(renderTablePage(responseJSON, currentPage, "customer-container", "customer", "getAllCustomer"))
        .done(response => {
            $.when($("#customer-table-content").html(response))
            .done(() => {
                $(".loading").hide(DEFAULT_TIME_VALUE)
            })
        })
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function selectCustomer(app) {
    clearSelectionCustomer()
    const customer = app.id
    $("#customer-apply").attr("disabled", false)

    $("#" + customer).attr("class", "gray")

    $("#selected_customer").val(customer)
}

function applyCustomer() {
    $(".loading").show(DEFAULT_TIME_VALUE)

    const data = {
        customer: $("#selected_customer").val(),
        transaction: getTransactionNumber()
    }

    $.ajax({
        type: "POST",
        url: CASHIER_API + 'order.php',
        data: jQuery.param({ applyCustomer: "yes", data: data }),
        processData: false
    })
    .done(response => {
        responseJSON = JSON.parse(response)

        showNotification(responseJSON.text, responseJSON.type)
        closeCustomerForm()
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function closeCustomerForm() {
    clearSelectionCustomer()
    disableButtons($("#product-value").val())
    hideForm("div15")
}

function saveCustomer() {
    $(".loading").show(DEFAULT_TIME_VALUE)

    const data = getInput()

    if(data.status == true) {
        $.ajax({
            type: "POST",
            url: CASHIER_API + 'generic.php',
            data: jQuery.param({ saveCustomer: "yes", data: data.data }),
            processData: false
        })
        .done(response => {
            responseJSON = JSON.parse(response)
    
            if(responseJSON.type == "success") {
                showNotification(responseJSON.text, responseJSON.type)
                getAllCustomer(1)
            }
        })
        .fail(() => {
            showNotification("Error 408: Connection Timeout", "error")
            $(".loading").hide(DEFAULT_TIME_VALUE)
        })
    }
    else {
        $(".loading").hide(DEFAULT_TIME_VALUE)
    }
}

function getInput() {
    const fname = $("#fname").val()
    const lname = $("#lname").val()
    const address = $("#address").val()
    const bday = $("#bday").val()

    if(checkIfEmpty(fname).status == true) {
        if(checkIfEmpty(lname).status == true) {
            if(checkIfEmpty(address).status == true) {
                if(checkIfEmpty(bday).status == true) {

                    const array = {
                        fname: fname,
                        lname: lname,
                        address: address,
                        bday: bday
                    }

                    return {
                        status: true,
                        data: array
                    }
                }
                else {
                    showNotification("Birth day cannot be empty!", "error")
                    return {
                        status: false
                    }
                }
            }
            else {
                showNotification("Address cannot be empty!", "error")
                return {
                    status: false
                }
            }
        }
        else {
            showNotification("Last name cannot be empty!", "error")
            return {
                status: false
            }
        }
    }
    else {
        showNotification("First name cannot be empty!", "error")
        return {
            status: false
        }
    }
}

function checkIfEmpty(data) {
    let flag = false

    if(data != "") {
        flag = true
    }

    return {
        status: flag
    }
}

function clearSelectionCustomer() {
    $.when($("#customer-table-content tr").attr("class", "white white-hover"))
    .done(() => {
        $("#selected_customer").val("")
        $("#customer-apply").attr("disabled", true)
    })
}