window.onload = getSpecialRequests

function checkTextbox() {
    const data = getTextBoxData()

    if(checkIfEmpty(data).status == true) {
        $("#specialrequest-apply").attr("disabled", false)
    }
    else {
        $("#specialrequest-apply").attr("disabled", true)
    }
}

function selectSpecialRequest(app) {
    const data = app.value

    if(data == "NULL") {
        $("#specialrequest-text").val("")
        $("#specialrequest-apply").attr("disabled", true)
        return
    }

    $("#specialrequest-text").val(data)
    $("#specialrequest-apply").attr("disabled", false)
}

function getSpecialRequests() {
    $.ajax({
        url: '../api/special_request.php?get',
        processData: false
    })
    .done(response => {
        responseJSON = JSON.parse(response)

        for(let values in responseJSON) {
            $("#special_requst_combo").append(`<option value = "` + responseJSON[values].description + `">` + responseJSON[values].description + `</option>`)
        }
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function applyRequest() {
    $(".loading").show(DEFAULT_TIME_VALUE)
    const data =  {
        product: $("#product-value").val(),
        transaction: getTransactionNumber(),
        request: getTextBoxData()
    }

    $.ajax({
        type: "POST",
        url: CASHIER_API + 'order.php',
        data: jQuery.param({ applyRequest: "yes", data: data }),
        processData: false
    })
    .done(response => {
        responseJSON = JSON.parse(response)
        $(".loading").hide(DEFAULT_TIME_VALUE)

        showNotification(responseJSON.text, responseJSON.type)
        getCurrentTransaction(data.transaction)
        CloseSpecialRequest()
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function CloseSpecialRequest() {
    disableButtons($("#product-value").val())
    $("#specialrequest-text").val("")
    hideForm("div16")
}

function getTextBoxData() {
    const data = $("#specialrequest-text").val()

    return data
}