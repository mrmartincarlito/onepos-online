function appendSettleValue(value) {
    let existing = $("#settle_amount").val()

    if(existing == "") {
        existing = 0
    }
    let total = (parseFloat(existing) + parseFloat(value)).toFixed(2)

    $("#settle_amount").val(total)
}

function getTenderTypes(currentPage) {
    $.ajax({
        type: "GET",
        url: CASHIER_API + 'generic.php',
        data: jQuery.param({ getTenderTypes: "yes" }),
        processData: false
    })
    .done(response => {
        responseJSON = JSON.parse(response)

        $.when(renderTablePage(responseJSON, currentPage, "tender-type-container", "tender-type", "getTenderTypes"))
        .done(response => {
            $.when($("#tender-type-table-content").html(response))
            .done(() => {
                $(".loading").hide(DEFAULT_TIME_VALUE)
            })
        })
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function selectTender(app) {
    $("#tender-type-table-content tr").attr("class", "white white-hover")
    const form = app.id
    const tenderId = form.split("-")
    $("#tender-apply").attr("disabled", false)
    $("#" + form).attr("class", "gray")
    const value = $("#" + form + " td").html()
    const detail = $("#" + form).attr("name")
    const change = $("#" + form).attr("data")
    const id = tenderId[1]
    
    $("#selected_tender_type").val(value)
    $("#selected_tender_required_detail_tag").val(detail)
    $("#selected_tender_allow_change").val(change)
    $("#selected_tender_id").val(id)
}

function printBill(){
    $.ajax({
        type: "GET",
        url: PRINTBILL_API,
        data: jQuery.param({ trn_number: sessionStorage.getItem("current-transaction") }),
        processData: false
    })
    .done(response => {
        
        response = JSON.parse(response)

        if(ENVIRONMENT == 1) {
            let receipt = setBody("PRINTBILL", response)

            if(receipt.type == true) {
                showNotification("Printing...")
            }
        }
        else {
            if(response.error == "true"){
                showNotification(responseJSON.message, "error")
            }
        }
          //let myWindow = window.open('', 'PrintBill', 'height=400,width=600');
            //myWindow.document.write('<html><head><title>Print Bill</title>');
            //myWindow.document.write('</head><body style="font-size:20px;">');
            //myWindow.document.write(response);
            //myWindow.document.write('</body></html>');
            //myWindow.document.close(); // necessary for IE >= 10

            //myWindow.print();
            //myWindow.close();
         
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}