const DEFAULT_TIME_VALUE = 10
const CASHIER_API = "../api/cashier/";
const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'

$(() => {
    checkCurrentTransaction()
    //resetLoginData()
    $.when(setNotification("scripts/notification", "parent"))
    .done(() => {
        $.when(getUser())
        .done(response => {
            responseJSON = JSON.parse(response)
            $("#greetings").append("Good day, " + responseJSON.username + "!") //cashier username

            $.when(getLogDate()) //awaits ajax request for get date
            .done(response => {
                responseJSON = JSON.parse(response)

                $("#branch_code").html(responseJSON.branch)
                $("#logdate").html("LOG DATE: " + responseJSON.date)

                const today = new Date()
                let month = today.getMonth() + 1
                let day = today.getDate()

                if(month < 10) {
                    month = "0" + month
                }
                if(day < 10) {
                    day = "0" + day
                }
                let current_date = today.getFullYear() + "-" + month + "-" + day

                if(current_date != responseJSON.date) {
                    $("#logdate").attr("class", "label w100 h100 red-font")
                    $("#logtime").attr("class", "label w100 h100 red-font")
                    determineLogDateError(current_date, responseJSON.date)
                }
                
                timer()
            })
        })
    })
})
