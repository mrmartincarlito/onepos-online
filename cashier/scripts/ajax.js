function getUser() { //ajax for getting user info
	return $.ajax({
	    type: "GET",
	    url: '../api/auth.php',
	    data: jQuery.param({ getLoggedUserDetails: "yes" }),
		processData: false
	})
}

function getLogDate() { //ajax for getting logdate
	return $.ajax({
	    type: "GET",
	    url: CASHIER_API + 'generic.php',
	    data: jQuery.param({ getLogDate: "yes" }),
		processData: false
	})
}

function saveGuestcount(value, trnNumber) { //ajax for updating guest count
	return $.ajax({
	    type: "GET",
	    url: CASHIER_API + 'order.php',
	    data: jQuery.param({ saveGuestcount: "yes", guestCount: value, trnNumber: trnNumber }),
		processData: false
	})
}

function getCategory() { //ajax for getting categories
    $(".loading").show(DEFAULT_TIME_VALUE)
    $(".background").hide(DEFAULT_TIME_VALUE)
    $(".categories-menu").show(DEFAULT_TIME_VALUE)
	$.ajax({
	    type: "GET",
	    url: '../api/categories.php',
	    data: jQuery.param({ getCategories: "yes" }),
		processData: false
	})
	.done(response => {
        $.when(getGridSize())
        .done(response_grid => {
            const grid_size = JSON.parse(response_grid)
            responseJSON = JSON.parse(response)
            let render = "<tr>"
            let counter = 1
            const image = "with-image"
            const noimage = "no-image"
    
            for(let data in responseJSON) {
                if(counter < grid_size) {
                    if(responseJSON[data].image_file == "") {
                        render += renderCategory(responseJSON[data], grid_size, noimage)
                    }
                    else {
                        render += renderCategory(responseJSON[data], grid_size, image)
                    }
                }
                else {
                    counter = 0

                    if(responseJSON[data].image_file == "") {
                        render += renderCategory(responseJSON[data], grid_size, noimage) + `</tr>`
                    }
                    else {
                        render += renderCategory(responseJSON[data], grid_size, image) + `</tr>`
                    }
                }
                counter++
            }
            $("#categories-menu").html(render)
            $(".loading").hide(DEFAULT_TIME_VALUE)
        })
	})
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function getProducts(value) { //ajax for getting products based on categories
    $(".loading").show(DEFAULT_TIME_VALUE)
    $(".categories-menu").hide(DEFAULT_TIME_VALUE)
    $(".products-menu").show(DEFAULT_TIME_VALUE)
	$.ajax({
	    type: "GET",
	    url: '../api/menu.php',
	    data: jQuery.param({ getProducts: "yes", category: value }),
		processData: false
	})
	.done(response => {
        $.when(getGridSize())
        .done(response_grid => {
            const grid_size = JSON.parse(response_grid)
            
            responseJSON = JSON.parse(response)
            let render = `<tr><td class = "grid-` + grid_size + ` center-text" style = "font-size: 10em;"><button class = "transparent w100 h100 font back" onClick = "backToCategory();"><i class="fas fa-arrow-alt-circle-left"></i></button></td>`
            let counter = 1
            let flag = true
            const image = "with-image"
            const noimage = "no-image"

            for(let data in responseJSON) {
                if(flag) {
                    if(counter < grid_size - 1) {
                        if(responseJSON[data].image_file == "") {
                            render += renderProduct(responseJSON[data], noimage, grid_size)
                        }
                        else {
                            render += renderProduct(responseJSON[data], image, grid_size)
                        }
                    }
                    else {
                        flag = false
                        counter = 0

                        if(responseJSON[data].image_file == "") {
                            render += renderProduct(responseJSON[data], noimage, grid_size) + `</tr>`
                        }
                        else {
                            render += renderProduct(responseJSON[data], image, grid_size) + `</tr>`
                        }
                    }
                }
                else {
                    if(counter < grid_size) {
                        if(responseJSON[data].image_file == "") {
                            render += renderProduct(responseJSON[data], noimage, grid_size)
                        }
                        else {
                            render += renderProduct(responseJSON[data], image, grid_size)
                        }
                    }
                    else {
                        counter = 0

                        if(responseJSON[data].image_file == "") {
                            render += renderProduct(responseJSON[data], noimage, grid_size) + `</tr>`
                        }
                        else {
                            render += renderProduct(responseJSON[data], image, grid_size) + `</tr>`
                        }
                    }
                }
                counter++
            }
            $("#products-menu").html(render)
            $(".loading").hide(DEFAULT_TIME_VALUE)
        })
	})
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function saveNumberProduct(form) { //ajax for saving selected product
    $(".loading").show(DEFAULT_TIME_VALUE)
    let quantity = $("#item-number").val()
    if(form == "cancel") {
        quantity = 1
    }
    if(quantity == "" || quantity == 0) {
        quantity = 1
    }
    const data = {
        trnNumber: getTransactionNumber(),
        quantity: quantity,
        product: $("#product_id").val()
    }

    $.ajax({
        type: "GET",
        url: CASHIER_API + 'order.php',
        data: jQuery.param({ newOrder: "yes", data: data }),
        processData: false
    })
    .done(response => {
        responseJSON = JSON.parse(response)

        if(responseJSON.type == "success") {
            getCurrentTransaction(data.trnNumber, "new")

            $.when(disableButtons(product_id))
            .done(() => {
                $.when($(".div11").hide(DEFAULT_TIME_VALUE))
                .done(() => {
                    $(".div12").hide(DEFAULT_TIME_VALUE)
                    $("#item-number").val("")
                })
            })
        }else{
            showNotification(responseJSON.text, responseJSON.type)
        }
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function saveServiceType(product, type) { //save edit of service type
    const transac = getTransactionNumber()

    $.ajax({
        type: "GET",
        url: CASHIER_API + 'order.php',
        data: jQuery.param({ saveServiceType: "yes", trnNumber: transac, product: product, type: type }),
        processData: false
    })
    .done(response => {
        responseJSON = JSON.parse(response)
        $(".loading").hide(DEFAULT_TIME_VALUE)

        if(responseJSON.type == "success") {
            $.when(hideForm("div13"))
            .done(() => {
                disableButtons(product)
                $(".products-menu").hide(DEFAULT_TIME_VALUE)
                $("#" + product).find('td').each (function() {
                    if($(this).html() == "DINE IN" || $(this).html() == "TAKEOUT") {
                        $(this).html(type)
                    }
                });    
                showNotification(responseJSON.text, responseJSON.type)
            })
        }
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function voidAll() {
    $.when(getLogDate())
    .done(date => {
        dateJSON = $.parseJSON(date)
        $.ajax({
            type: "GET",
            url: CASHIER_API + 'generic.php',
            data: jQuery.param({ voidAll: "yes", date: dateJSON.id }),
            processData: false
        })
        .done(response => {
            responseJSON = JSON.parse(response)
    
            if(responseJSON.type == "success") {
                hideForm("div31")
            }
            showNotification(responseJSON.text, responseJSON.type)
        })
    })
    .fail(() => {
        showNotification("Error 408: Connection Timeout", "error")
        $(".loading").hide(DEFAULT_TIME_VALUE)
    })
}

function getTotalAmount() {
    const transaction_number = getTransactionNumber()
    return $.ajax({
        type: "GET",
        url: CASHIER_API + 'generic.php',
        data: jQuery.param({ getTotalAmount: "yes", transaction_number: transaction_number}),
        processData: false
    })
}

function getGridSize() {
    return $.ajax({
        type: "GET",
        url: CASHIER_API + 'generic.php',
        data: jQuery.param({ getGridSize: "yes" }),
        processData: false
    })
}

function getDenominations() {
    return $.ajax({
        type: "GET",
        url: '../api/denominations.php?get',
        processData: false
    })
}