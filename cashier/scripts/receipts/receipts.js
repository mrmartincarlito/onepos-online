let SETTLE_API = "../api/cashier/settle.php"
let ENVIRONMENT = ""
let PRINTBILL_API = "../api/cashier/printbill.php"

$(() => {
    $.when(getEnvironment())
    .done(response => {
        let data = JSON.parse(response)

        if(data == 1) {
            SETTLE_API = "../api/cashier/test-settle.php"
            PRINTBILL_API = "../api/cashier/test-settle.php"
            ENVIRONMENT = 1
        }
        else {
            SETTLE_API = "../api/cashier/settle.php"
            PRINTBILL_API = "../api/cashier/printbill.php"
            ENVIRONMENT = 0
        }
    })
})

function getEnvironment() {
    return $.ajax({
        url: '../api/cashier/generic.php?getEnvironment',
        processData: false
    })
}

//

function setBody(type, data, tender) {
    const header = getHeaderDetails()
    const footer = getFooterDetails(data, type)

    items = getItemDetails(data, type, tender)
    
    let myWindow = window.open();
    myWindow.document.write('<html><head><style>body {margin: 0;padding: 0;width: 600px;height: auto;}.receipt{font-family: Courier New, Courier, monospace !important;margin: 0;padding: 0;width: 100%;font-size: 17px;text-align: justify;}pre{font-family: Courier New, monospace !important;} @media print { pre {font: 17px "Courier New", sans-serif !important; font-weight: 500;}} @page { font-family: "Courier New", monospace;}</style>');
    myWindow.document.write('</head><body>');

    type == "PRINTBILL" ? myWindow.document.write(`<br><br><div class = "receipt">` + items + `<div>` + footer + `</div></div>`) : myWindow.document.write(`<br><br><div class = "receipt"><div>` + header + `</div>` + items + `<div>` + footer + `</div></div>`)

    myWindow.document.close(); // necessary for IE >= 10

    myWindow.print();
    myWindow.close();

    return {
        type: true
    }
}

function getHeaderDetails() {
    return "<pre>     1TEQ PROVIDERS INCORPORATED\n         Owned & Operated by\n Unit D MCY Bldg.137 Roosevelt Ave.\n   Brgy Paraiso Quezon City Manila</pre>"
}

function getItemDetails(data, type) {
    let items = data.items
    let subtotal = data.total_quantity
    let subtotal_amount = 0
    let isDiscounted = false
    let amount = ""
    let total = type == "PRINTBILL" ? parseFloat(data.total_cost_bill) : parseFloat(data.order["amount_payable"])
    let changed = parseFloat(data.order["amount_change"])

    let render = "<pre>"

    if(type == "REPRINT" || type == "REFUND") {
        render += "------------------------------------\n               " + type + "\n"
    }

    render += "------------------------------------\nQTY   DESCRIPTION             AMOUNT\n------------------------------------\n"

    for(let item in items) {

        let quantity = getStringQuantity(items[item].quantity)
        if(typeof(items[item].discount_id) == "object"){
            amount = getStringAmount(numberWithCommas(parseFloat(items[item].total_cost).toFixed(2)), "nodiscount")
        }
        else {
            amount = getStringAmount(numberWithCommas(parseFloat(items[item].total_cost).toFixed(2)), "discount")
            if(item == items.length - 1) {
                amount = getStringAmount(numberWithCommas(parseFloat(items[item].total_cost).toFixed(2)), "nodiscount")
            }
            isDiscounted = true
        } 
        let description = getStringDescription(items[item].long_descp, numberWithCommas(parseFloat(items[item].total_cost).toFixed(2)), amount)
        subtotal_amount += parseFloat(items[item].total_cost)
        subtotal_amount += items[item].discount_amount == null ? 0 : parseFloat(items[item].discount_amount)

        render += quantity + description + "\n"
    }

    render += "------------------------------------\n"

    render += getLeftAlign("SUB-TOTAL:") + getRightAlign(subtotal + " ITEMS(S)") + getRightAlign(numberWithCommas(subtotal_amount.toFixed(2))) + "\n"

    if(isDiscounted) {
        for(let index in data.discount) {
            render += getDiscount(data.discount[index])
        }
    }

    if(type == "REFUND") {
        render += "\n"
        let refund_string = "REFUND TOTAL:             "
        let refund_amount_string = isDiscounted ? getStringAmount("-" + numberWithCommas(parseFloat(total).toFixed(2)), "discount") : getStringAmount("-" + numberWithCommas(parseFloat(subtotal_amount).toFixed(2)), "nodiscount")
        render += refund_string + refund_amount_string + "\n"

        let total_string = "TOTAL:                    "
        let total_amount_string = isDiscounted ? getStringAmount(numberWithCommas(parseFloat(total - total).toFixed(2)), "discount") : getStringAmount(numberWithCommas(parseFloat(subtotal_amount - subtotal_amount).toFixed(2)), "nodiscount")
    
        render += total_string + total_amount_string
    }
    else {

        let total_string = "TOTAL:                    "
        let total_amount_string = isDiscounted ? getStringAmount(numberWithCommas(parseFloat(total).toFixed(2)), "discount") : getStringAmount(numberWithCommas(parseFloat(total).toFixed(2)), "nodiscount")
    
        render += total_string + total_amount_string
    }

    if(type != "PRINTBILL" && type != "REFUND") {
        render += "\n------------------------------------\n"

        for(let tenders in data.tenders) {
            let payed_string = data.tenders[tenders].tender_list
            let payed_amount_string = isDiscounted ? getStringAmount(numberWithCommas(parseFloat(data.tenders[tenders].tender["amount_payed"]).toFixed(2)), "discount") : getStringAmount(numberWithCommas(parseFloat(data.tenders[tenders].tender["amount_payed"]).toFixed(2)), "nodiscount")
        
            render += getLeftAlign(payed_string) + getLeftAlign("") + getRightAlign(payed_amount_string) + "\n"
        }
    
        let changed_string = "CHANGE:                   "
        let changed_amount_string = getStringAmount(numberWithCommas(parseFloat(changed).toFixed(2)), "nodiscount")
    
        render += changed_string + changed_amount_string
    }

    render += "</pre>"

    return render
}

function getFooterDetails (data, type) {
    let cashier = data.order["cashier_username"]
    let transaction_number = ""
    if(type == "REFUND" || type == "REPRINT") {
        transaction_number = data.trn_number
    }
    else {
        transaction_number = data.order["trn_number"]
    }
    let order_number = data.order["order_number"]
    let or_number = data.or_number
    let datetime = data.order["date_time"]
    let render = ("<pre>------------------------------------\n")

    render += getLine("CASHIER: " + cashier)

    render += getLine("TRANS#: " + addZeroes(transaction_number) + "  ORDER#: " + addZeroes(order_number))

    render += getLine("OR#: " + addZeroes(or_number))

    render += getLine("DATE/TIME: " + datetime + "\n")

    render += type == "PRINTBILL" ? getLine("END OF BILLING") : ""

    render += "        THANK YOU FOR DINING!\n====================================\n\n      <b>THIS RECEIPT IS NOT VALID</b>\n       <b>FOR CLAIM OF INPUT TAX</b></pre>"

    return render
}

function getLine(data) {
    const character_length = 36
    let render = ""

    const splitted_line = data.split("")
    const remaining_char = character_length - splitted_line.length

    half = remaining_char / 2
    
    //checks if there is decimal point
    const result = (half - Math.floor(half)) !== 0

    if(result === true) {
        //removes decimal point then increment number of pages by 1
        half = Math.trunc(half)
        half++
    }

    //populate space before the word
    for(let i = 1; i <= half; i++) {
        render += " "
    }

    render += data

    //adds new line
    render += "\n"

    return render
}

function getStringQuantity(data) {
    if(data < 10) {
        return "  " + data + "   "
    }
    else if(data < 100) {
        return " " + data + "   "
    }
    else if(data < 1000) {
        return "" + data + "   "
    }
    return data + "   "
}

function getStringDescription(desc, cost, amount) {
    const character_length = 20
    let render = ""
    let line = desc + " @ " + cost
    let splitted_data = line.split("")
    let remaining_char = character_length - splitted_data.length

    if(remaining_char >= 0) {
        render += line

        for(let i = 1; i <= remaining_char; i++) {
            render += " "
        }

        render += amount
    }
    else {
        line = desc
        splitted_data = line.split("")
        remaining_char = character_length - splitted_data.length
        render += line

        for(let i = 1; i <= remaining_char; i++) {
            render += " "
        }

        render += amount + "\n"

        const cost_length = "@ " + cost
        const split_cost = cost_length.split("")
        remaining_char = character_length - split_cost.length + 6

        for(let i = 1; i <= remaining_char; i++) {
            render += " "
        }

        render += cost_length
    }

    return render
}

function getStringAmount(data) {
    const character_length = 10
    const converted_data = String(data)
    const splitted_data = converted_data.split("")
    const remaining_char = character_length - splitted_data.length
    let render = ""

    if(remaining_char >= 0) {
        for(let i = 1; i<= remaining_char; i++) {
            render += " "
        }
        render += data
    }

    return render
}

function getDiscount(discount) {
    let render = ""

    let total = discount.total
    if(total > 0) {
        if(discount.type == "PERCENTAGE") {
            render += getLeftAlign("  " + discount.description + ":") + getRightAlign(discount.value + "%") + getRightAlign("-" + numberWithCommas(total.toFixed(2))) + "\n"
        }
        else {
            if(discount.description == "Manual") {
                render += getLeftAlign("") + getRightAlign(discount.total + ".00") + getRightAlign("-" + numberWithCommas(total.toFixed(2))) + "\n"
            }
            else {
                render += getLeftAlign("  " + discount.description + ":") + getRightAlign(discount.value + ".00") + getRightAlign("-" + numberWithCommas(total.toFixed(2))) + "\n"
            }
        }
    }

    return render
}

function getLeftAlign(data) {
    const character_length = 12
    const converted_data = String(data)
    let render = ""
    const splitted_data = converted_data.split("")
    const remaining_char = character_length - splitted_data.length

    if(remaining_char >= 0) {
        render += data

        for(let i = 1; i<= remaining_char; i++) {
            render += " "
        }
    }
    else {
        for(let i = 0; i<= splitted_data.length - 1 + remaining_char; i++) {
            render += splitted_data[i]
        }
    }

    return render
}

function getRightAlign(data) {
    const character_length = 12
    const converted_data = String(data)
    let render = ""
    const splitted_data = converted_data.split("")
    const remaining_char = character_length - splitted_data.length

    if(remaining_char >= 0) {
        for(let i = 1; i<= remaining_char; i++) {
            render += " "
        }

        render += data
    }

    return render
}

function addZeroes(data) {
    const character_length = 6
    const converted_data = String(data)
    const splitted_data = converted_data.split("")
    const remaining_char = character_length - splitted_data.length
    let newData = ""

    if(remaining_char >= 0) {
        for(let i = 1; i<= remaining_char; i++) {
            newData += "0"
        }

        newData += data
    }

    return newData
}