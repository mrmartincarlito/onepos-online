let READINGS_API = "../api/cashier/test-reading.php"

$(() => {
    $.when(getEnvironment())
    .done(response => {
        let data = JSON.parse(response)

        if(data == 1) {
            ENVIRONMENT = 1
        }
    })
})

function getEnvironment() {
    return $.ajax({
        url: '../api/cashier/generic.php?getEnvironment',
        processData: false
    })
}

function setReading(data, type) {
    const header = getHeaderDetails()
    const footer = getReadingFooter()
    const item = getReadingItems(data, type)

    console.log(header + footer + item)
    
    const myWindow = window.open();
    myWindow.document.write('<html><head><style>body {margin: 0;padding: 0;width: 600px;height: auto;}.receipt{font-family: Courier New, Courier, monospace !important;margin: 0;padding: 0;width: 100%;font-size: 17px;text-align: justify;}pre{font-family: Courier New, monospace !important;} @media print { pre {font: 17px; "Courier New", sans-serif !important; font-weight: normal;}} @page { font-family: "Courier New", monospace;}</style>');
    myWindow.document.write('</head><body>');
    myWindow.document.write(`<br><br><div class = "receipt"><div>` + header + `</div>` + item + `<div>` + footer + `</div></div>`)

    myWindow.document.close(); // necessary for IE >= 10

    myWindow.print();
    myWindow.close();
}

function getReadingItems(data, type) {
    let render = "<pre>====================================\n"

    render += type == "z-read" ? getLeftSide("Day-end Report", 14) + getRightSide("ZREADING#" + addZeroes(data["cashier_report"].transaction_number), 22) : getLeftSide("Cashier Report", 14) + getRightSide("XREADING#" + addZeroes(data["cashier_report"].transaction_number), 22)

    for(let items in data) {
        if(items == "cashier_report") {
            render += titletLeftSide("Administrator") + titleRightSide(data[items].administrator)
            render += titletLeftSide("Cashier") + titleRightSide(data[items].cashier)
            render += titletLeftSide("Logdate") + titleRightSide(data[items].logdate)
            render += titletLeftSide("Date/Time") + titleRightSide(data[items].datetime)
            render += titletLeftSide("Terminal Code") + titleRightSide(data[items].terminal)
            render += titletLeftSide("Branch Code") + titleRightSide(data[items].branch_code)
            render += "------------------------------------\n"
        }

        if(items == "sales_summary") {
            render += titleRightSide("SALES SUMMARY")
            render += getLeftSide("Gross Sales:") + getCenterSide(data["transaction_details"].total) + getRightSide(numberWithCommas(data[items].gross_sales.toFixed(2)))
            for(let discounts in data[items].discount) {
                data[items].discount[discounts].total == 0 ? render += getLeftSide(data[items].discount[discounts].description) + getCenterSide(data[items].discount[discounts].discount_count) + getRightSide(numberWithCommas(data[items].discount[discounts].total.toFixed(2))) : render += getLeftSide(data[items].discount[discounts].description) + getCenterSide(data[items].discount[discounts].discount_count) + getRightSide("-" + numberWithCommas(data[items].discount[discounts].total.toFixed(2)))
            }
            render += getLeftSide("") + getCenterSide("") + getRightSide("------------")
            render += getLeftSide("Sub Total:") + getCenterSide("") + getRightSide(numberWithCommas(data[items].sub_total.toFixed(2)))
            render += "------------------------------------\n"
        }

        if(items == "transaction_details") {
            render += titleRightSide("TRANSACTION DETAILS")
            render += getLeftSide("Beginning OR No.:") + getCenterSide("") + getRightSide(addZeroes(data[items].beginning_or_number))
            render += getLeftSide("Ending OR No:") + getCenterSide("") + getRightSide(addZeroes(data[items].ending_or_number))
            render += getLeftSide("Total: ") + getCenterSide("") + getRightSide(data[items].total)
            render += getLeftSide("Void Transactions: ") + getCenterSide(data[items].void_transactions) + getRightSide(numberWithCommas(data[items].void_transaction_total_cost.toFixed(2)))
            render += getLeftSide("Void Items: ") + getCenterSide(data[items].void_items) + getRightSide(numberWithCommas(data[items].void_item_total_cost.toFixed(2)))
            render += getLeftSide("Refunded: ") + getCenterSide(data[items].refunded) + getRightSide(numberWithCommas(data[items].refund_item_total_cost.toFixed(2)))
            render += "------------------------------------\n"
        }

        if(items == "tender_summary") {
            let total_amount = 0
            let total_counter = 0
            render += titleRightSide("TENDER SUMMARY")
            for(let index in data[items].tender) {
                if(data[items].tender[index].total_counter > 0) {
                    render += getLeftSide(data[items].tender[index].tender) + getCenterSide(data[items].tender[index].total_counter) + getRightSide(numberWithCommas(data[items].tender[index].total_amount.toFixed(2)))
                    total_amount += data[items].tender[index].total_amount
                    total_counter += data[items].tender[index].total_counter
                }
            }
            render += getLeftSide("Refund: ") + getCenterSide("") + getRightSide("-" + numberWithCommas(data.refund_amount.toFixed(2)))
            render += getLeftSide("") + getCenterSide("") + getRightSide("------------")
            total_amount = data.refund_amount > 0 ? total_amount - data.refund_amount : total_amount
            render += getLeftSide("Total Tenders:") + getCenterSide(total_counter) + getRightSide(numberWithCommas(total_amount.toFixed(2)))
            render += "------------------------------------\n"
        }

        if(items == "sales_by_cashier") {
            let total_amount = 0
            render += titleRightSide("SALES SUMMARY BY CASHIER")
            for(let index in data[items].cashier) {
                if(data[items].cashier[index].total_sale > 0) {
                    render += getLeftSide(data[items].cashier[index].username + ":") + getCenterSide("") + getRightSide(numberWithCommas(data[items].cashier[index].total_sale.toFixed(2)))
                    total_amount += data[items].cashier[index].total_sale
                }
            }
            render += getLeftSide("Refund: ") + getCenterSide("") + getRightSide("-" + numberWithCommas(data.refund_amount.toFixed(2)))
            render += getLeftSide("") + getCenterSide("") + getRightSide("------------")
            total_amount = data.refund_amount > 0 ? total_amount - data.refund_amount : total_amount
            render += getLeftSide("Total Cashier Sales:") + getCenterSide("") + getRightSide(numberWithCommas(total_amount.toFixed(2)))
            render += "------------------------------------\n"
        }

        if(items == "sales_by_category") {
            let total_amount = 0
            render += titleRightSide("CATEGORY SALES SUMMARY")
            for(let index in data[items].category) {
                if(data[items].category[index].total_sale > 0) {
                    render += getLeftSide(data[items].category[index].total_sale, 5, "right") + " " + getCenterSide(data[items].category[index].category_name, 17, "left") + " " + getRightSide(numberWithCommas(data[items].category[index].total_cost.toFixed(2)))
                    total_amount += data[items].category[index].total_cost
                }
            }
            render += getLeftSide("Refund: ") + getCenterSide("") + getRightSide("-" + numberWithCommas(data.refund_amount.toFixed(2)))
            render += getLeftSide("") + getCenterSide("") + getRightSide("------------")
            total_amount = data.refund_amount > 0 ? total_amount - data.refund_amount : total_amount
            render += getLeftSide("Total Sales by Category:", 24) + getCenterSide("", 0) + getRightSide(numberWithCommas(total_amount.toFixed(2)))
            render += "------------------------------------\n"
        }

        if(items == "sales_by_menu") {
            let total_amount = 0
            render += titleRightSide("ITEM SALES SUMMARY")
            for(let index in data[items].menu) {
                if(data[items].menu[index].total_sale > 0) {
                    render += titleRightSide(data[items].menu[index].category_name)

                    for(let menu_index in data[items].menu[index].menu) {
                        if(data[items].menu[index].menu[menu_index].total_sale > 0) {
                            render += getLeftSide(data[items].menu[index].menu[menu_index].total_sale, 5, "right") + " " + getCenterSide(menu_index, 17, "left") + " " + getRightSide(numberWithCommas(data[items].menu[index].menu[menu_index].total_cost.toFixed(2)))
                        }
                    }
                    total_amount += data[items].menu[index].total_cost
                }
            }
            render += getLeftSide("Refund: ") + getCenterSide("") + getRightSide("-" + numberWithCommas(data.refund_amount.toFixed(2)))
            render += getLeftSide("") + getCenterSide("") + getRightSide("------------")
            total_amount = data.refund_amount > 0 ? total_amount - data.refund_amount : total_amount
            render += getLeftSide("Total Sales by Menu:", 24) + getCenterSide("", 0) + getRightSide(numberWithCommas(total_amount.toFixed(2)))
        }
    }

    render += "</pre>"

    return render
}

function getReadingFooter() {
    let render = "<pre>====================================\n\n"

    render += getLine("THANK YOU FOR DINING!")

    render += " THIS IS NOT YOUR OFFICIAL RECEIPT!\n"

    render += "\n====================================</pre>"

    return render
}

function titletLeftSide(data) {
    const character_length = 13
    const converted_data = String(data)
    let render = ""
    const splitted_data = converted_data.split("")
    const remaining_char = character_length - splitted_data.length

    if(remaining_char >= 0) {
        render += data

        for(let i = 1; i<= remaining_char; i++) {
            render += " "
        }
        
        render += ":"
    }

    return render
}

function titleRightSide(data) {
    return data + "\n"
}

function getLeftSide(data, character_length = 20, type = "left") {
    const converted_data = String(data)
    let render = ""
    const splitted_data = converted_data.split("")
    const remaining_char = character_length - splitted_data.length

    if(remaining_char >= 0) {

        if(type == "left") {
            render += data
    
            for(let i = 1; i<= remaining_char; i++) {
                render += " "
            }
        }
        else {
            for(let i = 1; i<= remaining_char; i++) {
                render += " "
            }

            render += data
        }
    }

    return render
}

function getCenterSide(data, character_length = 4, type = "right") {
    const converted_data = String(data)
    let render = ""
    const splitted_data = converted_data.split("")
    const remaining_char = character_length - splitted_data.length

    if(remaining_char >= 0) {

        if(type == "left") {
            render += data
    
            for(let i = 1; i<= remaining_char; i++) {
                render += " "
            }
        }
        else {
            for(let i = 1; i<= remaining_char; i++) {
                render += " "
            }

            render += data
        }
    }

    return render
}

function getRightSide(data, character_length = 12) {
    const converted_data = String(data)
    let render = ""
    const splitted_data = converted_data.split("")
    const remaining_char = character_length - splitted_data.length

    if(remaining_char >= 0) {
        for(let i = 1; i<= remaining_char; i++) {
            render += " "
        }

        render += data + "\n"
    }

    return render
}

function numberWithCommas(x) {
    let parts = x.toString().split(".")
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    return parts.join(".")
}