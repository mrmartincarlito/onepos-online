

//==============================================START OF PAGINATION============================================
function renderPage(data, type, array_list) {
    let render = ""

    if(type == "customer") {
        //renders table data
        render += `<tr style = "cursor: pointer;" class = "white white-hover" id = "` + data.id + `" onClick = "selectCustomer(this);"><td class = "border-1px" style = "padding: 8px;">` + data.fname + ` ` + data.lname + `</td><td class = "border-1px" style = "padding: 8px;">` + data.address + `</td><td class = "border-1px" style = "padding: 8px;">` + data.bday + `</td></tr>`
    }
    else if(type == "recall-order") {
        //renders table data
        render += `<tr style = "cursor: pointer;" class = "white white-hover" id = "` + data.id + `" onClick = "selectSuspendedOrder(this);"><td class = "border-1px" style = "padding: 8px;">` + data.id + `</td><td class = "border-1px" style = "padding: 8px;">` + data.trn_number + `</td><td class = "border-1px" style = "padding: 8px;">` + data.date_time + `</td></tr>`
    }
    else if(type == "tender-type") {
        //renders table data
        render += `<tr style = "cursor: pointer;" class = "white white-hover" name = ` + data.required_detail_tag + ` data = ` + data.allow_change + ` id = "tender-` + data.id + `" onClick = "selectTender(this);"><td class = "border-1px" style = "padding: 8px;">` + data.description + `</td></tr>`
    }
    else if(type == "refund") {
        //renders table data
        render += `<tr style = "cursor: pointer;" class = "white white-hover" id = "refund-` + data.item.id + `" onClick = "selectOrder(this);"><td class = "border-1px" style = "padding: 8px;">` + array_list.transaction_number + `</td><td class = "border-1px" style = "padding: 8px;">` + data.item.name + `</td><td class = "border-1px" style = "padding: 8px;">` + data.quantity + `</td><td class = "border-1px" style = "padding: 8px;">` + data.total_cost + `</td><td class = "border-1px" style = "padding: 8px;">` + array_list.date + `</td></tr>`
    }
    else {
        //renders table data
        if(data.type == "PERCENTAGE") {
            render += `<tr style = "cursor: pointer;" class = "white white-hover" id = "` + data.id + `" onClick = "selectDiscount(this);"><td class = "border-1px" style = "padding: 8px;">` + data.description + `</td><td class = "border-1px" style = "padding: 8px;">` + data.equivalent_value + `%</td></tr>`
        }
        else {
            if(data.description != "Discount") {
                render += `<tr style = "cursor: pointer;" class = "white white-hover" id = "` + data.id + `" onClick = "selectDiscount(this);"><td class = "border-1px" style = "padding: 8px;">` + data.description + `</td><td class = "border-1px" style = "padding: 8px;">` + data.equivalent_value + `.00</td></tr>`
            }
        }
    }

    return render
}

function renderTablePage(responseJSON, currentPage, pageContainer, type, function_name) {
    let range = 7 //range of user infos to display
    if(type == "discount" || type == "tender-type") {
        range = 3
    }
    else if(type == "refund") {
        range = 5
    }
    const start = (currentPage - 1) * range //sets the starting index
    const limit = start + range //sets the end of index
    let render = ""

    //if page 1
    if(start == 0 ) {
        //if page 1 and length is less than the limit
        if(type == "refund") {
            if(responseJSON.items.length < limit) {
                const array_list = {
                    id: responseJSON.order.id,
                    transaction_number: responseJSON.order.trn_number,
                    date: responseJSON.order.date_time
                }
                for(let data in responseJSON.items) {
                    render += renderPage(responseJSON.items[data], type, array_list)
                }
            }
            else {
                const number_of_page = countPage(responseJSON["items"].length, type)

                renderButton = showNextButton(number_of_page, currentPage, function_name)
                const array_list = {
                    id: responseJSON.order.id,
                    transaction_number: responseJSON.order.trn_number,
                    date: responseJSON.order.date_time
                }
                for(let data = start; data < limit; data++) {
                    render += renderPage(responseJSON.items[data], type, array_list)
                }
                $("#" + pageContainer).html(renderButton)
            }
        }
        else {
            if(responseJSON.length < limit) {
                for(let data in responseJSON) {
                    render += renderPage(responseJSON[data], type)
                }
            }
            //if page 1 and length is higher than the limit
            else {
                //calculates number of pages
                const number_of_page = countPage(responseJSON.length, type)
    
                //gets all possible buttons for pages
                renderButton = showNextButton(number_of_page, currentPage, function_name)
                
                //renders user infos
                for(let data = start; data < limit; data++) {
                    render += renderPage(responseJSON[data], type)
                }
                $("#" + pageContainer).html(renderButton)
            }
        }
    }
    //page 2 and beyond
    else {
        if(type == "refund") {
            const number_of_page = countPage(responseJSON["items"].length, type)
            renderButton = showNextButton(number_of_page, currentPage, function_name)
            if(responseJSON.items.length < limit) {
                const array_list = {
                    id: responseJSON.order.id,
                    transaction_number: responseJSON.order.trn_number,
                    date: responseJSON.order.date_time
                }
                for(let data = start; data < responseJSON.items.length; data++) {
                    render += renderPage(responseJSON.items[data], type, array_list)
                }
            }
            else {
                const array_list = {
                    id: responseJSON.order.id,
                    transaction_number: responseJSON.order.trn_number,
                    date: responseJSON.order.date_time
                }
                for(let data = start; data < limit; data++) {
                    render += renderPage(responseJSON.items[data], type, array_list)
                }
            }
            $("#" + pageContainer).html(renderButton)
        }
        else {
            const number_of_page = countPage(responseJSON.length, type)
            renderButton = showNextButton(number_of_page, currentPage, function_name)
            //checks if number of result is less than the limit
            if(responseJSON.length < limit) {
                for(let data = start; data < responseJSON.length; data++) {
                    render += renderPage(responseJSON[data], type)
                }
            }
            else {
                for(let data = start; data < limit; data++) {
                    render += renderPage(responseJSON[data], type)
                }
            }
            $("#" + pageContainer).html(renderButton)
        }
    }

    return render
}

function countPage(data, type) {
    //calculates number of pages
    let number_of_page = parseFloat(data) / 7

    if(type == "discount" || type == "tender-type") {
        number_of_page = parseFloat(data) / 3
    }
    else if(type == "refund") {
        number_of_page = parseFloat(data) / 5
    }

    //checks if there is decimal point
    const result = (number_of_page - Math.floor(number_of_page)) !== 0

    if(result === true) {
        //removes decimal point then increment number of pages by 1
        number_of_page = Math.trunc(number_of_page)
        number_of_page++
    }

    return number_of_page
}

function showNextButton(count, currentPage, function_name) {
    currentPage = parseInt(currentPage)
    const range = 2
    let page = ""
    //checks if previous and last will display
    if(currentPage > 1) {
        page += `<button class = "button-page blue white-font border-2px" style = "margin-left: 5%;" onClick = "` + function_name + `(1);"><i class="fas fa-angle-double-left"></i></button>`

        let previous = currentPage - 1

        page += `<button class = "button-page blue white-font border-2px" style = "margin-left: 5%;" onClick = "` + function_name + `(` + previous + `);"><i class="fas fa-angle-left"></i></button>`
    }
    //checks current pages to show 1, 2, 3...
    for(let j = (currentPage - range); j < (currentPage + range); j++) {
        if((j > 0) && (j <= count)) {
            if(j == currentPage) {
                page += `<button class = "button-page black white-font border-2px" style = "margin-left: 5%;" onClick = "` + function_name + `(` + j + `);" disabled>` + j + `</button>`
            }
            else {
                page += `<button class = "button-page blue white-font border-2px" style = "margin-left: 5%;" onClick = "` + function_name + `(` + j + `);">` + j + `</button>`
            }
        }
    }
    //checks if total pages are greater than 1
    if(count > 1) {
        if(currentPage != count) {
            let next = currentPage + 1
    
            page += `<button class = "button-page blue white-font border-2px" style = "margin-left: 5%;" onClick = "` + function_name + `(` + next + `);"><i class="fas fa-angle-right"></i></button>`
    
            page += `<button class = "button-page blue white-font border-2px" style = "margin-left: 5%;" onClick = "` + function_name + `(` + count + `);"><i class="fas fa-angle-double-right"></i></button>`
        }
    }

    return page
}
//==============================================END OF PAGINATION==============================================