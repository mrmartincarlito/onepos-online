function resetLoginData() {
    sessionStorage.setItem("current-transaction", "NONE")
}

function resetPage() {
    window.location.href = "./"
}

function goAdmin() {
    window.location.href = "../?starter_page"
}

function timer() {
    const today = new Date()
    let seconds = today.getSeconds()
    let minutes = today.getMinutes()
    let hours = today.getHours()
    let daylight = "AM"
    if(hours > 12) {
        hours = hours - 12
        daylight = "PM"
    }
    if(minutes < 10) {
        minutes = "0" + minutes
    }
    if(seconds < 10) {
        seconds = "0" + seconds
    }
    const time = hours + ":" + minutes + ":" + seconds + " " + daylight

    $("#logtime").html(time)

    setTimeout(timer, 1000)
}

function determineLogDateError(current_date, log_date) {
    const splitted_current_date = current_date.split("-")
    const splitted_log_date = log_date.split("-")
    let message

    if(parseFloat(splitted_current_date[0]) < parseFloat(splitted_log_date[0])) {
        message = "Your log date is ahead. Please check if your system time is correct!"
    }
    else if(parseFloat(splitted_current_date[0]) == parseFloat(splitted_log_date[0])) {
        if(parseFloat(splitted_current_date[1]) < parseFloat(splitted_log_date[1])) {
            message = "Your log date is ahead. Please check if your system time is correct!"
        }
        else if(parseFloat(splitted_current_date[1]) == parseFloat(splitted_log_date[1])) {
            if(parseFloat(splitted_current_date[2]) < parseFloat(splitted_log_date[2])) {
                message = "Your log date is ahead. Please check if your system time is correct!"
            }
            else {
                message = "Your log date is behind!"
                $("#log_date_error").html(message + " Do you want to Zread?")
                showForm("div30")
            }
        }
        else {
            message = "Your log date is behind!"
            $("#log_date_error").html(message + " Do you want to Zread?")
            showForm("div30")
        }
    }
    else {
        message = "Your log date is behind!"
        $("#log_date_error").html(message + " Do you want to Zread?")
        showForm("div30")
    }
    
    showNotification(message, "error", 10000)
}

function logout() {
    window.location.href = "../logout.php"
}

function service(app) {
    $(".loading").show(DEFAULT_TIME_VALUE)
    const product = $("#product-value").val()
    const type = $("#" + app.id).html()

    saveServiceType(product, type)
}

function showZRead() {
    const code = codeGenerator()

    $("#zread_code").html(code.code)

    showForm("div28")
}

function showQuantity () {
    const product = $("#product-value").val()
    $.when(showForm("div12"))
    .done(() => {
        $("#product_id").val(product)
    })
}

function showCustomer () {
    $(".loading").show(DEFAULT_TIME_VALUE)
    $.when(getAllCustomer('1'))
    .done(() => {
        showForm("div15")
    })
}

function showDiscount () {
    $(".loading").show(DEFAULT_TIME_VALUE)
    $.when(getAllDiscounts('1'))
    .done(() => {
        showForm("div17")
    })
}
function showForm(form) {
    $.when($(".div11").show(DEFAULT_TIME_VALUE))
    .done(() => {
        $("input[type=text]").val("")
        $("input[type=number]").val("")
        $("." + form).show(DEFAULT_TIME_VALUE)
    })
}

function hideForm(form) {
    $.when($("." + form).hide(DEFAULT_TIME_VALUE))
    .done(() => {
        $(".div11").hide(DEFAULT_TIME_VALUE)
    })
}

function hideAll(value) {
    if(value != "manager") {
        $(".categories-menu").hide(DEFAULT_TIME_VALUE)
        $(".products-menu").hide(DEFAULT_TIME_VALUE)
        $(".div3").attr("style", "overflow-y: scroll;")
    }

    for(let i = 8; i < 34; i++) {
        $(".div" + i).hide(DEFAULT_TIME_VALUE)
    }
}

function closeManagerMenu() {
    $("#manager-menu").hide(DEFAULT_TIME_VALUE)
    $(".div3").attr("style", "overflow-y: scroll;")
}

function appendValue(app, form) { //append value in guest/item quantity-form using on screen keyboard
    const value = app.innerHTML
    let currentValue
    let finalValue
    let form_id

    if(form == "guest") {
        currentValue = $("#guest-number").val()
        form_id = "guest-number"
    }
    else {
        currentValue = $("#item-number").val()
        form_id = "item-number"
    }

    finalValue = parseInt(currentValue + value)

    $("#" + form_id).val(finalValue)
}

function clearForm(form) { //clear guest/item quantity-form
    if(form == "guest") {
        $("#guest-number").val("")
    }
    else {
        $("#item-number").val("")
    }
}

function deleteOne(form) { //delete one value in guest/item quantity-form
    let currentValue
    let form_id 

    if(form == "guest") {
        currentValue = $("#guest-number").val()
        form_id = "guest-number"
    }
    else {
        currentValue = $("#item-number").val()
        form_id = "item-number"
    }

    const splitValue = currentValue.split("")
    let finalValue = ""

    if(splitValue.length < 2) {
        finalValue = "0"
    }
    else {
        splitValue.splice(splitValue.length - 1)
        for(let i in splitValue) {
            finalValue += splitValue[i]
        }
    }

    $("#" + form_id).val(finalValue)
}

function newTransaction() { //show guest-form
    $(".loading").show(DEFAULT_TIME_VALUE)
    closeManagerMenu()
    hideAll('all')
    $("#transaction").attr("disabled", true)
    $("#transaction").attr("class", "button w100 h100 gray font")
    $("#settle").attr("disabled", false)
    $("#settle").attr("class", "button w100 h100 black white-font font")
    $("#print").attr("disabled", false)
    $("#print").attr("class", "button w100 h100 black white-font font")
    $.when(saveNewTransaction())
    .done(() => {
        getCategory()
    })
    /*$.when($(".div11").show(DEFAULT_TIME_VALUE))
    .done(() => {
        $.when($(".div8").slideDown(500))
        .done(() => {
            $("#guest-number").val("")
            
        })
    })*/
}

/*function saveNumber(data) { //save guest count
    let value = $("#guest-number").val()
    if(data == 1 || data == 0 || data == "") {
        value = 1
    }
    $("#guest-number-count").html("GUEST NO: " + value)
    if(value == "" || value == 1 || value == 0) {
        $.when($(".div8").slideUp(500))
        .done(() => {
            $(".div11").hide(DEFAULT_TIME_VALUE)
            getCategory()
        })
    }
    else {
        const transac = getTransactionNumber()

        $.when(saveGuestcount(value, transac))
        .done(response => {
            responseJSON = $.parseJSON(response)

            if(responseJSON.type == "success") {
                $.when($(".div8").slideUp(500))
                .done(() => {
                    $(".div11").hide(DEFAULT_TIME_VALUE)
                    getCategory()
                })
            }
        })
    }
}*/

function getTransactionNumber() {
    return sessionStorage.getItem("current-transaction")
}

function inputQuantity(product) { //show item count form
    $.when($(".div11").show(DEFAULT_TIME_VALUE))
    .done(() => {
        $("#item-number").val("")
        $("#product_id").val(product)
        $(".div12").show(DEFAULT_TIME_VALUE)
    })
}

function enableButtons(app) {
    $.when($("#order-table tr").attr("class", "black white-font font"))
    .done(() => {
        $("#" + app.id).attr("class", "blue white-font");
        $("#discount").attr("disabled", false);
        $("#discount").attr("class", "button w100 h100 black white-font font")
        $("#dinein").attr("disabled", false);
        $("#dinein").attr("class", "button w100 h100 black white-font font")
        $("#special").attr("disabled", false);
        $("#special").attr("class", "button w100 h100 black white-font font")
        $("#quantity").attr("disabled", false);
        $("#quantity").attr("class", "button w100 h100 black white-font font")
        $("#customer").attr("disabled", false);
        $("#customer").attr("class", "button w100 h100 black white-font font")
        $("#product-value").val(app.id)
    })
}

function disableButtons(app) {
    if(typeof(app) == "object") {
        $("#order-table tr").attr("class", "black white-font font");
    }
    else {
        if(app != "settle") {
            $("#order-table tr").attr("class", "black white-font font");
        }
        else {
            $("#settle").attr("disabled", true)
            $("#settle").attr("class", "button w100 h100 gray font")
            $("#print").attr("disabled", true)
            $("#print").attr("class", "button w100 h100 gray font")
        }
    }
    $("#discount").attr("disabled", true);
    $("#discount").attr("class", "button w100 h100 gray font")
    $("#dinein").attr("disabled", true);
    $("#dinein").attr("class", "button w100 h100 gray font")
    $("#special").attr("disabled", true);
    $("#special").attr("class", "button w100 h100 gray font")
    $("#quantity").attr("disabled", true);
    $("#quantity").attr("class", "button w100 h100 gray font")
    $("#customer").attr("disabled", true);
    $("#customer").attr("class", "button w100 h100 gray font")
    $("#product-value").val("")
}

function backToCategory() {
    $(".products-menu").hide(DEFAULT_TIME_VALUE)
    getCategory()
}

function goToRecall() {
    $.when($(".div31").hide(DEFAULT_TIME_VALUE))
    .done(() => {
        showForm("div22")
    })
}

function showTender() {
    $(".loading").show(DEFAULT_TIME_VALUE)
    let value = $("#order-table").html()

    if(value.trim() == "" || value == null) {
        $(".loading").hide(DEFAULT_TIME_VALUE)
        showNotification("No item to settle", "warning")
    }
    else {
        getTenderTypes("1")
        showForm("div24")
    }
}

function showSettle() {
    $(".loading").show(DEFAULT_TIME_VALUE)
    $.when(getTotalAmount())
    .done(response => {
        responseJSON = JSON.parse(response)

        const total = responseJSON.total

        $("#tender-apply").attr("disabled", true)
        const value = $("#selected_tender_type").val()
        const id = $("#selected_tender_id").val()
        
        if(value == "CASH") {
            $.when($(".div24").hide())
            .done(() => {
                $("#settle_total_amount").html("TOTAL: " + total.toFixed(2))
                $.when(getDenominations())
                .done(response => {
                    responseJSON = JSON.parse(response)
                    let counter = 0
                    let render = ""

                    for(let i in responseJSON) {
                        if(counter == 0) {
                            render += `<tr>`
                        }
                        if(responseJSON[i].equivalent_value < 20) {
                            render += `<td class = "white-hover border-2px font2" style = "padding: 4px; cursor: pointer; background: url(design/tender/` + responseJSON[i].equivalent_value + `.jpg);  background-size: 60% 100%; color: rgba(50, 50, 50, 0); background-repeat: no-repeat;" onclick = "appendSettleValue('` + responseJSON[i].equivalent_value + `');">` + responseJSON[i].equivalent_value.toFixed(2) + `</td>`
                        }
                        else {
                            render += `<td class = "white-hover border-2px font2" style = "padding: 4px; cursor: pointer; background: url(design/tender/` + responseJSON[i].equivalent_value + `.jpg);  background-size: 100% 100%; color: rgba(50, 50, 50, 0); background-repeat: no-repeat;" onclick = "appendSettleValue('` + responseJSON[i].equivalent_value + `');">` + responseJSON[i].equivalent_value.toFixed(2) + `</td>`
                        }
                        counter++
                        if(counter == 3) {
                            render += `</tr>`
                            counter = 0
                        }
                    }
                    $("#denomination-table-content").html(render)
                    showForm("div21")
                    $(".loading").hide(DEFAULT_TIME_VALUE)
                })
            })
        }
        else {
            $.when($(".div24").hide())
            .done(() => {
                $("#settle_total_amount_by_tender").html("TOTAL: " + total.toFixed(2))
                $("#tender_info").html(value)
                $("#tender_info").attr("name", id)
                showForm("div34")
                $(".loading").hide(DEFAULT_TIME_VALUE)
            })
        }
    })
}

function autoComputeTotalAmount(){
  let totalAmount = 0
  $('#order-table tr').each(function() {
      let amount = this.cells[3].innerHTML;
      totalAmount = totalAmount + parseFloat(amount)
  });

  $("#totalAmount").html(totalAmount.toFixed(2))
}

function codeGenerator() {
    let result = ""
    const charactersLength = characters.length
    for ( let i = 0; i < 6; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength))
    }
    return {
    	code : result
    }
}

function saveTenderDetails() {
    const change = $("#selected_tender_allow_change").val()
    const detail = $("#selected_tender_required_detail_tag").val()
    let card_amount = $("#card_amount").val()
    const card_number = $("#card_number").val()
    const approval_number = $("#approval_number").val()
    const account_name = $("#account_name").val()
    const bank_name = $("#bank_name").val()
    let temp = $("#settle_total_amount_by_tender").html()
    temp = temp.split(":")
    let total = temp[1]
    total = parseFloat(total)
    const transaction_number = getTransactionNumber()
    const id = $("#tender_info").attr("name")
    const type = $("#tender_info").html()
    let change_amount = 0

    if(change == "NO") {
        if(card_amount == "" || card_amount == null || card_amount == 0 || card_amount > total) {
            card_amount = total
        }
    }
    else {
        if(card_amount == "" || card_amount == null || card_amount == 0) {
            card_amount = total
        }

        if(card_amount > total) {
            change_amount = card_amount - total
        }
    }

    if(detail == "YES") {
        //for verification
    }
    const data = {
        "card_number": card_number,
        "approval_number": approval_number,
        "account_name": account_name,
        "bank_name": bank_name
    }

    hideForm("div34")
    savePayment(total, transaction_number, card_amount, change_amount, id, data, type)
}

function renderCategory(array, length, type) {
    let render = ""

    if(type == "no-image") {
        render = `<td class = "grid-` + length + ` white center-text border-2px" onClick = "getProducts(` + array.id + `);" style = "
        box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%); border-radius: 8px;"><button class = "grid w100 h100 white-font" >` + array.description + `</button></td>`
        return render
    }
    
    render = `<td class = "grid-` + length + ` white center-text border-2px"onClick = "getProducts(` + array.id + `);" style = "
    box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%); border-radius: 8px; background: url('../api/` + array.image_file + `'); background-size: 100% 100%; background-repeat: no-repeat;"><button class = "grid w100 h100 white-font" >` + array.description + `</button></td>`
    return render
}

function renderProduct(array, type, length) {
    let render = ""

    if(type == "with-image") {
        render += `<td class = "grid-` + length + ` white-hover center-text border-2px" style = "
        box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%); border-radius: 8px;background: url('../api/` + array.image_file + `'); background-size: 100% 100%; background-repeat: no-repeat;" ><button class = "grid w100 h100 white-font" onClick = "inputQuantity(` + array.id + `);">` + array.name + `</button></td>`
    }
    else {
        render += `<td class = "grid-` + length + ` white white-hover center-text border-2px" style = "box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%); border-radius: 8px;background-size: 100% 100%; background-repeat: no-repeat;" ><button class = "grid w100 h100 white-font font" onClick = "inputQuantity(` + array.id + `);">` + array.name + `</button></td>`
    }

    return render
}