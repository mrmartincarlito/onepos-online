<?php 
	require_once("./api/config.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/icon.png">
  <title>Back Office Login</title>
  <?php include('./includes/load_on_page_start.php') ?>
	<?php include('./includes/pages/login/css_dependencies.php') ?>
  <style>
    .loginBG {
      position: fixed;
      height: 100%;
      background: url("plugins/images/login/background.jpg");
      background-repeat: no-repeat;
      background-size: 100% 100%;
    }
    @media only screen and (orientation: portrait) {
      .loginBG {
        background: url("plugins/images/login/CP.jpg");
        background-repeat: no-repeat;
        background-size: 100% 100%;
      }
    }
  </style>
</head>

<body>
  <!-- Preloader -->
  <div class="preloader">
    <div class="cssload-speeding-wheel"></div>
  </div>
  <section id="wrapper" class = "loginBG">
    <div class="login-box">
      <div class="white-box" style = "text-align: center; background-color: #f8f8ff;">
        <form class="form-horizontal form-material" id="loginform">
        <img src="plugins/images/IMG-Logo.png" alt="home" class="dark-logo" /><h1>ONEPOS-ONLINE</h1>
          <div id="origlogin">
            <div class="form-group m-t-40">
              <div class="col-xs-12">
                <input class="form-control" type="text" name="username" id="username" placeholder="Username">
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-12">
                <input class="form-control" type="password"  name="password" id="password" placeholder="Password">
              </div>
            </div>
              <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
          </div>
          <div class="form-group text-center m-t-20">
            <div class="col-xs-12">
              <button class="btn btn-warning btn-lg btn-block text-uppercase waves-effect waves-light" type="button" onclick="window.close()">Exit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
  <?php include('./includes/load_on_page_end.php') ?>
	<?php include('./includes/pages/login/script_dependencies.php') ?>
  <script src="./includes/pages/login/script.js"></script>
</body>

</html>